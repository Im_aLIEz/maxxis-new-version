﻿namespace maxxis
{
    partial class ReportViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.rv_reportViewer_GAR = new Microsoft.Reporting.WinForms.ReportViewer();
            this.btn_loadReport = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.dtp_toDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_fromDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.tbl_guestArrivalReportsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.GAR_DataReport = new maxxis.GAR_DataReport();
            this.tbl_guestArrivalReportsTableAdapter = new maxxis.GAR_DataReportTableAdapters.tbl_guestArrivalReportsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.tbl_guestArrivalReportsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GAR_DataReport)).BeginInit();
            this.SuspendLayout();
            // 
            // rv_reportViewer_GAR
            // 
            this.rv_reportViewer_GAR.AutoScroll = true;
            reportDataSource1.Name = "RDLC_guestArrival";
            reportDataSource1.Value = this.tbl_guestArrivalReportsBindingSource;
            this.rv_reportViewer_GAR.LocalReport.DataSources.Add(reportDataSource1);
            this.rv_reportViewer_GAR.LocalReport.ReportEmbeddedResource = "maxxis.Report_RDLC_guestArrival.rdlc";
            this.rv_reportViewer_GAR.Location = new System.Drawing.Point(12, 64);
            this.rv_reportViewer_GAR.Name = "rv_reportViewer_GAR";
            this.rv_reportViewer_GAR.ServerReport.BearerToken = null;
            this.rv_reportViewer_GAR.Size = new System.Drawing.Size(1260, 615);
            this.rv_reportViewer_GAR.TabIndex = 0;
            // 
            // btn_loadReport
            // 
            this.btn_loadReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_loadReport.FlatAppearance.BorderSize = 0;
            this.btn_loadReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_loadReport.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_loadReport.ForeColor = System.Drawing.Color.White;
            this.btn_loadReport.Location = new System.Drawing.Point(432, 18);
            this.btn_loadReport.Name = "btn_loadReport";
            this.btn_loadReport.Size = new System.Drawing.Size(108, 30);
            this.btn_loadReport.TabIndex = 91;
            this.btn_loadReport.Text = "LOAD";
            this.btn_loadReport.UseVisualStyleBackColor = false;
            this.btn_loadReport.Click += new System.EventHandler(this.btn_loadReport_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(226, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 17);
            this.label9.TabIndex = 90;
            this.label9.Text = "To Date:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtp_toDate
            // 
            this.dtp_toDate.CalendarFont = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_toDate.CalendarTitleBackColor = System.Drawing.Color.White;
            this.dtp_toDate.CustomFormat = "MM/dd/yyyy";
            this.dtp_toDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_toDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_toDate.Location = new System.Drawing.Point(294, 23);
            this.dtp_toDate.Name = "dtp_toDate";
            this.dtp_toDate.Size = new System.Drawing.Size(113, 22);
            this.dtp_toDate.TabIndex = 89;
            this.dtp_toDate.Value = new System.DateTime(2018, 4, 23, 0, 0, 0, 0);
            // 
            // dtp_fromDate
            // 
            this.dtp_fromDate.CalendarFont = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_fromDate.CalendarTitleBackColor = System.Drawing.Color.White;
            this.dtp_fromDate.CustomFormat = "MM/dd/yyyy";
            this.dtp_fromDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_fromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_fromDate.Location = new System.Drawing.Point(94, 23);
            this.dtp_fromDate.Name = "dtp_fromDate";
            this.dtp_fromDate.Size = new System.Drawing.Size(107, 22);
            this.dtp_fromDate.TabIndex = 88;
            this.dtp_fromDate.Value = new System.DateTime(2018, 4, 23, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 17);
            this.label1.TabIndex = 87;
            this.label1.Text = "From Date:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbl_guestArrivalReportsBindingSource
            // 
            this.tbl_guestArrivalReportsBindingSource.DataMember = "tbl_guestArrivalReports";
            this.tbl_guestArrivalReportsBindingSource.DataSource = this.GAR_DataReport;
            // 
            // GAR_DataReport
            // 
            this.GAR_DataReport.DataSetName = "GAR_DataReport";
            this.GAR_DataReport.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tbl_guestArrivalReportsTableAdapter
            // 
            this.tbl_guestArrivalReportsTableAdapter.ClearBeforeFill = true;
            // 
            // ReportViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 691);
            this.Controls.Add(this.btn_loadReport);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dtp_toDate);
            this.Controls.Add(this.dtp_fromDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rv_reportViewer_GAR);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ReportViewer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ReportViewer";
            this.Load += new System.EventHandler(this.ReportViewer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbl_guestArrivalReportsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GAR_DataReport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rv_reportViewer_GAR;
        private System.Windows.Forms.Button btn_loadReport;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtp_toDate;
        private System.Windows.Forms.DateTimePicker dtp_fromDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource tbl_guestArrivalReportsBindingSource;
        private GAR_DataReport GAR_DataReport;
        private GAR_DataReportTableAdapters.tbl_guestArrivalReportsTableAdapter tbl_guestArrivalReportsTableAdapter;
    }
}