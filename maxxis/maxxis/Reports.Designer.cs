﻿namespace maxxis
{
    partial class Reports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_reports = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_loadReport = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.dtp_toDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_fromDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_print_AL = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.dgv_accomodationList = new System.Windows.Forms.DataGridView();
            this.folioNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fullNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reservationDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkInDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dueOutDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numberOfNightsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.preAuthorizedAmountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accomodationListReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel9 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_print_NCO = new System.Windows.Forms.Button();
            this.btn_print_CLR = new System.Windows.Forms.Button();
            this.btn_print_RLR = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_print_GAR = new System.Windows.Forms.Button();
            this.pnl_nightCutOffReport = new System.Windows.Forms.Panel();
            this.dgv_nightCutOffReport = new System.Windows.Forms.DataGridView();
            this.folioNumberDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reservationNumberDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fullNameDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bookingDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roomNumbersDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roomTypesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reservationDateDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cancellationDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkInDateDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dueOutDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkOutDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numberOfNightsDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cancellationPenaltyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalPriceOfAmenitiesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalPriceForLossBreakageFinesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nightCutOffReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel4 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pnl_cancellationStatisticReport = new System.Windows.Forms.Panel();
            this.dgv_cancellationStatisticReport = new System.Windows.Forms.DataGridView();
            this.reservationNumberDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fullNameDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bookingDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roomNumbersDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roomTypeDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reservationDateDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cancellationDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkInDateDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dueOutDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transportationDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flightNumberDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pickUpTimeDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purposeOfStayDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adultsDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.childrenDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cancellationListReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel7 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.pnl_reservationStatisticReport = new System.Windows.Forms.Panel();
            this.dgv_reservationStatistic = new System.Windows.Forms.DataGridView();
            this.reservationNumberDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fullNameDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bookingDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roomNumbersDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roomTypeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reservationDateDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkInDateDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dueOutDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transportationDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flightNumberDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pickUpTimeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purposeOfStayDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adultsDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.childrenDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reservationListReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel6 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.pnl_guestArrivalStatistic = new System.Windows.Forms.Panel();
            this.dgv_arrivalStatistic = new System.Windows.Forms.DataGridView();
            this.reservationNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fullNameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bookingDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roomNumbersDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roomTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reservationDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkInDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dueOutDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transportationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flightNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pickUpTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purposeOfStayDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adultsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.childrenDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.guestArrivalReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_accomodationList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accomodationListReportBindingSource)).BeginInit();
            this.panel9.SuspendLayout();
            this.pnl_nightCutOffReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_nightCutOffReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nightCutOffReportBindingSource)).BeginInit();
            this.panel4.SuspendLayout();
            this.pnl_cancellationStatisticReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_cancellationStatisticReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancellationListReportBindingSource)).BeginInit();
            this.panel7.SuspendLayout();
            this.pnl_reservationStatisticReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_reservationStatistic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reservationListReportBindingSource)).BeginInit();
            this.panel6.SuspendLayout();
            this.pnl_guestArrivalStatistic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_arrivalStatistic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.guestArrivalReportBindingSource)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel1.Controls.Add(this.lbl_reports);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1320, 31);
            this.panel1.TabIndex = 11;
            // 
            // lbl_reports
            // 
            this.lbl_reports.AutoSize = true;
            this.lbl_reports.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_reports.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_reports.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_reports.Location = new System.Drawing.Point(12, 9);
            this.lbl_reports.Name = "lbl_reports";
            this.lbl_reports.Size = new System.Drawing.Size(63, 14);
            this.lbl_reports.TabIndex = 0;
            this.lbl_reports.Text = "REPORTS";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.Controls.Add(this.btn_loadReport);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.dtp_toDate);
            this.panel2.Controls.Add(this.dtp_fromDate);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 31);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1320, 36);
            this.panel2.TabIndex = 12;
            // 
            // btn_loadReport
            // 
            this.btn_loadReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_loadReport.FlatAppearance.BorderSize = 0;
            this.btn_loadReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_loadReport.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_loadReport.ForeColor = System.Drawing.Color.White;
            this.btn_loadReport.Location = new System.Drawing.Point(435, 3);
            this.btn_loadReport.Name = "btn_loadReport";
            this.btn_loadReport.Size = new System.Drawing.Size(108, 30);
            this.btn_loadReport.TabIndex = 81;
            this.btn_loadReport.Text = "LOAD";
            this.btn_loadReport.UseVisualStyleBackColor = false;
            this.btn_loadReport.Click += new System.EventHandler(this.btn_loadReport_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(229, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 17);
            this.label9.TabIndex = 80;
            this.label9.Text = "To Date:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtp_toDate
            // 
            this.dtp_toDate.CalendarFont = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_toDate.CalendarTitleBackColor = System.Drawing.Color.White;
            this.dtp_toDate.CustomFormat = "MM/dd/yyyy";
            this.dtp_toDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_toDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_toDate.Location = new System.Drawing.Point(297, 8);
            this.dtp_toDate.Name = "dtp_toDate";
            this.dtp_toDate.Size = new System.Drawing.Size(113, 22);
            this.dtp_toDate.TabIndex = 79;
            this.dtp_toDate.Value = new System.DateTime(2018, 4, 23, 0, 0, 0, 0);
            // 
            // dtp_fromDate
            // 
            this.dtp_fromDate.CalendarFont = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_fromDate.CalendarTitleBackColor = System.Drawing.Color.White;
            this.dtp_fromDate.CustomFormat = "MM/dd/yyyy";
            this.dtp_fromDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_fromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_fromDate.Location = new System.Drawing.Point(97, 8);
            this.dtp_fromDate.Name = "dtp_fromDate";
            this.dtp_fromDate.Size = new System.Drawing.Size(107, 22);
            this.dtp_fromDate.TabIndex = 78;
            this.dtp_fromDate.Value = new System.DateTime(2018, 4, 23, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 17);
            this.label1.TabIndex = 77;
            this.label1.Text = "From Date:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.Controls.Add(this.btn_print_AL);
            this.panel3.Controls.Add(this.panel8);
            this.panel3.Controls.Add(this.btn_print_NCO);
            this.panel3.Controls.Add(this.btn_print_CLR);
            this.panel3.Controls.Add(this.btn_print_RLR);
            this.panel3.Controls.Add(this.btn_close);
            this.panel3.Controls.Add(this.btn_print_GAR);
            this.panel3.Controls.Add(this.pnl_nightCutOffReport);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.pnl_cancellationStatisticReport);
            this.panel3.Controls.Add(this.pnl_reservationStatisticReport);
            this.panel3.Controls.Add(this.pnl_guestArrivalStatistic);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 67);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1320, 663);
            this.panel3.TabIndex = 13;
            // 
            // btn_print_AL
            // 
            this.btn_print_AL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_print_AL.FlatAppearance.BorderSize = 0;
            this.btn_print_AL.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_print_AL.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_print_AL.ForeColor = System.Drawing.Color.White;
            this.btn_print_AL.Location = new System.Drawing.Point(15, 379);
            this.btn_print_AL.Name = "btn_print_AL";
            this.btn_print_AL.Size = new System.Drawing.Size(224, 35);
            this.btn_print_AL.TabIndex = 26;
            this.btn_print_AL.Text = "PRINT ACCOMMODATION LIST";
            this.btn_print_AL.UseVisualStyleBackColor = false;
            this.btn_print_AL.Click += new System.EventHandler(this.btn_print_AL_Click);
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.dgv_accomodationList);
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Location = new System.Drawing.Point(15, 6);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1256, 367);
            this.panel8.TabIndex = 25;
            // 
            // dgv_accomodationList
            // 
            this.dgv_accomodationList.AllowUserToAddRows = false;
            this.dgv_accomodationList.AllowUserToDeleteRows = false;
            this.dgv_accomodationList.AutoGenerateColumns = false;
            this.dgv_accomodationList.BackgroundColor = System.Drawing.Color.White;
            this.dgv_accomodationList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_accomodationList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_accomodationList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_accomodationList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.folioNumberDataGridViewTextBoxColumn,
            this.fullNameDataGridViewTextBoxColumn,
            this.reservationDateDataGridViewTextBoxColumn,
            this.checkInDateDataGridViewTextBoxColumn,
            this.dueOutDataGridViewTextBoxColumn,
            this.numberOfNightsDataGridViewTextBoxColumn,
            this.preAuthorizedAmountDataGridViewTextBoxColumn});
            this.dgv_accomodationList.DataSource = this.accomodationListReportBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_accomodationList.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_accomodationList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_accomodationList.EnableHeadersVisualStyles = false;
            this.dgv_accomodationList.Location = new System.Drawing.Point(0, 31);
            this.dgv_accomodationList.Name = "dgv_accomodationList";
            this.dgv_accomodationList.ReadOnly = true;
            this.dgv_accomodationList.RowHeadersVisible = false;
            this.dgv_accomodationList.Size = new System.Drawing.Size(1254, 334);
            this.dgv_accomodationList.TabIndex = 13;
            // 
            // folioNumberDataGridViewTextBoxColumn
            // 
            this.folioNumberDataGridViewTextBoxColumn.DataPropertyName = "FolioNumber";
            this.folioNumberDataGridViewTextBoxColumn.HeaderText = "Folio Number";
            this.folioNumberDataGridViewTextBoxColumn.Name = "folioNumberDataGridViewTextBoxColumn";
            this.folioNumberDataGridViewTextBoxColumn.ReadOnly = true;
            this.folioNumberDataGridViewTextBoxColumn.Width = 200;
            // 
            // fullNameDataGridViewTextBoxColumn
            // 
            this.fullNameDataGridViewTextBoxColumn.DataPropertyName = "FullName";
            this.fullNameDataGridViewTextBoxColumn.HeaderText = "Full Name";
            this.fullNameDataGridViewTextBoxColumn.Name = "fullNameDataGridViewTextBoxColumn";
            this.fullNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.fullNameDataGridViewTextBoxColumn.Width = 200;
            // 
            // reservationDateDataGridViewTextBoxColumn
            // 
            this.reservationDateDataGridViewTextBoxColumn.DataPropertyName = "ReservationDate";
            this.reservationDateDataGridViewTextBoxColumn.HeaderText = "Reservation Date";
            this.reservationDateDataGridViewTextBoxColumn.Name = "reservationDateDataGridViewTextBoxColumn";
            this.reservationDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.reservationDateDataGridViewTextBoxColumn.Width = 150;
            // 
            // checkInDateDataGridViewTextBoxColumn
            // 
            this.checkInDateDataGridViewTextBoxColumn.DataPropertyName = "CheckInDate";
            this.checkInDateDataGridViewTextBoxColumn.HeaderText = "Check-In Date";
            this.checkInDateDataGridViewTextBoxColumn.Name = "checkInDateDataGridViewTextBoxColumn";
            this.checkInDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.checkInDateDataGridViewTextBoxColumn.Width = 150;
            // 
            // dueOutDataGridViewTextBoxColumn
            // 
            this.dueOutDataGridViewTextBoxColumn.DataPropertyName = "DueOut";
            this.dueOutDataGridViewTextBoxColumn.HeaderText = "DueOut";
            this.dueOutDataGridViewTextBoxColumn.Name = "dueOutDataGridViewTextBoxColumn";
            this.dueOutDataGridViewTextBoxColumn.ReadOnly = true;
            this.dueOutDataGridViewTextBoxColumn.Width = 150;
            // 
            // numberOfNightsDataGridViewTextBoxColumn
            // 
            this.numberOfNightsDataGridViewTextBoxColumn.DataPropertyName = "NumberOfNights";
            this.numberOfNightsDataGridViewTextBoxColumn.HeaderText = "Number Of Nights";
            this.numberOfNightsDataGridViewTextBoxColumn.Name = "numberOfNightsDataGridViewTextBoxColumn";
            this.numberOfNightsDataGridViewTextBoxColumn.ReadOnly = true;
            this.numberOfNightsDataGridViewTextBoxColumn.Width = 150;
            // 
            // preAuthorizedAmountDataGridViewTextBoxColumn
            // 
            this.preAuthorizedAmountDataGridViewTextBoxColumn.DataPropertyName = "PreAuthorizedAmount";
            this.preAuthorizedAmountDataGridViewTextBoxColumn.HeaderText = "Pre-Authorized Amount";
            this.preAuthorizedAmountDataGridViewTextBoxColumn.Name = "preAuthorizedAmountDataGridViewTextBoxColumn";
            this.preAuthorizedAmountDataGridViewTextBoxColumn.ReadOnly = true;
            this.preAuthorizedAmountDataGridViewTextBoxColumn.Width = 200;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel9.Controls.Add(this.label7);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1254, 31);
            this.panel9.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label7.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label7.Location = new System.Drawing.Point(5, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(131, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "ACCOMODATION LIST";
            // 
            // btn_print_NCO
            // 
            this.btn_print_NCO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_print_NCO.FlatAppearance.BorderSize = 0;
            this.btn_print_NCO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_print_NCO.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_print_NCO.ForeColor = System.Drawing.Color.White;
            this.btn_print_NCO.Location = new System.Drawing.Point(16, 2143);
            this.btn_print_NCO.Name = "btn_print_NCO";
            this.btn_print_NCO.Size = new System.Drawing.Size(224, 35);
            this.btn_print_NCO.TabIndex = 24;
            this.btn_print_NCO.Text = "PRINT NIGHT-CUT OFF REPORT";
            this.btn_print_NCO.UseVisualStyleBackColor = false;
            this.btn_print_NCO.Click += new System.EventHandler(this.btn_print_NCO_Click);
            // 
            // btn_print_CLR
            // 
            this.btn_print_CLR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_print_CLR.FlatAppearance.BorderSize = 0;
            this.btn_print_CLR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_print_CLR.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_print_CLR.ForeColor = System.Drawing.Color.White;
            this.btn_print_CLR.Location = new System.Drawing.Point(15, 1701);
            this.btn_print_CLR.Name = "btn_print_CLR";
            this.btn_print_CLR.Size = new System.Drawing.Size(224, 35);
            this.btn_print_CLR.TabIndex = 23;
            this.btn_print_CLR.Text = "PRINT CANCELLATION LIST REPORT";
            this.btn_print_CLR.UseVisualStyleBackColor = false;
            this.btn_print_CLR.Click += new System.EventHandler(this.btn_print_CLR_Click);
            // 
            // btn_print_RLR
            // 
            this.btn_print_RLR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_print_RLR.FlatAppearance.BorderSize = 0;
            this.btn_print_RLR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_print_RLR.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_print_RLR.ForeColor = System.Drawing.Color.White;
            this.btn_print_RLR.Location = new System.Drawing.Point(15, 1260);
            this.btn_print_RLR.Name = "btn_print_RLR";
            this.btn_print_RLR.Size = new System.Drawing.Size(224, 35);
            this.btn_print_RLR.TabIndex = 22;
            this.btn_print_RLR.Text = "PRINT RESERVATION LIST REPORT";
            this.btn_print_RLR.UseVisualStyleBackColor = false;
            this.btn_print_RLR.Click += new System.EventHandler(this.btn_print_RLR_Click);
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_close.FlatAppearance.BorderSize = 0;
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_close.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_close.ForeColor = System.Drawing.Color.White;
            this.btn_close.Location = new System.Drawing.Point(1079, 2172);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(191, 35);
            this.btn_close.TabIndex = 21;
            this.btn_close.Text = "CLOSE";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_print_GAR
            // 
            this.btn_print_GAR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_print_GAR.FlatAppearance.BorderSize = 0;
            this.btn_print_GAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_print_GAR.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_print_GAR.ForeColor = System.Drawing.Color.White;
            this.btn_print_GAR.Location = new System.Drawing.Point(15, 821);
            this.btn_print_GAR.Name = "btn_print_GAR";
            this.btn_print_GAR.Size = new System.Drawing.Size(224, 35);
            this.btn_print_GAR.TabIndex = 20;
            this.btn_print_GAR.Text = "PRINT GUEST ARRIVAL REPORT";
            this.btn_print_GAR.UseVisualStyleBackColor = false;
            this.btn_print_GAR.Click += new System.EventHandler(this.btn_print_GAR_Click);
            // 
            // pnl_nightCutOffReport
            // 
            this.pnl_nightCutOffReport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_nightCutOffReport.Controls.Add(this.dgv_nightCutOffReport);
            this.pnl_nightCutOffReport.Controls.Add(this.panel4);
            this.pnl_nightCutOffReport.Location = new System.Drawing.Point(16, 1771);
            this.pnl_nightCutOffReport.Name = "pnl_nightCutOffReport";
            this.pnl_nightCutOffReport.Size = new System.Drawing.Size(1256, 367);
            this.pnl_nightCutOffReport.TabIndex = 4;
            // 
            // dgv_nightCutOffReport
            // 
            this.dgv_nightCutOffReport.AllowUserToAddRows = false;
            this.dgv_nightCutOffReport.AllowUserToDeleteRows = false;
            this.dgv_nightCutOffReport.AutoGenerateColumns = false;
            this.dgv_nightCutOffReport.BackgroundColor = System.Drawing.Color.White;
            this.dgv_nightCutOffReport.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_nightCutOffReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_nightCutOffReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_nightCutOffReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.folioNumberDataGridViewTextBoxColumn1,
            this.reservationNumberDataGridViewTextBoxColumn3,
            this.fullNameDataGridViewTextBoxColumn4,
            this.bookingDataGridViewTextBoxColumn3,
            this.roomNumbersDataGridViewTextBoxColumn3,
            this.roomTypesDataGridViewTextBoxColumn,
            this.reservationDateDataGridViewTextBoxColumn4,
            this.cancellationDateDataGridViewTextBoxColumn1,
            this.checkInDateDataGridViewTextBoxColumn4,
            this.dueOutDataGridViewTextBoxColumn4,
            this.checkOutDateDataGridViewTextBoxColumn,
            this.numberOfNightsDataGridViewTextBoxColumn1,
            this.cancellationPenaltyDataGridViewTextBoxColumn,
            this.totalPriceOfAmenitiesDataGridViewTextBoxColumn,
            this.totalPriceForLossBreakageFinesDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn1});
            this.dgv_nightCutOffReport.DataSource = this.nightCutOffReportBindingSource;
            this.dgv_nightCutOffReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_nightCutOffReport.EnableHeadersVisualStyles = false;
            this.dgv_nightCutOffReport.Location = new System.Drawing.Point(0, 31);
            this.dgv_nightCutOffReport.Name = "dgv_nightCutOffReport";
            this.dgv_nightCutOffReport.ReadOnly = true;
            this.dgv_nightCutOffReport.RowHeadersVisible = false;
            this.dgv_nightCutOffReport.Size = new System.Drawing.Size(1254, 334);
            this.dgv_nightCutOffReport.TabIndex = 16;
            // 
            // folioNumberDataGridViewTextBoxColumn1
            // 
            this.folioNumberDataGridViewTextBoxColumn1.DataPropertyName = "FolioNumber";
            this.folioNumberDataGridViewTextBoxColumn1.HeaderText = "Folio Number";
            this.folioNumberDataGridViewTextBoxColumn1.Name = "folioNumberDataGridViewTextBoxColumn1";
            this.folioNumberDataGridViewTextBoxColumn1.ReadOnly = true;
            this.folioNumberDataGridViewTextBoxColumn1.Width = 200;
            // 
            // reservationNumberDataGridViewTextBoxColumn3
            // 
            this.reservationNumberDataGridViewTextBoxColumn3.DataPropertyName = "ReservationNumber";
            this.reservationNumberDataGridViewTextBoxColumn3.HeaderText = "Reservation Number";
            this.reservationNumberDataGridViewTextBoxColumn3.Name = "reservationNumberDataGridViewTextBoxColumn3";
            this.reservationNumberDataGridViewTextBoxColumn3.ReadOnly = true;
            this.reservationNumberDataGridViewTextBoxColumn3.Width = 200;
            // 
            // fullNameDataGridViewTextBoxColumn4
            // 
            this.fullNameDataGridViewTextBoxColumn4.DataPropertyName = "FullName";
            this.fullNameDataGridViewTextBoxColumn4.HeaderText = "Full Name";
            this.fullNameDataGridViewTextBoxColumn4.Name = "fullNameDataGridViewTextBoxColumn4";
            this.fullNameDataGridViewTextBoxColumn4.ReadOnly = true;
            this.fullNameDataGridViewTextBoxColumn4.Width = 200;
            // 
            // bookingDataGridViewTextBoxColumn3
            // 
            this.bookingDataGridViewTextBoxColumn3.DataPropertyName = "Booking";
            this.bookingDataGridViewTextBoxColumn3.HeaderText = "Booking";
            this.bookingDataGridViewTextBoxColumn3.Name = "bookingDataGridViewTextBoxColumn3";
            this.bookingDataGridViewTextBoxColumn3.ReadOnly = true;
            this.bookingDataGridViewTextBoxColumn3.Width = 150;
            // 
            // roomNumbersDataGridViewTextBoxColumn3
            // 
            this.roomNumbersDataGridViewTextBoxColumn3.DataPropertyName = "RoomNumbers";
            this.roomNumbersDataGridViewTextBoxColumn3.HeaderText = "Room Numbers";
            this.roomNumbersDataGridViewTextBoxColumn3.Name = "roomNumbersDataGridViewTextBoxColumn3";
            this.roomNumbersDataGridViewTextBoxColumn3.ReadOnly = true;
            this.roomNumbersDataGridViewTextBoxColumn3.Width = 150;
            // 
            // roomTypesDataGridViewTextBoxColumn
            // 
            this.roomTypesDataGridViewTextBoxColumn.DataPropertyName = "RoomTypes";
            this.roomTypesDataGridViewTextBoxColumn.HeaderText = "Room Types";
            this.roomTypesDataGridViewTextBoxColumn.Name = "roomTypesDataGridViewTextBoxColumn";
            this.roomTypesDataGridViewTextBoxColumn.ReadOnly = true;
            this.roomTypesDataGridViewTextBoxColumn.Width = 200;
            // 
            // reservationDateDataGridViewTextBoxColumn4
            // 
            this.reservationDateDataGridViewTextBoxColumn4.DataPropertyName = "ReservationDate";
            this.reservationDateDataGridViewTextBoxColumn4.HeaderText = "Reservation Date";
            this.reservationDateDataGridViewTextBoxColumn4.Name = "reservationDateDataGridViewTextBoxColumn4";
            this.reservationDateDataGridViewTextBoxColumn4.ReadOnly = true;
            this.reservationDateDataGridViewTextBoxColumn4.Width = 150;
            // 
            // cancellationDateDataGridViewTextBoxColumn1
            // 
            this.cancellationDateDataGridViewTextBoxColumn1.DataPropertyName = "CancellationDate";
            this.cancellationDateDataGridViewTextBoxColumn1.HeaderText = "Cancellation Date";
            this.cancellationDateDataGridViewTextBoxColumn1.Name = "cancellationDateDataGridViewTextBoxColumn1";
            this.cancellationDateDataGridViewTextBoxColumn1.ReadOnly = true;
            this.cancellationDateDataGridViewTextBoxColumn1.Width = 150;
            // 
            // checkInDateDataGridViewTextBoxColumn4
            // 
            this.checkInDateDataGridViewTextBoxColumn4.DataPropertyName = "CheckInDate";
            this.checkInDateDataGridViewTextBoxColumn4.HeaderText = "Check-In Date";
            this.checkInDateDataGridViewTextBoxColumn4.Name = "checkInDateDataGridViewTextBoxColumn4";
            this.checkInDateDataGridViewTextBoxColumn4.ReadOnly = true;
            this.checkInDateDataGridViewTextBoxColumn4.Width = 150;
            // 
            // dueOutDataGridViewTextBoxColumn4
            // 
            this.dueOutDataGridViewTextBoxColumn4.DataPropertyName = "DueOut";
            this.dueOutDataGridViewTextBoxColumn4.HeaderText = "DueOut";
            this.dueOutDataGridViewTextBoxColumn4.Name = "dueOutDataGridViewTextBoxColumn4";
            this.dueOutDataGridViewTextBoxColumn4.ReadOnly = true;
            this.dueOutDataGridViewTextBoxColumn4.Width = 150;
            // 
            // checkOutDateDataGridViewTextBoxColumn
            // 
            this.checkOutDateDataGridViewTextBoxColumn.DataPropertyName = "CheckOutDate";
            this.checkOutDateDataGridViewTextBoxColumn.HeaderText = "Check-Out Date";
            this.checkOutDateDataGridViewTextBoxColumn.Name = "checkOutDateDataGridViewTextBoxColumn";
            this.checkOutDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.checkOutDateDataGridViewTextBoxColumn.Width = 150;
            // 
            // numberOfNightsDataGridViewTextBoxColumn1
            // 
            this.numberOfNightsDataGridViewTextBoxColumn1.DataPropertyName = "NumberOfNights";
            this.numberOfNightsDataGridViewTextBoxColumn1.HeaderText = "Number Of Nights";
            this.numberOfNightsDataGridViewTextBoxColumn1.Name = "numberOfNightsDataGridViewTextBoxColumn1";
            this.numberOfNightsDataGridViewTextBoxColumn1.ReadOnly = true;
            this.numberOfNightsDataGridViewTextBoxColumn1.Width = 150;
            // 
            // cancellationPenaltyDataGridViewTextBoxColumn
            // 
            this.cancellationPenaltyDataGridViewTextBoxColumn.DataPropertyName = "CancellationPenalty";
            this.cancellationPenaltyDataGridViewTextBoxColumn.HeaderText = "Cancellation Penalty";
            this.cancellationPenaltyDataGridViewTextBoxColumn.Name = "cancellationPenaltyDataGridViewTextBoxColumn";
            this.cancellationPenaltyDataGridViewTextBoxColumn.ReadOnly = true;
            this.cancellationPenaltyDataGridViewTextBoxColumn.Width = 200;
            // 
            // totalPriceOfAmenitiesDataGridViewTextBoxColumn
            // 
            this.totalPriceOfAmenitiesDataGridViewTextBoxColumn.DataPropertyName = "TotalPriceOfAmenities";
            this.totalPriceOfAmenitiesDataGridViewTextBoxColumn.HeaderText = "Total Price Of Amenities";
            this.totalPriceOfAmenitiesDataGridViewTextBoxColumn.Name = "totalPriceOfAmenitiesDataGridViewTextBoxColumn";
            this.totalPriceOfAmenitiesDataGridViewTextBoxColumn.ReadOnly = true;
            this.totalPriceOfAmenitiesDataGridViewTextBoxColumn.Width = 300;
            // 
            // totalPriceForLossBreakageFinesDataGridViewTextBoxColumn
            // 
            this.totalPriceForLossBreakageFinesDataGridViewTextBoxColumn.DataPropertyName = "TotalPriceForLossBreakageFines";
            this.totalPriceForLossBreakageFinesDataGridViewTextBoxColumn.HeaderText = "Total Price For Loss Breakage Fines";
            this.totalPriceForLossBreakageFinesDataGridViewTextBoxColumn.Name = "totalPriceForLossBreakageFinesDataGridViewTextBoxColumn";
            this.totalPriceForLossBreakageFinesDataGridViewTextBoxColumn.ReadOnly = true;
            this.totalPriceForLossBreakageFinesDataGridViewTextBoxColumn.Width = 300;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "TotalBillAccumulated";
            this.dataGridViewTextBoxColumn1.HeaderText = "Total Bill Accumulated";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 300;
            // 
            // nightCutOffReportBindingSource
            // 
            this.nightCutOffReportBindingSource.DataSource = typeof(maxxis.NightCutOff_Report);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel4.Controls.Add(this.label6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1254, 31);
            this.panel4.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label6.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label6.Location = new System.Drawing.Point(5, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(221, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "NIGHT-CUT OFF STATISTIC REPORTS";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1291, 2209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 3;
            // 
            // pnl_cancellationStatisticReport
            // 
            this.pnl_cancellationStatisticReport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_cancellationStatisticReport.Controls.Add(this.dgv_cancellationStatisticReport);
            this.pnl_cancellationStatisticReport.Controls.Add(this.panel7);
            this.pnl_cancellationStatisticReport.Location = new System.Drawing.Point(15, 1328);
            this.pnl_cancellationStatisticReport.Name = "pnl_cancellationStatisticReport";
            this.pnl_cancellationStatisticReport.Size = new System.Drawing.Size(1256, 367);
            this.pnl_cancellationStatisticReport.TabIndex = 2;
            // 
            // dgv_cancellationStatisticReport
            // 
            this.dgv_cancellationStatisticReport.AllowUserToAddRows = false;
            this.dgv_cancellationStatisticReport.AllowUserToDeleteRows = false;
            this.dgv_cancellationStatisticReport.AutoGenerateColumns = false;
            this.dgv_cancellationStatisticReport.BackgroundColor = System.Drawing.Color.White;
            this.dgv_cancellationStatisticReport.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_cancellationStatisticReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_cancellationStatisticReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_cancellationStatisticReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.reservationNumberDataGridViewTextBoxColumn2,
            this.fullNameDataGridViewTextBoxColumn3,
            this.bookingDataGridViewTextBoxColumn2,
            this.roomNumbersDataGridViewTextBoxColumn2,
            this.roomTypeDataGridViewTextBoxColumn2,
            this.reservationDateDataGridViewTextBoxColumn3,
            this.cancellationDateDataGridViewTextBoxColumn,
            this.checkInDateDataGridViewTextBoxColumn3,
            this.dueOutDataGridViewTextBoxColumn3,
            this.transportationDataGridViewTextBoxColumn2,
            this.flightNumberDataGridViewTextBoxColumn2,
            this.pickUpTimeDataGridViewTextBoxColumn2,
            this.purposeOfStayDataGridViewTextBoxColumn2,
            this.adultsDataGridViewTextBoxColumn2,
            this.childrenDataGridViewTextBoxColumn2});
            this.dgv_cancellationStatisticReport.DataSource = this.cancellationListReportBindingSource;
            this.dgv_cancellationStatisticReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_cancellationStatisticReport.EnableHeadersVisualStyles = false;
            this.dgv_cancellationStatisticReport.Location = new System.Drawing.Point(0, 31);
            this.dgv_cancellationStatisticReport.Name = "dgv_cancellationStatisticReport";
            this.dgv_cancellationStatisticReport.ReadOnly = true;
            this.dgv_cancellationStatisticReport.RowHeadersVisible = false;
            this.dgv_cancellationStatisticReport.Size = new System.Drawing.Size(1254, 334);
            this.dgv_cancellationStatisticReport.TabIndex = 15;
            // 
            // reservationNumberDataGridViewTextBoxColumn2
            // 
            this.reservationNumberDataGridViewTextBoxColumn2.DataPropertyName = "ReservationNumber";
            this.reservationNumberDataGridViewTextBoxColumn2.HeaderText = "Reservation Number";
            this.reservationNumberDataGridViewTextBoxColumn2.Name = "reservationNumberDataGridViewTextBoxColumn2";
            this.reservationNumberDataGridViewTextBoxColumn2.ReadOnly = true;
            this.reservationNumberDataGridViewTextBoxColumn2.Width = 200;
            // 
            // fullNameDataGridViewTextBoxColumn3
            // 
            this.fullNameDataGridViewTextBoxColumn3.DataPropertyName = "FullName";
            this.fullNameDataGridViewTextBoxColumn3.HeaderText = "Full Name";
            this.fullNameDataGridViewTextBoxColumn3.Name = "fullNameDataGridViewTextBoxColumn3";
            this.fullNameDataGridViewTextBoxColumn3.ReadOnly = true;
            this.fullNameDataGridViewTextBoxColumn3.Width = 200;
            // 
            // bookingDataGridViewTextBoxColumn2
            // 
            this.bookingDataGridViewTextBoxColumn2.DataPropertyName = "Booking";
            this.bookingDataGridViewTextBoxColumn2.HeaderText = "Booking";
            this.bookingDataGridViewTextBoxColumn2.Name = "bookingDataGridViewTextBoxColumn2";
            this.bookingDataGridViewTextBoxColumn2.ReadOnly = true;
            this.bookingDataGridViewTextBoxColumn2.Width = 150;
            // 
            // roomNumbersDataGridViewTextBoxColumn2
            // 
            this.roomNumbersDataGridViewTextBoxColumn2.DataPropertyName = "RoomNumbers";
            this.roomNumbersDataGridViewTextBoxColumn2.HeaderText = "Room Numbers";
            this.roomNumbersDataGridViewTextBoxColumn2.Name = "roomNumbersDataGridViewTextBoxColumn2";
            this.roomNumbersDataGridViewTextBoxColumn2.ReadOnly = true;
            this.roomNumbersDataGridViewTextBoxColumn2.Width = 150;
            // 
            // roomTypeDataGridViewTextBoxColumn2
            // 
            this.roomTypeDataGridViewTextBoxColumn2.DataPropertyName = "RoomType";
            this.roomTypeDataGridViewTextBoxColumn2.HeaderText = "Room Type";
            this.roomTypeDataGridViewTextBoxColumn2.Name = "roomTypeDataGridViewTextBoxColumn2";
            this.roomTypeDataGridViewTextBoxColumn2.ReadOnly = true;
            this.roomTypeDataGridViewTextBoxColumn2.Width = 200;
            // 
            // reservationDateDataGridViewTextBoxColumn3
            // 
            this.reservationDateDataGridViewTextBoxColumn3.DataPropertyName = "ReservationDate";
            this.reservationDateDataGridViewTextBoxColumn3.HeaderText = "Reservation Date";
            this.reservationDateDataGridViewTextBoxColumn3.Name = "reservationDateDataGridViewTextBoxColumn3";
            this.reservationDateDataGridViewTextBoxColumn3.ReadOnly = true;
            this.reservationDateDataGridViewTextBoxColumn3.Width = 150;
            // 
            // cancellationDateDataGridViewTextBoxColumn
            // 
            this.cancellationDateDataGridViewTextBoxColumn.DataPropertyName = "CancellationDate";
            this.cancellationDateDataGridViewTextBoxColumn.HeaderText = "Cancellation Date";
            this.cancellationDateDataGridViewTextBoxColumn.Name = "cancellationDateDataGridViewTextBoxColumn";
            this.cancellationDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.cancellationDateDataGridViewTextBoxColumn.Width = 150;
            // 
            // checkInDateDataGridViewTextBoxColumn3
            // 
            this.checkInDateDataGridViewTextBoxColumn3.DataPropertyName = "CheckInDate";
            this.checkInDateDataGridViewTextBoxColumn3.HeaderText = "Check-In Date";
            this.checkInDateDataGridViewTextBoxColumn3.Name = "checkInDateDataGridViewTextBoxColumn3";
            this.checkInDateDataGridViewTextBoxColumn3.ReadOnly = true;
            this.checkInDateDataGridViewTextBoxColumn3.Width = 150;
            // 
            // dueOutDataGridViewTextBoxColumn3
            // 
            this.dueOutDataGridViewTextBoxColumn3.DataPropertyName = "DueOut";
            this.dueOutDataGridViewTextBoxColumn3.HeaderText = "DueOut";
            this.dueOutDataGridViewTextBoxColumn3.Name = "dueOutDataGridViewTextBoxColumn3";
            this.dueOutDataGridViewTextBoxColumn3.ReadOnly = true;
            this.dueOutDataGridViewTextBoxColumn3.Width = 150;
            // 
            // transportationDataGridViewTextBoxColumn2
            // 
            this.transportationDataGridViewTextBoxColumn2.DataPropertyName = "Transportation";
            this.transportationDataGridViewTextBoxColumn2.HeaderText = "Transportation";
            this.transportationDataGridViewTextBoxColumn2.Name = "transportationDataGridViewTextBoxColumn2";
            this.transportationDataGridViewTextBoxColumn2.ReadOnly = true;
            this.transportationDataGridViewTextBoxColumn2.Width = 150;
            // 
            // flightNumberDataGridViewTextBoxColumn2
            // 
            this.flightNumberDataGridViewTextBoxColumn2.DataPropertyName = "FlightNumber";
            this.flightNumberDataGridViewTextBoxColumn2.HeaderText = "Flight Number";
            this.flightNumberDataGridViewTextBoxColumn2.Name = "flightNumberDataGridViewTextBoxColumn2";
            this.flightNumberDataGridViewTextBoxColumn2.ReadOnly = true;
            this.flightNumberDataGridViewTextBoxColumn2.Width = 150;
            // 
            // pickUpTimeDataGridViewTextBoxColumn2
            // 
            this.pickUpTimeDataGridViewTextBoxColumn2.DataPropertyName = "PickUpTime";
            this.pickUpTimeDataGridViewTextBoxColumn2.HeaderText = "Pick-Up Time";
            this.pickUpTimeDataGridViewTextBoxColumn2.Name = "pickUpTimeDataGridViewTextBoxColumn2";
            this.pickUpTimeDataGridViewTextBoxColumn2.ReadOnly = true;
            this.pickUpTimeDataGridViewTextBoxColumn2.Width = 150;
            // 
            // purposeOfStayDataGridViewTextBoxColumn2
            // 
            this.purposeOfStayDataGridViewTextBoxColumn2.DataPropertyName = "PurposeOfStay";
            this.purposeOfStayDataGridViewTextBoxColumn2.HeaderText = "Purpose Of Stay";
            this.purposeOfStayDataGridViewTextBoxColumn2.Name = "purposeOfStayDataGridViewTextBoxColumn2";
            this.purposeOfStayDataGridViewTextBoxColumn2.ReadOnly = true;
            this.purposeOfStayDataGridViewTextBoxColumn2.Width = 150;
            // 
            // adultsDataGridViewTextBoxColumn2
            // 
            this.adultsDataGridViewTextBoxColumn2.DataPropertyName = "Adults";
            this.adultsDataGridViewTextBoxColumn2.HeaderText = "Adults";
            this.adultsDataGridViewTextBoxColumn2.Name = "adultsDataGridViewTextBoxColumn2";
            this.adultsDataGridViewTextBoxColumn2.ReadOnly = true;
            this.adultsDataGridViewTextBoxColumn2.Width = 150;
            // 
            // childrenDataGridViewTextBoxColumn2
            // 
            this.childrenDataGridViewTextBoxColumn2.DataPropertyName = "Children";
            this.childrenDataGridViewTextBoxColumn2.HeaderText = "Children";
            this.childrenDataGridViewTextBoxColumn2.Name = "childrenDataGridViewTextBoxColumn2";
            this.childrenDataGridViewTextBoxColumn2.ReadOnly = true;
            this.childrenDataGridViewTextBoxColumn2.Width = 150;
            // 
            // cancellationListReportBindingSource
            // 
            this.cancellationListReportBindingSource.DataSource = typeof(maxxis.CancellationList_Report);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel7.Controls.Add(this.label5);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1254, 31);
            this.panel7.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label5.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label5.Location = new System.Drawing.Point(5, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(210, 14);
            this.label5.TabIndex = 0;
            this.label5.Text = "CANCELLATION STATISTIC REPORT";
            // 
            // pnl_reservationStatisticReport
            // 
            this.pnl_reservationStatisticReport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_reservationStatisticReport.Controls.Add(this.dgv_reservationStatistic);
            this.pnl_reservationStatisticReport.Controls.Add(this.panel6);
            this.pnl_reservationStatisticReport.Location = new System.Drawing.Point(15, 887);
            this.pnl_reservationStatisticReport.Name = "pnl_reservationStatisticReport";
            this.pnl_reservationStatisticReport.Size = new System.Drawing.Size(1256, 367);
            this.pnl_reservationStatisticReport.TabIndex = 1;
            // 
            // dgv_reservationStatistic
            // 
            this.dgv_reservationStatistic.AllowUserToAddRows = false;
            this.dgv_reservationStatistic.AllowUserToDeleteRows = false;
            this.dgv_reservationStatistic.AutoGenerateColumns = false;
            this.dgv_reservationStatistic.BackgroundColor = System.Drawing.Color.White;
            this.dgv_reservationStatistic.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_reservationStatistic.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_reservationStatistic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_reservationStatistic.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.reservationNumberDataGridViewTextBoxColumn1,
            this.fullNameDataGridViewTextBoxColumn2,
            this.bookingDataGridViewTextBoxColumn1,
            this.roomNumbersDataGridViewTextBoxColumn1,
            this.roomTypeDataGridViewTextBoxColumn1,
            this.reservationDateDataGridViewTextBoxColumn2,
            this.checkInDateDataGridViewTextBoxColumn2,
            this.dueOutDataGridViewTextBoxColumn2,
            this.transportationDataGridViewTextBoxColumn1,
            this.flightNumberDataGridViewTextBoxColumn1,
            this.pickUpTimeDataGridViewTextBoxColumn1,
            this.purposeOfStayDataGridViewTextBoxColumn1,
            this.adultsDataGridViewTextBoxColumn1,
            this.childrenDataGridViewTextBoxColumn1});
            this.dgv_reservationStatistic.DataSource = this.reservationListReportBindingSource;
            this.dgv_reservationStatistic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_reservationStatistic.EnableHeadersVisualStyles = false;
            this.dgv_reservationStatistic.Location = new System.Drawing.Point(0, 31);
            this.dgv_reservationStatistic.Name = "dgv_reservationStatistic";
            this.dgv_reservationStatistic.ReadOnly = true;
            this.dgv_reservationStatistic.RowHeadersVisible = false;
            this.dgv_reservationStatistic.Size = new System.Drawing.Size(1254, 334);
            this.dgv_reservationStatistic.TabIndex = 14;
            // 
            // reservationNumberDataGridViewTextBoxColumn1
            // 
            this.reservationNumberDataGridViewTextBoxColumn1.DataPropertyName = "ReservationNumber";
            this.reservationNumberDataGridViewTextBoxColumn1.HeaderText = "Reservation Number";
            this.reservationNumberDataGridViewTextBoxColumn1.Name = "reservationNumberDataGridViewTextBoxColumn1";
            this.reservationNumberDataGridViewTextBoxColumn1.ReadOnly = true;
            this.reservationNumberDataGridViewTextBoxColumn1.Width = 200;
            // 
            // fullNameDataGridViewTextBoxColumn2
            // 
            this.fullNameDataGridViewTextBoxColumn2.DataPropertyName = "FullName";
            this.fullNameDataGridViewTextBoxColumn2.HeaderText = "Full Name";
            this.fullNameDataGridViewTextBoxColumn2.Name = "fullNameDataGridViewTextBoxColumn2";
            this.fullNameDataGridViewTextBoxColumn2.ReadOnly = true;
            this.fullNameDataGridViewTextBoxColumn2.Width = 200;
            // 
            // bookingDataGridViewTextBoxColumn1
            // 
            this.bookingDataGridViewTextBoxColumn1.DataPropertyName = "Booking";
            this.bookingDataGridViewTextBoxColumn1.HeaderText = "Booking";
            this.bookingDataGridViewTextBoxColumn1.Name = "bookingDataGridViewTextBoxColumn1";
            this.bookingDataGridViewTextBoxColumn1.ReadOnly = true;
            this.bookingDataGridViewTextBoxColumn1.Width = 150;
            // 
            // roomNumbersDataGridViewTextBoxColumn1
            // 
            this.roomNumbersDataGridViewTextBoxColumn1.DataPropertyName = "RoomNumbers";
            this.roomNumbersDataGridViewTextBoxColumn1.HeaderText = "Room Numbers";
            this.roomNumbersDataGridViewTextBoxColumn1.Name = "roomNumbersDataGridViewTextBoxColumn1";
            this.roomNumbersDataGridViewTextBoxColumn1.ReadOnly = true;
            this.roomNumbersDataGridViewTextBoxColumn1.Width = 150;
            // 
            // roomTypeDataGridViewTextBoxColumn1
            // 
            this.roomTypeDataGridViewTextBoxColumn1.DataPropertyName = "RoomType";
            this.roomTypeDataGridViewTextBoxColumn1.HeaderText = "Room Type";
            this.roomTypeDataGridViewTextBoxColumn1.Name = "roomTypeDataGridViewTextBoxColumn1";
            this.roomTypeDataGridViewTextBoxColumn1.ReadOnly = true;
            this.roomTypeDataGridViewTextBoxColumn1.Width = 200;
            // 
            // reservationDateDataGridViewTextBoxColumn2
            // 
            this.reservationDateDataGridViewTextBoxColumn2.DataPropertyName = "ReservationDate";
            this.reservationDateDataGridViewTextBoxColumn2.HeaderText = "Reservation Date";
            this.reservationDateDataGridViewTextBoxColumn2.Name = "reservationDateDataGridViewTextBoxColumn2";
            this.reservationDateDataGridViewTextBoxColumn2.ReadOnly = true;
            this.reservationDateDataGridViewTextBoxColumn2.Width = 150;
            // 
            // checkInDateDataGridViewTextBoxColumn2
            // 
            this.checkInDateDataGridViewTextBoxColumn2.DataPropertyName = "CheckInDate";
            this.checkInDateDataGridViewTextBoxColumn2.HeaderText = "Check-In Date";
            this.checkInDateDataGridViewTextBoxColumn2.Name = "checkInDateDataGridViewTextBoxColumn2";
            this.checkInDateDataGridViewTextBoxColumn2.ReadOnly = true;
            this.checkInDateDataGridViewTextBoxColumn2.Width = 150;
            // 
            // dueOutDataGridViewTextBoxColumn2
            // 
            this.dueOutDataGridViewTextBoxColumn2.DataPropertyName = "DueOut";
            this.dueOutDataGridViewTextBoxColumn2.HeaderText = "DueOut";
            this.dueOutDataGridViewTextBoxColumn2.Name = "dueOutDataGridViewTextBoxColumn2";
            this.dueOutDataGridViewTextBoxColumn2.ReadOnly = true;
            this.dueOutDataGridViewTextBoxColumn2.Width = 150;
            // 
            // transportationDataGridViewTextBoxColumn1
            // 
            this.transportationDataGridViewTextBoxColumn1.DataPropertyName = "Transportation";
            this.transportationDataGridViewTextBoxColumn1.HeaderText = "Transportation";
            this.transportationDataGridViewTextBoxColumn1.Name = "transportationDataGridViewTextBoxColumn1";
            this.transportationDataGridViewTextBoxColumn1.ReadOnly = true;
            this.transportationDataGridViewTextBoxColumn1.Width = 150;
            // 
            // flightNumberDataGridViewTextBoxColumn1
            // 
            this.flightNumberDataGridViewTextBoxColumn1.DataPropertyName = "FlightNumber";
            this.flightNumberDataGridViewTextBoxColumn1.HeaderText = "Flight Number";
            this.flightNumberDataGridViewTextBoxColumn1.Name = "flightNumberDataGridViewTextBoxColumn1";
            this.flightNumberDataGridViewTextBoxColumn1.ReadOnly = true;
            this.flightNumberDataGridViewTextBoxColumn1.Width = 150;
            // 
            // pickUpTimeDataGridViewTextBoxColumn1
            // 
            this.pickUpTimeDataGridViewTextBoxColumn1.DataPropertyName = "PickUpTime";
            this.pickUpTimeDataGridViewTextBoxColumn1.HeaderText = "Pick-Up Time";
            this.pickUpTimeDataGridViewTextBoxColumn1.Name = "pickUpTimeDataGridViewTextBoxColumn1";
            this.pickUpTimeDataGridViewTextBoxColumn1.ReadOnly = true;
            this.pickUpTimeDataGridViewTextBoxColumn1.Width = 150;
            // 
            // purposeOfStayDataGridViewTextBoxColumn1
            // 
            this.purposeOfStayDataGridViewTextBoxColumn1.DataPropertyName = "PurposeOfStay";
            this.purposeOfStayDataGridViewTextBoxColumn1.HeaderText = "Purpose Of Stay";
            this.purposeOfStayDataGridViewTextBoxColumn1.Name = "purposeOfStayDataGridViewTextBoxColumn1";
            this.purposeOfStayDataGridViewTextBoxColumn1.ReadOnly = true;
            this.purposeOfStayDataGridViewTextBoxColumn1.Width = 150;
            // 
            // adultsDataGridViewTextBoxColumn1
            // 
            this.adultsDataGridViewTextBoxColumn1.DataPropertyName = "Adults";
            this.adultsDataGridViewTextBoxColumn1.HeaderText = "Adults";
            this.adultsDataGridViewTextBoxColumn1.Name = "adultsDataGridViewTextBoxColumn1";
            this.adultsDataGridViewTextBoxColumn1.ReadOnly = true;
            this.adultsDataGridViewTextBoxColumn1.Width = 150;
            // 
            // childrenDataGridViewTextBoxColumn1
            // 
            this.childrenDataGridViewTextBoxColumn1.DataPropertyName = "Children";
            this.childrenDataGridViewTextBoxColumn1.HeaderText = "Children";
            this.childrenDataGridViewTextBoxColumn1.Name = "childrenDataGridViewTextBoxColumn1";
            this.childrenDataGridViewTextBoxColumn1.ReadOnly = true;
            this.childrenDataGridViewTextBoxColumn1.Width = 150;
            // 
            // reservationListReportBindingSource
            // 
            this.reservationListReportBindingSource.DataSource = typeof(maxxis.reservationList_Report);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel6.Controls.Add(this.label3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1254, 31);
            this.panel6.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label3.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label3.Location = new System.Drawing.Point(5, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(205, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "RESERVATION STATISTIC REPORT";
            // 
            // pnl_guestArrivalStatistic
            // 
            this.pnl_guestArrivalStatistic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_guestArrivalStatistic.Controls.Add(this.dgv_arrivalStatistic);
            this.pnl_guestArrivalStatistic.Controls.Add(this.panel5);
            this.pnl_guestArrivalStatistic.Location = new System.Drawing.Point(15, 448);
            this.pnl_guestArrivalStatistic.Name = "pnl_guestArrivalStatistic";
            this.pnl_guestArrivalStatistic.Size = new System.Drawing.Size(1256, 367);
            this.pnl_guestArrivalStatistic.TabIndex = 0;
            // 
            // dgv_arrivalStatistic
            // 
            this.dgv_arrivalStatistic.AllowUserToAddRows = false;
            this.dgv_arrivalStatistic.AllowUserToDeleteRows = false;
            this.dgv_arrivalStatistic.AutoGenerateColumns = false;
            this.dgv_arrivalStatistic.BackgroundColor = System.Drawing.Color.White;
            this.dgv_arrivalStatistic.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_arrivalStatistic.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_arrivalStatistic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_arrivalStatistic.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.reservationNumberDataGridViewTextBoxColumn,
            this.fullNameDataGridViewTextBoxColumn1,
            this.bookingDataGridViewTextBoxColumn,
            this.roomNumbersDataGridViewTextBoxColumn,
            this.roomTypeDataGridViewTextBoxColumn,
            this.reservationDateDataGridViewTextBoxColumn1,
            this.checkInDateDataGridViewTextBoxColumn1,
            this.dueOutDataGridViewTextBoxColumn1,
            this.transportationDataGridViewTextBoxColumn,
            this.flightNumberDataGridViewTextBoxColumn,
            this.pickUpTimeDataGridViewTextBoxColumn,
            this.purposeOfStayDataGridViewTextBoxColumn,
            this.adultsDataGridViewTextBoxColumn,
            this.childrenDataGridViewTextBoxColumn});
            this.dgv_arrivalStatistic.DataSource = this.guestArrivalReportBindingSource;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_arrivalStatistic.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgv_arrivalStatistic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_arrivalStatistic.EnableHeadersVisualStyles = false;
            this.dgv_arrivalStatistic.Location = new System.Drawing.Point(0, 31);
            this.dgv_arrivalStatistic.Name = "dgv_arrivalStatistic";
            this.dgv_arrivalStatistic.ReadOnly = true;
            this.dgv_arrivalStatistic.RowHeadersVisible = false;
            this.dgv_arrivalStatistic.Size = new System.Drawing.Size(1254, 334);
            this.dgv_arrivalStatistic.TabIndex = 13;
            // 
            // reservationNumberDataGridViewTextBoxColumn
            // 
            this.reservationNumberDataGridViewTextBoxColumn.DataPropertyName = "ReservationNumber";
            this.reservationNumberDataGridViewTextBoxColumn.HeaderText = "Reservation Number";
            this.reservationNumberDataGridViewTextBoxColumn.Name = "reservationNumberDataGridViewTextBoxColumn";
            this.reservationNumberDataGridViewTextBoxColumn.ReadOnly = true;
            this.reservationNumberDataGridViewTextBoxColumn.Width = 200;
            // 
            // fullNameDataGridViewTextBoxColumn1
            // 
            this.fullNameDataGridViewTextBoxColumn1.DataPropertyName = "FullName";
            this.fullNameDataGridViewTextBoxColumn1.HeaderText = "Full Name";
            this.fullNameDataGridViewTextBoxColumn1.Name = "fullNameDataGridViewTextBoxColumn1";
            this.fullNameDataGridViewTextBoxColumn1.ReadOnly = true;
            this.fullNameDataGridViewTextBoxColumn1.Width = 200;
            // 
            // bookingDataGridViewTextBoxColumn
            // 
            this.bookingDataGridViewTextBoxColumn.DataPropertyName = "Booking";
            this.bookingDataGridViewTextBoxColumn.HeaderText = "Booking";
            this.bookingDataGridViewTextBoxColumn.Name = "bookingDataGridViewTextBoxColumn";
            this.bookingDataGridViewTextBoxColumn.ReadOnly = true;
            this.bookingDataGridViewTextBoxColumn.Width = 150;
            // 
            // roomNumbersDataGridViewTextBoxColumn
            // 
            this.roomNumbersDataGridViewTextBoxColumn.DataPropertyName = "RoomNumbers";
            this.roomNumbersDataGridViewTextBoxColumn.HeaderText = "Room Numbers";
            this.roomNumbersDataGridViewTextBoxColumn.Name = "roomNumbersDataGridViewTextBoxColumn";
            this.roomNumbersDataGridViewTextBoxColumn.ReadOnly = true;
            this.roomNumbersDataGridViewTextBoxColumn.Width = 150;
            // 
            // roomTypeDataGridViewTextBoxColumn
            // 
            this.roomTypeDataGridViewTextBoxColumn.DataPropertyName = "RoomType";
            this.roomTypeDataGridViewTextBoxColumn.HeaderText = "Room Type";
            this.roomTypeDataGridViewTextBoxColumn.Name = "roomTypeDataGridViewTextBoxColumn";
            this.roomTypeDataGridViewTextBoxColumn.ReadOnly = true;
            this.roomTypeDataGridViewTextBoxColumn.Width = 200;
            // 
            // reservationDateDataGridViewTextBoxColumn1
            // 
            this.reservationDateDataGridViewTextBoxColumn1.DataPropertyName = "ReservationDate";
            this.reservationDateDataGridViewTextBoxColumn1.HeaderText = "Reservation Date";
            this.reservationDateDataGridViewTextBoxColumn1.Name = "reservationDateDataGridViewTextBoxColumn1";
            this.reservationDateDataGridViewTextBoxColumn1.ReadOnly = true;
            this.reservationDateDataGridViewTextBoxColumn1.Width = 150;
            // 
            // checkInDateDataGridViewTextBoxColumn1
            // 
            this.checkInDateDataGridViewTextBoxColumn1.DataPropertyName = "CheckInDate";
            this.checkInDateDataGridViewTextBoxColumn1.HeaderText = "Check-In Date";
            this.checkInDateDataGridViewTextBoxColumn1.Name = "checkInDateDataGridViewTextBoxColumn1";
            this.checkInDateDataGridViewTextBoxColumn1.ReadOnly = true;
            this.checkInDateDataGridViewTextBoxColumn1.Width = 150;
            // 
            // dueOutDataGridViewTextBoxColumn1
            // 
            this.dueOutDataGridViewTextBoxColumn1.DataPropertyName = "DueOut";
            this.dueOutDataGridViewTextBoxColumn1.HeaderText = "Due Out";
            this.dueOutDataGridViewTextBoxColumn1.Name = "dueOutDataGridViewTextBoxColumn1";
            this.dueOutDataGridViewTextBoxColumn1.ReadOnly = true;
            this.dueOutDataGridViewTextBoxColumn1.Width = 150;
            // 
            // transportationDataGridViewTextBoxColumn
            // 
            this.transportationDataGridViewTextBoxColumn.DataPropertyName = "Transportation";
            this.transportationDataGridViewTextBoxColumn.HeaderText = "Transportation";
            this.transportationDataGridViewTextBoxColumn.Name = "transportationDataGridViewTextBoxColumn";
            this.transportationDataGridViewTextBoxColumn.ReadOnly = true;
            this.transportationDataGridViewTextBoxColumn.Width = 150;
            // 
            // flightNumberDataGridViewTextBoxColumn
            // 
            this.flightNumberDataGridViewTextBoxColumn.DataPropertyName = "FlightNumber";
            this.flightNumberDataGridViewTextBoxColumn.HeaderText = "Flight Number";
            this.flightNumberDataGridViewTextBoxColumn.Name = "flightNumberDataGridViewTextBoxColumn";
            this.flightNumberDataGridViewTextBoxColumn.ReadOnly = true;
            this.flightNumberDataGridViewTextBoxColumn.Width = 150;
            // 
            // pickUpTimeDataGridViewTextBoxColumn
            // 
            this.pickUpTimeDataGridViewTextBoxColumn.DataPropertyName = "PickUpTime";
            this.pickUpTimeDataGridViewTextBoxColumn.HeaderText = "Pick-Up Time";
            this.pickUpTimeDataGridViewTextBoxColumn.Name = "pickUpTimeDataGridViewTextBoxColumn";
            this.pickUpTimeDataGridViewTextBoxColumn.ReadOnly = true;
            this.pickUpTimeDataGridViewTextBoxColumn.Width = 150;
            // 
            // purposeOfStayDataGridViewTextBoxColumn
            // 
            this.purposeOfStayDataGridViewTextBoxColumn.DataPropertyName = "PurposeOfStay";
            this.purposeOfStayDataGridViewTextBoxColumn.HeaderText = "Purpose Of Stay";
            this.purposeOfStayDataGridViewTextBoxColumn.Name = "purposeOfStayDataGridViewTextBoxColumn";
            this.purposeOfStayDataGridViewTextBoxColumn.ReadOnly = true;
            this.purposeOfStayDataGridViewTextBoxColumn.Width = 150;
            // 
            // adultsDataGridViewTextBoxColumn
            // 
            this.adultsDataGridViewTextBoxColumn.DataPropertyName = "Adults";
            this.adultsDataGridViewTextBoxColumn.HeaderText = "Adults";
            this.adultsDataGridViewTextBoxColumn.Name = "adultsDataGridViewTextBoxColumn";
            this.adultsDataGridViewTextBoxColumn.ReadOnly = true;
            this.adultsDataGridViewTextBoxColumn.Width = 150;
            // 
            // childrenDataGridViewTextBoxColumn
            // 
            this.childrenDataGridViewTextBoxColumn.DataPropertyName = "Children";
            this.childrenDataGridViewTextBoxColumn.HeaderText = "Children";
            this.childrenDataGridViewTextBoxColumn.Name = "childrenDataGridViewTextBoxColumn";
            this.childrenDataGridViewTextBoxColumn.ReadOnly = true;
            this.childrenDataGridViewTextBoxColumn.Width = 150;
            // 
            // guestArrivalReportBindingSource
            // 
            this.guestArrivalReportBindingSource.DataSource = typeof(maxxis.guestArrival_Report);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel5.Controls.Add(this.label2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1254, 31);
            this.panel5.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label2.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label2.Location = new System.Drawing.Point(5, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(215, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "GUEST ARRIVAL STATISTIC REPORT";
            // 
            // Reports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(1320, 730);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Reports";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reports";
            this.Load += new System.EventHandler(this.Reports_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_accomodationList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accomodationListReportBindingSource)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.pnl_nightCutOffReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_nightCutOffReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nightCutOffReportBindingSource)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.pnl_cancellationStatisticReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_cancellationStatisticReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancellationListReportBindingSource)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.pnl_reservationStatisticReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_reservationStatistic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reservationListReportBindingSource)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.pnl_guestArrivalStatistic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_arrivalStatistic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.guestArrivalReportBindingSource)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_reports;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel pnl_guestArrivalStatistic;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgv_arrivalStatistic;
        private System.Windows.Forms.Panel pnl_reservationStatisticReport;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgv_reservationStatistic;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pnl_cancellationStatisticReport;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dgv_cancellationStatisticReport;
        private System.Windows.Forms.Panel pnl_nightCutOffReport;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_print_GAR;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgv_nightCutOffReport;
        private System.Windows.Forms.Button btn_loadReport;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtp_toDate;
        private System.Windows.Forms.DateTimePicker dtp_fromDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalBillAccumulatedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalBillAccumulatedDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalBillAccumulatedDataGridViewTextBoxColumn2;
        private System.Windows.Forms.Button btn_print_CLR;
        private System.Windows.Forms.Button btn_print_RLR;
        private System.Windows.Forms.Button btn_print_NCO;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.DataGridView dgv_accomodationList;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_print_AL;
        private System.Windows.Forms.BindingSource cancellationListReportBindingSource;
        private System.Windows.Forms.BindingSource reservationListReportBindingSource;
        private System.Windows.Forms.BindingSource guestArrivalReportBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn reservationNumberDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn fullNameDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn bookingDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn roomNumbersDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn roomTypeDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn reservationDateDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn cancellationDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn checkInDateDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dueOutDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn transportationDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn flightNumberDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn pickUpTimeDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn purposeOfStayDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn adultsDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn childrenDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn reservationNumberDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fullNameDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn bookingDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn roomNumbersDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn roomTypeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn reservationDateDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn checkInDateDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dueOutDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn transportationDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn flightNumberDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn pickUpTimeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn purposeOfStayDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn adultsDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn childrenDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn reservationNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fullNameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn bookingDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn roomNumbersDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn roomTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reservationDateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn checkInDateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dueOutDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn transportationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn flightNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pickUpTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn purposeOfStayDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn adultsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn childrenDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource accomodationListReportBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn folioNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fullNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reservationDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn checkInDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dueOutDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberOfNightsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn preAuthorizedAmountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn folioNumberDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn reservationNumberDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn fullNameDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn bookingDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn roomNumbersDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn roomTypesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reservationDateDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn cancellationDateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn checkInDateDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dueOutDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn checkOutDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberOfNightsDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cancellationPenaltyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalPriceOfAmenitiesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalPriceForLossBreakageFinesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource nightCutOffReportBindingSource;
    }
}