﻿namespace maxxis
{
    partial class Guest_Folio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.guestFolio_panel = new System.Windows.Forms.Panel();
            this.lbl_guestFolio = new System.Windows.Forms.Label();
            this.pcb_guestFolio = new System.Windows.Forms.PictureBox();
            this.pnl_guestFolio = new System.Windows.Forms.Panel();
            this.btn_GoBack = new System.Windows.Forms.Button();
            this.dtp_departureTime = new System.Windows.Forms.DateTimePicker();
            this.dtp_arrivalTime = new System.Windows.Forms.DateTimePicker();
            this.dtp_departureDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_arrivalDate = new System.Windows.Forms.DateTimePicker();
            this.lbl_child = new System.Windows.Forms.Label();
            this.lbl_adults = new System.Windows.Forms.Label();
            this.lbl_numberOfNights = new System.Windows.Forms.Label();
            this.lbl_departure = new System.Windows.Forms.Label();
            this.lbl_arrival = new System.Windows.Forms.Label();
            this.txt_purposeOfVisit = new System.Windows.Forms.TextBox();
            this.dtp_timeOfArrival = new System.Windows.Forms.DateTimePicker();
            this.txt_departureTransportation = new System.Windows.Forms.TextBox();
            this.lbl_dateOfArrival = new System.Windows.Forms.Label();
            this.txt_goingTo = new System.Windows.Forms.TextBox();
            this.lbl_timeOfArrival = new System.Windows.Forms.Label();
            this.lbl_purposeOfVisit = new System.Windows.Forms.Label();
            this.dtp_dateOfArrival = new System.Windows.Forms.DateTimePicker();
            this.lbl_goingTo = new System.Windows.Forms.Label();
            this.lbl_departureTransportation = new System.Windows.Forms.Label();
            this.txt_arrivedForm = new System.Windows.Forms.TextBox();
            this.lbl_serialNumber = new System.Windows.Forms.Label();
            this.txt_visaIssuePlace = new System.Windows.Forms.TextBox();
            this.lbl_visaNumber = new System.Windows.Forms.Label();
            this.txt_visaNumber = new System.Windows.Forms.TextBox();
            this.lbl_visaDate = new System.Windows.Forms.Label();
            this.dtp_visaDate = new System.Windows.Forms.DateTimePicker();
            this.lbl_vissaIssuePlace = new System.Windows.Forms.Label();
            this.txt_serialNumber = new System.Windows.Forms.TextBox();
            this.lbl_arrivedForm = new System.Windows.Forms.Label();
            this.dgv_billingSummary = new System.Windows.Forms.DataGridView();
            this.col_Particulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Rate_Cost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_TotalAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_checkOut = new System.Windows.Forms.Button();
            this.cmb_homeCountry = new System.Windows.Forms.ComboBox();
            this.lbl_homeCountry = new System.Windows.Forms.Label();
            this.lbl_companyAgency = new System.Windows.Forms.Label();
            this.txt_companyAgency = new System.Windows.Forms.TextBox();
            this.lbl_dateOfBirth = new System.Windows.Forms.Label();
            this.dtb_dateOfBirth = new System.Windows.Forms.DateTimePicker();
            this.lbl_province = new System.Windows.Forms.Label();
            this.txt_province = new System.Windows.Forms.TextBox();
            this.lbl_city = new System.Windows.Forms.Label();
            this.txt_city = new System.Windows.Forms.TextBox();
            this.lbl_address = new System.Windows.Forms.Label();
            this.txt_address = new System.Windows.Forms.TextBox();
            this.txt_middleName = new System.Windows.Forms.TextBox();
            this.lbl_middleName = new System.Windows.Forms.Label();
            this.lbl_title = new System.Windows.Forms.Label();
            this.lbl_email = new System.Windows.Forms.Label();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.lbl_contactDetails = new System.Windows.Forms.Label();
            this.txt_contactDetails = new System.Windows.Forms.TextBox();
            this.lbl_last_name = new System.Windows.Forms.Label();
            this.lbl_showRates = new System.Windows.Forms.Label();
            this.txt_firstname = new System.Windows.Forms.TextBox();
            this.cbox_title = new System.Windows.Forms.ComboBox();
            this.lbl_firstname = new System.Windows.Forms.Label();
            this.txt_lastname = new System.Windows.Forms.TextBox();
            this.lbl_modeOfPayment = new System.Windows.Forms.Label();
            this.rbtn_partialCredit = new System.Windows.Forms.RadioButton();
            this.rbtn_credit = new System.Windows.Forms.RadioButton();
            this.rbtn_cash = new System.Windows.Forms.RadioButton();
            this.txt_folioNumber = new System.Windows.Forms.TextBox();
            this.lbl_folioNumber = new System.Windows.Forms.Label();
            this.txt_billTo = new System.Windows.Forms.TextBox();
            this.lbl_billTo = new System.Windows.Forms.Label();
            this.txt_expire = new System.Windows.Forms.TextBox();
            this.txt_cardNumber = new System.Windows.Forms.TextBox();
            this.cmb_type = new System.Windows.Forms.ComboBox();
            this.pcb_reloadGuestFolio = new System.Windows.Forms.PictureBox();
            this.pcb_searchFolioNumber = new System.Windows.Forms.PictureBox();
            this.pcb_addBillTo = new System.Windows.Forms.PictureBox();
            this.pcb_searchBillTo = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.txt_balance = new System.Windows.Forms.TextBox();
            this.txt_totalPayment = new System.Windows.Forms.TextBox();
            this.txt_totalDiscount = new System.Windows.Forms.TextBox();
            this.txt_subTotal = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_otherCharges = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txt_referenceNumber = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txt_fee_specialArrangements = new System.Windows.Forms.TextBox();
            this.lbl_specialArrangements = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txt_fee_additionalAdultsAndChildren = new System.Windows.Forms.TextBox();
            this.txt_fee_totalNumberOfChildren = new System.Windows.Forms.TextBox();
            this.txt_fee_totalNumberOfAdults = new System.Windows.Forms.TextBox();
            this.lbl_additionalCompanionChildren = new System.Windows.Forms.Label();
            this.lbl_additionalCompanionAdults = new System.Windows.Forms.Label();
            this.total_numberOfChildren = new System.Windows.Forms.TextBox();
            this.txt_totalNumberOfAdults = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.dgv_payments = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.dgv_discount = new System.Windows.Forms.DataGridView();
            this.col_Date_discountDGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Type_discountDGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Discount_discountDGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.btn_edit = new System.Windows.Forms.Button();
            this.txt_guestRemarks = new System.Windows.Forms.TextBox();
            this.panel25 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.txt_child = new System.Windows.Forms.TextBox();
            this.txt_adults = new System.Windows.Forms.TextBox();
            this.txt_noofnights = new System.Windows.Forms.TextBox();
            this.stayInformation_panel = new System.Windows.Forms.Panel();
            this.lbl_stayInformation = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.btn_apply = new System.Windows.Forms.Button();
            this.txt_discountDescription = new System.Windows.Forms.TextBox();
            this.lbl_discountDescription = new System.Windows.Forms.Label();
            this.lbl_discountAmount = new System.Windows.Forms.Label();
            this.txt_discountAmount = new System.Windows.Forms.TextBox();
            this.panel35 = new System.Windows.Forms.Panel();
            this.lbl_discount = new System.Windows.Forms.Label();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.lbl_credit = new System.Windows.Forms.Label();
            this.lbl_type = new System.Windows.Forms.Label();
            this.lbl_cardNumber = new System.Windows.Forms.Label();
            this.lbl_expire = new System.Windows.Forms.Label();
            this.txt_cvv = new System.Windows.Forms.TextBox();
            this.lbl_cvv = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.txt_change = new System.Windows.Forms.TextBox();
            this.txt_cashPayment = new System.Windows.Forms.TextBox();
            this.lbl_cashPayment = new System.Windows.Forms.Label();
            this.lbl_change = new System.Windows.Forms.Label();
            this.txt_cashhanded = new System.Windows.Forms.TextBox();
            this.lbl_cashHandedByGuest = new System.Windows.Forms.Label();
            this.panel31 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_requiredPrePayment = new System.Windows.Forms.TextBox();
            this.lbl_requiredPrePayment = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_scanImage = new System.Windows.Forms.Button();
            this.btn_attachImage = new System.Windows.Forms.Button();
            this.pcb_image = new System.Windows.Forms.PictureBox();
            this.settlementOption_panel = new System.Windows.Forms.Panel();
            this.lbl_settlementOption = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.guestFolio_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_guestFolio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_billingSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_reloadGuestFolio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_searchFolioNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_addBillTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_searchBillTo)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_payments)).BeginInit();
            this.panel20.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_discount)).BeginInit();
            this.panel18.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel9.SuspendLayout();
            this.stayInformation_panel.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_image)).BeginInit();
            this.settlementOption_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // guestFolio_panel
            // 
            this.guestFolio_panel.Controls.Add(this.lbl_guestFolio);
            this.guestFolio_panel.Controls.Add(this.pcb_guestFolio);
            this.guestFolio_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.guestFolio_panel.Location = new System.Drawing.Point(0, 0);
            this.guestFolio_panel.Name = "guestFolio_panel";
            this.guestFolio_panel.Size = new System.Drawing.Size(1300, 35);
            this.guestFolio_panel.TabIndex = 1;
            // 
            // lbl_guestFolio
            // 
            this.lbl_guestFolio.AutoSize = true;
            this.lbl_guestFolio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_guestFolio.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_guestFolio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_guestFolio.Location = new System.Drawing.Point(37, 9);
            this.lbl_guestFolio.Name = "lbl_guestFolio";
            this.lbl_guestFolio.Size = new System.Drawing.Size(100, 17);
            this.lbl_guestFolio.TabIndex = 0;
            this.lbl_guestFolio.Text = "GUEST FOLIO";
            // 
            // pcb_guestFolio
            // 
            this.pcb_guestFolio.BackgroundImage = global::maxxis.Properties.Resources.Guest_Information;
            this.pcb_guestFolio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_guestFolio.Dock = System.Windows.Forms.DockStyle.Left;
            this.pcb_guestFolio.Location = new System.Drawing.Point(0, 0);
            this.pcb_guestFolio.Name = "pcb_guestFolio";
            this.pcb_guestFolio.Size = new System.Drawing.Size(41, 35);
            this.pcb_guestFolio.TabIndex = 1;
            this.pcb_guestFolio.TabStop = false;
            // 
            // pnl_guestFolio
            // 
            this.pnl_guestFolio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pnl_guestFolio.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnl_guestFolio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl_guestFolio.Location = new System.Drawing.Point(0, 35);
            this.pnl_guestFolio.Name = "pnl_guestFolio";
            this.pnl_guestFolio.Size = new System.Drawing.Size(179, 695);
            this.pnl_guestFolio.TabIndex = 2;
            // 
            // btn_GoBack
            // 
            this.btn_GoBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_GoBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_GoBack.FlatAppearance.BorderSize = 0;
            this.btn_GoBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_GoBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_GoBack.ForeColor = System.Drawing.Color.White;
            this.btn_GoBack.Image = global::maxxis.Properties.Resources.Go_Back;
            this.btn_GoBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_GoBack.Location = new System.Drawing.Point(313, 31);
            this.btn_GoBack.Name = "btn_GoBack";
            this.btn_GoBack.Size = new System.Drawing.Size(128, 35);
            this.btn_GoBack.TabIndex = 2;
            this.btn_GoBack.Text = "GO BACK";
            this.btn_GoBack.UseVisualStyleBackColor = false;
            this.btn_GoBack.Click += new System.EventHandler(this.btn_GoBack_Click);
            // 
            // dtp_departureTime
            // 
            this.dtp_departureTime.CustomFormat = "  hh:mm:ss tt";
            this.dtp_departureTime.Enabled = false;
            this.dtp_departureTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_departureTime.Location = new System.Drawing.Point(403, 82);
            this.dtp_departureTime.Name = "dtp_departureTime";
            this.dtp_departureTime.ShowCheckBox = true;
            this.dtp_departureTime.ShowUpDown = true;
            this.dtp_departureTime.Size = new System.Drawing.Size(138, 20);
            this.dtp_departureTime.TabIndex = 41;
            this.dtp_departureTime.Value = new System.DateTime(2018, 3, 26, 12, 0, 0, 0);
            // 
            // dtp_arrivalTime
            // 
            this.dtp_arrivalTime.CustomFormat = "  hh:mm:ss tt";
            this.dtp_arrivalTime.Enabled = false;
            this.dtp_arrivalTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_arrivalTime.Location = new System.Drawing.Point(403, 41);
            this.dtp_arrivalTime.Name = "dtp_arrivalTime";
            this.dtp_arrivalTime.ShowCheckBox = true;
            this.dtp_arrivalTime.ShowUpDown = true;
            this.dtp_arrivalTime.Size = new System.Drawing.Size(138, 20);
            this.dtp_arrivalTime.TabIndex = 40;
            this.dtp_arrivalTime.Value = new System.DateTime(2018, 3, 26, 2, 0, 0, 0);
            // 
            // dtp_departureDate
            // 
            this.dtp_departureDate.CustomFormat = "MMMM dd, yyyy    -dddd";
            this.dtp_departureDate.Enabled = false;
            this.dtp_departureDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_departureDate.Location = new System.Drawing.Point(129, 83);
            this.dtp_departureDate.Name = "dtp_departureDate";
            this.dtp_departureDate.ShowCheckBox = true;
            this.dtp_departureDate.Size = new System.Drawing.Size(268, 20);
            this.dtp_departureDate.TabIndex = 39;
            // 
            // dtp_arrivalDate
            // 
            this.dtp_arrivalDate.CustomFormat = "MMMM dd, yyyy    -dddd";
            this.dtp_arrivalDate.Enabled = false;
            this.dtp_arrivalDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_arrivalDate.Location = new System.Drawing.Point(129, 41);
            this.dtp_arrivalDate.Name = "dtp_arrivalDate";
            this.dtp_arrivalDate.ShowCheckBox = true;
            this.dtp_arrivalDate.Size = new System.Drawing.Size(268, 20);
            this.dtp_arrivalDate.TabIndex = 38;
            // 
            // lbl_child
            // 
            this.lbl_child.AutoSize = true;
            this.lbl_child.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_child.Location = new System.Drawing.Point(317, 125);
            this.lbl_child.Name = "lbl_child";
            this.lbl_child.Size = new System.Drawing.Size(50, 19);
            this.lbl_child.TabIndex = 34;
            this.lbl_child.Text = "Child:";
            // 
            // lbl_adults
            // 
            this.lbl_adults.AutoSize = true;
            this.lbl_adults.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_adults.Location = new System.Drawing.Point(194, 125);
            this.lbl_adults.Name = "lbl_adults";
            this.lbl_adults.Size = new System.Drawing.Size(58, 19);
            this.lbl_adults.TabIndex = 31;
            this.lbl_adults.Text = "Adults:";
            // 
            // lbl_numberOfNights
            // 
            this.lbl_numberOfNights.AutoSize = true;
            this.lbl_numberOfNights.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_numberOfNights.Location = new System.Drawing.Point(15, 125);
            this.lbl_numberOfNights.Name = "lbl_numberOfNights";
            this.lbl_numberOfNights.Size = new System.Drawing.Size(116, 19);
            this.lbl_numberOfNights.TabIndex = 27;
            this.lbl_numberOfNights.Text = "No. of Night(s):";
            // 
            // lbl_departure
            // 
            this.lbl_departure.AutoSize = true;
            this.lbl_departure.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_departure.Location = new System.Drawing.Point(15, 86);
            this.lbl_departure.Name = "lbl_departure";
            this.lbl_departure.Size = new System.Drawing.Size(82, 19);
            this.lbl_departure.TabIndex = 26;
            this.lbl_departure.Text = "Departure";
            // 
            // lbl_arrival
            // 
            this.lbl_arrival.AutoSize = true;
            this.lbl_arrival.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_arrival.Location = new System.Drawing.Point(15, 44);
            this.lbl_arrival.Name = "lbl_arrival";
            this.lbl_arrival.Size = new System.Drawing.Size(56, 19);
            this.lbl_arrival.TabIndex = 25;
            this.lbl_arrival.Text = "Arrival";
            // 
            // txt_purposeOfVisit
            // 
            this.txt_purposeOfVisit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_purposeOfVisit.Location = new System.Drawing.Point(189, 68);
            this.txt_purposeOfVisit.Name = "txt_purposeOfVisit";
            this.txt_purposeOfVisit.Size = new System.Drawing.Size(366, 23);
            this.txt_purposeOfVisit.TabIndex = 44;
            // 
            // dtp_timeOfArrival
            // 
            this.dtp_timeOfArrival.CustomFormat = "  hh:mm:ss tt";
            this.dtp_timeOfArrival.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_timeOfArrival.Location = new System.Drawing.Point(189, 39);
            this.dtp_timeOfArrival.Name = "dtp_timeOfArrival";
            this.dtp_timeOfArrival.ShowCheckBox = true;
            this.dtp_timeOfArrival.ShowUpDown = true;
            this.dtp_timeOfArrival.Size = new System.Drawing.Size(166, 20);
            this.dtp_timeOfArrival.TabIndex = 43;
            // 
            // txt_departureTransportation
            // 
            this.txt_departureTransportation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_departureTransportation.Location = new System.Drawing.Point(189, 126);
            this.txt_departureTransportation.Name = "txt_departureTransportation";
            this.txt_departureTransportation.Size = new System.Drawing.Size(366, 23);
            this.txt_departureTransportation.TabIndex = 42;
            // 
            // lbl_dateOfArrival
            // 
            this.lbl_dateOfArrival.AutoSize = true;
            this.lbl_dateOfArrival.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dateOfArrival.Location = new System.Drawing.Point(14, 13);
            this.lbl_dateOfArrival.Name = "lbl_dateOfArrival";
            this.lbl_dateOfArrival.Size = new System.Drawing.Size(114, 19);
            this.lbl_dateOfArrival.TabIndex = 26;
            this.lbl_dateOfArrival.Text = "Date of Arrival";
            // 
            // txt_goingTo
            // 
            this.txt_goingTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_goingTo.Location = new System.Drawing.Point(189, 97);
            this.txt_goingTo.Name = "txt_goingTo";
            this.txt_goingTo.Size = new System.Drawing.Size(366, 23);
            this.txt_goingTo.TabIndex = 41;
            // 
            // lbl_timeOfArrival
            // 
            this.lbl_timeOfArrival.AutoSize = true;
            this.lbl_timeOfArrival.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_timeOfArrival.Location = new System.Drawing.Point(14, 42);
            this.lbl_timeOfArrival.Name = "lbl_timeOfArrival";
            this.lbl_timeOfArrival.Size = new System.Drawing.Size(116, 19);
            this.lbl_timeOfArrival.TabIndex = 27;
            this.lbl_timeOfArrival.Text = "Time of Arrival";
            // 
            // lbl_purposeOfVisit
            // 
            this.lbl_purposeOfVisit.AutoSize = true;
            this.lbl_purposeOfVisit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_purposeOfVisit.Location = new System.Drawing.Point(14, 71);
            this.lbl_purposeOfVisit.Name = "lbl_purposeOfVisit";
            this.lbl_purposeOfVisit.Size = new System.Drawing.Size(124, 19);
            this.lbl_purposeOfVisit.TabIndex = 28;
            this.lbl_purposeOfVisit.Text = "Purpose of Visit";
            // 
            // dtp_dateOfArrival
            // 
            this.dtp_dateOfArrival.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtp_dateOfArrival.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtp_dateOfArrival.CustomFormat = "  dd/MM/yyyy     - dddd";
            this.dtp_dateOfArrival.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_dateOfArrival.Location = new System.Drawing.Point(189, 10);
            this.dtp_dateOfArrival.Name = "dtp_dateOfArrival";
            this.dtp_dateOfArrival.ShowCheckBox = true;
            this.dtp_dateOfArrival.Size = new System.Drawing.Size(239, 20);
            this.dtp_dateOfArrival.TabIndex = 39;
            // 
            // lbl_goingTo
            // 
            this.lbl_goingTo.AutoSize = true;
            this.lbl_goingTo.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_goingTo.Location = new System.Drawing.Point(14, 100);
            this.lbl_goingTo.Name = "lbl_goingTo";
            this.lbl_goingTo.Size = new System.Drawing.Size(71, 19);
            this.lbl_goingTo.TabIndex = 29;
            this.lbl_goingTo.Text = "Going to";
            // 
            // lbl_departureTransportation
            // 
            this.lbl_departureTransportation.AutoSize = true;
            this.lbl_departureTransportation.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_departureTransportation.Location = new System.Drawing.Point(14, 129);
            this.lbl_departureTransportation.Name = "lbl_departureTransportation";
            this.lbl_departureTransportation.Size = new System.Drawing.Size(172, 19);
            this.lbl_departureTransportation.TabIndex = 30;
            this.lbl_departureTransportation.Text = "Depart. Transportation";
            // 
            // txt_arrivedForm
            // 
            this.txt_arrivedForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_arrivedForm.Location = new System.Drawing.Point(190, 124);
            this.txt_arrivedForm.Name = "txt_arrivedForm";
            this.txt_arrivedForm.Size = new System.Drawing.Size(365, 23);
            this.txt_arrivedForm.TabIndex = 42;
            // 
            // lbl_serialNumber
            // 
            this.lbl_serialNumber.AutoSize = true;
            this.lbl_serialNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_serialNumber.Location = new System.Drawing.Point(15, 12);
            this.lbl_serialNumber.Name = "lbl_serialNumber";
            this.lbl_serialNumber.Size = new System.Drawing.Size(77, 19);
            this.lbl_serialNumber.TabIndex = 26;
            this.lbl_serialNumber.Text = "Serial No";
            // 
            // txt_visaIssuePlace
            // 
            this.txt_visaIssuePlace.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_visaIssuePlace.Location = new System.Drawing.Point(190, 95);
            this.txt_visaIssuePlace.Name = "txt_visaIssuePlace";
            this.txt_visaIssuePlace.Size = new System.Drawing.Size(365, 23);
            this.txt_visaIssuePlace.TabIndex = 41;
            // 
            // lbl_visaNumber
            // 
            this.lbl_visaNumber.AutoSize = true;
            this.lbl_visaNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_visaNumber.Location = new System.Drawing.Point(15, 41);
            this.lbl_visaNumber.Name = "lbl_visaNumber";
            this.lbl_visaNumber.Size = new System.Drawing.Size(65, 19);
            this.lbl_visaNumber.TabIndex = 27;
            this.lbl_visaNumber.Text = "Visa No";
            // 
            // txt_visaNumber
            // 
            this.txt_visaNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_visaNumber.Location = new System.Drawing.Point(190, 37);
            this.txt_visaNumber.Name = "txt_visaNumber";
            this.txt_visaNumber.Size = new System.Drawing.Size(365, 23);
            this.txt_visaNumber.TabIndex = 40;
            // 
            // lbl_visaDate
            // 
            this.lbl_visaDate.AutoSize = true;
            this.lbl_visaDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_visaDate.Location = new System.Drawing.Point(15, 70);
            this.lbl_visaDate.Name = "lbl_visaDate";
            this.lbl_visaDate.Size = new System.Drawing.Size(79, 19);
            this.lbl_visaDate.TabIndex = 28;
            this.lbl_visaDate.Text = "Visa Date";
            // 
            // dtp_visaDate
            // 
            this.dtp_visaDate.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtp_visaDate.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtp_visaDate.CustomFormat = "  dd/MM/yyyy     - dddd";
            this.dtp_visaDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_visaDate.Location = new System.Drawing.Point(190, 66);
            this.dtp_visaDate.Name = "dtp_visaDate";
            this.dtp_visaDate.ShowCheckBox = true;
            this.dtp_visaDate.Size = new System.Drawing.Size(238, 20);
            this.dtp_visaDate.TabIndex = 39;
            // 
            // lbl_vissaIssuePlace
            // 
            this.lbl_vissaIssuePlace.AutoSize = true;
            this.lbl_vissaIssuePlace.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_vissaIssuePlace.Location = new System.Drawing.Point(15, 99);
            this.lbl_vissaIssuePlace.Name = "lbl_vissaIssuePlace";
            this.lbl_vissaIssuePlace.Size = new System.Drawing.Size(137, 19);
            this.lbl_vissaIssuePlace.TabIndex = 29;
            this.lbl_vissaIssuePlace.Text = "Vissa Issue Place";
            // 
            // txt_serialNumber
            // 
            this.txt_serialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_serialNumber.Location = new System.Drawing.Point(190, 8);
            this.txt_serialNumber.Name = "txt_serialNumber";
            this.txt_serialNumber.Size = new System.Drawing.Size(365, 23);
            this.txt_serialNumber.TabIndex = 31;
            // 
            // lbl_arrivedForm
            // 
            this.lbl_arrivedForm.AutoSize = true;
            this.lbl_arrivedForm.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_arrivedForm.Location = new System.Drawing.Point(15, 128);
            this.lbl_arrivedForm.Name = "lbl_arrivedForm";
            this.lbl_arrivedForm.Size = new System.Drawing.Size(104, 19);
            this.lbl_arrivedForm.TabIndex = 30;
            this.lbl_arrivedForm.Text = "Arrived Form";
            // 
            // dgv_billingSummary
            // 
            this.dgv_billingSummary.AllowUserToAddRows = false;
            this.dgv_billingSummary.AllowUserToDeleteRows = false;
            this.dgv_billingSummary.BackgroundColor = System.Drawing.Color.Silver;
            this.dgv_billingSummary.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_billingSummary.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv_billingSummary.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_billingSummary.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_billingSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_billingSummary.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_Particulars,
            this.col_Qty,
            this.col_Unit,
            this.col_Rate_Cost,
            this.col_TotalAmount});
            this.dgv_billingSummary.EnableHeadersVisualStyles = false;
            this.dgv_billingSummary.Location = new System.Drawing.Point(23, 47);
            this.dgv_billingSummary.Name = "dgv_billingSummary";
            this.dgv_billingSummary.ReadOnly = true;
            this.dgv_billingSummary.RowHeadersVisible = false;
            this.dgv_billingSummary.Size = new System.Drawing.Size(856, 147);
            this.dgv_billingSummary.TabIndex = 42;
            // 
            // col_Particulars
            // 
            this.col_Particulars.HeaderText = "Particulars";
            this.col_Particulars.Name = "col_Particulars";
            this.col_Particulars.ReadOnly = true;
            this.col_Particulars.Width = 250;
            // 
            // col_Qty
            // 
            this.col_Qty.HeaderText = "Qty";
            this.col_Qty.Name = "col_Qty";
            this.col_Qty.ReadOnly = true;
            this.col_Qty.Width = 80;
            // 
            // col_Unit
            // 
            this.col_Unit.HeaderText = "Unit";
            this.col_Unit.Name = "col_Unit";
            this.col_Unit.ReadOnly = true;
            // 
            // col_Rate_Cost
            // 
            this.col_Rate_Cost.HeaderText = "Rate/Cost";
            this.col_Rate_Cost.Name = "col_Rate_Cost";
            this.col_Rate_Cost.ReadOnly = true;
            this.col_Rate_Cost.Width = 150;
            // 
            // col_TotalAmount
            // 
            this.col_TotalAmount.HeaderText = "Amount";
            this.col_TotalAmount.Name = "col_TotalAmount";
            this.col_TotalAmount.ReadOnly = true;
            this.col_TotalAmount.Width = 150;
            // 
            // btn_checkOut
            // 
            this.btn_checkOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_checkOut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_checkOut.FlatAppearance.BorderSize = 0;
            this.btn_checkOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_checkOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_checkOut.ForeColor = System.Drawing.Color.White;
            this.btn_checkOut.Image = global::maxxis.Properties.Resources.guestFolio_Info;
            this.btn_checkOut.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_checkOut.Location = new System.Drawing.Point(461, 31);
            this.btn_checkOut.Name = "btn_checkOut";
            this.btn_checkOut.Size = new System.Drawing.Size(128, 35);
            this.btn_checkOut.TabIndex = 3;
            this.btn_checkOut.Text = "CHECK OUT";
            this.btn_checkOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_checkOut.UseVisualStyleBackColor = false;
            this.btn_checkOut.Click += new System.EventHandler(this.btn_checkOut_Click);
            // 
            // cmb_homeCountry
            // 
            this.cmb_homeCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_homeCountry.Enabled = false;
            this.cmb_homeCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_homeCountry.FormattingEnabled = true;
            this.cmb_homeCountry.Location = new System.Drawing.Point(695, 163);
            this.cmb_homeCountry.Name = "cmb_homeCountry";
            this.cmb_homeCountry.Size = new System.Drawing.Size(192, 23);
            this.cmb_homeCountry.TabIndex = 76;
            // 
            // lbl_homeCountry
            // 
            this.lbl_homeCountry.AutoSize = true;
            this.lbl_homeCountry.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_homeCountry.Location = new System.Drawing.Point(692, 145);
            this.lbl_homeCountry.Name = "lbl_homeCountry";
            this.lbl_homeCountry.Size = new System.Drawing.Size(100, 17);
            this.lbl_homeCountry.TabIndex = 75;
            this.lbl_homeCountry.Text = "Home Country";
            // 
            // lbl_companyAgency
            // 
            this.lbl_companyAgency.AutoSize = true;
            this.lbl_companyAgency.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_companyAgency.Location = new System.Drawing.Point(468, 199);
            this.lbl_companyAgency.Name = "lbl_companyAgency";
            this.lbl_companyAgency.Size = new System.Drawing.Size(123, 17);
            this.lbl_companyAgency.TabIndex = 70;
            this.lbl_companyAgency.Text = "Company/Agency";
            // 
            // txt_companyAgency
            // 
            this.txt_companyAgency.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_companyAgency.Location = new System.Drawing.Point(471, 218);
            this.txt_companyAgency.Name = "txt_companyAgency";
            this.txt_companyAgency.ReadOnly = true;
            this.txt_companyAgency.Size = new System.Drawing.Size(300, 21);
            this.txt_companyAgency.TabIndex = 69;
            // 
            // lbl_dateOfBirth
            // 
            this.lbl_dateOfBirth.AutoSize = true;
            this.lbl_dateOfBirth.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dateOfBirth.Location = new System.Drawing.Point(631, 92);
            this.lbl_dateOfBirth.Name = "lbl_dateOfBirth";
            this.lbl_dateOfBirth.Size = new System.Drawing.Size(89, 17);
            this.lbl_dateOfBirth.TabIndex = 68;
            this.lbl_dateOfBirth.Text = "Date of Birth";
            // 
            // dtb_dateOfBirth
            // 
            this.dtb_dateOfBirth.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtb_dateOfBirth.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtb_dateOfBirth.CustomFormat = "MMMM dd, yyyy    -dddd";
            this.dtb_dateOfBirth.Enabled = false;
            this.dtb_dateOfBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtb_dateOfBirth.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtb_dateOfBirth.Location = new System.Drawing.Point(634, 111);
            this.dtb_dateOfBirth.Name = "dtb_dateOfBirth";
            this.dtb_dateOfBirth.Size = new System.Drawing.Size(208, 21);
            this.dtb_dateOfBirth.TabIndex = 67;
            // 
            // lbl_province
            // 
            this.lbl_province.AutoSize = true;
            this.lbl_province.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_province.Location = new System.Drawing.Point(173, 199);
            this.lbl_province.Name = "lbl_province";
            this.lbl_province.Size = new System.Drawing.Size(64, 17);
            this.lbl_province.TabIndex = 66;
            this.lbl_province.Text = "Province";
            // 
            // txt_province
            // 
            this.txt_province.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_province.Location = new System.Drawing.Point(176, 218);
            this.txt_province.Name = "txt_province";
            this.txt_province.ReadOnly = true;
            this.txt_province.Size = new System.Drawing.Size(289, 21);
            this.txt_province.TabIndex = 65;
            // 
            // lbl_city
            // 
            this.lbl_city.AutoSize = true;
            this.lbl_city.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_city.Location = new System.Drawing.Point(468, 146);
            this.lbl_city.Name = "lbl_city";
            this.lbl_city.Size = new System.Drawing.Size(33, 17);
            this.lbl_city.TabIndex = 64;
            this.lbl_city.Text = "City";
            // 
            // txt_city
            // 
            this.txt_city.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_city.Location = new System.Drawing.Point(471, 165);
            this.txt_city.Name = "txt_city";
            this.txt_city.ReadOnly = true;
            this.txt_city.Size = new System.Drawing.Size(218, 21);
            this.txt_city.TabIndex = 63;
            // 
            // lbl_address
            // 
            this.lbl_address.AutoSize = true;
            this.lbl_address.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_address.Location = new System.Drawing.Point(173, 146);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(60, 17);
            this.lbl_address.TabIndex = 62;
            this.lbl_address.Text = "Address";
            // 
            // txt_address
            // 
            this.txt_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_address.Location = new System.Drawing.Point(176, 165);
            this.txt_address.Name = "txt_address";
            this.txt_address.ReadOnly = true;
            this.txt_address.Size = new System.Drawing.Size(289, 21);
            this.txt_address.TabIndex = 61;
            // 
            // txt_middleName
            // 
            this.txt_middleName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_middleName.Location = new System.Drawing.Point(683, 55);
            this.txt_middleName.Name = "txt_middleName";
            this.txt_middleName.ReadOnly = true;
            this.txt_middleName.Size = new System.Drawing.Size(204, 21);
            this.txt_middleName.TabIndex = 60;
            // 
            // lbl_middleName
            // 
            this.lbl_middleName.AutoSize = true;
            this.lbl_middleName.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_middleName.Location = new System.Drawing.Point(680, 38);
            this.lbl_middleName.Name = "lbl_middleName";
            this.lbl_middleName.Size = new System.Drawing.Size(93, 17);
            this.lbl_middleName.TabIndex = 59;
            this.lbl_middleName.Text = "Middle Name";
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title.Location = new System.Drawing.Point(173, 37);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(36, 17);
            this.lbl_title.TabIndex = 58;
            this.lbl_title.Text = "Title";
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_email.Location = new System.Drawing.Point(403, 92);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(45, 17);
            this.lbl_email.TabIndex = 57;
            this.lbl_email.Text = "Email";
            // 
            // txt_email
            // 
            this.txt_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_email.Location = new System.Drawing.Point(406, 111);
            this.txt_email.Name = "txt_email";
            this.txt_email.ReadOnly = true;
            this.txt_email.Size = new System.Drawing.Size(222, 21);
            this.txt_email.TabIndex = 56;
            // 
            // lbl_contactDetails
            // 
            this.lbl_contactDetails.AutoSize = true;
            this.lbl_contactDetails.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_contactDetails.Location = new System.Drawing.Point(173, 92);
            this.lbl_contactDetails.Name = "lbl_contactDetails";
            this.lbl_contactDetails.Size = new System.Drawing.Size(106, 17);
            this.lbl_contactDetails.TabIndex = 55;
            this.lbl_contactDetails.Text = "Contact Details";
            // 
            // txt_contactDetails
            // 
            this.txt_contactDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_contactDetails.Location = new System.Drawing.Point(176, 111);
            this.txt_contactDetails.Name = "txt_contactDetails";
            this.txt_contactDetails.ReadOnly = true;
            this.txt_contactDetails.Size = new System.Drawing.Size(224, 21);
            this.txt_contactDetails.TabIndex = 54;
            // 
            // lbl_last_name
            // 
            this.lbl_last_name.AutoSize = true;
            this.lbl_last_name.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_last_name.Location = new System.Drawing.Point(261, 38);
            this.lbl_last_name.Name = "lbl_last_name";
            this.lbl_last_name.Size = new System.Drawing.Size(76, 17);
            this.lbl_last_name.TabIndex = 53;
            this.lbl_last_name.Text = "Last Name";
            // 
            // lbl_showRates
            // 
            this.lbl_showRates.AutoSize = true;
            this.lbl_showRates.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_showRates.Location = new System.Drawing.Point(806, 263);
            this.lbl_showRates.Name = "lbl_showRates";
            this.lbl_showRates.Size = new System.Drawing.Size(94, 17);
            this.lbl_showRates.TabIndex = 16;
            this.lbl_showRates.Text = "Show More +";
            this.lbl_showRates.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_showRates.Click += new System.EventHandler(this.lbl_showRates_Click);
            // 
            // txt_firstname
            // 
            this.txt_firstname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_firstname.Location = new System.Drawing.Point(454, 55);
            this.txt_firstname.Name = "txt_firstname";
            this.txt_firstname.ReadOnly = true;
            this.txt_firstname.Size = new System.Drawing.Size(220, 21);
            this.txt_firstname.TabIndex = 12;
            // 
            // cbox_title
            // 
            this.cbox_title.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbox_title.Enabled = false;
            this.cbox_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_title.FormattingEnabled = true;
            this.cbox_title.Location = new System.Drawing.Point(176, 54);
            this.cbox_title.Name = "cbox_title";
            this.cbox_title.Size = new System.Drawing.Size(78, 23);
            this.cbox_title.TabIndex = 11;
            // 
            // lbl_firstname
            // 
            this.lbl_firstname.AutoSize = true;
            this.lbl_firstname.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_firstname.Location = new System.Drawing.Point(451, 38);
            this.lbl_firstname.Name = "lbl_firstname";
            this.lbl_firstname.Size = new System.Drawing.Size(78, 17);
            this.lbl_firstname.TabIndex = 10;
            this.lbl_firstname.Text = "First Name";
            // 
            // txt_lastname
            // 
            this.txt_lastname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_lastname.Location = new System.Drawing.Point(264, 55);
            this.txt_lastname.Name = "txt_lastname";
            this.txt_lastname.ReadOnly = true;
            this.txt_lastname.Size = new System.Drawing.Size(178, 21);
            this.txt_lastname.TabIndex = 9;
            // 
            // lbl_modeOfPayment
            // 
            this.lbl_modeOfPayment.AutoSize = true;
            this.lbl_modeOfPayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_modeOfPayment.Location = new System.Drawing.Point(65, 49);
            this.lbl_modeOfPayment.Name = "lbl_modeOfPayment";
            this.lbl_modeOfPayment.Size = new System.Drawing.Size(144, 19);
            this.lbl_modeOfPayment.TabIndex = 48;
            this.lbl_modeOfPayment.Text = "Mode Of Payment:";
            // 
            // rbtn_partialCredit
            // 
            this.rbtn_partialCredit.AutoSize = true;
            this.rbtn_partialCredit.Enabled = false;
            this.rbtn_partialCredit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_partialCredit.Location = new System.Drawing.Point(435, 47);
            this.rbtn_partialCredit.Name = "rbtn_partialCredit";
            this.rbtn_partialCredit.Size = new System.Drawing.Size(111, 21);
            this.rbtn_partialCredit.TabIndex = 47;
            this.rbtn_partialCredit.Text = "Partial Credit";
            this.rbtn_partialCredit.UseVisualStyleBackColor = true;
            // 
            // rbtn_credit
            // 
            this.rbtn_credit.AutoSize = true;
            this.rbtn_credit.Enabled = false;
            this.rbtn_credit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_credit.Location = new System.Drawing.Point(324, 47);
            this.rbtn_credit.Name = "rbtn_credit";
            this.rbtn_credit.Size = new System.Drawing.Size(65, 21);
            this.rbtn_credit.TabIndex = 46;
            this.rbtn_credit.Text = "Credit";
            this.rbtn_credit.UseVisualStyleBackColor = true;
            // 
            // rbtn_cash
            // 
            this.rbtn_cash.AutoSize = true;
            this.rbtn_cash.Checked = true;
            this.rbtn_cash.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_cash.Location = new System.Drawing.Point(222, 47);
            this.rbtn_cash.Name = "rbtn_cash";
            this.rbtn_cash.Size = new System.Drawing.Size(59, 21);
            this.rbtn_cash.TabIndex = 45;
            this.rbtn_cash.TabStop = true;
            this.rbtn_cash.Text = "Cash";
            this.rbtn_cash.UseVisualStyleBackColor = true;
            // 
            // txt_folioNumber
            // 
            this.txt_folioNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_folioNumber.Location = new System.Drawing.Point(1001, 186);
            this.txt_folioNumber.Name = "txt_folioNumber";
            this.txt_folioNumber.Size = new System.Drawing.Size(343, 23);
            this.txt_folioNumber.TabIndex = 21;
            // 
            // lbl_folioNumber
            // 
            this.lbl_folioNumber.AutoSize = true;
            this.lbl_folioNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_folioNumber.Location = new System.Drawing.Point(906, 190);
            this.lbl_folioNumber.Name = "lbl_folioNumber";
            this.lbl_folioNumber.Size = new System.Drawing.Size(72, 19);
            this.lbl_folioNumber.TabIndex = 20;
            this.lbl_folioNumber.Text = "Folio No.";
            // 
            // txt_billTo
            // 
            this.txt_billTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_billTo.Location = new System.Drawing.Point(1001, 148);
            this.txt_billTo.Name = "txt_billTo";
            this.txt_billTo.Size = new System.Drawing.Size(343, 23);
            this.txt_billTo.TabIndex = 17;
            // 
            // lbl_billTo
            // 
            this.lbl_billTo.AutoSize = true;
            this.lbl_billTo.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_billTo.Location = new System.Drawing.Point(906, 153);
            this.lbl_billTo.Name = "lbl_billTo";
            this.lbl_billTo.Size = new System.Drawing.Size(55, 19);
            this.lbl_billTo.TabIndex = 16;
            this.lbl_billTo.Text = "Bill To";
            // 
            // txt_expire
            // 
            this.txt_expire.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_expire.Location = new System.Drawing.Point(497, 86);
            this.txt_expire.Name = "txt_expire";
            this.txt_expire.Size = new System.Drawing.Size(120, 23);
            this.txt_expire.TabIndex = 15;
            // 
            // txt_cardNumber
            // 
            this.txt_cardNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cardNumber.Location = new System.Drawing.Point(106, 83);
            this.txt_cardNumber.Name = "txt_cardNumber";
            this.txt_cardNumber.Size = new System.Drawing.Size(287, 23);
            this.txt_cardNumber.TabIndex = 13;
            // 
            // cmb_type
            // 
            this.cmb_type.FormattingEnabled = true;
            this.cmb_type.Location = new System.Drawing.Point(106, 44);
            this.cmb_type.Name = "cmb_type";
            this.cmb_type.Size = new System.Drawing.Size(287, 21);
            this.cmb_type.TabIndex = 10;
            // 
            // pcb_reloadGuestFolio
            // 
            this.pcb_reloadGuestFolio.BackColor = System.Drawing.Color.Silver;
            this.pcb_reloadGuestFolio.BackgroundImage = global::maxxis.Properties.Resources.reload;
            this.pcb_reloadGuestFolio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_reloadGuestFolio.Location = new System.Drawing.Point(1395, 182);
            this.pcb_reloadGuestFolio.Name = "pcb_reloadGuestFolio";
            this.pcb_reloadGuestFolio.Size = new System.Drawing.Size(33, 31);
            this.pcb_reloadGuestFolio.TabIndex = 23;
            this.pcb_reloadGuestFolio.TabStop = false;
            // 
            // pcb_searchFolioNumber
            // 
            this.pcb_searchFolioNumber.BackColor = System.Drawing.Color.Silver;
            this.pcb_searchFolioNumber.BackgroundImage = global::maxxis.Properties.Resources.search;
            this.pcb_searchFolioNumber.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_searchFolioNumber.Location = new System.Drawing.Point(1350, 182);
            this.pcb_searchFolioNumber.Name = "pcb_searchFolioNumber";
            this.pcb_searchFolioNumber.Size = new System.Drawing.Size(33, 31);
            this.pcb_searchFolioNumber.TabIndex = 22;
            this.pcb_searchFolioNumber.TabStop = false;
            // 
            // pcb_addBillTo
            // 
            this.pcb_addBillTo.BackColor = System.Drawing.Color.Silver;
            this.pcb_addBillTo.BackgroundImage = global::maxxis.Properties.Resources.add;
            this.pcb_addBillTo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_addBillTo.Location = new System.Drawing.Point(1395, 141);
            this.pcb_addBillTo.Name = "pcb_addBillTo";
            this.pcb_addBillTo.Size = new System.Drawing.Size(33, 31);
            this.pcb_addBillTo.TabIndex = 19;
            this.pcb_addBillTo.TabStop = false;
            // 
            // pcb_searchBillTo
            // 
            this.pcb_searchBillTo.BackColor = System.Drawing.Color.Silver;
            this.pcb_searchBillTo.BackgroundImage = global::maxxis.Properties.Resources.search;
            this.pcb_searchBillTo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_searchBillTo.Location = new System.Drawing.Point(1350, 141);
            this.pcb_searchBillTo.Name = "pcb_searchBillTo";
            this.pcb_searchBillTo.Size = new System.Drawing.Size(33, 31);
            this.pcb_searchBillTo.TabIndex = 18;
            this.pcb_searchBillTo.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.Controls.Add(this.panel29);
            this.panel2.Controls.Add(this.panel22);
            this.panel2.Controls.Add(this.panel28);
            this.panel2.Controls.Add(this.panel19);
            this.panel2.Controls.Add(this.panel21);
            this.panel2.Controls.Add(this.panel17);
            this.panel2.Controls.Add(this.panel16);
            this.panel2.Controls.Add(this.panel15);
            this.panel2.Controls.Add(this.panel14);
            this.panel2.Controls.Add(this.panel13);
            this.panel2.Controls.Add(this.panel12);
            this.panel2.Controls.Add(this.panel11);
            this.panel2.Controls.Add(this.panel10);
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(179, 35);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1121, 695);
            this.panel2.TabIndex = 4;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel29.Controls.Add(this.btn_GoBack);
            this.panel29.Controls.Add(this.btn_checkOut);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel29.Location = new System.Drawing.Point(0, 2806);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(905, 90);
            this.panel29.TabIndex = 24;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.Silver;
            this.panel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel22.Controls.Add(this.txt_balance);
            this.panel22.Controls.Add(this.txt_totalPayment);
            this.panel22.Controls.Add(this.txt_totalDiscount);
            this.panel22.Controls.Add(this.txt_subTotal);
            this.panel22.Controls.Add(this.label18);
            this.panel22.Controls.Add(this.label9);
            this.panel22.Controls.Add(this.label10);
            this.panel22.Controls.Add(this.label11);
            this.panel22.Controls.Add(this.txt_otherCharges);
            this.panel22.Controls.Add(this.label20);
            this.panel22.Controls.Add(this.txt_referenceNumber);
            this.panel22.Controls.Add(this.label19);
            this.panel22.Controls.Add(this.txt_fee_specialArrangements);
            this.panel22.Controls.Add(this.lbl_specialArrangements);
            this.panel22.Controls.Add(this.label17);
            this.panel22.Controls.Add(this.label16);
            this.panel22.Controls.Add(this.label15);
            this.panel22.Controls.Add(this.label14);
            this.panel22.Controls.Add(this.label13);
            this.panel22.Controls.Add(this.label12);
            this.panel22.Controls.Add(this.txt_fee_additionalAdultsAndChildren);
            this.panel22.Controls.Add(this.txt_fee_totalNumberOfChildren);
            this.panel22.Controls.Add(this.txt_fee_totalNumberOfAdults);
            this.panel22.Controls.Add(this.lbl_additionalCompanionChildren);
            this.panel22.Controls.Add(this.lbl_additionalCompanionAdults);
            this.panel22.Controls.Add(this.total_numberOfChildren);
            this.panel22.Controls.Add(this.txt_totalNumberOfAdults);
            this.panel22.Controls.Add(this.label21);
            this.panel22.Controls.Add(this.label22);
            this.panel22.Controls.Add(this.label23);
            this.panel22.Controls.Add(this.panel27);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(0, 2301);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(905, 505);
            this.panel22.TabIndex = 23;
            // 
            // txt_balance
            // 
            this.txt_balance.BackColor = System.Drawing.Color.Silver;
            this.txt_balance.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_balance.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_balance.Location = new System.Drawing.Point(748, 478);
            this.txt_balance.Name = "txt_balance";
            this.txt_balance.ReadOnly = true;
            this.txt_balance.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_balance.Size = new System.Drawing.Size(145, 16);
            this.txt_balance.TabIndex = 157;
            this.txt_balance.Text = "Php 0.00";
            // 
            // txt_totalPayment
            // 
            this.txt_totalPayment.BackColor = System.Drawing.Color.Silver;
            this.txt_totalPayment.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_totalPayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_totalPayment.Location = new System.Drawing.Point(748, 456);
            this.txt_totalPayment.Name = "txt_totalPayment";
            this.txt_totalPayment.ReadOnly = true;
            this.txt_totalPayment.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_totalPayment.Size = new System.Drawing.Size(145, 16);
            this.txt_totalPayment.TabIndex = 156;
            this.txt_totalPayment.Text = "Php 0.00";
            // 
            // txt_totalDiscount
            // 
            this.txt_totalDiscount.BackColor = System.Drawing.Color.Silver;
            this.txt_totalDiscount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_totalDiscount.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_totalDiscount.Location = new System.Drawing.Point(748, 431);
            this.txt_totalDiscount.Name = "txt_totalDiscount";
            this.txt_totalDiscount.ReadOnly = true;
            this.txt_totalDiscount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_totalDiscount.Size = new System.Drawing.Size(145, 16);
            this.txt_totalDiscount.TabIndex = 155;
            this.txt_totalDiscount.Text = "Php 0.00";
            // 
            // txt_subTotal
            // 
            this.txt_subTotal.BackColor = System.Drawing.Color.Silver;
            this.txt_subTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_subTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_subTotal.Location = new System.Drawing.Point(748, 408);
            this.txt_subTotal.Name = "txt_subTotal";
            this.txt_subTotal.ReadOnly = true;
            this.txt_subTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_subTotal.Size = new System.Drawing.Size(145, 16);
            this.txt_subTotal.TabIndex = 154;
            this.txt_subTotal.Text = "Php 0.00";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(6, 478);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 17);
            this.label18.TabIndex = 153;
            this.label18.Text = "Balance";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 456);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 17);
            this.label9.TabIndex = 152;
            this.label9.Text = "Total Payment";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 431);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 17);
            this.label10.TabIndex = 151;
            this.label10.Text = "Total Discount";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 407);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 17);
            this.label11.TabIndex = 150;
            this.label11.Text = "Sub-Total";
            // 
            // txt_otherCharges
            // 
            this.txt_otherCharges.BackColor = System.Drawing.Color.Silver;
            this.txt_otherCharges.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_otherCharges.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_otherCharges.Location = new System.Drawing.Point(748, 356);
            this.txt_otherCharges.Name = "txt_otherCharges";
            this.txt_otherCharges.ReadOnly = true;
            this.txt_otherCharges.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_otherCharges.Size = new System.Drawing.Size(145, 16);
            this.txt_otherCharges.TabIndex = 149;
            this.txt_otherCharges.Text = "Php 0.00";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(6, 355);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(224, 17);
            this.label20.TabIndex = 148;
            this.label20.Text = "Other charges (items/services):";
            // 
            // txt_referenceNumber
            // 
            this.txt_referenceNumber.BackColor = System.Drawing.Color.Silver;
            this.txt_referenceNumber.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_referenceNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_referenceNumber.Location = new System.Drawing.Point(226, 318);
            this.txt_referenceNumber.Name = "txt_referenceNumber";
            this.txt_referenceNumber.Size = new System.Drawing.Size(100, 16);
            this.txt_referenceNumber.TabIndex = 147;
            this.txt_referenceNumber.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(6, 317);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(214, 17);
            this.label19.TabIndex = 146;
            this.label19.Text = "Reservation reference number:";
            // 
            // txt_fee_specialArrangements
            // 
            this.txt_fee_specialArrangements.BackColor = System.Drawing.Color.Silver;
            this.txt_fee_specialArrangements.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_fee_specialArrangements.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_fee_specialArrangements.Location = new System.Drawing.Point(748, 280);
            this.txt_fee_specialArrangements.Name = "txt_fee_specialArrangements";
            this.txt_fee_specialArrangements.ReadOnly = true;
            this.txt_fee_specialArrangements.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_fee_specialArrangements.Size = new System.Drawing.Size(145, 16);
            this.txt_fee_specialArrangements.TabIndex = 145;
            this.txt_fee_specialArrangements.Text = "Php 0.00";
            // 
            // lbl_specialArrangements
            // 
            this.lbl_specialArrangements.AutoSize = true;
            this.lbl_specialArrangements.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_specialArrangements.Location = new System.Drawing.Point(6, 279);
            this.lbl_specialArrangements.Name = "lbl_specialArrangements";
            this.lbl_specialArrangements.Size = new System.Drawing.Size(42, 17);
            this.lbl_specialArrangements.TabIndex = 144;
            this.lbl_specialArrangements.Text = "None";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 258);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(161, 17);
            this.label17.TabIndex = 143;
            this.label17.Text = "Special Arrangements:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label16.Location = new System.Drawing.Point(20, 226);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(687, 17);
            this.label16.TabIndex = 142;
            this.label16.Text = "* A perfect cup of coffee or tea at a touch of a button with express coffee capsu" +
    "le compliments of the hotel.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label15.Location = new System.Drawing.Point(20, 205);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(478, 17);
            this.label15.TabIndex = 141;
            this.label15.Text = "* Complimentary use of hotel facilities including the room deck whirlpool.";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label14.Location = new System.Drawing.Point(20, 184);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(409, 17);
            this.label14.TabIndex = 140;
            this.label14.Text = "* Complimentary high-speed WIFI access throughout your stay.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label13.Location = new System.Drawing.Point(20, 164);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(602, 17);
            this.label13.TabIndex = 139;
            this.label13.Text = "* Enjoy a sumptuous set breakfast at your leisure from 7:00AM to 1:00PM at Luna R" +
    "estaurant.";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 141);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 17);
            this.label12.TabIndex = 138;
            this.label12.Text = "Inclusions:";
            // 
            // txt_fee_additionalAdultsAndChildren
            // 
            this.txt_fee_additionalAdultsAndChildren.BackColor = System.Drawing.Color.Silver;
            this.txt_fee_additionalAdultsAndChildren.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_fee_additionalAdultsAndChildren.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_fee_additionalAdultsAndChildren.Location = new System.Drawing.Point(748, 97);
            this.txt_fee_additionalAdultsAndChildren.Name = "txt_fee_additionalAdultsAndChildren";
            this.txt_fee_additionalAdultsAndChildren.ReadOnly = true;
            this.txt_fee_additionalAdultsAndChildren.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_fee_additionalAdultsAndChildren.Size = new System.Drawing.Size(145, 16);
            this.txt_fee_additionalAdultsAndChildren.TabIndex = 137;
            this.txt_fee_additionalAdultsAndChildren.Text = "Php 0.00";
            // 
            // txt_fee_totalNumberOfChildren
            // 
            this.txt_fee_totalNumberOfChildren.BackColor = System.Drawing.Color.Silver;
            this.txt_fee_totalNumberOfChildren.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_fee_totalNumberOfChildren.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_fee_totalNumberOfChildren.Location = new System.Drawing.Point(748, 71);
            this.txt_fee_totalNumberOfChildren.Name = "txt_fee_totalNumberOfChildren";
            this.txt_fee_totalNumberOfChildren.ReadOnly = true;
            this.txt_fee_totalNumberOfChildren.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_fee_totalNumberOfChildren.Size = new System.Drawing.Size(145, 16);
            this.txt_fee_totalNumberOfChildren.TabIndex = 136;
            this.txt_fee_totalNumberOfChildren.Text = "Php 0.00";
            // 
            // txt_fee_totalNumberOfAdults
            // 
            this.txt_fee_totalNumberOfAdults.BackColor = System.Drawing.Color.Silver;
            this.txt_fee_totalNumberOfAdults.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_fee_totalNumberOfAdults.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_fee_totalNumberOfAdults.Location = new System.Drawing.Point(748, 42);
            this.txt_fee_totalNumberOfAdults.Name = "txt_fee_totalNumberOfAdults";
            this.txt_fee_totalNumberOfAdults.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_fee_totalNumberOfAdults.Size = new System.Drawing.Size(145, 16);
            this.txt_fee_totalNumberOfAdults.TabIndex = 135;
            this.txt_fee_totalNumberOfAdults.Text = "Php 0.00";
            // 
            // lbl_additionalCompanionChildren
            // 
            this.lbl_additionalCompanionChildren.AutoSize = true;
            this.lbl_additionalCompanionChildren.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_additionalCompanionChildren.Location = new System.Drawing.Point(387, 97);
            this.lbl_additionalCompanionChildren.Name = "lbl_additionalCompanionChildren";
            this.lbl_additionalCompanionChildren.Size = new System.Drawing.Size(94, 17);
            this.lbl_additionalCompanionChildren.TabIndex = 134;
            this.lbl_additionalCompanionChildren.Text = "Child/ren (0)";
            // 
            // lbl_additionalCompanionAdults
            // 
            this.lbl_additionalCompanionAdults.AutoSize = true;
            this.lbl_additionalCompanionAdults.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_additionalCompanionAdults.Location = new System.Drawing.Point(306, 97);
            this.lbl_additionalCompanionAdults.Name = "lbl_additionalCompanionAdults";
            this.lbl_additionalCompanionAdults.Size = new System.Drawing.Size(81, 17);
            this.lbl_additionalCompanionAdults.TabIndex = 133;
            this.lbl_additionalCompanionAdults.Text = "Adult/s (0)";
            // 
            // total_numberOfChildren
            // 
            this.total_numberOfChildren.BackColor = System.Drawing.Color.Silver;
            this.total_numberOfChildren.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.total_numberOfChildren.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total_numberOfChildren.Location = new System.Drawing.Point(332, 72);
            this.total_numberOfChildren.Name = "total_numberOfChildren";
            this.total_numberOfChildren.Size = new System.Drawing.Size(100, 16);
            this.total_numberOfChildren.TabIndex = 132;
            this.total_numberOfChildren.Text = "0";
            // 
            // txt_totalNumberOfAdults
            // 
            this.txt_totalNumberOfAdults.BackColor = System.Drawing.Color.Silver;
            this.txt_totalNumberOfAdults.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_totalNumberOfAdults.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_totalNumberOfAdults.Location = new System.Drawing.Point(297, 43);
            this.txt_totalNumberOfAdults.Name = "txt_totalNumberOfAdults";
            this.txt_totalNumberOfAdults.Size = new System.Drawing.Size(100, 16);
            this.txt_totalNumberOfAdults.TabIndex = 131;
            this.txt_totalNumberOfAdults.Text = "0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(6, 97);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(295, 17);
            this.label21.TabIndex = 130;
            this.label21.Text = "Additional companion (without breakfast):";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(6, 71);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(320, 17);
            this.label22.TabIndex = 129;
            this.label22.Text = "Total number of child/ren (without breakfast):";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(6, 42);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(285, 17);
            this.label23.TabIndex = 128;
            this.label23.Text = "Total number of adult/s (with breakfast):";
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel27.Controls.Add(this.label8);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel27.Location = new System.Drawing.Point(0, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(903, 29);
            this.panel27.TabIndex = 47;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label8.Location = new System.Drawing.Point(4, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Folio Summary";
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel28.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel28.Location = new System.Drawing.Point(0, 2281);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(905, 20);
            this.panel28.TabIndex = 22;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Silver;
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.dgv_payments);
            this.panel19.Controls.Add(this.panel20);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 2074);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(905, 207);
            this.panel19.TabIndex = 21;
            // 
            // dgv_payments
            // 
            this.dgv_payments.AllowUserToAddRows = false;
            this.dgv_payments.AllowUserToDeleteRows = false;
            this.dgv_payments.BackgroundColor = System.Drawing.Color.Silver;
            this.dgv_payments.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_payments.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv_payments.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_payments.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgv_payments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_payments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.dgv_payments.EnableHeadersVisualStyles = false;
            this.dgv_payments.Location = new System.Drawing.Point(23, 48);
            this.dgv_payments.Name = "dgv_payments";
            this.dgv_payments.ReadOnly = true;
            this.dgv_payments.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_payments.RowHeadersVisible = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_payments.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgv_payments.Size = new System.Drawing.Size(856, 136);
            this.dgv_payments.TabIndex = 48;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 250;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 350;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Discount";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 250;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel20.Controls.Add(this.label7);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(903, 29);
            this.panel20.TabIndex = 47;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label7.Location = new System.Drawing.Point(4, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Payments";
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 2054);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(905, 20);
            this.panel21.TabIndex = 20;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Silver;
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.dgv_discount);
            this.panel17.Controls.Add(this.panel18);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 1860);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(905, 194);
            this.panel17.TabIndex = 19;
            // 
            // dgv_discount
            // 
            this.dgv_discount.AllowUserToAddRows = false;
            this.dgv_discount.AllowUserToDeleteRows = false;
            this.dgv_discount.BackgroundColor = System.Drawing.Color.Silver;
            this.dgv_discount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_discount.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv_discount.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_discount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgv_discount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_discount.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_Date_discountDGV,
            this.col_Type_discountDGV,
            this.col_Discount_discountDGV});
            this.dgv_discount.EnableHeadersVisualStyles = false;
            this.dgv_discount.Location = new System.Drawing.Point(23, 48);
            this.dgv_discount.Name = "dgv_discount";
            this.dgv_discount.ReadOnly = true;
            this.dgv_discount.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_discount.RowHeadersVisible = false;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_discount.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgv_discount.Size = new System.Drawing.Size(856, 123);
            this.dgv_discount.TabIndex = 48;
            // 
            // col_Date_discountDGV
            // 
            this.col_Date_discountDGV.HeaderText = "Date";
            this.col_Date_discountDGV.Name = "col_Date_discountDGV";
            this.col_Date_discountDGV.ReadOnly = true;
            this.col_Date_discountDGV.Width = 200;
            // 
            // col_Type_discountDGV
            // 
            this.col_Type_discountDGV.HeaderText = "Type";
            this.col_Type_discountDGV.Name = "col_Type_discountDGV";
            this.col_Type_discountDGV.ReadOnly = true;
            this.col_Type_discountDGV.Width = 350;
            // 
            // col_Discount_discountDGV
            // 
            this.col_Discount_discountDGV.HeaderText = "Discount";
            this.col_Discount_discountDGV.Name = "col_Discount_discountDGV";
            this.col_Discount_discountDGV.ReadOnly = true;
            this.col_Discount_discountDGV.Width = 250;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel18.Controls.Add(this.label6);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(903, 29);
            this.panel18.TabIndex = 47;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label6.Location = new System.Drawing.Point(4, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Discounts";
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 1840);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(905, 20);
            this.panel16.TabIndex = 18;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.dgv_billingSummary);
            this.panel15.Controls.Add(this.panel26);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 1622);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(905, 218);
            this.panel15.TabIndex = 17;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel26.Controls.Add(this.label5);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel26.Location = new System.Drawing.Point(0, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(903, 29);
            this.panel26.TabIndex = 46;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label5.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label5.Location = new System.Drawing.Point(4, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Billing Summary";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 1602);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(905, 20);
            this.panel14.TabIndex = 16;
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.btn_edit);
            this.panel13.Controls.Add(this.txt_guestRemarks);
            this.panel13.Controls.Add(this.panel25);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 1482);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(905, 120);
            this.panel13.TabIndex = 15;
            // 
            // btn_edit
            // 
            this.btn_edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_edit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_edit.FlatAppearance.BorderSize = 0;
            this.btn_edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_edit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_edit.ForeColor = System.Drawing.Color.White;
            this.btn_edit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_edit.Location = new System.Drawing.Point(752, 35);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(146, 27);
            this.btn_edit.TabIndex = 46;
            this.btn_edit.Text = "EDIT";
            this.btn_edit.UseVisualStyleBackColor = false;
            // 
            // txt_guestRemarks
            // 
            this.txt_guestRemarks.BackColor = System.Drawing.Color.Silver;
            this.txt_guestRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_guestRemarks.Location = new System.Drawing.Point(9, 35);
            this.txt_guestRemarks.Multiline = true;
            this.txt_guestRemarks.Name = "txt_guestRemarks";
            this.txt_guestRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_guestRemarks.Size = new System.Drawing.Size(737, 75);
            this.txt_guestRemarks.TabIndex = 0;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel25.Controls.Add(this.label4);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel25.Location = new System.Drawing.Point(0, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(903, 29);
            this.panel25.TabIndex = 45;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label4.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label4.Location = new System.Drawing.Point(4, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Guest Remarks";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 1462);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(905, 20);
            this.panel12.TabIndex = 14;
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.panel24);
            this.panel11.Controls.Add(this.panel23);
            this.panel11.Controls.Add(this.panel3);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 1114);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(905, 348);
            this.panel11.TabIndex = 13;
            // 
            // panel24
            // 
            this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel24.Controls.Add(this.txt_purposeOfVisit);
            this.panel24.Controls.Add(this.dtp_timeOfArrival);
            this.panel24.Controls.Add(this.lbl_departureTransportation);
            this.panel24.Controls.Add(this.txt_departureTransportation);
            this.panel24.Controls.Add(this.lbl_goingTo);
            this.panel24.Controls.Add(this.lbl_dateOfArrival);
            this.panel24.Controls.Add(this.dtp_dateOfArrival);
            this.panel24.Controls.Add(this.txt_goingTo);
            this.panel24.Controls.Add(this.lbl_purposeOfVisit);
            this.panel24.Controls.Add(this.lbl_timeOfArrival);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(0, 187);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(903, 160);
            this.panel24.TabIndex = 46;
            // 
            // panel23
            // 
            this.panel23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel23.Controls.Add(this.txt_arrivedForm);
            this.panel23.Controls.Add(this.lbl_visaNumber);
            this.panel23.Controls.Add(this.lbl_serialNumber);
            this.panel23.Controls.Add(this.lbl_arrivedForm);
            this.panel23.Controls.Add(this.txt_visaIssuePlace);
            this.panel23.Controls.Add(this.txt_serialNumber);
            this.panel23.Controls.Add(this.lbl_vissaIssuePlace);
            this.panel23.Controls.Add(this.txt_visaNumber);
            this.panel23.Controls.Add(this.dtp_visaDate);
            this.panel23.Controls.Add(this.lbl_visaDate);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 29);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(903, 158);
            this.panel23.TabIndex = 45;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel3.Controls.Add(this.label3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(903, 29);
            this.panel3.TabIndex = 44;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label3.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label3.Location = new System.Drawing.Point(4, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Visa Information";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 1094);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(905, 20);
            this.panel10.TabIndex = 12;
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.txt_child);
            this.panel9.Controls.Add(this.txt_adults);
            this.panel9.Controls.Add(this.txt_noofnights);
            this.panel9.Controls.Add(this.dtp_departureTime);
            this.panel9.Controls.Add(this.stayInformation_panel);
            this.panel9.Controls.Add(this.dtp_arrivalTime);
            this.panel9.Controls.Add(this.lbl_arrival);
            this.panel9.Controls.Add(this.dtp_departureDate);
            this.panel9.Controls.Add(this.lbl_departure);
            this.panel9.Controls.Add(this.dtp_arrivalDate);
            this.panel9.Controls.Add(this.lbl_numberOfNights);
            this.panel9.Controls.Add(this.lbl_adults);
            this.panel9.Controls.Add(this.lbl_child);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 932);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(905, 162);
            this.panel9.TabIndex = 11;
            // 
            // txt_child
            // 
            this.txt_child.Location = new System.Drawing.Point(371, 124);
            this.txt_child.Name = "txt_child";
            this.txt_child.ReadOnly = true;
            this.txt_child.Size = new System.Drawing.Size(58, 20);
            this.txt_child.TabIndex = 46;
            // 
            // txt_adults
            // 
            this.txt_adults.Location = new System.Drawing.Point(258, 125);
            this.txt_adults.Name = "txt_adults";
            this.txt_adults.ReadOnly = true;
            this.txt_adults.Size = new System.Drawing.Size(58, 20);
            this.txt_adults.TabIndex = 45;
            // 
            // txt_noofnights
            // 
            this.txt_noofnights.Location = new System.Drawing.Point(137, 125);
            this.txt_noofnights.Name = "txt_noofnights";
            this.txt_noofnights.ReadOnly = true;
            this.txt_noofnights.Size = new System.Drawing.Size(58, 20);
            this.txt_noofnights.TabIndex = 44;
            // 
            // stayInformation_panel
            // 
            this.stayInformation_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.stayInformation_panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.stayInformation_panel.Controls.Add(this.lbl_stayInformation);
            this.stayInformation_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.stayInformation_panel.Location = new System.Drawing.Point(0, 0);
            this.stayInformation_panel.Name = "stayInformation_panel";
            this.stayInformation_panel.Size = new System.Drawing.Size(903, 29);
            this.stayInformation_panel.TabIndex = 43;
            // 
            // lbl_stayInformation
            // 
            this.lbl_stayInformation.AutoSize = true;
            this.lbl_stayInformation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_stayInformation.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_stayInformation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_stayInformation.Location = new System.Drawing.Point(13, 4);
            this.lbl_stayInformation.Name = "lbl_stayInformation";
            this.lbl_stayInformation.Size = new System.Drawing.Size(113, 17);
            this.lbl_stayInformation.TabIndex = 0;
            this.lbl_stayInformation.Text = "Stay Information";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 912);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(905, 20);
            this.panel8.TabIndex = 10;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Silver;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.panel34);
            this.panel6.Controls.Add(this.panel32);
            this.panel6.Controls.Add(this.panel30);
            this.panel6.Controls.Add(this.lbl_modeOfPayment);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.rbtn_partialCredit);
            this.panel6.Controls.Add(this.pcb_reloadGuestFolio);
            this.panel6.Controls.Add(this.rbtn_credit);
            this.panel6.Controls.Add(this.pcb_searchBillTo);
            this.panel6.Controls.Add(this.rbtn_cash);
            this.panel6.Controls.Add(this.pcb_addBillTo);
            this.panel6.Controls.Add(this.pcb_searchFolioNumber);
            this.panel6.Controls.Add(this.txt_folioNumber);
            this.panel6.Controls.Add(this.lbl_folioNumber);
            this.panel6.Controls.Add(this.txt_billTo);
            this.panel6.Controls.Add(this.lbl_billTo);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 313);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(905, 599);
            this.panel6.TabIndex = 9;
            // 
            // panel34
            // 
            this.panel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel34.Controls.Add(this.btn_apply);
            this.panel34.Controls.Add(this.txt_discountDescription);
            this.panel34.Controls.Add(this.lbl_discountDescription);
            this.panel34.Controls.Add(this.lbl_discountAmount);
            this.panel34.Controls.Add(this.txt_discountAmount);
            this.panel34.Controls.Add(this.panel35);
            this.panel34.Location = new System.Drawing.Point(41, 403);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(814, 165);
            this.panel34.TabIndex = 56;
            // 
            // btn_apply
            // 
            this.btn_apply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_apply.FlatAppearance.BorderSize = 0;
            this.btn_apply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_apply.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_apply.ForeColor = System.Drawing.Color.White;
            this.btn_apply.Location = new System.Drawing.Point(524, 101);
            this.btn_apply.Name = "btn_apply";
            this.btn_apply.Size = new System.Drawing.Size(139, 35);
            this.btn_apply.TabIndex = 17;
            this.btn_apply.Text = "APPLY";
            this.btn_apply.UseVisualStyleBackColor = false;
            // 
            // txt_discountDescription
            // 
            this.txt_discountDescription.Location = new System.Drawing.Point(149, 87);
            this.txt_discountDescription.Multiline = true;
            this.txt_discountDescription.Name = "txt_discountDescription";
            this.txt_discountDescription.Size = new System.Drawing.Size(347, 62);
            this.txt_discountDescription.TabIndex = 16;
            // 
            // lbl_discountDescription
            // 
            this.lbl_discountDescription.AutoSize = true;
            this.lbl_discountDescription.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_discountDescription.Location = new System.Drawing.Point(10, 108);
            this.lbl_discountDescription.Name = "lbl_discountDescription";
            this.lbl_discountDescription.Size = new System.Drawing.Size(116, 19);
            this.lbl_discountDescription.TabIndex = 15;
            this.lbl_discountDescription.Text = "Discount Desc.";
            // 
            // lbl_discountAmount
            // 
            this.lbl_discountAmount.AutoSize = true;
            this.lbl_discountAmount.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_discountAmount.Location = new System.Drawing.Point(10, 52);
            this.lbl_discountAmount.Name = "lbl_discountAmount";
            this.lbl_discountAmount.Size = new System.Drawing.Size(133, 19);
            this.lbl_discountAmount.TabIndex = 14;
            this.lbl_discountAmount.Text = "Discount Amount";
            // 
            // txt_discountAmount
            // 
            this.txt_discountAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_discountAmount.Location = new System.Drawing.Point(149, 51);
            this.txt_discountAmount.Name = "txt_discountAmount";
            this.txt_discountAmount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_discountAmount.Size = new System.Drawing.Size(242, 23);
            this.txt_discountAmount.TabIndex = 13;
            this.txt_discountAmount.Text = "0.00";
            // 
            // panel35
            // 
            this.panel35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel35.Controls.Add(this.lbl_discount);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel35.Location = new System.Drawing.Point(0, 0);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(812, 31);
            this.panel35.TabIndex = 12;
            // 
            // lbl_discount
            // 
            this.lbl_discount.AutoSize = true;
            this.lbl_discount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_discount.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_discount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_discount.Location = new System.Drawing.Point(11, 8);
            this.lbl_discount.Name = "lbl_discount";
            this.lbl_discount.Size = new System.Drawing.Size(68, 14);
            this.lbl_discount.TabIndex = 0;
            this.lbl_discount.Text = "DISCOUNT";
            // 
            // panel32
            // 
            this.panel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel32.Controls.Add(this.panel33);
            this.panel32.Controls.Add(this.lbl_type);
            this.panel32.Controls.Add(this.lbl_cardNumber);
            this.panel32.Controls.Add(this.lbl_expire);
            this.panel32.Controls.Add(this.txt_cvv);
            this.panel32.Controls.Add(this.lbl_cvv);
            this.panel32.Controls.Add(this.cmb_type);
            this.panel32.Controls.Add(this.txt_cardNumber);
            this.panel32.Controls.Add(this.txt_expire);
            this.panel32.Location = new System.Drawing.Point(42, 248);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(814, 127);
            this.panel32.TabIndex = 55;
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel33.Controls.Add(this.lbl_credit);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel33.Location = new System.Drawing.Point(0, 0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(812, 31);
            this.panel33.TabIndex = 11;
            // 
            // lbl_credit
            // 
            this.lbl_credit.AutoSize = true;
            this.lbl_credit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_credit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_credit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_credit.Location = new System.Drawing.Point(11, 8);
            this.lbl_credit.Name = "lbl_credit";
            this.lbl_credit.Size = new System.Drawing.Size(50, 14);
            this.lbl_credit.TabIndex = 0;
            this.lbl_credit.Text = "CREDIT";
            // 
            // lbl_type
            // 
            this.lbl_type.AutoSize = true;
            this.lbl_type.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_type.Location = new System.Drawing.Point(10, 43);
            this.lbl_type.Name = "lbl_type";
            this.lbl_type.Size = new System.Drawing.Size(44, 19);
            this.lbl_type.TabIndex = 28;
            this.lbl_type.Text = "Type";
            // 
            // lbl_cardNumber
            // 
            this.lbl_cardNumber.AutoSize = true;
            this.lbl_cardNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cardNumber.Location = new System.Drawing.Point(10, 87);
            this.lbl_cardNumber.Name = "lbl_cardNumber";
            this.lbl_cardNumber.Size = new System.Drawing.Size(75, 19);
            this.lbl_cardNumber.TabIndex = 30;
            this.lbl_cardNumber.Text = "Card No*";
            // 
            // lbl_expire
            // 
            this.lbl_expire.AutoSize = true;
            this.lbl_expire.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_expire.Location = new System.Drawing.Point(424, 84);
            this.lbl_expire.Name = "lbl_expire";
            this.lbl_expire.Size = new System.Drawing.Size(61, 19);
            this.lbl_expire.TabIndex = 32;
            this.lbl_expire.Text = "Expire*";
            // 
            // txt_cvv
            // 
            this.txt_cvv.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cvv.Location = new System.Drawing.Point(497, 42);
            this.txt_cvv.Name = "txt_cvv";
            this.txt_cvv.Size = new System.Drawing.Size(120, 23);
            this.txt_cvv.TabIndex = 35;
            // 
            // lbl_cvv
            // 
            this.lbl_cvv.AutoSize = true;
            this.lbl_cvv.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cvv.Location = new System.Drawing.Point(424, 44);
            this.lbl_cvv.Name = "lbl_cvv";
            this.lbl_cvv.Size = new System.Drawing.Size(46, 19);
            this.lbl_cvv.TabIndex = 34;
            this.lbl_cvv.Text = "CVV*";
            // 
            // panel30
            // 
            this.panel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel30.Controls.Add(this.txt_change);
            this.panel30.Controls.Add(this.txt_cashPayment);
            this.panel30.Controls.Add(this.lbl_cashPayment);
            this.panel30.Controls.Add(this.lbl_change);
            this.panel30.Controls.Add(this.txt_cashhanded);
            this.panel30.Controls.Add(this.lbl_cashHandedByGuest);
            this.panel30.Controls.Add(this.panel31);
            this.panel30.Location = new System.Drawing.Point(41, 80);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(816, 145);
            this.panel30.TabIndex = 54;
            // 
            // txt_change
            // 
            this.txt_change.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_change.Location = new System.Drawing.Point(197, 108);
            this.txt_change.Name = "txt_change";
            this.txt_change.ReadOnly = true;
            this.txt_change.Size = new System.Drawing.Size(226, 23);
            this.txt_change.TabIndex = 50;
            this.txt_change.Text = "0.00";
            this.txt_change.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_cashPayment
            // 
            this.txt_cashPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cashPayment.Location = new System.Drawing.Point(197, 41);
            this.txt_cashPayment.Name = "txt_cashPayment";
            this.txt_cashPayment.ReadOnly = true;
            this.txt_cashPayment.Size = new System.Drawing.Size(228, 23);
            this.txt_cashPayment.TabIndex = 52;
            this.txt_cashPayment.Text = "0.00";
            this.txt_cashPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbl_cashPayment
            // 
            this.lbl_cashPayment.AutoSize = true;
            this.lbl_cashPayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cashPayment.Location = new System.Drawing.Point(10, 45);
            this.lbl_cashPayment.Name = "lbl_cashPayment";
            this.lbl_cashPayment.Size = new System.Drawing.Size(115, 19);
            this.lbl_cashPayment.TabIndex = 51;
            this.lbl_cashPayment.Text = "Cash Payment";
            // 
            // lbl_change
            // 
            this.lbl_change.AutoSize = true;
            this.lbl_change.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_change.Location = new System.Drawing.Point(10, 112);
            this.lbl_change.Name = "lbl_change";
            this.lbl_change.Size = new System.Drawing.Size(65, 19);
            this.lbl_change.TabIndex = 49;
            this.lbl_change.Text = "Change";
            // 
            // txt_cashhanded
            // 
            this.txt_cashhanded.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cashhanded.Location = new System.Drawing.Point(197, 75);
            this.txt_cashhanded.Name = "txt_cashhanded";
            this.txt_cashhanded.Size = new System.Drawing.Size(226, 23);
            this.txt_cashhanded.TabIndex = 48;
            this.txt_cashhanded.Text = "0.00";
            this.txt_cashhanded.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_cashhanded.TextChanged += new System.EventHandler(this.txt_cashhanded_TextChanged);
            // 
            // lbl_cashHandedByGuest
            // 
            this.lbl_cashHandedByGuest.AutoSize = true;
            this.lbl_cashHandedByGuest.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cashHandedByGuest.Location = new System.Drawing.Point(10, 79);
            this.lbl_cashHandedByGuest.Name = "lbl_cashHandedByGuest";
            this.lbl_cashHandedByGuest.Size = new System.Drawing.Size(178, 19);
            this.lbl_cashHandedByGuest.TabIndex = 47;
            this.lbl_cashHandedByGuest.Text = "Cash Handed by Guest";
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel31.Controls.Add(this.label26);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel31.Location = new System.Drawing.Point(0, 0);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(814, 31);
            this.panel31.TabIndex = 11;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label26.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label26.Location = new System.Drawing.Point(11, 8);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(40, 14);
            this.label26.TabIndex = 0;
            this.label26.Text = "CASH";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label2);
            this.panel7.Controls.Add(this.txt_requiredPrePayment);
            this.panel7.Controls.Add(this.lbl_requiredPrePayment);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(903, 31);
            this.panel7.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label2.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label2.Location = new System.Drawing.Point(11, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Settlement Options";
            // 
            // txt_requiredPrePayment
            // 
            this.txt_requiredPrePayment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_requiredPrePayment.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_requiredPrePayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_requiredPrePayment.ForeColor = System.Drawing.Color.White;
            this.txt_requiredPrePayment.Location = new System.Drawing.Point(747, 6);
            this.txt_requiredPrePayment.Name = "txt_requiredPrePayment";
            this.txt_requiredPrePayment.ReadOnly = true;
            this.txt_requiredPrePayment.Size = new System.Drawing.Size(150, 17);
            this.txt_requiredPrePayment.TabIndex = 50;
            this.txt_requiredPrePayment.Text = "0.00";
            this.txt_requiredPrePayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbl_requiredPrePayment
            // 
            this.lbl_requiredPrePayment.AutoSize = true;
            this.lbl_requiredPrePayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_requiredPrePayment.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_requiredPrePayment.Location = new System.Drawing.Point(569, 5);
            this.lbl_requiredPrePayment.Name = "lbl_requiredPrePayment";
            this.lbl_requiredPrePayment.Size = new System.Drawing.Size(114, 19);
            this.lbl_requiredPrePayment.TabIndex = 49;
            this.lbl_requiredPrePayment.Text = "Total Payment";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 293);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(905, 20);
            this.panel5.TabIndex = 8;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Silver;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.btn_scanImage);
            this.panel4.Controls.Add(this.btn_attachImage);
            this.panel4.Controls.Add(this.pcb_image);
            this.panel4.Controls.Add(this.cmb_homeCountry);
            this.panel4.Controls.Add(this.settlementOption_panel);
            this.panel4.Controls.Add(this.lbl_homeCountry);
            this.panel4.Controls.Add(this.lbl_title);
            this.panel4.Controls.Add(this.lbl_companyAgency);
            this.panel4.Controls.Add(this.txt_lastname);
            this.panel4.Controls.Add(this.txt_companyAgency);
            this.panel4.Controls.Add(this.lbl_firstname);
            this.panel4.Controls.Add(this.lbl_province);
            this.panel4.Controls.Add(this.lbl_dateOfBirth);
            this.panel4.Controls.Add(this.txt_province);
            this.panel4.Controls.Add(this.cbox_title);
            this.panel4.Controls.Add(this.lbl_city);
            this.panel4.Controls.Add(this.dtb_dateOfBirth);
            this.panel4.Controls.Add(this.txt_city);
            this.panel4.Controls.Add(this.txt_firstname);
            this.panel4.Controls.Add(this.lbl_address);
            this.panel4.Controls.Add(this.txt_address);
            this.panel4.Controls.Add(this.lbl_email);
            this.panel4.Controls.Add(this.txt_email);
            this.panel4.Controls.Add(this.lbl_last_name);
            this.panel4.Controls.Add(this.lbl_contactDetails);
            this.panel4.Controls.Add(this.txt_contactDetails);
            this.panel4.Controls.Add(this.lbl_middleName);
            this.panel4.Controls.Add(this.lbl_showRates);
            this.panel4.Controls.Add(this.txt_middleName);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(905, 293);
            this.panel4.TabIndex = 7;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // btn_scanImage
            // 
            this.btn_scanImage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_scanImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_scanImage.FlatAppearance.BorderSize = 0;
            this.btn_scanImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_scanImage.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_scanImage.ForeColor = System.Drawing.Color.White;
            this.btn_scanImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_scanImage.Location = new System.Drawing.Point(26, 232);
            this.btn_scanImage.Name = "btn_scanImage";
            this.btn_scanImage.Size = new System.Drawing.Size(128, 32);
            this.btn_scanImage.TabIndex = 79;
            this.btn_scanImage.Text = "SCAN IMAGE";
            this.btn_scanImage.UseVisualStyleBackColor = false;
            // 
            // btn_attachImage
            // 
            this.btn_attachImage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_attachImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_attachImage.FlatAppearance.BorderSize = 0;
            this.btn_attachImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_attachImage.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_attachImage.ForeColor = System.Drawing.Color.White;
            this.btn_attachImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_attachImage.Location = new System.Drawing.Point(26, 186);
            this.btn_attachImage.Name = "btn_attachImage";
            this.btn_attachImage.Size = new System.Drawing.Size(128, 32);
            this.btn_attachImage.TabIndex = 78;
            this.btn_attachImage.Text = "ATTACH IMAGE";
            this.btn_attachImage.UseVisualStyleBackColor = false;
            // 
            // pcb_image
            // 
            this.pcb_image.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pcb_image.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcb_image.Location = new System.Drawing.Point(13, 37);
            this.pcb_image.Name = "pcb_image";
            this.pcb_image.Size = new System.Drawing.Size(154, 134);
            this.pcb_image.TabIndex = 77;
            this.pcb_image.TabStop = false;
            // 
            // settlementOption_panel
            // 
            this.settlementOption_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.settlementOption_panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.settlementOption_panel.Controls.Add(this.lbl_settlementOption);
            this.settlementOption_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.settlementOption_panel.Location = new System.Drawing.Point(0, 0);
            this.settlementOption_panel.Name = "settlementOption_panel";
            this.settlementOption_panel.Size = new System.Drawing.Size(903, 31);
            this.settlementOption_panel.TabIndex = 11;
            // 
            // lbl_settlementOption
            // 
            this.lbl_settlementOption.AutoSize = true;
            this.lbl_settlementOption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_settlementOption.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_settlementOption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_settlementOption.Location = new System.Drawing.Point(11, 8);
            this.lbl_settlementOption.Name = "lbl_settlementOption";
            this.lbl_settlementOption.Size = new System.Drawing.Size(115, 15);
            this.lbl_settlementOption.TabIndex = 0;
            this.lbl_settlementOption.Text = "Guest Information";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(905, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(199, 2896);
            this.panel1.TabIndex = 5;
            // 
            // Guest_Folio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.ClientSize = new System.Drawing.Size(1300, 730);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnl_guestFolio);
            this.Controls.Add(this.guestFolio_panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Guest_Folio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Guest_Folio";
            this.Load += new System.EventHandler(this.Guest_Folio_Load);
            this.guestFolio_panel.ResumeLayout(false);
            this.guestFolio_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_guestFolio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_billingSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_reloadGuestFolio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_searchFolioNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_addBillTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_searchBillTo)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_payments)).EndInit();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_discount)).EndInit();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.stayInformation_panel.ResumeLayout(false);
            this.stayInformation_panel.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panel35.PerformLayout();
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            this.panel33.ResumeLayout(false);
            this.panel33.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_image)).EndInit();
            this.settlementOption_panel.ResumeLayout(false);
            this.settlementOption_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel guestFolio_panel;
        private System.Windows.Forms.Label lbl_guestFolio;
        private System.Windows.Forms.PictureBox pcb_guestFolio;
        private System.Windows.Forms.Panel pnl_guestFolio;
        private System.Windows.Forms.TextBox txt_expire;
        private System.Windows.Forms.TextBox txt_cardNumber;
        private System.Windows.Forms.ComboBox cmb_type;
        private System.Windows.Forms.PictureBox pcb_addBillTo;
        private System.Windows.Forms.TextBox txt_billTo;
        private System.Windows.Forms.Label lbl_billTo;
        private System.Windows.Forms.PictureBox pcb_reloadGuestFolio;
        private System.Windows.Forms.PictureBox pcb_searchFolioNumber;
        private System.Windows.Forms.TextBox txt_folioNumber;
        private System.Windows.Forms.Label lbl_folioNumber;
        private System.Windows.Forms.PictureBox pcb_searchBillTo;
        private System.Windows.Forms.Label lbl_child;
        private System.Windows.Forms.Label lbl_adults;
        private System.Windows.Forms.Label lbl_numberOfNights;
        private System.Windows.Forms.Label lbl_departure;
        private System.Windows.Forms.Label lbl_arrival;
        private System.Windows.Forms.DateTimePicker dtp_departureDate;
        private System.Windows.Forms.DateTimePicker dtp_arrivalDate;
        private System.Windows.Forms.DateTimePicker dtp_departureTime;
        private System.Windows.Forms.DateTimePicker dtp_arrivalTime;
        private System.Windows.Forms.Button btn_GoBack;
        private System.Windows.Forms.TextBox txt_serialNumber;
        private System.Windows.Forms.Label lbl_arrivedForm;
        private System.Windows.Forms.Label lbl_vissaIssuePlace;
        private System.Windows.Forms.Label lbl_visaDate;
        private System.Windows.Forms.Label lbl_visaNumber;
        private System.Windows.Forms.Label lbl_serialNumber;
        private System.Windows.Forms.DateTimePicker dtp_visaDate;
        private System.Windows.Forms.TextBox txt_arrivedForm;
        private System.Windows.Forms.TextBox txt_visaIssuePlace;
        private System.Windows.Forms.TextBox txt_visaNumber;
        private System.Windows.Forms.TextBox txt_departureTransportation;
        private System.Windows.Forms.Label lbl_dateOfArrival;
        private System.Windows.Forms.TextBox txt_goingTo;
        private System.Windows.Forms.Label lbl_timeOfArrival;
        private System.Windows.Forms.Label lbl_purposeOfVisit;
        private System.Windows.Forms.DateTimePicker dtp_dateOfArrival;
        private System.Windows.Forms.Label lbl_goingTo;
        private System.Windows.Forms.Label lbl_departureTransportation;
        private System.Windows.Forms.TextBox txt_purposeOfVisit;
        private System.Windows.Forms.DateTimePicker dtp_timeOfArrival;
        private System.Windows.Forms.Label lbl_last_name;
        private System.Windows.Forms.Label lbl_showRates;
        private System.Windows.Forms.TextBox txt_firstname;
        private System.Windows.Forms.ComboBox cbox_title;
        private System.Windows.Forms.Label lbl_firstname;
        private System.Windows.Forms.TextBox txt_lastname;
        private System.Windows.Forms.Button btn_checkOut;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.Label lbl_contactDetails;
        private System.Windows.Forms.TextBox txt_contactDetails;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.TextBox txt_middleName;
        private System.Windows.Forms.Label lbl_middleName;
        private System.Windows.Forms.Label lbl_province;
        private System.Windows.Forms.TextBox txt_province;
        private System.Windows.Forms.Label lbl_city;
        private System.Windows.Forms.TextBox txt_city;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.TextBox txt_address;
        private System.Windows.Forms.Label lbl_dateOfBirth;
        private System.Windows.Forms.DateTimePicker dtb_dateOfBirth;
        private System.Windows.Forms.ComboBox cmb_homeCountry;
        private System.Windows.Forms.Label lbl_homeCountry;
        private System.Windows.Forms.Label lbl_companyAgency;
        private System.Windows.Forms.TextBox txt_companyAgency;
        private System.Windows.Forms.Label lbl_modeOfPayment;
        private System.Windows.Forms.RadioButton rbtn_partialCredit;
        private System.Windows.Forms.RadioButton rbtn_credit;
        private System.Windows.Forms.RadioButton rbtn_cash;
        private System.Windows.Forms.DataGridView dgv_billingSummary;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel settlementOption_panel;
        private System.Windows.Forms.Label lbl_settlementOption;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_cashPayment;
        private System.Windows.Forms.Label lbl_cashPayment;
        private System.Windows.Forms.TextBox txt_requiredPrePayment;
        private System.Windows.Forms.Label lbl_requiredPrePayment;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel stayInformation_panel;
        private System.Windows.Forms.Label lbl_stayInformation;
        private System.Windows.Forms.TextBox txt_guestRemarks;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_edit;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.DataGridView dgv_discount;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Date_discountDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Type_discountDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Discount_discountDGV;
        private System.Windows.Forms.DataGridView dgv_payments;
        private System.Windows.Forms.TextBox txt_balance;
        private System.Windows.Forms.TextBox txt_totalPayment;
        private System.Windows.Forms.TextBox txt_totalDiscount;
        private System.Windows.Forms.TextBox txt_subTotal;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txt_otherCharges;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txt_referenceNumber;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txt_fee_specialArrangements;
        private System.Windows.Forms.Label lbl_specialArrangements;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txt_fee_additionalAdultsAndChildren;
        private System.Windows.Forms.TextBox txt_fee_totalNumberOfChildren;
        private System.Windows.Forms.TextBox txt_fee_totalNumberOfAdults;
        private System.Windows.Forms.Label lbl_additionalCompanionChildren;
        private System.Windows.Forms.Label lbl_additionalCompanionAdults;
        private System.Windows.Forms.TextBox total_numberOfChildren;
        private System.Windows.Forms.TextBox txt_totalNumberOfAdults;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txt_child;
        private System.Windows.Forms.TextBox txt_adults;
        private System.Windows.Forms.TextBox txt_noofnights;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Particulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Qty;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Rate_Cost;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_TotalAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.PictureBox pcb_image;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.TextBox txt_change;
        private System.Windows.Forms.Label lbl_change;
        private System.Windows.Forms.TextBox txt_cashhanded;
        private System.Windows.Forms.Label lbl_cashHandedByGuest;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Label lbl_credit;
        private System.Windows.Forms.Label lbl_type;
        private System.Windows.Forms.Label lbl_cardNumber;
        private System.Windows.Forms.Label lbl_expire;
        private System.Windows.Forms.TextBox txt_cvv;
        private System.Windows.Forms.Label lbl_cvv;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Button btn_apply;
        private System.Windows.Forms.TextBox txt_discountDescription;
        private System.Windows.Forms.Label lbl_discountDescription;
        private System.Windows.Forms.Label lbl_discountAmount;
        private System.Windows.Forms.TextBox txt_discountAmount;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Label lbl_discount;
        private System.Windows.Forms.Button btn_scanImage;
        private System.Windows.Forms.Button btn_attachImage;
    }
}