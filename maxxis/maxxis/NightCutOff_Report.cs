﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace maxxis
{
    class NightCutOff_Report
    {
        public string FolioNumber { get; set; }

        public string ReservationNumber { get; set; }

        public string FullName { get; set; }

        public string Booking { get; set; }

        public string RoomNumbers { get; set; }

        public string RoomTypes { get; set; }

        public string ReservationDate { get; set; }

        public string CancellationDate { get; set; }

        public string CheckInDate { get; set; }

        public string DueOut { get; set; }

        public string CheckOutDate { get; set; }

        public int NumberOfNights { get; set; }

        public float CancellationPenalty { get; set; }

        public float TotalPriceOfAmenities { get; set; }

        public float TotalPriceForLossBreakageFines { get; set; }

        public float TotalBillAccumulated { get; set; }


    }
}
