﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace maxxis
{
    class auditTrail_class
    {
        /*******************************************************************************
       * Variables and Instantiations                                                 *
       *******************************************************************************/

        //Connection String
        string con;

        //Database connection and reader
        SqlConnection conDatabase;
        SqlCommand cmdDataBase;
        SqlDataReader dataReader;

        //Instantiations
        userInfo_class UIC;

        //Variables
        string action_login;
        string action_logout;
        string dateAndTime;
        /*******************************************************************************
        * Protected Properties                                                         *
        *******************************************************************************/


        /*******************************************************************************
        * Private Properties                                                           *
        *******************************************************************************/


        /*******************************************************************************
        * Public Properties                                                            *
        *******************************************************************************/


        /*******************************************************************************
        * Protected Methods                                                            *
        *******************************************************************************/


        /*******************************************************************************
        * Private Methods                                                              *
        *******************************************************************************/
        private void defaultValues()
        {
            action_login = "Login";
            action_logout = "Logout";
            dateAndTime = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt");
        }

        /*******************************************************************************
        * Public Methods                                                               *
        *******************************************************************************/

        //Login audit trail
        public void recordLogin(string userId_param, string UserName_param, string FullName_param, string ShiftNumber_param, string Position_param)
        {
            defaultValues();

            UIC = new userInfo_class();

            con = System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString;

            conDatabase = new SqlConnection(con);

            conDatabase.Open();

            cmdDataBase = new SqlCommand("INSERT INTO tbl_auditTrail(" +
                                         "user_id," +
                                         "userName, " +
                                         "fullName, " +
                                         "actionPerformed, " +
                                         "dateActionPerformed, " +
                                         "shiftNumber, " +
                                         "position)" +
                                         "VALUES(" +
                                         "@User_id, " +
                                         "@UserName, " +
                                         "@FullName, " +
                                         "@ActionPerformed, " +
                                         "@DateActionPerformed, " +
                                         "@shiftNumber, " +
                                         "@Position)", conDatabase);

            cmdDataBase.Parameters.AddWithValue("@User_id", userId_param);
            cmdDataBase.Parameters.AddWithValue("@UserName", UserName_param);
            cmdDataBase.Parameters.AddWithValue("@FullName", FullName_param);
            cmdDataBase.Parameters.AddWithValue("@ActionPerformed", action_login);
            cmdDataBase.Parameters.AddWithValue("@DateActionPerformed", dateAndTime);
            cmdDataBase.Parameters.AddWithValue("@shiftNumber", ShiftNumber_param);
            cmdDataBase.Parameters.AddWithValue("@Position", Position_param);

            cmdDataBase.ExecuteNonQuery();

            conDatabase.Close();
        }

        //Logout audit trail
        public void recordLogout(string userId_param, string UserName_param, string FullName_param, string ShiftNumber_param, string Position_param)
        {
            defaultValues();

            UIC = new userInfo_class();

            con = System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString;

            conDatabase = new SqlConnection(con);

            conDatabase.Open();

            cmdDataBase = new SqlCommand("INSERT INTO tbl_auditTrail(" +
                                         "user_id," +
                                         "userName, " +
                                         "fullName, " +
                                         "actionPerformed, " +
                                         "dateActionPerformed, " +
                                         "shiftNumber, " +
                                         "position)" +
                                         "VALUES(" +
                                         "@User_id, " +
                                         "@UserName, " +
                                         "@FullName, " +
                                         "@ActionPerformed, " +
                                         "@DateActionPerformed, " +
                                         "@shiftNumber, " +
                                         "@Position)", conDatabase);

            cmdDataBase.Parameters.AddWithValue("@User_id", userId_param);
            cmdDataBase.Parameters.AddWithValue("@UserName", UserName_param);
            cmdDataBase.Parameters.AddWithValue("@FullName", FullName_param);
            cmdDataBase.Parameters.AddWithValue("@ActionPerformed", action_logout);
            cmdDataBase.Parameters.AddWithValue("@DateActionPerformed", dateAndTime);
            cmdDataBase.Parameters.AddWithValue("@shiftNumber", ShiftNumber_param);
            cmdDataBase.Parameters.AddWithValue("@Position", Position_param);

            cmdDataBase.ExecuteNonQuery();

            conDatabase.Close();
        }
    }
}
