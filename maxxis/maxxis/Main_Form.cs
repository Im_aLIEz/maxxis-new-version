﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace maxxis
{
    public partial class Main_Form : Form
    {
        string con = System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString;
        string hash = "MAXXis";
        //Variables
        float Occupancy;
        float occupancyRate;

        string outoforderChecker;
        string occupiedChecker;
        string guestIdHolder;
        string GuestName;
        string Guestcheckout;
        string GuestStatus;
        string dirtyChecker;
        
        //Room Variables
        string[] roomtype;
        string[] roomnumber;
        string[] roomrate;
        string[] roomrater;
        string[] guestidarr;

        int[] buttonColorCode1;
        int[] buttonColorCode2;
        int[] buttonColorCode3;

        List<string> roomtypeList = new List<string>();
        List<string> roomnumberList = new List<string>();
        List<string> roomrateList = new List<string>();
        List<string> roomraterList = new List<string>();
        List<string> guestidlist = new List<string>();
        List<int> buttoncolorcodelist1 = new List<int>();
        List<int> buttoncolorcodelist2 = new List<int>();
        List<int> buttoncolorcodelist3 = new List<int>();

        int occupiedCounter;
        int reservedCounter;
        int vacantCounter;
        int dirtyCounter;
        int outoforderCounter;

        string startaccessencrypt;
        string endaccessencrypt;

        DateTime startaccess;
        DateTime endaccess;

        private static string selectedguest;
        private static string selectedRoom;

        Guest_Folio guestF = new Guest_Folio();

        public string SelectedGuest
        {
            get { return selectedguest; }
            set { selectedguest = value; }
        }
        public string SelectedRoom
        {
            get { return selectedRoom; }
            set { selectedRoom = value; }
        }

        public Main_Form()
        {
            InitializeComponent();
        }

        private void btn_GuestMessage_Click(object sender, EventArgs e)
        {
            Confirmation_Email frm_email = new Confirmation_Email();
            this.Hide();
            frm_email.ShowDialog();
            this.Show();
            
        }

        private void btn_GuestFolio_Click(object sender, EventArgs e)
        {
            this.Hide();
            Guest_Folio GF = new Guest_Folio();
            GF.ShowDialog();
            guestF.Guest_Folio_Load(guestF, EventArgs.Empty);
        }

        private void pnl_MainPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_GuestArrivalList_Click(object sender, EventArgs e)
        {
            guestArrivalList_form GAF = new guestArrivalList_form();
            GAF.ShowDialog();
        }

        private void btn_New_Walkin_Guest_Click(object sender, EventArgs e)
        {
            
            Access();
            AccessEnc();
            if (DateTime.Now < startaccess)
            {
                MessageBox.Show("Error Code: 0239, Please Contact Your Technical Support");
            }
            else if (DateTime.Now > endaccess)
            {
                MessageBox.Show("Error Code: 0239, Please Contact Your Technical Support");
            }
            
            this.Hide();
            WalkinReservation_form WRF = new WalkinReservation_form();
            WRF.ShowDialog();
        }

        internal void Main_Form_Load(object sender, EventArgs e)
        {
            
            Access();
            AccessEnc();
            if(DateTime.Now < startaccess)
            {
                MessageBox.Show("Error Code: 0239, Please Contact Your Technical Support");
            }
            else if (DateTime.Now > endaccess)
            {
                MessageBox.Show("Error Code: 0239, Please Contact Your Technical Support");
            }
            
            RoomLoadOccupy();
            RoomOccupyRater();
            RoomRateDisplay();
            RoomDisplay();
            RoomOccupancySetter();
        }

        private void btn_SignOut_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        

        private void btn_reservationList_Click(object sender, EventArgs e)
        {
            ReservationList_Form RF = new ReservationList_Form();
            this.Hide();
            RF.Show();
        }
        //Methods
        public void RoomLoadOccupy()
        {
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select Sum(occupied/48 * 100)from tbl_rooms ", conDatabase);
            conDatabase.Open();
            SqlDataAdapter sda = new SqlDataAdapter();
            sda.SelectCommand = cmdDataBase;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            try
            {
                foreach (DataRow item in dt.Rows)
                {
                    Occupancy = float.Parse(item[0].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            conDatabase.Close();
        }
        public void RoomOccupyRater()
        {
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select Max(discount) from tbl_occupyrate where occupyrate > '" + Occupancy +"'", conDatabase);
            conDatabase.Open();
            SqlDataAdapter sda = new SqlDataAdapter();
            sda.SelectCommand = cmdDataBase;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            try
            {
                foreach (DataRow item in dt.Rows)
                {
                    occupancyRate = float.Parse(item[0].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            conDatabase.Close();
        }
        public void RoomRateDisplay()
        {
            dirtyCounter = 0;
            occupiedCounter = 0;
            vacantCounter = 0;
            roomtypeList.Clear();
            roomnumberList.Clear();
            guestidlist.Clear();
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT dbo.tbl_roomrate.rate - (dbo.tbl_roomrate.rate * '" + occupancyRate + "') + dbo.tbl_rooms.roomrate AS Total, dbo.tbl_rooms.*FROM dbo.tbl_rooms CROSS JOIN dbo.tbl_roomrate", conDatabase);
            conDatabase.Open();
            SqlDataAdapter sda = new SqlDataAdapter();
            sda.SelectCommand = cmdDataBase;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            try
            {
                foreach (DataRow item in dt.Rows)
                {
                    occupiedChecker = item[6].ToString();
                    dirtyChecker = item[5].ToString();
                    outoforderChecker = item[15].ToString();
                    roomtypeList.Add(item[4].ToString());
                    roomnumberList.Add(item[2].ToString());
                    guestidlist.Add(item[3].ToString());
                    if(occupiedChecker == "1")
                    {
                        guestIdHolder = item[3].ToString();
                        GuestRoomIdentifier();
                        if(GuestStatus=="1")
                        {
                            roomrateList.Add(GuestName);
                            roomraterList.Add((item[0].ToString()));
                            buttoncolorcodelist1.Add(0);
                            buttoncolorcodelist2.Add(128);
                            buttoncolorcodelist3.Add(0);
                        }
                        else if (Convert.ToDateTime(Guestcheckout) < DateTime.Now && DateTime.Now.TimeOfDay > TimeSpan.Parse("12:00"))
                        {
                            roomrateList.Add(GuestName);
                            roomraterList.Add((item[0].ToString()));
                            buttoncolorcodelist1.Add(255);
                            buttoncolorcodelist2.Add(0);
                            buttoncolorcodelist3.Add(0);
                            occupiedCounter = occupiedCounter + 1;
                        }
                        else
                        {
                            roomrateList.Add(GuestName);
                            roomraterList.Add((item[0].ToString()));
                            buttoncolorcodelist1.Add(0);
                            buttoncolorcodelist2.Add(0);
                            buttoncolorcodelist3.Add(255);
                            occupiedCounter = occupiedCounter + 1;
                        }
                    }
                    else if(dirtyChecker == "1")
                    {
                        roomrateList.Add((item[0].ToString()));
                        roomraterList.Add((item[0].ToString()));
                        buttoncolorcodelist1.Add(165);
                        buttoncolorcodelist2.Add(42);
                        buttoncolorcodelist3.Add(42);
                        dirtyCounter = dirtyCounter + 1;
                    }
                    else if (outoforderChecker == "1")
                    {
                        roomrateList.Add((item[0].ToString()));
                        roomraterList.Add((item[0].ToString()));
                        buttoncolorcodelist1.Add(47);
                        buttoncolorcodelist2.Add(79);
                        buttoncolorcodelist3.Add(79);
                        outoforderCounter = outoforderCounter + 1;
                    }
                    else
                    {
                        roomrateList.Add((item[0].ToString()));
                        roomraterList.Add((item[0].ToString()));
                        buttoncolorcodelist1.Add(0);
                        buttoncolorcodelist2.Add(128);
                        buttoncolorcodelist3.Add(0);
                        vacantCounter = vacantCounter + 1;
                    }
                    
                }
                roomtype = roomtypeList.ToArray();
                roomnumber = roomnumberList.ToArray();
                roomrate = roomrateList.ToArray();
                roomrater = roomraterList.ToArray();
                guestidarr = guestidlist.ToArray();
                buttonColorCode1 = buttoncolorcodelist1.ToArray();
                buttonColorCode2 = buttoncolorcodelist2.ToArray();
                buttonColorCode3 = buttoncolorcodelist3.ToArray();
                roomtypeList.Clear();
            roomnumberList.Clear();
            guestidlist.Clear();
            buttoncolorcodelist1.Clear();
            buttoncolorcodelist2.Clear();
            buttoncolorcodelist3.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            conDatabase.Close();
        }
        public void RoomOccupancySetter()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "update tbl_currentoccupancy Set occupancy = '" + occupancyRate + "' where id = 0;";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void GuestRoomIdentifier()
        {
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT dbo.tbl_guestinfo.lastname + ' ' + dbo.tbl_guestinfo.firstname AS 'Full Name', dbo.tbl_stayinfo.departure, dbo.tbl_guestinfo.express_active FROM dbo.tbl_guestinfo INNER JOIN dbo.tbl_stayinfo ON dbo.tbl_guestinfo.id = dbo.tbl_stayinfo.guest_id Where dbo.tbl_guestinfo.id = '" + guestIdHolder + "'", conDatabase);
            conDatabase.Open();
            SqlDataAdapter sda = new SqlDataAdapter();
            sda.SelectCommand = cmdDataBase;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            try
            {
                foreach (DataRow item in dt.Rows)
                {
                    GuestName = item[0].ToString();
                    Guestcheckout = item[1].ToString();
                    GuestStatus = item[2].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            conDatabase.Close();
        }
        public void RoomDisplay()
        {
            if (roomtype.Length != 0)
            {
                btn_Room21.Text = roomtype[0] + "\n  " + roomnumber[0] + "\n" + roomrate[0];
                btn_Room22.Text = roomtype[1] + "\n  " + roomnumber[1] + "\n" + roomrate[1];
                btn_Room23.Text = roomtype[2] + "\n  " + roomnumber[2] + "\n" + roomrate[2];
                btn_Room24.Text = roomtype[3] + "\n  " + roomnumber[3] + "\n" + roomrate[3];
                btn_Room25.Text = roomtype[4] + "\n  " + roomnumber[4] + "\n" + roomrate[4];
                btn_Room26.Text = roomtype[5] + "\n  " + roomnumber[5] + "\n" + roomrate[5];
                btn_Room31.Text = roomtype[6] + "\n  " + roomnumber[6] + "\n" + roomrate[6];
                btn_Room32.Text = roomtype[7] + "\n  " + roomnumber[7] + "\n" + roomrate[7];
                btn_Room33.Text = roomtype[8] + "\n  " + roomnumber[8] + "\n" + roomrate[8];
                btn_Room34.Text = roomtype[9] + "\n  " + roomnumber[9] + "\n" + roomrate[9];
                btn_Room35.Text = roomtype[10] + "\n  " + roomnumber[10] + "\n" + roomrate[10];
                btn_Room36.Text = roomtype[11] + "\n  " + roomnumber[11] + "\n" + roomrate[11];
                btn_Room51.Text = roomtype[12] + "\n  " + roomnumber[12] + "\n" + roomrate[12];
                btn_Room52.Text = roomtype[13] + "\n  " + roomnumber[13] + "\n" + roomrate[13];
                btn_Room53.Text = roomtype[14] + "\n  " + roomnumber[14] + "\n" + roomrate[14];
                btn_Room54.Text = roomtype[15] + "\n  " + roomnumber[15] + "\n" + roomrate[15];
                btn_Room55.Text = roomtype[16] + "\n  " + roomnumber[16] + "\n" + roomrate[16];
                btn_Room56.Text = roomtype[17] + "\n  " + roomnumber[17] + "\n" + roomrate[17];
                btn_Room61.Text = roomtype[18] + "\n  " + roomnumber[18] + "\n" + roomrate[18];
                btn_Room62.Text = roomtype[19] + "\n  " + roomnumber[19] + "\n" + roomrate[19];
                btn_Room63.Text = roomtype[20] + "\n  " + roomnumber[20] + "\n" + roomrate[20];
                btn_Room64.Text = roomtype[21] + "\n  " + roomnumber[21] + "\n" + roomrate[21];
                btn_Room65.Text = roomtype[22] + "\n  " + roomnumber[22] + "\n" + roomrate[22];
                btn_Room66.Text = roomtype[23] + "\n  " + roomnumber[23] + "\n" + roomrate[23];
                btn_Room71.Text = roomtype[24] + "\n  " + roomnumber[24] + "\n" + roomrate[24];
                btn_Room72.Text = roomtype[25] + "\n  " + roomnumber[25] + "\n" + roomrate[25];
                btn_Room73.Text = roomtype[26] + "\n  " + roomnumber[26] + "\n" + roomrate[26];
                btn_Room74.Text = roomtype[27] + "\n  " + roomnumber[27] + "\n" + roomrate[27];
                btn_Room75.Text = roomtype[28] + "\n  " + roomnumber[28] + "\n" + roomrate[28];
                btn_Room76.Text = roomtype[29] + "\n  " + roomnumber[29] + "\n" + roomrate[29];
                btn_Room81.Text = roomtype[30] + "\n  " + roomnumber[30] + "\n" + roomrate[30];
                btn_Room82.Text = roomtype[31] + "\n  " + roomnumber[31] + "\n" + roomrate[31];
                btn_Room83.Text = roomtype[32] + "\n  " + roomnumber[32] + "\n" + roomrate[32];
                btn_Room84.Text = roomtype[33] + "\n  " + roomnumber[33] + "\n" + roomrate[33];
                btn_Room85.Text = roomtype[34] + "\n  " + roomnumber[34] + "\n" + roomrate[34];
                btn_Room86.Text = roomtype[35] + "\n  " + roomnumber[35] + "\n" + roomrate[35];
                btn_Room91.Text = roomtype[36] + "\n  " + roomnumber[36] + "\n" + roomrate[36];
                btn_Room92.Text = roomtype[37] + "\n  " + roomnumber[37] + "\n" + roomrate[37];
                btn_Room93.Text = roomtype[38] + "\n  " + roomnumber[38] + "\n" + roomrate[38];
                btn_Room94.Text = roomtype[39] + "\n  " + roomnumber[39] + "\n" + roomrate[39];
                btn_Room95.Text = roomtype[40] + "\n  " + roomnumber[40] + "\n" + roomrate[40];
                btn_Room96.Text = roomtype[41] + "\n  " + roomnumber[41] + "\n" + roomrate[41];
                btn_Room101.Text = roomtype[42] + "\n  " + roomnumber[42] + "\n" + roomrate[42];
                btn_Room102.Text = roomtype[43] + "\n  " + roomnumber[43] + "\n" + roomrate[43];
                btn_Room103.Text = roomtype[44] + "\n  " + roomnumber[44] + "\n" + roomrate[44];
                btn_Room104.Text = roomtype[45] + "\n  " + roomnumber[45] + "\n" + roomrate[45];
                btn_Room105.Text = roomtype[46] + "\n  " + roomnumber[46] + "\n" + roomrate[46];
                btn_Room106.Text = roomtype[47] + "\n  " + roomnumber[47] + "\n" + roomrate[47];

                btn_Room21.BackColor = Color.FromArgb(buttonColorCode1[0], buttonColorCode2[0], buttonColorCode3[0]);
                btn_Room22.BackColor = Color.FromArgb(buttonColorCode1[1], buttonColorCode2[1], buttonColorCode3[1]);
                btn_Room23.BackColor = Color.FromArgb(buttonColorCode1[2], buttonColorCode2[2], buttonColorCode3[2]);
                btn_Room24.BackColor = Color.FromArgb(buttonColorCode1[3], buttonColorCode2[3], buttonColorCode3[3]);
                btn_Room25.BackColor = Color.FromArgb(buttonColorCode1[4], buttonColorCode2[4], buttonColorCode3[4]);
                btn_Room26.BackColor = Color.FromArgb(buttonColorCode1[5], buttonColorCode2[5], buttonColorCode3[5]);
                btn_Room31.BackColor = Color.FromArgb(buttonColorCode1[6], buttonColorCode2[6], buttonColorCode3[6]);
                btn_Room32.BackColor = Color.FromArgb(buttonColorCode1[7], buttonColorCode2[7], buttonColorCode3[7]);
                btn_Room33.BackColor = Color.FromArgb(buttonColorCode1[8], buttonColorCode2[8], buttonColorCode3[8]);
                btn_Room34.BackColor = Color.FromArgb(buttonColorCode1[9], buttonColorCode2[9], buttonColorCode3[9]);
                btn_Room35.BackColor = Color.FromArgb(buttonColorCode1[10], buttonColorCode2[10], buttonColorCode3[10]);
                btn_Room36.BackColor = Color.FromArgb(buttonColorCode1[11], buttonColorCode2[11], buttonColorCode3[11]);
                btn_Room51.BackColor = Color.FromArgb(buttonColorCode1[12], buttonColorCode2[12], buttonColorCode3[12]);
                btn_Room52.BackColor = Color.FromArgb(buttonColorCode1[13], buttonColorCode2[13], buttonColorCode3[13]);
                btn_Room53.BackColor = Color.FromArgb(buttonColorCode1[14], buttonColorCode2[14], buttonColorCode3[14]);
                btn_Room54.BackColor = Color.FromArgb(buttonColorCode1[15], buttonColorCode2[15], buttonColorCode3[15]);
                btn_Room55.BackColor = Color.FromArgb(buttonColorCode1[16], buttonColorCode2[16], buttonColorCode3[16]);
                btn_Room56.BackColor = Color.FromArgb(buttonColorCode1[17], buttonColorCode2[17], buttonColorCode3[17]);
                btn_Room61.BackColor = Color.FromArgb(buttonColorCode1[18], buttonColorCode2[18], buttonColorCode3[18]);
                btn_Room62.BackColor = Color.FromArgb(buttonColorCode1[19], buttonColorCode2[19], buttonColorCode3[19]);
                btn_Room63.BackColor = Color.FromArgb(buttonColorCode1[20], buttonColorCode2[20], buttonColorCode3[20]);
                btn_Room64.BackColor = Color.FromArgb(buttonColorCode1[21], buttonColorCode2[21], buttonColorCode3[21]);
                btn_Room65.BackColor = Color.FromArgb(buttonColorCode1[22], buttonColorCode2[22], buttonColorCode3[22]);
                btn_Room66.BackColor = Color.FromArgb(buttonColorCode1[23], buttonColorCode2[23], buttonColorCode3[23]);
                btn_Room71.BackColor = Color.FromArgb(buttonColorCode1[24], buttonColorCode2[24], buttonColorCode3[24]);
                btn_Room72.BackColor = Color.FromArgb(buttonColorCode1[25], buttonColorCode2[25], buttonColorCode3[25]);
                btn_Room73.BackColor = Color.FromArgb(buttonColorCode1[26], buttonColorCode2[26], buttonColorCode3[26]);
                btn_Room74.BackColor = Color.FromArgb(buttonColorCode1[27], buttonColorCode2[27], buttonColorCode3[27]);
                btn_Room75.BackColor = Color.FromArgb(buttonColorCode1[28], buttonColorCode2[28], buttonColorCode3[28]);
                btn_Room76.BackColor = Color.FromArgb(buttonColorCode1[29], buttonColorCode2[29], buttonColorCode3[29]);
                btn_Room81.BackColor = Color.FromArgb(buttonColorCode1[30], buttonColorCode2[30], buttonColorCode3[30]);
                btn_Room82.BackColor = Color.FromArgb(buttonColorCode1[31], buttonColorCode2[31], buttonColorCode3[31]);
                btn_Room83.BackColor = Color.FromArgb(buttonColorCode1[32], buttonColorCode2[32], buttonColorCode3[32]);
                btn_Room84.BackColor = Color.FromArgb(buttonColorCode1[33], buttonColorCode2[33], buttonColorCode3[33]);
                btn_Room85.BackColor = Color.FromArgb(buttonColorCode1[34], buttonColorCode2[34], buttonColorCode3[34]);
                btn_Room86.BackColor = Color.FromArgb(buttonColorCode1[35], buttonColorCode2[35], buttonColorCode3[35]);
                btn_Room91.BackColor = Color.FromArgb(buttonColorCode1[36], buttonColorCode2[36], buttonColorCode3[36]);
                btn_Room92.BackColor = Color.FromArgb(buttonColorCode1[37], buttonColorCode2[37], buttonColorCode3[37]);
                btn_Room93.BackColor = Color.FromArgb(buttonColorCode1[38], buttonColorCode2[38], buttonColorCode3[38]);
                btn_Room94.BackColor = Color.FromArgb(buttonColorCode1[39], buttonColorCode2[39], buttonColorCode3[39]);
                btn_Room95.BackColor = Color.FromArgb(buttonColorCode1[40], buttonColorCode2[40], buttonColorCode3[40]);
                btn_Room96.BackColor = Color.FromArgb(buttonColorCode1[41], buttonColorCode2[41], buttonColorCode3[41]);
                btn_Room101.BackColor = Color.FromArgb(buttonColorCode1[42], buttonColorCode2[42], buttonColorCode3[42]);
                btn_Room102.BackColor = Color.FromArgb(buttonColorCode1[43], buttonColorCode2[43], buttonColorCode3[43]);
                btn_Room103.BackColor = Color.FromArgb(buttonColorCode1[44], buttonColorCode2[44], buttonColorCode3[44]);
                btn_Room104.BackColor = Color.FromArgb(buttonColorCode1[45], buttonColorCode2[45], buttonColorCode3[45]);
                btn_Room105.BackColor = Color.FromArgb(buttonColorCode1[46], buttonColorCode2[46], buttonColorCode3[46]);
                btn_Room106.BackColor = Color.FromArgb(buttonColorCode1[47], buttonColorCode2[47], buttonColorCode3[47]);

                btn_Occupied.Text = "Occupied (" + occupiedCounter.ToString() + ")";
                btn_Vacant.Text = "Vacant (" + vacantCounter.ToString() + ")";
            }
        }
        public void RoomBorderColorReset()
        {
            btn_Room21.FlatAppearance.BorderColor = Color.Green;
            btn_Room22.FlatAppearance.BorderColor = Color.Green;
            btn_Room23.FlatAppearance.BorderColor = Color.Green;
            btn_Room24.FlatAppearance.BorderColor = Color.Green;
            btn_Room25.FlatAppearance.BorderColor = Color.Green;
            btn_Room26.FlatAppearance.BorderColor = Color.Green;
            btn_Room31.FlatAppearance.BorderColor = Color.Green;
            btn_Room32.FlatAppearance.BorderColor = Color.Green;
            btn_Room33.FlatAppearance.BorderColor = Color.Green;
            btn_Room34.FlatAppearance.BorderColor = Color.Green;
            btn_Room35.FlatAppearance.BorderColor = Color.Green;
            btn_Room36.FlatAppearance.BorderColor = Color.Green;
            btn_Room51.FlatAppearance.BorderColor = Color.Green;
            btn_Room52.FlatAppearance.BorderColor = Color.Green;
            btn_Room53.FlatAppearance.BorderColor = Color.Green;
            btn_Room54.FlatAppearance.BorderColor = Color.Green;
            btn_Room55.FlatAppearance.BorderColor = Color.Green;
            btn_Room56.FlatAppearance.BorderColor = Color.Green;
            btn_Room61.FlatAppearance.BorderColor = Color.Green;
            btn_Room62.FlatAppearance.BorderColor = Color.Green;
            btn_Room63.FlatAppearance.BorderColor = Color.Green;
            btn_Room64.FlatAppearance.BorderColor = Color.Green;
            btn_Room65.FlatAppearance.BorderColor = Color.Green;
            btn_Room66.FlatAppearance.BorderColor = Color.Green;
            btn_Room71.FlatAppearance.BorderColor = Color.Green;
            btn_Room72.FlatAppearance.BorderColor = Color.Green;
            btn_Room73.FlatAppearance.BorderColor = Color.Green;
            btn_Room74.FlatAppearance.BorderColor = Color.Green;
            btn_Room75.FlatAppearance.BorderColor = Color.Green;
            btn_Room76.FlatAppearance.BorderColor = Color.Green;
            btn_Room81.FlatAppearance.BorderColor = Color.Green;
            btn_Room82.FlatAppearance.BorderColor = Color.Green;
            btn_Room83.FlatAppearance.BorderColor = Color.Green;
            btn_Room84.FlatAppearance.BorderColor = Color.Green;
            btn_Room85.FlatAppearance.BorderColor = Color.Green;
            btn_Room86.FlatAppearance.BorderColor = Color.Green;
            btn_Room91.FlatAppearance.BorderColor = Color.Green;
            btn_Room92.FlatAppearance.BorderColor = Color.Green;
            btn_Room93.FlatAppearance.BorderColor = Color.Green;
            btn_Room94.FlatAppearance.BorderColor = Color.Green;
            btn_Room95.FlatAppearance.BorderColor = Color.Green;
            btn_Room96.FlatAppearance.BorderColor = Color.Green;
            btn_Room101.FlatAppearance.BorderColor = Color.Green;
            btn_Room102.FlatAppearance.BorderColor = Color.Green;
            btn_Room103.FlatAppearance.BorderColor = Color.Green;
            btn_Room104.FlatAppearance.BorderColor = Color.Green;
            btn_Room105.FlatAppearance.BorderColor = Color.Green;
            btn_Room106.FlatAppearance.BorderColor = Color.Green;
        }
        public void RoomClean()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "update tbl_rooms set dirty = 0, available = 1 where room_no = '" + selectedRoom + "';";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
                Main_Form frm = new Main_Form();
                this.Hide();
                frm.Show();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void Access()
        {
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select * From tbl_online", conDatabase);
            conDatabase.Open();
            SqlDataAdapter sda = new SqlDataAdapter();
            sda.SelectCommand = cmdDataBase;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            try
            {
                foreach (DataRow item in dt.Rows)
                {
                    startaccessencrypt = item[1].ToString(); 
                    endaccessencrypt = item[2].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            conDatabase.Close();
        }
        public void AccessEnc()
        {
            byte[] data = Convert.FromBase64String(startaccessencrypt);
            byte[] data1 = Convert.FromBase64String(endaccessencrypt);
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
                using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider() { Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7})
                {
                    ICryptoTransform transform = tripDes.CreateDecryptor();
                    byte[] results = transform.TransformFinalBlock(data, 0, data.Length);
                    byte[] results1 = transform.TransformFinalBlock(data1, 0, data1.Length);
                    startaccess = Convert.ToDateTime(UTF8Encoding.UTF8.GetString(results));
                    endaccess = Convert.ToDateTime(UTF8Encoding.UTF8.GetString(results1));
                }
            }
        }

        private void btn_NewReservation_Click(object sender, EventArgs e)
        {
            ReservationV2_Form frm = new ReservationV2_Form();
            this.Hide();
            frm.Show();
        }

        private void btn_FrontDesk_Click(object sender, EventArgs e)
        {
            Front_Desk.FrontDesk_Form frm = new Front_Desk.FrontDesk_Form();
            frm.ShowDialog();
        }
        private void btn_Room21_Click(object sender, EventArgs e)
        {
            SelectedRoom = "21";
            SelectedGuest = guestidarr[0].ToString();
            RoomBorderColorReset();
            btn_Room21.FlatAppearance.BorderColor = Color.White;
        }
        private void btn_Room22_Click(object sender, EventArgs e)
        {
            SelectedRoom = "22";
            SelectedGuest = guestidarr[1].ToString();
            RoomBorderColorReset();
            btn_Room22.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room23_Click(object sender, EventArgs e)
        {
            SelectedRoom = "23";
            SelectedGuest = guestidarr[2].ToString();
            RoomBorderColorReset();
            btn_Room23.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room24_Click(object sender, EventArgs e)
        {
            SelectedRoom = "24";
            SelectedGuest = guestidarr[3].ToString();
            RoomBorderColorReset();
            btn_Room24.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room25_Click(object sender, EventArgs e)
        {
            SelectedRoom = "25";
            SelectedGuest = guestidarr[4].ToString();
            RoomBorderColorReset();
            btn_Room25.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room26_Click(object sender, EventArgs e)
        {
            SelectedRoom = "26";
            SelectedGuest = guestidarr[5].ToString();
            RoomBorderColorReset();
            btn_Room26.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room31_Click(object sender, EventArgs e)
        {
            SelectedRoom = "31";
            SelectedGuest = guestidarr[6].ToString();
            RoomBorderColorReset();
            btn_Room31.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room32_Click(object sender, EventArgs e)
        {
            SelectedRoom = "32";
            SelectedGuest = guestidarr[7].ToString();
            RoomBorderColorReset();
            btn_Room32.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room33_Click(object sender, EventArgs e)
        {
            SelectedRoom = "33";
            SelectedGuest = guestidarr[8].ToString();
            RoomBorderColorReset();
            btn_Room33.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room34_Click(object sender, EventArgs e)
        {
            SelectedRoom = "34";
            SelectedGuest = guestidarr[9].ToString();
            RoomBorderColorReset();
            btn_Room34.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room35_Click(object sender, EventArgs e)
        {
            SelectedRoom = "35";
            SelectedGuest = guestidarr[10].ToString();
            RoomBorderColorReset();
            btn_Room35.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room36_Click(object sender, EventArgs e)
        {
            SelectedRoom = "36";
            SelectedGuest = guestidarr[11].ToString();
            RoomBorderColorReset();
            btn_Room36.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room51_Click(object sender, EventArgs e)
        {
            SelectedRoom = "51";
            SelectedGuest = guestidarr[12].ToString();
            RoomBorderColorReset();
            btn_Room51.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room52_Click(object sender, EventArgs e)
        {
            SelectedRoom = "52";
            SelectedGuest = guestidarr[13].ToString();
            RoomBorderColorReset();
            btn_Room52.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room53_Click(object sender, EventArgs e)
        {
            SelectedRoom = "53";
            SelectedGuest = guestidarr[14].ToString();
            RoomBorderColorReset();
            btn_Room53.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room54_Click(object sender, EventArgs e)
        {
            SelectedRoom = "54";
            SelectedGuest = guestidarr[15].ToString();
            RoomBorderColorReset();
            btn_Room54.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room55_Click(object sender, EventArgs e)
        {
            SelectedRoom = "55";
            SelectedGuest = guestidarr[16].ToString();
            RoomBorderColorReset();
            btn_Room55.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room56_Click(object sender, EventArgs e)
        {
            SelectedRoom = "56";
            SelectedGuest = guestidarr[17].ToString();
            RoomBorderColorReset();
            btn_Room56.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room61_Click(object sender, EventArgs e)
        {
            SelectedRoom = "61";
            SelectedGuest = guestidarr[18].ToString();
            RoomBorderColorReset();
            btn_Room61.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room62_Click(object sender, EventArgs e)
        {
            SelectedRoom = "62";
            SelectedGuest = guestidarr[19].ToString();
            RoomBorderColorReset();
            btn_Room62.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room63_Click(object sender, EventArgs e)
        {
            SelectedRoom = "63";
            SelectedGuest = guestidarr[20].ToString();
            RoomBorderColorReset();
            btn_Room63.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room64_Click(object sender, EventArgs e)
        {
            SelectedRoom = "64";
            SelectedGuest = guestidarr[21].ToString();
            RoomBorderColorReset();
            btn_Room64.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room65_Click(object sender, EventArgs e)
        {
            SelectedRoom = "65";
            SelectedGuest = guestidarr[22].ToString();
            RoomBorderColorReset();
            btn_Room65.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room66_Click(object sender, EventArgs e)
        {
            SelectedRoom = "66";
            SelectedGuest = guestidarr[23].ToString();
            RoomBorderColorReset();
            btn_Room66.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room71_Click(object sender, EventArgs e)
        {
            SelectedRoom = "71";
            SelectedGuest = guestidarr[24].ToString();
            RoomBorderColorReset();
            btn_Room71.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room72_Click(object sender, EventArgs e)
        {
            SelectedRoom = "72";
            SelectedGuest = guestidarr[25].ToString();
            RoomBorderColorReset();
            btn_Room72.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room73_Click(object sender, EventArgs e)
        {
            SelectedRoom = "73";
            SelectedGuest = guestidarr[26].ToString();
            RoomBorderColorReset();
            btn_Room73.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room74_Click(object sender, EventArgs e)
        {
            SelectedRoom = "74";
            SelectedGuest = guestidarr[27].ToString();
            RoomBorderColorReset();
            btn_Room74.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room75_Click(object sender, EventArgs e)
        {
            SelectedRoom = "75";
            SelectedGuest = guestidarr[28 ].ToString();
            RoomBorderColorReset();
            btn_Room75.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room76_Click(object sender, EventArgs e)
        {
            SelectedRoom = "76";
            SelectedGuest = guestidarr[29].ToString();
            RoomBorderColorReset();
            btn_Room76.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room81_Click(object sender, EventArgs e)
        {
            SelectedRoom = "81";
            SelectedGuest = guestidarr[30].ToString();
            RoomBorderColorReset();
            btn_Room81.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room82_Click(object sender, EventArgs e)
        {
            SelectedRoom = "82";
            SelectedGuest = guestidarr[31].ToString();
            RoomBorderColorReset();
            btn_Room82.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room83_Click(object sender, EventArgs e)
        {
            SelectedRoom = "83";
            SelectedGuest = guestidarr[32].ToString();
            RoomBorderColorReset();
            btn_Room83.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room84_Click(object sender, EventArgs e)
        {
            SelectedRoom = "84";
            SelectedGuest = guestidarr[33].ToString();
            RoomBorderColorReset();
            btn_Room84.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room85_Click(object sender, EventArgs e)
        {
            SelectedRoom = "85";
            SelectedGuest = guestidarr[34].ToString();
            RoomBorderColorReset();
            btn_Room85.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room86_Click(object sender, EventArgs e)
        {
            SelectedRoom = "86";
            SelectedGuest = guestidarr[35].ToString();
            RoomBorderColorReset();
            btn_Room86.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room91_Click(object sender, EventArgs e)
        {
            SelectedRoom = "91";
            SelectedGuest = guestidarr[36].ToString();
            RoomBorderColorReset();
            btn_Room91.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room92_Click(object sender, EventArgs e)
        {
            SelectedRoom = "92";
            SelectedGuest = guestidarr[37].ToString();
            RoomBorderColorReset();
            btn_Room92.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room93_Click(object sender, EventArgs e)
        {
            SelectedRoom = "93";
            SelectedGuest = guestidarr[38].ToString();
            RoomBorderColorReset();
            btn_Room93.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room94_Click(object sender, EventArgs e)
        {
            SelectedRoom = "94";
            SelectedGuest = guestidarr[39].ToString();
            RoomBorderColorReset();
            btn_Room94.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room95_Click(object sender, EventArgs e)
        {
            SelectedRoom = "95";
            SelectedGuest = guestidarr[40].ToString();
            RoomBorderColorReset();
            btn_Room95.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room96_Click(object sender, EventArgs e)
        {
            SelectedRoom = "96";
            SelectedGuest = guestidarr[41].ToString();
            RoomBorderColorReset();
            btn_Room96.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room101_Click(object sender, EventArgs e)
        {
            SelectedRoom = "101";
            SelectedGuest = guestidarr[42].ToString();
            RoomBorderColorReset();
            btn_Room101.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room102_Click(object sender, EventArgs e)
        {
            SelectedRoom = "102";
            SelectedGuest = guestidarr[43].ToString();
            RoomBorderColorReset();
            btn_Room102.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room103_Click(object sender, EventArgs e)
        {
            SelectedRoom = "103";
            SelectedGuest = guestidarr[44].ToString();
            RoomBorderColorReset();
            btn_Room103.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room104_Click(object sender, EventArgs e)
        {
            SelectedRoom = "104";
            SelectedGuest = guestidarr[45].ToString();
            RoomBorderColorReset();
            btn_Room104.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room105_Click(object sender, EventArgs e)
        {
            SelectedRoom = "105";
            SelectedGuest = guestidarr[46].ToString();
            RoomBorderColorReset();
            btn_Room105.FlatAppearance.BorderColor = Color.White;
        }

        private void btn_Room106_Click(object sender, EventArgs e)
        {
            SelectedRoom = "106";
            SelectedGuest = guestidarr[47].ToString();
            RoomBorderColorReset();
            btn_Room106.FlatAppearance.BorderColor = Color.White;
        }
        

       

        private void btn_Room21_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "21";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room22_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "22";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room23_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "23";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room24_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "24";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room25_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "25";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room26_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "26";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room31_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "31";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room32_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "32";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room33_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "33";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room34_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "34";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room35_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "35";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room36_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "36";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room51_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "51";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room52_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "52";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room53_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "53";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room54_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "54";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room55_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "55";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room56_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "56";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room61_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "61";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room62_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "62";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room63_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "63";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room64_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "64";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room65_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "65";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room66_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "66";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room71_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "71";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room72_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "72";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room73_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "73";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room74_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "74";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room75_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "75";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room76_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "76";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room81_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "81";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room82_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "82";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room83_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room ? ", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "83";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room84_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "84";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room85_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "85";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room86_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "86";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room91_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "91";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room92_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "92";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room93_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "93";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room94_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "94";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room95_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "95";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room96_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "96";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room101_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "101";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room102_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "102";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room103_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "103";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room104_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "104";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room105_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "105";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Room106_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to clean this room?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    selectedRoom = "106";
                    RoomClean();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
        }

        private void btn_Reports_Click(object sender, EventArgs e)
        {
            Reports reports = new Reports();
            this.Hide();
            reports.ShowDialog();
            this.Show();
        }


        private void timer_Tick(object sender, EventArgs e)
        {
            lbl_timer.Text = DateTime.Now.ToString("hh:mm:ss tt");
            if(DateTime.Now.TimeOfDay == TimeSpan.Parse("12:00"))
            {
                Main_Form frm = new Main_Form();
                this.Hide();
                frm.Show();
            }
        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            
            Main_Form frm = new Main_Form();
            this.Hide();
            frm.Show();
        }
    }
}
