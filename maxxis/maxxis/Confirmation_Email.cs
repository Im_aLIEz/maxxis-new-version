﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//Needed for the email
using System.Net.Mail;
using System.Net;

namespace maxxis
{
    public partial class Confirmation_Email : Form
    {
        public Confirmation_Email()
        {
            InitializeComponent();
        }

        private void Confirmation_Email_Load(object sender, EventArgs e)
        {
            Confirmation();
        }

        public void Confirmation()
        {
                var toAddress_confirmation = new MailAddress("ikharimgloria@gmail.com", "Sir Jr");
                var fromAddress_confirmation = new MailAddress("frostmournhunger@gmail.com", "Francis");
                var ccAddress_confirmation = new MailAddress("frostmournhunger@gmail.com", "Tolentino");
                const string fromPassword_confirmation = "bexydxe4yo";
                
                //I dunno about this, seems like a connection string to the email
                var smtp = new SmtpClient
                {
                    //Host is the email extension, {gmail, yahoomail, outlook, etc.} maybe you can edit this by getting and setting a variable
                    Host = "smtp.gmail.com",

                    //i dunno about the port
                    Port = 587,

                    //i don't know this too
                    EnableSsl = true,

                    //The delivery method used for sending the email
                    DeliveryMethod = SmtpDeliveryMethod.Network,

                    //The credentials of the sender, this is a must and correct because you might get an exception error if data inserted here is not correct
                    Credentials = new NetworkCredential(fromAddress_confirmation.Address, fromPassword_confirmation),

                    //Request time out
                    Timeout = 20000
                };


                using (var message = new MailMessage(fromAddress_confirmation, toAddress_confirmation)
                {

                    IsBodyHtml = true, //Regex.IsMatch(txt_codeBlock.Text, @"<\s*([^ >]+)[^>]*>.*?<\s*/\s*\1\s*>"),
                    Subject = "Test",
                    Body = "<!DOCTYPE html>"
                            + "<html>"

                            + "<body>"

                            + "<p style = \"text-align:center \"> <img src = \"http://i345.photobucket.com/albums/p373/adellyinashtear/logo%20final_zpsqrtq1ovx.png \" alt = \"MAXX Hotel Logo \" width = \"100% \" height = \"350 \" align = \"middle \" > </p>"


                            + "<h1 style = \"text-align:center; \"> Welcome to MAXX Hotel Makati </h1>"

                            + "<p style = \"font-family:Times New Roman \"> Dear <span style = \"color: #ff4300; \"> Mr.Suliba, Jan Ryan </span>, </p>"
                            + "<p> </p>"

                            + "<p style = \"font-family:Times New Roman \"> Thank you for choosing to stay at Maxx Hotel Makati.We are pleased to confirm your booking as follows: </p>"
                              + "<p> </p>"
                              + "<p style = \"font-family:Times New Roman \"> Check -in date: <span style = \"color: #ff4300; \"> Tuesday 20 March 2018 </span> </p>"
                              + "<p> </p>"
                              + "<p style = \"font-family:Times New Roman \"> Check -out date: <span style = \"color: #ff4300; \"> Wednesday 21 March 2018 </span> </p>"
                              + "<p> </p>"
                              + "<p style = \"font-family:Times New Roman \"> Number of nights stay: <span style = \"color: #ff4300; \"> 1 night/s </span> </p>"
                              + "<p> </p>"

                              + "<table style = \"width:100% \">"

                              + "<tr>"

                                + "<th style = \"text-align:left; font-family:Times New Roman \"> Item/s </th>"
                                + "<th style = \"text-align:left; font-family:Times New Roman \"> Quantity </th>"
                                + "<th style = \"text-align:left; font-family:Times New Roman \"> Room Rate </th>"
                                + "<th style = \"text-align:left; font-family:Times New Roman \"> Amount Due </th>"

                              + "</tr>"


                              + "<tr>"

                                + "<td style = \"font-family:Times New Roman \"> <b> Urban Room </b> </td>"
                                + "<td style = \"font-family:Times New Roman \"> <b> 1 </b> </td>"
                                + "<td style = \"font-family:Times New Roman \"> <b> 3,900.00 </b> </td>"
                                + "<td style = \"font-family:Times New Roman \"> <b> 3,900.00 </b> </td>"

                             + "</tr>"


                              + "<tr>"

                                + "<td style = \"font-family:Times New Roman \"> <b> Additional companion: Adult </b> </td>"
                                + "<td style = \"font-family:Times New Roman \"> <b> 1 </b> </td>"
                                + "<td style = \"font-family:Times New Roman \"> <b> 750.00 </b> </td>"
                                + "<td style = \"font-family:Times New Roman \"> <b> 750.00 </b> </td>"

                              + "</tr>"


                              + "<tr>"

                                 + "<td style = \"font-family:Times New Roman \"> <b> Additional companion: Child/ren </b> </td>"
                                 + "<td style = \"font-family:Times New Roman \"> <b> 1 </b> </td>"
                                 + "<td style = \"font-family:Times New Roman; border-bottom: 2px solid red \"> <b> 350.00 </b> </td>"
                                 + "<td style = \"font-family:Times New Roman; border-bottom: 2px solid red \"> <b> 350.00 </b> </td>"

                               + "</tr>"


                               + "<tr>"

                               + "<td style = \"font-family:Times New Roman \"> </td>"

                               + "<td style = \"font-family:Times New Roman \"> </td>"

                               + "<td style = \"font-family:Times New Roman; border-top: 2px solid red \"> <b> Total Amount </b> </td>"

                               + "<td style = \"font-family:Times New Roman; border-top: 2px solid red \"> <b> 5,000 </b> </td>"

                               + "</tr>"


                               + "<tr>"

                               + "<td style = \"font-family:Times New Roman \"> </td>"

                               + "<td style = \"font-family:Times New Roman \"> </td>"

                               + "<td style = \"font-family:Times New Roman \"> <b> (Less)Discount </b> </td>"

                               + "<td style = \"font-family:Times New Roman \"> <b> (0.00) </b> </td>"

                               + "</tr>"


                               + "<tr>"

                               + "<td style = \"font-family:Times New Roman \"> </td>"

                               + "<td style = \"font-family:Times New Roman \"> </td>"

                               + "<td style = \"font-family:Times New Roman; border-bottom: 2px solid red \"> <b> (Less)Pre-authorized Amount </b> </td>"

                               + "<td style = \"font-family:Times New Roman; border-bottom: 2px solid red \"> <b> (5, 000) </b> </td>"

                               + "</tr>"


                               + "<tr>"

                               + "<td style = \"font-family:Times New Roman \"> </td>"

                               + "<td style = \"font-family:Times New Roman \"> </td>"

                               + "<td style = \"font-family:Times New Roman; border-top: 2px solid red \"> <b> Balance Due </b> </td>"

                               + "<td style = \"font-family:Times New Roman; border-top: 2px solid red \"> <span style = \"color: #ff4300; \"> <b> 0.00 </b> </span> </td>"

                               + "</tr>"

                              + "</table>"

                                + "<p> </p>"

                                + "<p style = \"font-family:Times New Roman \"> Total number of rooms: <span style = \"color: #ff4300; \"> 1 </span> </p>"
                                + "<p> </p>"


                                + "<p style = \"font-family:Times New Roman \"> Total number of Adult/s(with breakfast): <span style = \"color: #ff4300; \"> 1 </span> </p>"
                                + "<p> </p>"


                                + "<p style = \"font-family:Times New Roman \"> Total number of Child/ren(without breakfast): <span style = \"color: #ff4300; \" > 0 </span> </p>"
                                + "<p> </p>"


                                + "<p style = \"font-family:Times New Roman \"> Additional companion(without breakfast): Adult/s <span style = \"color: #ff4300; \"> 1 </span> Child/ren <span style = \"color: #ff4300; \" > 0 </span> </p>"
                                + "<p> </p>"


                                + "<p style = \"font-family:Times New Roman \"> Inclusions: </p>"
                                + "<p> </p>"


                                + "<p style = \"font-family:Times New Roman; text-indent:50px \"> • Enjoy a sumptuous set breakfast at your leisure from 7:00 AM to 1:00 PM at Luna Restaurant. </p>"
                                + "<p> </p>"


                                + "<p style = \"font-family:Times New Roman; text-indent:50px \"> • Complimentary high-speed WIFI access throughout your stay. </p>"
                                + "<p> </p>"


                                + "<p style = \"font-family:Times New Roman; text-indent:50px \"> • Complimentary use of hotel facilities including the roof deck whirlpool. </p>"
                                + "<p> </p>"


                                + "<p style = \"font-family:Times New Roman; text-indent:50px \"> • A perfect cup of coffee or tea at a touch of a button with express coffee capsule compliments of the hotel. </p>"
                                + "<p> </p>"


                                + "<p style = \"font-family:Times New Roman; text-indent:50px \"> • Others(visit www.maxxhotel.ph) </p>"
                                + "<p> </p>"


                                + "<p style = \"font-family:Times New Roman \"> <b> Note: </b> Expenses incurred during your stay will be on your personal account and will be added to the bill upon check-out </p>"
                                + "<p> </p>"


                                + "<p style = \"font-family:Times New Roman \"> Purpose of stay: <span style = \"color: #ff4300; \"> None </span> </p>"
                                + "<p> </p>"


                                + "<p style = \"font-family:Times New Roman \"> Payment Details: <span style = \"color: #ff4300; \"> None </span> </p>"
                                + "<p> </p>"


                                + "<p style = \"font-family:Times New Roman \"> Pre-authorized amount: <span style = \"color: #ff4300; \"> 5,000 </span> </p>"
                                + "<p> </p>"


                                + "<p style = \"font-size:13px; font-family:Times New Roman \"> *Room rate is quoted in Philippine Peso and is inclusive of goverment tax and VAT(no service charge). </p>"


                                + "<hr>"


                                + "<p style = \"font-size:13px; font-family:Times New Roman \"> <b> Check-in time: </b> 2 PM; <b> Check-out time: </b> 12 NN; Please inform the Front Desk through the express service button in your room telephone if you wish to retain the room for longer period (subject to room availability). The hotel imposes a half-day charge for early check-in and late check-out. </p>"
                                + "<p> </p>"


                                + "<p style = \"font-size:13px; font-family:Times New Roman \"> <b> Identification: </b> A valid government-issued ID(with photo) is presented upon check-in at the hotel. </p>"
                                + "<p> </p>"


                                + "<p style = \"font-size:13px; font-family:Times New Roman \"> <b> No-Smoking: </b> The hotel’s roof deck is the designated smoking area for guests who would like to smoke a cigarette.All floors are non - smoking areas and smoking inside your room is strictly prohibited and is subject to a cleaning fee equivalent to a two(2) night's stay. </p>"
                                + "<p> </p>"


                                + "<p style = \"font-size:13px; font-family:Times New Roman \"> <b> In-room safe. </b> Each room is provided with a digital safe for the safekeeping of your valuable items.We encourage you to use this safe as the hotel and the management will not be responsible for the loss of your personal belongings. </p>"
                                + "<p> </p>"


                                + "<p style = \"font-size:13px; font-family:Times New Roman \"> <b> In-room safe: </b> Each room is provided with a digital safe for the safekeeping of your valuable items.We encourage you to use this safe as the hotel and the management will not be responsible for the loss of your personal belongings. </p>"
                                + "<p> </p>"


                                + "<p style = \"font-size:13px; font-family:Times New Roman \"> <b> Luggage Handling: </b> For your security, please present your luggage claim stub to the attendant to retrieve your belongings.As such, the Hotel will not be held liable to any loss/damage during the handling of your luggage.Utmost care was considered in safekeeping your luggage. </p>"
                                + "<p> </p>"


                                + "<p style = \"font-size:13px; font-family:Times New Roman \"> <b> Terms and Conditions: </b> Please click here to view the terms and condition governing the use of Maxx Hotel online booking system. </p>"
                                + "<p> </p>"



                                + "<p style = \"font-size:13px; font-family:Times New Roman \"> <b> Cancellation: </b> Cancellation of a guaranteed booking must be made five(5) days prior to arrival date.Otherwise, a cancellation or no show fee equivalent to one(1) night stay will be automatically charged to the credit card account you have provided.Cutting short the duration of your stay for any cause will be treated as cancellation of reservation and is subject to the same condition as cancellation. </p>"
                                + "<p> </p>"


                                + "<hr>"


                                + "<center> <p style = \"font-family:Times New Roman \"> <b> Live life to the MAXX. </b> </p> <center>"
                                + "<p> </p>"



                                + "<center> <p style = \"font-family:Times New Roman \"> <b> MAXX Hotel Makati </b> </p> <center>"
                                + "<p> </p>"



                                + "<center> <p style = \"font-family:Times New Roman \"> +63 2 810 MAXX(6299) | stayhappy@maxxhotel.ph | maxxhotel.ph </p> <center>"
                                + "<p> </p>"


                                + "</body>"
                                + "</html>"

                })
                {
                    smtp.Send(message);
                    this.Close();
                }
        }
    }
}
