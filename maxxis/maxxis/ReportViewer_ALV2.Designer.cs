﻿namespace maxxis
{
    partial class ReportViewer_ALV2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.rv_reportViewer_AL = new Microsoft.Reporting.WinForms.ReportViewer();
            this.AL_DataReportV2 = new maxxis.AL_DataReportV2();
            this.tbl_accomodationDetailsReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbl_accomodationDetailsReportTableAdapter = new maxxis.AL_DataReportV2TableAdapters.tbl_accomodationDetailsReportTableAdapter();
            this.btn_loadReport = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.dtp_toDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_fromDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.AL_DataReportV2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbl_accomodationDetailsReportBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // rv_reportViewer_AL
            // 
            reportDataSource3.Name = "RDLC_ALV2";
            reportDataSource3.Value = this.tbl_accomodationDetailsReportBindingSource;
            this.rv_reportViewer_AL.LocalReport.DataSources.Add(reportDataSource3);
            this.rv_reportViewer_AL.LocalReport.ReportEmbeddedResource = "maxxis.Report_RDLC_accommoDationDetailsV2.rdlc";
            this.rv_reportViewer_AL.Location = new System.Drawing.Point(12, 63);
            this.rv_reportViewer_AL.Name = "rv_reportViewer_AL";
            this.rv_reportViewer_AL.ServerReport.BearerToken = null;
            this.rv_reportViewer_AL.Size = new System.Drawing.Size(1260, 616);
            this.rv_reportViewer_AL.TabIndex = 2;
            // 
            // AL_DataReportV2
            // 
            this.AL_DataReportV2.DataSetName = "AL_DataReportV2";
            this.AL_DataReportV2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tbl_accomodationDetailsReportBindingSource
            // 
            this.tbl_accomodationDetailsReportBindingSource.DataMember = "tbl_accomodationDetailsReport";
            this.tbl_accomodationDetailsReportBindingSource.DataSource = this.AL_DataReportV2;
            // 
            // tbl_accomodationDetailsReportTableAdapter
            // 
            this.tbl_accomodationDetailsReportTableAdapter.ClearBeforeFill = true;
            // 
            // btn_loadReport
            // 
            this.btn_loadReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_loadReport.FlatAppearance.BorderSize = 0;
            this.btn_loadReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_loadReport.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_loadReport.ForeColor = System.Drawing.Color.White;
            this.btn_loadReport.Location = new System.Drawing.Point(435, 16);
            this.btn_loadReport.Name = "btn_loadReport";
            this.btn_loadReport.Size = new System.Drawing.Size(108, 30);
            this.btn_loadReport.TabIndex = 101;
            this.btn_loadReport.Text = "LOAD";
            this.btn_loadReport.UseVisualStyleBackColor = false;
            this.btn_loadReport.Click += new System.EventHandler(this.btn_loadReport_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(229, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 17);
            this.label9.TabIndex = 100;
            this.label9.Text = "To Date:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtp_toDate
            // 
            this.dtp_toDate.CalendarFont = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_toDate.CalendarTitleBackColor = System.Drawing.Color.White;
            this.dtp_toDate.CustomFormat = "MM/dd/yyyy";
            this.dtp_toDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_toDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_toDate.Location = new System.Drawing.Point(297, 21);
            this.dtp_toDate.Name = "dtp_toDate";
            this.dtp_toDate.Size = new System.Drawing.Size(113, 22);
            this.dtp_toDate.TabIndex = 99;
            this.dtp_toDate.Value = new System.DateTime(2018, 4, 23, 0, 0, 0, 0);
            // 
            // dtp_fromDate
            // 
            this.dtp_fromDate.CalendarFont = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_fromDate.CalendarTitleBackColor = System.Drawing.Color.White;
            this.dtp_fromDate.CustomFormat = "MM/dd/yyyy";
            this.dtp_fromDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_fromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_fromDate.Location = new System.Drawing.Point(97, 21);
            this.dtp_fromDate.Name = "dtp_fromDate";
            this.dtp_fromDate.Size = new System.Drawing.Size(107, 22);
            this.dtp_fromDate.TabIndex = 98;
            this.dtp_fromDate.Value = new System.DateTime(2018, 4, 23, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 17);
            this.label1.TabIndex = 97;
            this.label1.Text = "From Date:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ReportViewer_ALV2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 691);
            this.Controls.Add(this.btn_loadReport);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dtp_toDate);
            this.Controls.Add(this.dtp_fromDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rv_reportViewer_AL);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ReportViewer_ALV2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ReportViewer_ALV2";
            this.Load += new System.EventHandler(this.ReportViewer_ALV2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AL_DataReportV2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbl_accomodationDetailsReportBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Microsoft.Reporting.WinForms.ReportViewer rv_reportViewer_AL;
        private System.Windows.Forms.BindingSource tbl_accomodationDetailsReportBindingSource;
        private AL_DataReportV2 AL_DataReportV2;
        private AL_DataReportV2TableAdapters.tbl_accomodationDetailsReportTableAdapter tbl_accomodationDetailsReportTableAdapter;
        private System.Windows.Forms.Button btn_loadReport;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtp_toDate;
        private System.Windows.Forms.DateTimePicker dtp_fromDate;
        private System.Windows.Forms.Label label1;
    }
}