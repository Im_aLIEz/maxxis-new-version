﻿namespace maxxis
{
    partial class CompanionName_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lbl_additionalCompanionNames = new System.Windows.Forms.Label();
            this.dgv_listOfNames = new System.Windows.Forms.DataGridView();
            this.col_firsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_middleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_lastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_companion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lbl_additionalCompanionNames = new System.Windows.Forms.Label();
            this.dgv_listOfNames = new System.Windows.Forms.DataGridView();
            this.txt_firstName = new System.Windows.Forms.TextBox();
            this.lbl_firstName = new System.Windows.Forms.Label();
            this.txt_middleName = new System.Windows.Forms.TextBox();
            this.txt_lastName = new System.Windows.Forms.TextBox();
            this.lbl_middleName = new System.Windows.Forms.Label();
            this.lbl_lastName = new System.Windows.Forms.Label();
            this.btn_GoBack = new System.Windows.Forms.Button();
            this.btn_remove = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_edit = new System.Windows.Forms.Button();
            this.cmb_companion = new System.Windows.Forms.ComboBox();
            this.lbl_companion = new System.Windows.Forms.Label();
            this.pcb_image = new System.Windows.Forms.PictureBox();
            this.btn_attachImage = new System.Windows.Forms.Button();
            this.btn_scanImage = new System.Windows.Forms.Button();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listOfNames)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_image)).BeginInit();
            this.col_firsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_middleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_lastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_companion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listOfNames)).BeginInit();
            this.SuspendLayout();
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.lbl_additionalCompanionNames);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(632, 31);
            this.panel7.TabIndex = 13;
            // 
            // lbl_additionalCompanionNames
            // 
            this.lbl_additionalCompanionNames.AutoSize = true;
            this.lbl_additionalCompanionNames.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_additionalCompanionNames.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_additionalCompanionNames.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_additionalCompanionNames.Location = new System.Drawing.Point(11, 8);
            this.lbl_additionalCompanionNames.Name = "lbl_additionalCompanionNames";
            this.lbl_additionalCompanionNames.Size = new System.Drawing.Size(213, 15);
            this.lbl_additionalCompanionNames.TabIndex = 0;
            this.lbl_additionalCompanionNames.Text = "ADDITIONAL COMPANION NAMES";
            // 
            // dgv_listOfNames
            // 
            this.dgv_listOfNames.AllowUserToAddRows = false;
            this.dgv_listOfNames.AllowUserToDeleteRows = false;
            this.dgv_listOfNames.BackgroundColor = System.Drawing.Color.White;
            this.dgv_listOfNames.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;

            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_listOfNames.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_listOfNames.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;

            this.dgv_listOfNames.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_listOfNames.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_firsName,
            this.col_middleName,
            this.col_lastName,
            this.col_companion});
            this.dgv_listOfNames.EnableHeadersVisualStyles = false;
            this.dgv_listOfNames.Location = new System.Drawing.Point(12, 238);
            this.dgv_listOfNames.Location = new System.Drawing.Point(15, 96);
            this.dgv_listOfNames.Name = "dgv_listOfNames";
            this.dgv_listOfNames.ReadOnly = true;
            this.dgv_listOfNames.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_listOfNames.RowHeadersVisible = false;
            this.dgv_listOfNames.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;

            this.dgv_listOfNames.Size = new System.Drawing.Size(608, 187);
            this.dgv_listOfNames.TabIndex = 14;
            // 
            // col_firsName
            // 
            this.col_firsName.HeaderText = "FIRST NAME";
            this.col_firsName.Name = "col_firsName";
            this.col_firsName.ReadOnly = true;
            this.col_firsName.Width = 150;
            // 
            // col_middleName
            // 
            this.col_middleName.HeaderText = "MIDDLE NAME";
            this.col_middleName.Name = "col_middleName";
            this.col_middleName.ReadOnly = true;
            this.col_middleName.Width = 150;
            // 
            // col_lastName
            // 
            this.col_lastName.HeaderText = "LAST NAME";
            this.col_lastName.Name = "col_lastName";
            this.col_lastName.ReadOnly = true;
            this.col_lastName.Width = 150;
            // 
            // col_companion
            // 
            this.col_companion.HeaderText = "COMPANION TYPE";
            this.col_companion.Name = "col_companion";
            this.col_companion.ReadOnly = true;
            this.col_companion.Width = 150;
            // 
            // txt_firstName
            // 
            this.txt_firstName.Location = new System.Drawing.Point(15, 200);
            this.dgv_listOfNames.Size = new System.Drawing.Size(605, 183);
            this.dgv_listOfNames.TabIndex = 14;
            // 
            // txt_firstName
            // 
            this.txt_firstName.Location = new System.Drawing.Point(15, 47);
            this.txt_firstName.Name = "txt_firstName";
            this.txt_firstName.Size = new System.Drawing.Size(155, 20);
            this.txt_firstName.TabIndex = 15;
            // 
            // lbl_firstName
            // 
            this.lbl_firstName.AutoSize = true;
            this.lbl_firstName.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

            this.lbl_firstName.Location = new System.Drawing.Point(12, 183);
            this.lbl_firstName.Location = new System.Drawing.Point(46, 70);
            this.lbl_firstName.Name = "lbl_firstName";
            this.lbl_firstName.Size = new System.Drawing.Size(89, 14);
            this.lbl_firstName.TabIndex = 16;
            this.lbl_firstName.Text = "FIRST NAME: *";
            // 
            // txt_middleName
            // 
            this.txt_middleName.Location = new System.Drawing.Point(176, 200);
            this.txt_middleName.Location = new System.Drawing.Point(176, 47);
            this.txt_middleName.Name = "txt_middleName";
            this.txt_middleName.Size = new System.Drawing.Size(155, 20);
            this.txt_middleName.TabIndex = 17;
            // 
            // txt_lastName
            // 
            this.txt_lastName.Location = new System.Drawing.Point(337, 200);
            this.txt_lastName.Location = new System.Drawing.Point(337, 47);
            this.txt_lastName.Name = "txt_lastName";
            this.txt_lastName.Size = new System.Drawing.Size(156, 20);
            this.txt_lastName.TabIndex = 18;
            // 
            // lbl_middleName
            // 
            this.lbl_middleName.AutoSize = true;
            this.lbl_middleName.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

            this.lbl_middleName.Location = new System.Drawing.Point(173, 183);

            this.lbl_middleName.Location = new System.Drawing.Point(208, 70);

            this.lbl_middleName.Name = "lbl_middleName";
            this.lbl_middleName.Size = new System.Drawing.Size(91, 14);
            this.lbl_middleName.TabIndex = 19;
            this.lbl_middleName.Text = "MIDDLE NAME:";
            // 
            // lbl_lastName
            // 
            this.lbl_lastName.AutoSize = true;
            this.lbl_lastName.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

            this.lbl_lastName.Location = new System.Drawing.Point(334, 183);

            this.lbl_lastName.Location = new System.Drawing.Point(374, 70);
            this.lbl_lastName.Name = "lbl_lastName";
            this.lbl_lastName.Size = new System.Drawing.Size(84, 14);
            this.lbl_lastName.TabIndex = 20;
            this.lbl_lastName.Text = "LAST NAME: *";
            // 
            // btn_GoBack
            // 
            this.btn_GoBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_GoBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_GoBack.FlatAppearance.BorderSize = 0;
            this.btn_GoBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_GoBack.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_GoBack.ForeColor = System.Drawing.Color.White;
            this.btn_GoBack.Image = global::maxxis.Properties.Resources.Go_Back;
            this.btn_GoBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;

            this.btn_GoBack.Location = new System.Drawing.Point(508, 437);

            this.btn_GoBack.Location = new System.Drawing.Point(511, 291);

            this.btn_GoBack.Name = "btn_GoBack";
            this.btn_GoBack.Size = new System.Drawing.Size(109, 32);
            this.btn_GoBack.TabIndex = 21;
            this.btn_GoBack.Text = "GO BACK";
            this.btn_GoBack.UseVisualStyleBackColor = false;
            // 
            // btn_remove
            // 
            this.btn_remove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_remove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_remove.FlatAppearance.BorderSize = 0;
            this.btn_remove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_remove.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_remove.ForeColor = System.Drawing.Color.White;
            this.btn_remove.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;

            this.btn_remove.Location = new System.Drawing.Point(393, 437);

            this.btn_remove.Location = new System.Drawing.Point(396, 291);

            this.btn_remove.Name = "btn_remove";
            this.btn_remove.Size = new System.Drawing.Size(109, 32);
            this.btn_remove.TabIndex = 22;
            this.btn_remove.Text = "REMOVE";
            this.btn_remove.UseVisualStyleBackColor = false;
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_add.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_add.FlatAppearance.BorderSize = 0;
            this.btn_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add.ForeColor = System.Drawing.Color.White;
            this.btn_add.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;

            this.btn_add.Location = new System.Drawing.Point(163, 437);

            this.btn_add.Location = new System.Drawing.Point(166, 291);

            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(109, 32);
            this.btn_add.TabIndex = 23;
            this.btn_add.Text = "ADD";
            this.btn_add.UseVisualStyleBackColor = false;
            // 
            // btn_edit
            // 
            this.btn_edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_edit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_edit.FlatAppearance.BorderSize = 0;
            this.btn_edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_edit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_edit.ForeColor = System.Drawing.Color.White;
            this.btn_edit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;

            this.btn_edit.Location = new System.Drawing.Point(278, 437);

            this.btn_edit.Location = new System.Drawing.Point(281, 291);

            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(109, 32);
            this.btn_edit.TabIndex = 24;
            this.btn_edit.Text = "EDIT";
            this.btn_edit.UseVisualStyleBackColor = false;
            // 
            // cmb_companion
            // 
            this.cmb_companion.FormattingEnabled = true;

            this.cmb_companion.Location = new System.Drawing.Point(499, 199);

            this.cmb_companion.Location = new System.Drawing.Point(499, 46);

            this.cmb_companion.Name = "cmb_companion";
            this.cmb_companion.Size = new System.Drawing.Size(121, 21);
            this.cmb_companion.TabIndex = 25;
            // 
            // lbl_companion
            // 
            this.lbl_companion.AutoSize = true;
            this.lbl_companion.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

            this.lbl_companion.Location = new System.Drawing.Point(496, 182);

            this.lbl_companion.Location = new System.Drawing.Point(518, 70);

            this.lbl_companion.Name = "lbl_companion";
            this.lbl_companion.Size = new System.Drawing.Size(90, 14);
            this.lbl_companion.TabIndex = 26;
            this.lbl_companion.Text = "COMPANION: *";
            // 
            // pcb_image
            // 
            this.pcb_image.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pcb_image.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcb_image.Location = new System.Drawing.Point(15, 37);
            this.pcb_image.Name = "pcb_image";
            this.pcb_image.Size = new System.Drawing.Size(154, 134);
            this.pcb_image.TabIndex = 27;
            this.pcb_image.TabStop = false;
            // 
            // btn_attachImage
            // 
            this.btn_attachImage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_attachImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_attachImage.FlatAppearance.BorderSize = 0;
            this.btn_attachImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_attachImage.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_attachImage.ForeColor = System.Drawing.Color.White;
            this.btn_attachImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_attachImage.Location = new System.Drawing.Point(180, 37);
            this.btn_attachImage.Name = "btn_attachImage";
            this.btn_attachImage.Size = new System.Drawing.Size(151, 32);
            this.btn_attachImage.TabIndex = 28;
            this.btn_attachImage.Text = "ATTACH IMAGE";
            this.btn_attachImage.UseVisualStyleBackColor = false;
            // 
            // btn_scanImage
            // 
            this.btn_scanImage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_scanImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_scanImage.FlatAppearance.BorderSize = 0;
            this.btn_scanImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_scanImage.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_scanImage.ForeColor = System.Drawing.Color.White;
            this.btn_scanImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_scanImage.Location = new System.Drawing.Point(180, 75);
            this.btn_scanImage.Name = "btn_scanImage";
            this.btn_scanImage.Size = new System.Drawing.Size(151, 32);
            this.btn_scanImage.TabIndex = 29;
            this.btn_scanImage.Text = "SCAN IMAGE";
            this.btn_scanImage.UseVisualStyleBackColor = false;

            // col_firsName
            // 
            this.col_firsName.HeaderText = "FIRST NAME";
            this.col_firsName.Name = "col_firsName";
            this.col_firsName.ReadOnly = true;
            this.col_firsName.Width = 150;
            // 
            // col_middleName
            // 
            this.col_middleName.HeaderText = "MIDDLE NAME";
            this.col_middleName.Name = "col_middleName";
            this.col_middleName.ReadOnly = true;
            this.col_middleName.Width = 150;
            // 
            // col_lastName
            // 
            this.col_lastName.HeaderText = "LAST NAME";
            this.col_lastName.Name = "col_lastName";
            this.col_lastName.ReadOnly = true;
            this.col_lastName.Width = 150;
            // 
            // col_companion
            // 
            this.col_companion.HeaderText = "COMPANION TYPE";
            this.col_companion.Name = "col_companion";
            this.col_companion.ReadOnly = true;
            this.col_companion.Width = 150;

            // 
            // CompanionName_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;

            this.ClientSize = new System.Drawing.Size(632, 482);
            this.Controls.Add(this.btn_scanImage);
            this.Controls.Add(this.btn_attachImage);
            this.Controls.Add(this.pcb_image);

            this.ClientSize = new System.Drawing.Size(632, 336);
            this.Controls.Add(this.lbl_companion);
            this.Controls.Add(this.cmb_companion);
            this.Controls.Add(this.btn_edit);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.btn_remove);
            this.Controls.Add(this.btn_GoBack);
            this.Controls.Add(this.lbl_lastName);
            this.Controls.Add(this.lbl_middleName);
            this.Controls.Add(this.txt_lastName);
            this.Controls.Add(this.txt_middleName);
            this.Controls.Add(this.lbl_firstName);
            this.Controls.Add(this.txt_firstName);
            this.Controls.Add(this.dgv_listOfNames);
            this.Controls.Add(this.panel7);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CompanionName_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CompanionName_Form";
            this.Load += new System.EventHandler(this.CompanionName_Form_Load);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listOfNames)).EndInit();

            ((System.ComponentModel.ISupportInitialize)(this.pcb_image)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lbl_additionalCompanionNames;
        private System.Windows.Forms.DataGridView dgv_listOfNames;
        private System.Windows.Forms.TextBox txt_firstName;
        private System.Windows.Forms.Label lbl_firstName;
        private System.Windows.Forms.TextBox txt_middleName;
        private System.Windows.Forms.TextBox txt_lastName;
        private System.Windows.Forms.Label lbl_middleName;
        private System.Windows.Forms.Label lbl_lastName;
        private System.Windows.Forms.Button btn_GoBack;
        private System.Windows.Forms.Button btn_remove;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_edit;
        private System.Windows.Forms.ComboBox cmb_companion;
        private System.Windows.Forms.Label lbl_companion;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_firsName;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_middleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_lastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_companion;
        private System.Windows.Forms.PictureBox pcb_image;
        private System.Windows.Forms.Button btn_attachImage;
        private System.Windows.Forms.Button btn_scanImage;
    }
}