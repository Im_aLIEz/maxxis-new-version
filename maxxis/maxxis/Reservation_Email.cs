﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

//Needed for the email
using System.Net.Mail;
using System.Net;

namespace maxxis
{
    public partial class Reservation_Email : Form
    {
        string con = System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString;
        //email and password of the sender
        private string email_sender;
        private string email_password;

        //Condition variable for the Query
        private string email_receiver;
        private string firstName;
        private string lastName;

        //methods
        /*
        public void DisplayBilling()
        {
            Main_Form mainfrm = new Main_Form();
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT * FROM dbo.tbl_billing Where guest_id = '" + mainfrm.SelectedGuest + "'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    txt_requiredPrePayment.Text = item[1].ToString();
                    txt_cashPayment.Text = item[5].ToString();
                    if (Int32.Parse(ucrselect) != 0)
                    {
                        dgv_billingSummary.Rows.Add("Urban Corner Room", ucrselect, "ROOM", item[9].ToString(), Int32.Parse(ucrselect) * Int32.Parse(item[9].ToString()));
                    }
                    if (Int32.Parse(urselect) != 0)
                    {
                        dgv_billingSummary.Rows.Add("Urban Room", urselect, "ROOM", item[8].ToString(), Int32.Parse(urselect) * Int32.Parse(item[8].ToString()));
                    }
                    if (Int32.Parse(uvrselect) != 0)
                    {
                        dgv_billingSummary.Rows.Add("Urban View Room", uvrselect, "ROOM", item[10].ToString(), Int32.Parse(uvrselect) * Int32.Parse(item[10].ToString()));
                    }
                    if (Decimal.Parse(item[7].ToString()) != 0)
                    {
                        dgv_billingSummary.Rows.Add("Items/Services", "", "Item/Services", "", item[7].ToString());
                    }
                    dgv_payments.Rows.Clear();
                    dgv_payments.Rows.Add("Total Amount", item[2]);
                    dgv_payments.Rows.Add("(Less) Discount");
                    dgv_payments.Rows.Add("(Less) Pre-Authorized Amount", item[1]);
                    txt_otherCharges.Text = item[7].ToString();
                    txt_subTotal.Text = item[1].ToString();
                    txt_totalPayment.Text = item[2].ToString();
                    txt_balance.Text = (Decimal.Parse(item[2].ToString()) - Decimal.Parse(item[5].ToString())).ToString("N2");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void DisplayGuestInfo()
        {
            Main_Form mainfrm = new Main_Form();
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT * FROM dbo.tbl_guestinfo where id = '" + mainfrm.SelectedGuest + "'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    cbox_title.Text = item[1].ToString();
                    txt_lastname.Text = item[2].ToString();
                    txt_firstname.Text = item[3].ToString();
                    txt_middleName.Text = item[4].ToString();
                    dtb_dateOfBirth.Text = item[5].ToString();
                    txt_contactDetails.Text = item[6].ToString();
                    txt_address.Text = item[7].ToString();
                    txt_email.Text = item[8].ToString();
                    txt_province.Text = item[9].ToString();
                    cmb_homeCountry.Text = item[10].ToString();
                    txt_companyAgency.Text = item[11].ToString();
                    txt_city.Text = item[12].ToString();
                    txt_guestRemarks.Text = item[20].ToString();
                    txt_serialNumber.Text = item[21].ToString();
                    urselect = item[17].ToString();
                    ucrselect = item[18].ToString();
                    uvrselect = item[19].ToString();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void DisplayPayments()
        {
            Main_Form mainfrm = new Main_Form();
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT * FROM dbo.tbl_billing Where guest_id = '" + mainfrm.SelectedGuest + "'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void DisplayBusiness()
        {
            Main_Form mainfrm = new Main_Form();
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT * FROM dbo.tbl_billing Where guest_id = '" + mainfrm.SelectedGuest + "'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void DisplayStayInfo()
        {
            Main_Form mainfrm = new Main_Form();
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT * FROM dbo.tbl_stayinfo Where guest_id = '" + mainfrm.SelectedGuest + "'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    dtp_arrivalDate.Text = item[2].ToString();
                    dtp_departureDate.Text = item[3].ToString();
                    txt_noofnights.Text = item[4].ToString();
                    txt_adults.Text = item[6].ToString();
                    txt_child.Text = item[7].ToString();
                    txt_totalNumberOfAdults.Text = item[6].ToString();
                    total_numberOfChildren.Text = item[7].ToString();
                    lbl_additionalCompanionAdults.Text = "Adult/s(" + item[8] + ")";
                    lbl_additionalCompanionChildren.Text = "Child/ren(" + item[9] + ")";
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        */
        public string EMAIL
        {
            get { return email_receiver; }
            set { email_receiver = value; }
        }

        public string FIRSTNAME
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public string LASTNAME
        {
            get { return lastName; }
            set { lastName = value; }
        }
        //variables to be filled including the variables above
        private string email_Receiver;
        private string title;
        private string fullname;
        private string checkIn;
        private string checkOut;
        private string numberOfNights;
        private string totalNumberOfRooms;
        private string totalNumberOfAdults;
        private string totalNumberOfChildrens;
        private string additionalCompanion_Adult;
        private string additionalCompanion_Children;
        private string airportPickVia;
        private string flightNumber;
        private string pickupTime;
        private string referenceNumber;

        public Reservation_Email()
        {
            InitializeComponent();
        }

        private void Reservation_Email_Load(object sender, EventArgs e)
        {
            //query first before all
            //insert query here with the email and fullname to fill the WHERE CONDITION;

            fullname = title + " " + lastName + ", " + firstName;

            Reservation();

            
        }

        public void Reservation()
        {

            var toAddress_reservation = new MailAddress(email_Receiver, firstName + " " + lastName);
            var fromAddress_reservation = new MailAddress(email_sender, "MAXX HOTEL MAKATI");
            const string fromPassword_reservation = "INSERT PASSWORD HERE";

            //I dunno about this, seems like a connection string to the email
            var smtp = new SmtpClient
            {
                //Host is the email extension, {gmail, yahoomail, outlook, etc.} maybe you can edit this by getting and setting a variable
                Host = "smtp.gmail.com",

                //i dunno about the port
                Port = 587,

                //i don't know this too
                EnableSsl = true,

                //The delivery method used for sending the email
                DeliveryMethod = SmtpDeliveryMethod.Network,

                //The credentials of the sender, this is a must and correct because you might get an exception error if data inserted here is not correct
                Credentials = new NetworkCredential(fromAddress_reservation.Address, fromPassword_reservation),

                //Request time out
                Timeout = 20000
            };


            using (var message = new MailMessage(fromAddress_reservation, toAddress_reservation)
            {

                IsBodyHtml = true, //Regex.IsMatch(txt_codeBlock.Text, @"<\s*([^ >]+)[^>]*>.*?<\s*/\s*\1\s*>"),
                Subject = "Maxx Hotel Makati: Reservation",
                Body = "<!DOCTYPE html>"
                        + "<html>"

                        + "<body>"
                        //+ "<p style=\"text-align:center \"> <img src=\"http://i345.photobucket.com/albums/p373/adellyinashtear/logo%20final_zpsqrtq1ovx.png \" alt = \"MAXX Hotel Logo \" width = \"100% \" height = \"350 \" align = \"middle \"> </p>"


                        + "<h1 style = \"text-align:center; \"> RESERVATION </h1>"

                        //full name of the receiver
                        + "<p style = \"font-family:Times New Roman \"> Dear<span style=\"color:#ff4300; \"> " + fullname + "</span>, </p>"
                        + "<p> </p>"

                        + "<p style = \"font-family:Times New Roman \"> Thank you for choosing to stay at Maxx Hotel Makati. Please see below your booking details: </p>"
                        + "<p> </p>"

                        //check in date
                        + "<p style = \"font-family:Times New Roman \"> Check-in date: <span style= \"color:#ff4300; \"> " + checkIn + "</span> </p>"
                        + "<p> </p>"

                        //check out date
                        + "<p style = \"font-family:Times New Roman \"> Check-out date: <span style= \"color:#ff4300; \"> " + checkOut + "</span> </p>"
                        + "<p> </p>"

                        //number of nights
                        + "<p style = \"font-family:Times New Roman \"> Number of nights stay: <span style= \"color:#ff4300; \"> " + numberOfNights + " night/s </span> </p>"
                        + "<p> </p>"


                        //IGNORE THIS FOR NOW!!!

                          /*
                          //Here is the start of the table
                          + "<table style = \"width:100% \">"


                            + "<tr>"

                                + "<th style = \"text-align:left; font-family:Times New Roman \">Item/s</th>"

                                + "<th style = \"text-align:left; font-family:Times New Roman \">Quantity</th>"

                                + "<th style = \"text-align:left; font-family:Times New Roman \">Room Rate</th>"

                                + "<th style = \"text-align:left; font-family:Times New Roman \">Amount Due</th>"

                            + "</tr>"


                            + "<tr>"

                              + "<td style = \"font-family:Times New Roman \"> <b> Urban Room </b> </td>"

                              + "<td style = \"font-family:Times New Roman \"> <b> 1 </b> </td>"

                              + "<td style = \"font-family:Times New Roman \"> <b> 3,900.00 </b> </td>"

                              + "<td style = \"font-family:Times New Roman \"> <b> 3,900.00 </b> </td>"

                           + "</tr>"


                           + "<tr>"

                              + "<td style = \"font-family:Times New Roman \"> <b> Additional companion: Adult </b> </td>"

                              + "<td style = \"font-family:Times New Roman \"> <b> 1 </b> </td>"

                              + "<td style = \"font-family:Times New Roman \"> <b> 750.00 </b> </td>"

                              + "<td style = \"font-family:Times New Roman \"> <b> 750.00 </b> </td>"

                           + "</tr>"

                           //This part is the last part of the table and must contain the border-bottom:2px solid red for the design
                           + "<tr>"

                              + "<td style = \"font-family:Times New Roman \"> <b> Additional companion: Child/ren </b> </td>"

                              + "<td style = \"font-family:Times New Roman \"> <b> 1 </b> </td>"

                              + "<td style = \"font-family:Times New Roman; border-bottom: 2px solid red \"> <b> 350.00 </b> </td>"

                              + "<td style = \"font-family:Times New Roman; border-bottom: 2px solid red \"> <b> 350.00 </b> </td>"

                               + "</tr>"


                           //This is the bottom of the table, this must not be change because here resides the holder of the total amounts inserted in the table
                           + "<tr>"
                           + "<td style = \"font-family:Times New Roman \"> </td>"
                           + "<td style = \"font-family:Times New Roman \"> </td>"
                           + "<td style = \"font-family:Times New Roman; border-top: 2px solid red \"> <b> Total Amount </b> </td>"
                           + "<td style = \"font-family:Times New Roman; border-top: 2px solid red \"> <b> 5,000 </b> </td>"
                           + "</tr>"


                           + "<tr>"
                           + "<td style = \"font-family:Times New Roman \"> </td>"
                           + "<td style = \"font-family:Times New Roman \"> </td>"
                           + "<td style = \"font-family:Times New Roman \"> <b> (Less)Discount </b> </td>"
                           + "<td style = \"font-family:Times New Roman \"> <b> (0.00) </b> </td>"
                           + "</tr>"


                           + "<tr>"
                           + "<td style = \"font-family:Times New Roman \"> </td>"
                           + "<td style = \"font-family:Times New Roman \"> </td>"
                           + "<td style = \"font-family:Times New Roman; border-bottom: 2px solid red \"> <b> (Less)Pre-authorized Amount </b> </td>"
                           + "<td style = \"font-family:Times New Roman; border-bottom: 2px solid red \"> <b> (5,000) </b> </td>"
                           + "</tr>"


                           + "<tr>"
                           + "<td style = \"font-family:Times New Roman \"> </td>"
                           + "<td style = \"font-family:Times New Roman \"> </td>"
                           + "<td style = \"font-family:Times New Roman; border-top: 2px solid red \"> <b> Balance Due </b> </td>"
                           + "<td style = \"font-family:Times New Roman; border-top: 2px solid red \"> <span style = \"color: #ff4300; \"> <b> 0.00 </b> </span> </td>"
                           + "</tr>"

                          + "</table>"
                          */


                          + "<p> </p>"

                          //Total number of rooms
                          + "<p style = \"font-family:Times New Roman \"> Total number of rooms: <span style = \"color:#ff4300; \"> " + totalNumberOfRooms +"</span> </p>"
                          + "<p> </p>"

                          //Total number of adults
                          + "<p style = \"font-family:Times New Roman \"> Total number of Adult/s (with breakfast): <span style= \"color: #ff4300; \"> " + totalNumberOfAdults + "</span> </p>"
                          + "<p> </p>"

                          //Total Number of child/ren
                          + "<p style = \"font-family:Times New Roman \"> Total number of Child/ren(without breakfast): <span style = \"color: #ff4300; \"> " + totalNumberOfChildrens + "</span> </p>"
                          + "<p> </p>"

                          //Additional Companions
                          + "<p style = \"font-family:Times New Roman \"> Additional companion(without breakfast): Adult/s <span style = \"color: #ff4300; \"> " + additionalCompanion_Adult + "</span> Child/ren <span style = \"color:#ff4300; \">" + additionalCompanion_Children + "</span> </p>"
                          + "<p> </p>"


                          + "<p style = \"font-family:Times New Roman \"> Inclusions: </p>"
                          + "<p> </p>"


                          + "<p style = \"font-family:Times New Roman; text-indent:50px \"> • Enjoy a sumptuous set breakfast at your leisure from 7:00 AM to 1:00 PM at Luna Restaurant. </p>"
                          + "<p> </p>"


                          + "<p style = \"font-family:Times New Roman; text-indent:50px \"> • Complimentary high-speed WIFI access throughout your stay. </p>"
                          + "<p> </p>"


                          + "<p style = \"font-family:Times New Roman; text-indent:50px \"> • Complimentary use of hotel facilities including the roof deck whirlpool. </p>"
                          + "<p> </p>"


                          + "<p style = \"font-family:Times New Roman; text-indent:50px \"> • A perfect cup of coffee or tea at a touch of a button with express coffee capsule compliments of the hotel. </p>"
                          + "<p> </p>"


                          + "<p style = \"font-family:Times New Roman; text-indent:50px \"> • Others(visit www.maxxhotel.ph) </p>"
                          + "<p> </p>"


                          + "<p style = \"font-family:Times New Roman; padding-top:5px \"> Special Arrangements: </p>"
                          + "<p> </p>"


                          + "<p style = \"font-family:Times New Roman; text-indent:30px \"> Airport pick-up via: <span style = \"color:#ff4300; \"> None </span> </p>"
                          + "<p> </p>"


                          + "<p style = \"font-family:Times New Roman; text-indent:30px \"> Flight number: <span style = \"color:#ff4300; \"> None </span> </p>"
                          + "<p > </p>"


                          + "<p style = \"font-family:Times New Roman; text-indent:30px \"> Pick-up time: <span style = \"color:#ff4300; \"> None </span> </p>"
                          + "<p> </p>"


                          + "<p style = \"font-family:Times New Roman; text-indent:30px \"> Purpose of stay: <span style = \"color: #ff4300; \"> None </span> </p>"
                          + "<p> </p>"


                          + "<p style = \"font-family:Times New Roman \"> Reservation reference number: <span style = \"color: #ff4300; \"> " + referenceNumber + "</span> </p>"
                          + "<p> </p>"


                          + "<p style = \"font-size:13px; font-family:Times New Roman \"> *Room rate is quoted in Philippine Peso and is inclusive of goverment tax and VAT (no service charge). </p>"


                          + "<hr>"


                          + "<p style = \"font-size:13px; font-family:Times New Roman \"> <b> Guarantee: </b> To confirm this reservation, an amount equivalent to one(1) night stay will be pre-authorized prior to arrival through your credit card. All non-guaranteed reservations will be automatically released after 6 PM of the last option dates given to confirm. (i.e. if reserving 9 days or more prior to check-in date, 48 hours to confirm; if reserving 2 days to 8 days prior, 24 hours to confirm; reserving on the same day of check-in must always be confirmed.) Please click the link below to confirm. </p>"
                          + "<p> </p>"


                          + "<p style = \"font-size:13px; font-family:Times New Roman \"> <b> Check-in time: </b> 2 PM; <b> Check-out time: </b> 12 NN.Please inform the Front Desk through the express service button in your room telephone if you wish to retain the room for longer period (subject to room availability). The hotel imposes a half-day charge for early check-in and late check-out. </p>"
                          + "<p> </p>"


                          + "<p style = \"font-size:13px; font-family:Times New Roman \"> <b> Identification: </b> A valid government-issued ID (with photo) is presented upon check-in at the hotel. </p>"
                          + "<p> </p>"


                          + "<p style = \"font-size:13px; font-family:Times New Roman \"> <b> No-Smoking: </b> The hotel’s roof deck is the designated smoking area for guests who would like to smoke a cigarette.All floors are non-smoking areas and smoking inside your room is strictly prohibited and is subject to a cleaning fee equivalent to a two(2) nights stay. </p>"
                          + "<p> </p>"


                          + "<p style = \"font-size:13px; font-family:Times New Roman \"> <b> Cancellation: </b> Cancellation of a guaranteed booking must be made five(5) days prior to arrival date.Otherwise, a cancellation or no show fee equivalent to one(1) night stay will be automatically charged to the credit card account you have provided. </p>"
                          + "<p> </p>"


                          + "<hr>"


                          + "<center> <button type = \"button \" style = \"font-size:20px; background:#017c18; border:#017c18; width:400px; height: 50px; color:#ffffff \"> <b> Confirm Booking </b> </button> </center>"
                          + "<p> </p>"


                          + "<center> <p style = \"font-family:Times New Roman \"> <b> Live life to the MAXX. </b> </p> <center>"
                          + "<p> </p>"


                          + "<center> <p style = \"font-family:Times New Roman \"> <b> MAXX Hotel Makati </b> </p> <center>"
                          + "<p> </p>"


                          + "<center> <p style = \"font-family:Times New Roman \"> +63 2 810 MAXX(6299) | stayhappy@maxxhotel.ph | maxxhotel.ph </p> <center>"
                          + "<p> </p>"


                          + "</body>"
                          + "</html>"



            })
            {
                smtp.Send(message);
                this.Close();
            }
        }
    }
}
