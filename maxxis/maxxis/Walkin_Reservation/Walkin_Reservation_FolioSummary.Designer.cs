﻿namespace maxxis.Walkin_Reservation
{
    partial class Walkin_Reservation_FolioSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl_folioSummary = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_rooms = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_balance = new System.Windows.Forms.TextBox();
            this.txt_totalPayment = new System.Windows.Forms.TextBox();
            this.txt_totalDiscount = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_otherCharges = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txt_fee_specialArrangements = new System.Windows.Forms.TextBox();
            this.lbl_specialArrangements = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_fee_additionalAdultsAndChildren = new System.Windows.Forms.TextBox();
            this.txt_fee_totalNumberOfRooms = new System.Windows.Forms.TextBox();
            this.lbl_additionalCompanionChildren = new System.Windows.Forms.Label();
            this.lbl_additionalCompanionAdults = new System.Windows.Forms.Label();
            this.total_numberOfChildren = new System.Windows.Forms.TextBox();
            this.txt_totalNumberOfAdults = new System.Windows.Forms.TextBox();
            this.txt_totalNumberOfRooms = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.summary_panel = new System.Windows.Forms.Panel();
            this.txt_folioSummary_balanceDue = new System.Windows.Forms.TextBox();
            this.lbl_balanceDue_folioSummary = new System.Windows.Forms.Label();
            this.lbl_folioSummary = new System.Windows.Forms.Label();
            this.pnl_folioSummary.SuspendLayout();
            this.panel1.SuspendLayout();
            this.summary_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_folioSummary
            // 
            this.pnl_folioSummary.AutoScrollMargin = new System.Drawing.Size(0, 100);
            this.pnl_folioSummary.BackColor = System.Drawing.Color.Silver;
            this.pnl_folioSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_folioSummary.Controls.Add(this.panel1);
            this.pnl_folioSummary.Controls.Add(this.label21);
            this.pnl_folioSummary.Controls.Add(this.summary_panel);
            this.pnl_folioSummary.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_folioSummary.Location = new System.Drawing.Point(0, 0);
            this.pnl_folioSummary.Name = "pnl_folioSummary";
            this.pnl_folioSummary.Size = new System.Drawing.Size(870, 479);
            this.pnl_folioSummary.TabIndex = 23;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.lbl_rooms);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.txt_balance);
            this.panel1.Controls.Add(this.txt_totalPayment);
            this.panel1.Controls.Add(this.txt_totalDiscount);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txt_otherCharges);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.txt_fee_specialArrangements);
            this.panel1.Controls.Add(this.lbl_specialArrangements);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.txt_fee_additionalAdultsAndChildren);
            this.panel1.Controls.Add(this.txt_fee_totalNumberOfRooms);
            this.panel1.Controls.Add(this.lbl_additionalCompanionChildren);
            this.panel1.Controls.Add(this.lbl_additionalCompanionAdults);
            this.panel1.Controls.Add(this.total_numberOfChildren);
            this.panel1.Controls.Add(this.txt_totalNumberOfAdults);
            this.panel1.Controls.Add(this.txt_totalNumberOfRooms);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Location = new System.Drawing.Point(0, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(868, 447);
            this.panel1.TabIndex = 96;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // lbl_rooms
            // 
            this.lbl_rooms.AutoSize = true;
            this.lbl_rooms.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_rooms.Location = new System.Drawing.Point(150, 98);
            this.lbl_rooms.Name = "lbl_rooms";
            this.lbl_rooms.Size = new System.Drawing.Size(56, 17);
            this.lbl_rooms.TabIndex = 131;
            this.lbl_rooms.Text = "Rooms";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 17);
            this.label2.TabIndex = 130;
            this.label2.Text = "Selected Rooms:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(20, 456);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 17);
            this.label9.TabIndex = 128;
            // 
            // txt_balance
            // 
            this.txt_balance.BackColor = System.Drawing.Color.Silver;
            this.txt_balance.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_balance.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_balance.Location = new System.Drawing.Point(694, 443);
            this.txt_balance.Name = "txt_balance";
            this.txt_balance.ReadOnly = true;
            this.txt_balance.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_balance.Size = new System.Drawing.Size(145, 16);
            this.txt_balance.TabIndex = 127;
            this.txt_balance.Text = "Php 0.00";
            // 
            // txt_totalPayment
            // 
            this.txt_totalPayment.BackColor = System.Drawing.Color.Silver;
            this.txt_totalPayment.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_totalPayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_totalPayment.Location = new System.Drawing.Point(694, 421);
            this.txt_totalPayment.Name = "txt_totalPayment";
            this.txt_totalPayment.ReadOnly = true;
            this.txt_totalPayment.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_totalPayment.Size = new System.Drawing.Size(145, 16);
            this.txt_totalPayment.TabIndex = 126;
            this.txt_totalPayment.Text = "Php 0.00";
            // 
            // txt_totalDiscount
            // 
            this.txt_totalDiscount.BackColor = System.Drawing.Color.Silver;
            this.txt_totalDiscount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_totalDiscount.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_totalDiscount.Location = new System.Drawing.Point(694, 396);
            this.txt_totalDiscount.Name = "txt_totalDiscount";
            this.txt_totalDiscount.ReadOnly = true;
            this.txt_totalDiscount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_totalDiscount.Size = new System.Drawing.Size(145, 16);
            this.txt_totalDiscount.TabIndex = 125;
            this.txt_totalDiscount.Text = "Php 0.00";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(20, 443);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 17);
            this.label18.TabIndex = 123;
            this.label18.Text = "Balance";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(20, 421);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 17);
            this.label8.TabIndex = 122;
            this.label8.Text = "Total Payment";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(20, 396);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 17);
            this.label6.TabIndex = 121;
            this.label6.Text = "Total Discount";
            // 
            // txt_otherCharges
            // 
            this.txt_otherCharges.BackColor = System.Drawing.Color.Silver;
            this.txt_otherCharges.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_otherCharges.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_otherCharges.Location = new System.Drawing.Point(694, 344);
            this.txt_otherCharges.Name = "txt_otherCharges";
            this.txt_otherCharges.ReadOnly = true;
            this.txt_otherCharges.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_otherCharges.Size = new System.Drawing.Size(145, 16);
            this.txt_otherCharges.TabIndex = 119;
            this.txt_otherCharges.Text = "Php 0.00";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(20, 343);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(224, 17);
            this.label20.TabIndex = 118;
            this.label20.Text = "Other charges (items/services):";
            // 
            // txt_fee_specialArrangements
            // 
            this.txt_fee_specialArrangements.BackColor = System.Drawing.Color.Silver;
            this.txt_fee_specialArrangements.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_fee_specialArrangements.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_fee_specialArrangements.Location = new System.Drawing.Point(694, 268);
            this.txt_fee_specialArrangements.Name = "txt_fee_specialArrangements";
            this.txt_fee_specialArrangements.ReadOnly = true;
            this.txt_fee_specialArrangements.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_fee_specialArrangements.Size = new System.Drawing.Size(145, 16);
            this.txt_fee_specialArrangements.TabIndex = 115;
            this.txt_fee_specialArrangements.Text = "Php 0.00";
            // 
            // lbl_specialArrangements
            // 
            this.lbl_specialArrangements.AutoSize = true;
            this.lbl_specialArrangements.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_specialArrangements.Location = new System.Drawing.Point(20, 267);
            this.lbl_specialArrangements.Name = "lbl_specialArrangements";
            this.lbl_specialArrangements.Size = new System.Drawing.Size(42, 17);
            this.lbl_specialArrangements.TabIndex = 114;
            this.lbl_specialArrangements.Text = "None";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(20, 246);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(161, 17);
            this.label17.TabIndex = 113;
            this.label17.Text = "Special Arrangements:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(34, 214);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(687, 17);
            this.label16.TabIndex = 112;
            this.label16.Text = "* A perfect cup of coffee or tea at a touch of a button with express coffee capsu" +
    "le compliments of the hotel.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(34, 193);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(478, 17);
            this.label15.TabIndex = 111;
            this.label15.Text = "* Complimentary use of hotel facilities including the room deck whirlpool.";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(34, 172);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(409, 17);
            this.label14.TabIndex = 110;
            this.label14.Text = "* Complimentary high-speed WIFI access throughout your stay.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(34, 152);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(602, 17);
            this.label13.TabIndex = 109;
            this.label13.Text = "* Enjoy a sumptuous set breakfast at your leisure from 7:00AM to 1:00PM at Luna R" +
    "estaurant.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(20, 129);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 17);
            this.label11.TabIndex = 108;
            this.label11.Text = "Inclusions:";
            // 
            // txt_fee_additionalAdultsAndChildren
            // 
            this.txt_fee_additionalAdultsAndChildren.BackColor = System.Drawing.Color.Silver;
            this.txt_fee_additionalAdultsAndChildren.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_fee_additionalAdultsAndChildren.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_fee_additionalAdultsAndChildren.Location = new System.Drawing.Point(703, 67);
            this.txt_fee_additionalAdultsAndChildren.Name = "txt_fee_additionalAdultsAndChildren";
            this.txt_fee_additionalAdultsAndChildren.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_fee_additionalAdultsAndChildren.Size = new System.Drawing.Size(145, 16);
            this.txt_fee_additionalAdultsAndChildren.TabIndex = 107;
            this.txt_fee_additionalAdultsAndChildren.Text = "Php 0.00";
            // 
            // txt_fee_totalNumberOfRooms
            // 
            this.txt_fee_totalNumberOfRooms.BackColor = System.Drawing.Color.Silver;
            this.txt_fee_totalNumberOfRooms.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_fee_totalNumberOfRooms.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_fee_totalNumberOfRooms.Location = new System.Drawing.Point(703, -16);
            this.txt_fee_totalNumberOfRooms.Name = "txt_fee_totalNumberOfRooms";
            this.txt_fee_totalNumberOfRooms.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_fee_totalNumberOfRooms.Size = new System.Drawing.Size(145, 16);
            this.txt_fee_totalNumberOfRooms.TabIndex = 104;
            this.txt_fee_totalNumberOfRooms.Text = "Php 0.00";
            // 
            // lbl_additionalCompanionChildren
            // 
            this.lbl_additionalCompanionChildren.AutoSize = true;
            this.lbl_additionalCompanionChildren.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_additionalCompanionChildren.Location = new System.Drawing.Point(407, 65);
            this.lbl_additionalCompanionChildren.Name = "lbl_additionalCompanionChildren";
            this.lbl_additionalCompanionChildren.Size = new System.Drawing.Size(94, 17);
            this.lbl_additionalCompanionChildren.TabIndex = 103;
            this.lbl_additionalCompanionChildren.Text = "Child/ren (0)";
            // 
            // lbl_additionalCompanionAdults
            // 
            this.lbl_additionalCompanionAdults.AutoSize = true;
            this.lbl_additionalCompanionAdults.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_additionalCompanionAdults.Location = new System.Drawing.Point(320, 65);
            this.lbl_additionalCompanionAdults.Name = "lbl_additionalCompanionAdults";
            this.lbl_additionalCompanionAdults.Size = new System.Drawing.Size(81, 17);
            this.lbl_additionalCompanionAdults.TabIndex = 102;
            this.lbl_additionalCompanionAdults.Text = "Adult/s (0)";
            // 
            // total_numberOfChildren
            // 
            this.total_numberOfChildren.BackColor = System.Drawing.Color.Silver;
            this.total_numberOfChildren.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.total_numberOfChildren.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total_numberOfChildren.Location = new System.Drawing.Point(346, 39);
            this.total_numberOfChildren.Name = "total_numberOfChildren";
            this.total_numberOfChildren.Size = new System.Drawing.Size(100, 16);
            this.total_numberOfChildren.TabIndex = 101;
            this.total_numberOfChildren.Text = "0";
            // 
            // txt_totalNumberOfAdults
            // 
            this.txt_totalNumberOfAdults.BackColor = System.Drawing.Color.Silver;
            this.txt_totalNumberOfAdults.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_totalNumberOfAdults.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_totalNumberOfAdults.Location = new System.Drawing.Point(311, 12);
            this.txt_totalNumberOfAdults.Name = "txt_totalNumberOfAdults";
            this.txt_totalNumberOfAdults.Size = new System.Drawing.Size(100, 16);
            this.txt_totalNumberOfAdults.TabIndex = 100;
            this.txt_totalNumberOfAdults.Text = "0";
            // 
            // txt_totalNumberOfRooms
            // 
            this.txt_totalNumberOfRooms.BackColor = System.Drawing.Color.Silver;
            this.txt_totalNumberOfRooms.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_totalNumberOfRooms.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_totalNumberOfRooms.Location = new System.Drawing.Point(179, -17);
            this.txt_totalNumberOfRooms.Name = "txt_totalNumberOfRooms";
            this.txt_totalNumberOfRooms.Size = new System.Drawing.Size(100, 16);
            this.txt_totalNumberOfRooms.TabIndex = 99;
            this.txt_totalNumberOfRooms.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(20, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(295, 17);
            this.label12.TabIndex = 98;
            this.label12.Text = "Additional companion (without breakfast):";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(20, 39);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(320, 17);
            this.label10.TabIndex = 97;
            this.label10.Text = "Total number of child/ren (without breakfast):";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(20, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(285, 17);
            this.label7.TabIndex = 96;
            this.label7.Text = "Total number of adult/s (with breakfast):";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(20, -17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(177, 17);
            this.label5.TabIndex = 95;
            this.label5.Text = "Total number of rooms:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(11, 595);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(0, 17);
            this.label21.TabIndex = 95;
            // 
            // summary_panel
            // 
            this.summary_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.summary_panel.Controls.Add(this.txt_folioSummary_balanceDue);
            this.summary_panel.Controls.Add(this.lbl_balanceDue_folioSummary);
            this.summary_panel.Controls.Add(this.lbl_folioSummary);
            this.summary_panel.Location = new System.Drawing.Point(0, 0);
            this.summary_panel.Name = "summary_panel";
            this.summary_panel.Size = new System.Drawing.Size(868, 31);
            this.summary_panel.TabIndex = 10;
            // 
            // txt_folioSummary_balanceDue
            // 
            this.txt_folioSummary_balanceDue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_folioSummary_balanceDue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_folioSummary_balanceDue.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_folioSummary_balanceDue.ForeColor = System.Drawing.Color.White;
            this.txt_folioSummary_balanceDue.Location = new System.Drawing.Point(702, 8);
            this.txt_folioSummary_balanceDue.Name = "txt_folioSummary_balanceDue";
            this.txt_folioSummary_balanceDue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_folioSummary_balanceDue.Size = new System.Drawing.Size(155, 17);
            this.txt_folioSummary_balanceDue.TabIndex = 72;
            this.txt_folioSummary_balanceDue.Text = "Php 0.00";
            // 
            // lbl_balanceDue_folioSummary
            // 
            this.lbl_balanceDue_folioSummary.AutoSize = true;
            this.lbl_balanceDue_folioSummary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_balanceDue_folioSummary.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_balanceDue_folioSummary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_balanceDue_folioSummary.Location = new System.Drawing.Point(587, 8);
            this.lbl_balanceDue_folioSummary.Name = "lbl_balanceDue_folioSummary";
            this.lbl_balanceDue_folioSummary.Size = new System.Drawing.Size(109, 17);
            this.lbl_balanceDue_folioSummary.TabIndex = 1;
            this.lbl_balanceDue_folioSummary.Text = "BALANCE DUE:";
            // 
            // lbl_folioSummary
            // 
            this.lbl_folioSummary.AutoSize = true;
            this.lbl_folioSummary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_folioSummary.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_folioSummary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_folioSummary.Location = new System.Drawing.Point(11, 8);
            this.lbl_folioSummary.Name = "lbl_folioSummary";
            this.lbl_folioSummary.Size = new System.Drawing.Size(121, 17);
            this.lbl_folioSummary.TabIndex = 0;
            this.lbl_folioSummary.Text = "FOLIO SUMMARY";
            // 
            // Walkin_Reservation_FolioSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(870, 479);
            this.Controls.Add(this.pnl_folioSummary);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Walkin_Reservation_FolioSummary";
            this.Text = "Walkin_Reservation_FolioSummary";
            this.Load += new System.EventHandler(this.Walkin_Reservation_FolioSummary_Load);
            this.pnl_folioSummary.ResumeLayout(false);
            this.pnl_folioSummary.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.summary_panel.ResumeLayout(false);
            this.summary_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_folioSummary;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel summary_panel;
        private System.Windows.Forms.TextBox txt_folioSummary_balanceDue;
        private System.Windows.Forms.Label lbl_balanceDue_folioSummary;
        private System.Windows.Forms.Label lbl_folioSummary;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_rooms;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt_balance;
        private System.Windows.Forms.TextBox txt_totalPayment;
        private System.Windows.Forms.TextBox txt_totalDiscount;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_otherCharges;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txt_fee_specialArrangements;
        private System.Windows.Forms.Label lbl_specialArrangements;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txt_fee_additionalAdultsAndChildren;
        private System.Windows.Forms.TextBox txt_fee_totalNumberOfRooms;
        private System.Windows.Forms.Label lbl_additionalCompanionChildren;
        private System.Windows.Forms.Label lbl_additionalCompanionAdults;
        private System.Windows.Forms.TextBox txt_totalNumberOfRooms;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox total_numberOfChildren;
        private System.Windows.Forms.TextBox txt_totalNumberOfAdults;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
    }
}