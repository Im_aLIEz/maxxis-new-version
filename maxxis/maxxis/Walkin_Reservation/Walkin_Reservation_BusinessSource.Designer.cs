﻿namespace maxxis.Walkin_Reservation
{
    partial class Walkin_Reservation_BusinessSource
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl_businessSource = new System.Windows.Forms.Panel();
            this.cbox_purposeofstay = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbox_marketsegment = new System.Windows.Forms.ComboBox();
            this.cbox_marketplace = new System.Windows.Forms.ComboBox();
            this.lbl_marketSource = new System.Windows.Forms.Label();
            this.businessSource_panel = new System.Windows.Forms.Panel();
            this.lbl_businessSource = new System.Windows.Forms.Label();
            this.pnl_businessSource.SuspendLayout();
            this.businessSource_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_businessSource
            // 
            this.pnl_businessSource.AutoScroll = true;
            this.pnl_businessSource.BackColor = System.Drawing.Color.Silver;
            this.pnl_businessSource.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_businessSource.Controls.Add(this.cbox_purposeofstay);
            this.pnl_businessSource.Controls.Add(this.label2);
            this.pnl_businessSource.Controls.Add(this.label1);
            this.pnl_businessSource.Controls.Add(this.cbox_marketsegment);
            this.pnl_businessSource.Controls.Add(this.cbox_marketplace);
            this.pnl_businessSource.Controls.Add(this.lbl_marketSource);
            this.pnl_businessSource.Controls.Add(this.businessSource_panel);
            this.pnl_businessSource.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_businessSource.Location = new System.Drawing.Point(0, 0);
            this.pnl_businessSource.Name = "pnl_businessSource";
            this.pnl_businessSource.Size = new System.Drawing.Size(870, 154);
            this.pnl_businessSource.TabIndex = 17;
            // 
            // cbox_purposeofstay
            // 
            this.cbox_purposeofstay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbox_purposeofstay.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_purposeofstay.FormattingEnabled = true;
            this.cbox_purposeofstay.Items.AddRange(new object[] {
            "Business",
            "Liesure",
            "Transit"});
            this.cbox_purposeofstay.Location = new System.Drawing.Point(576, 91);
            this.cbox_purposeofstay.Name = "cbox_purposeofstay";
            this.cbox_purposeofstay.Size = new System.Drawing.Size(281, 26);
            this.cbox_purposeofstay.TabIndex = 3;
            this.cbox_purposeofstay.SelectedIndexChanged += new System.EventHandler(this.cbox_purposeofstay_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(573, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 17);
            this.label2.TabIndex = 28;
            this.label2.Text = "Purpose of Stay";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(308, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 17);
            this.label1.TabIndex = 27;
            this.label1.Text = "Market Segment";
            // 
            // cbox_marketsegment
            // 
            this.cbox_marketsegment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbox_marketsegment.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_marketsegment.FormattingEnabled = true;
            this.cbox_marketsegment.Items.AddRange(new object[] {
            "OTA",
            "Travel Agency",
            "Corporate Account",
            "Hotel Tie Ups",
            "Direct Booking",
            "Walk In"});
            this.cbox_marketsegment.Location = new System.Drawing.Point(311, 91);
            this.cbox_marketsegment.Name = "cbox_marketsegment";
            this.cbox_marketsegment.Size = new System.Drawing.Size(259, 26);
            this.cbox_marketsegment.TabIndex = 2;
            this.cbox_marketsegment.SelectedIndexChanged += new System.EventHandler(this.cbox_marketsegment_SelectedIndexChanged);
            // 
            // cbox_marketplace
            // 
            this.cbox_marketplace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbox_marketplace.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_marketplace.FormattingEnabled = true;
            this.cbox_marketplace.Items.AddRange(new object[] {
            "Referal",
            "Website",
            "Booking Website",
            "Advertisement",
            "Direct",
            "Other"});
            this.cbox_marketplace.Location = new System.Drawing.Point(11, 91);
            this.cbox_marketplace.Name = "cbox_marketplace";
            this.cbox_marketplace.Size = new System.Drawing.Size(294, 26);
            this.cbox_marketplace.TabIndex = 1;
            this.cbox_marketplace.SelectedIndexChanged += new System.EventHandler(this.cbox_marketplace_SelectedIndexChanged);
            // 
            // lbl_marketSource
            // 
            this.lbl_marketSource.AutoSize = true;
            this.lbl_marketSource.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_marketSource.Location = new System.Drawing.Point(11, 71);
            this.lbl_marketSource.Name = "lbl_marketSource";
            this.lbl_marketSource.Size = new System.Drawing.Size(92, 17);
            this.lbl_marketSource.TabIndex = 14;
            this.lbl_marketSource.Text = "Market Place";
            // 
            // businessSource_panel
            // 
            this.businessSource_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.businessSource_panel.Controls.Add(this.lbl_businessSource);
            this.businessSource_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.businessSource_panel.Location = new System.Drawing.Point(0, 0);
            this.businessSource_panel.Name = "businessSource_panel";
            this.businessSource_panel.Size = new System.Drawing.Size(868, 31);
            this.businessSource_panel.TabIndex = 10;
            // 
            // lbl_businessSource
            // 
            this.lbl_businessSource.AutoSize = true;
            this.lbl_businessSource.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_businessSource.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_businessSource.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_businessSource.Location = new System.Drawing.Point(11, 8);
            this.lbl_businessSource.Name = "lbl_businessSource";
            this.lbl_businessSource.Size = new System.Drawing.Size(121, 14);
            this.lbl_businessSource.TabIndex = 0;
            this.lbl_businessSource.Text = "BUSINESS SOURCE";
            // 
            // Walkin_Reservation_BusinessSource
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(870, 479);
            this.Controls.Add(this.pnl_businessSource);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Walkin_Reservation_BusinessSource";
            this.Text = "Walkin_Reservation_BusinessSource";
            this.Load += new System.EventHandler(this.Walkin_Reservation_BusinessSource_Load);
            this.pnl_businessSource.ResumeLayout(false);
            this.pnl_businessSource.PerformLayout();
            this.businessSource_panel.ResumeLayout(false);
            this.businessSource_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_businessSource;
        private System.Windows.Forms.ComboBox cbox_marketsegment;
        private System.Windows.Forms.ComboBox cbox_marketplace;
        private System.Windows.Forms.Label lbl_marketSource;
        private System.Windows.Forms.Panel businessSource_panel;
        private System.Windows.Forms.Label lbl_businessSource;
        private System.Windows.Forms.ComboBox cbox_purposeofstay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}