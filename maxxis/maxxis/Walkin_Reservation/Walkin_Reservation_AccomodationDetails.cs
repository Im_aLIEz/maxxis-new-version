﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace maxxis.Walkin_Reservation
{
    public partial class Walkin_Reservation_AccomodationDetails : Form
    {
        int pwdqty;
        int urqty;
        int ucrqty;
        int uvrqty;

        //checkininfos
        private static string checkin;
        private static string checkout;
        private static string numberOfNights;
        private static string totalPayment;
        private static string pwdSelect;
        private static string ucrSelect;
        private static string uvrSelect;
        private static string urSelect;
        private static string pwdrate;
        private static string urrate;
        private static string ucrrate;
        private static string uvrrate;
        private static int billchecker;
        private static string pwdadult;
        private static string ucradult;
        private static string uvradult;
        private static string uradult;
        private static string pwdchild;
        private static string ucrchild;
        private static string uvrchild;
        private static string urchild;
        private static string extraadult;
        private static string extrachild;
        private static string remarks;
        private static string extrapayments;
        //variables
        int ch;
        int checker;
        int nights;
        private static string roomselected;
        string guestId;
        float Occupancy;
        float occupancyRate;
        public static decimal total;
        string con = System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString;

 

        public Walkin_Reservation_AccomodationDetails()
        {
            InitializeComponent();
        }

        private void dgv_listOfRooms_MouseClick(object sender, MouseEventArgs e)
        {

            
            
        }
        //publics
        public string RoomSelected
        {
            get { return roomselected; }
            set { roomselected = value; }
        }
        public string CheckIn
        {
            get { return checkin; }
            set { checkin = value; }
        }
        public string CheckOut
        {
            get { return checkout; }
            set { checkout = value; }
        }
        public string NumberOfNights
        {
            get { return numberOfNights; }
            set { numberOfNights = value; }
        }
        public string TotalPayment
        {
            get { return totalPayment; }
            set { totalPayment = value; }
        }
        public string UVRSelect
        {
            get { return uvrSelect; }
            set { uvrSelect = value; }
        }
        public string UVRadult
        {
            get { return uvradult; }
            set { uvradult = value; }
        }
        public string UVRchild
        {
            get { return uvrchild; }
            set { uvrchild = value; }
        }
        public string PWDSelect
        {
            get { return pwdSelect; }
            set { pwdSelect = value; }
        }
        public string PWDAdult
        {
            get { return pwdadult; }
            set { pwdadult = value; }
        }
        public string PWDchild
        {
            get { return pwdchild; }
            set { pwdchild = value; }
        }
        public string UCRSelect
        {
            get { return ucrSelect; }
            set { ucrSelect = value; }
        }
        public string UCRadult
        {
            get { return ucradult; }
            set { ucradult = value; }
        }
        public string UCRchild
        {
            get { return ucrchild; }
            set { ucrchild = value; }
        }
        public string URSelect
        {
            get { return urSelect; }
            set { urSelect = value; }
        }
        public string URadult
        {
            get { return uradult; }
            set { uradult = value; }
        }
        public string URchild
        {
            get { return urchild; }
            set { urchild = value; }
        }
        public string ExtraAdult
        {
            get { return extraadult; }
            set { extraadult = value; }
        }
        public string ExtraChild
        {
            get { return extrachild; }
            set { extrachild = value; }
        }
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }
        public string PWDRate
        {
            get { return pwdrate; }
            set { pwdrate = value; }
        }
        public string URRate
        {
            get { return urrate; }
            set { urrate = value; }
        }
        public string UCRRate
        {
            get { return ucrrate; }
            set { ucrrate = value; }
        }
        public string UVRRate
        {
            get { return uvrrate; }
            set { uvrrate = value; }
        }
        public int BillChecker
        {
            get { return billchecker; }
            set { billchecker = value; }
        }
        public Decimal Total
        {
            get { return total; }
            set { total = value; }
        }
        public string ExtraPayments
        {
            get { return extrapayments; }
            set { extrapayments = value; }
        }

        //Methods
        public void Display()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT Distinct dbo.tbl_rooms.room_type, dbo.tbl_roomrate.rate - (dbo.tbl_roomrate.rate * dbo.tbl_currentoccupancy.occupancy) + dbo.tbl_rooms.roomrate AS Total FROM dbo.tbl_rooms CROSS JOIN dbo.tbl_roomrate Cross Join dbo.tbl_currentoccupancy", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dgv_listOfRooms.Rows.Clear();
                foreach (DataRow item in dt.Rows)
                {
                    int n = dgv_listOfRooms.Rows.Add();
                    dgv_listOfRooms.Rows[n].Cells[2].Value = item[0].ToString();
                    dgv_listOfRooms.Rows[n].Cells[4].Value = item[1].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void RoomLoadOccupy()
        {
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select Sum(occupied/48 * 100)from tbl_rooms ", conDatabase);
            conDatabase.Open();
            SqlDataAdapter sda = new SqlDataAdapter();
            sda.SelectCommand = cmdDataBase;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            try
            {
                foreach (DataRow item in dt.Rows)
                {
                    Occupancy = float.Parse(item[0].ToString());
                }
                if (Occupancy < 59.99)
                {
                    occupancyRate = 0.40f;
                }
                else if (Occupancy < 69.99)
                {
                    occupancyRate = 0.30f;
                }
                else if (Occupancy < 79.99)
                {
                    occupancyRate = 0.20f;
                }
                else if (Occupancy < 89.99)
                {
                    occupancyRate = 0.10f;
                }
                else
                {
                    occupancyRate = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            conDatabase.Close();
        }
        public void TimeManager()
        {
            dtp_arrivalDate.Value = DateTime.Today;
            DateTime checkoutdate = DateTime.Today;
            dtp_departureDate.Value = checkoutdate.AddDays(Int32.Parse(lbl_nights.Text));
        }
        public void formUpdate()
        {
            
            CheckIn = dtp_arrivalDate.Text;
            CheckOut = dtp_departureDate.Text;
            numberOfNights = lbl_nights.Text;
            TotalPayment = txt_accomo_accomodationTotal.Text;
            Remarks = txt_guestRemarks.Text;

            if(checker != 0)
            {
                PWDSelect = dgv_listOfRooms.Rows[0].Cells[3].Value.ToString();
                UCRSelect = dgv_listOfRooms.Rows[1].Cells[3].Value.ToString();
                URSelect = dgv_listOfRooms.Rows[2].Cells[3].Value.ToString();
                UVRSelect = dgv_listOfRooms.Rows[3].Cells[3].Value.ToString();

                PWDAdult = dgv_listOfRooms.Rows[0].Cells[5].Value.ToString();
                UCRadult = dgv_listOfRooms.Rows[1].Cells[5].Value.ToString();
                URadult = dgv_listOfRooms.Rows[2].Cells[5].Value.ToString();
                UVRadult = dgv_listOfRooms.Rows[3].Cells[5].Value.ToString();

                PWDchild = dgv_listOfRooms.Rows[0].Cells[6].Value.ToString();
                UCRchild = dgv_listOfRooms.Rows[1].Cells[6].Value.ToString();
                URchild = dgv_listOfRooms.Rows[2].Cells[6].Value.ToString();
                UVRchild = dgv_listOfRooms.Rows[3].Cells[6].Value.ToString();

                ExtraAdult = cbox_extraadults.Text;
                ExtraChild = cbox_extrachildren.Text;
                ExtraPayments = (Double.Parse(txt_adultsTotalFee.Text) + Double.Parse(txt_childTotalFee.Text)).ToString("N2"); 

                PWDRate = dgv_listOfRooms.Rows[0].Cells[7].Value.ToString();
                UCRRate = dgv_listOfRooms.Rows[1].Cells[7].Value.ToString();
                URRate = dgv_listOfRooms.Rows[2].Cells[7].Value.ToString();
                UVRRate = dgv_listOfRooms.Rows[3].Cells[7].Value.ToString();

                if (Int32.Parse(dgv_listOfRooms.Rows[1].Cells[3].Value.ToString()) == 1 || Int32.Parse(dgv_listOfRooms.Rows[0].Cells[3].Value.ToString()) == 1 || Int32.Parse(dgv_listOfRooms.Rows[2].Cells[3].Value.ToString()) == 1 || Int32.Parse(dgv_listOfRooms.Rows[3].Cells[3].Value.ToString()) == 1)
                {
                    BillChecker = 1;

                }
                if(cbox_extraadults.Text == null)
                {
                    ExtraAdult = "0";
                }
                if(cbox_extrachildren.Text == null)
                {
                    ExtraChild = "0";
                }
            }
            
            
        }

        public void pwdqtycheck()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select sum(available) as Quantity From tbl_rooms where room_type = 'PWD Urban Room'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    pwdqty = Int32.Parse(item[0].ToString());
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void urqtycheck()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select sum(available) as Quantity From tbl_rooms where room_type = 'Urban Room'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    urqty = Int32.Parse(item[0].ToString());
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void ucrqtycheck()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select sum(available) as Quantity From tbl_rooms where room_type = 'Urban Corner Room'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    ucrqty = Int32.Parse(item[0].ToString());
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void uvrqtycheck()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select sum(available) as Quantity From tbl_rooms where room_type = 'Urban View Room'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    uvrqty = Int32.Parse(item[0].ToString());
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void Walkin_Reservation_AccomodationDetails_Load(object sender, EventArgs e)
        {
            dtp_arrivalDate.CustomFormat = "MM-dd-yyyy";
            dtp_departureDate.CustomFormat = "MM-dd-yyyy";
            RoomLoadOccupy();
            Display();
            lbl_nights.Text = "1";
            TimeManager();
            pwdqtycheck();
            urqtycheck();
            ucrqtycheck();
            uvrqtycheck();
            if(pwdqty == 0)
            {
                dgv_listOfRooms.Rows[0].DefaultCellStyle.BackColor = Color.Red;
            }
            if (ucrqty == 0)
            {
                dgv_listOfRooms.Rows[1].DefaultCellStyle.BackColor = Color.Red;

            }
            if (urqty == 0)
            {
                dgv_listOfRooms.Rows[2].DefaultCellStyle.BackColor = Color.Red;
            }
            if (uvrqty == 0)
            {
                dgv_listOfRooms.Rows[3].DefaultCellStyle.BackColor = Color.Red;
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban View Room")
            {
                numUD_roomQuantity.Maximum = uvrqty;
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban Room")
            {
                numUD_roomQuantity.Maximum = urqty;
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban Corner Room")
            {
                numUD_roomQuantity.Maximum = ucrqty;
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "PWD Urban Room")
            {
                numUD_roomQuantity.Maximum = pwdqty;
            }

            dtp_departureDate.MinDate = DateTime.Today.AddDays(1);
            dgv_listOfRooms.Rows[0].Cells[3].Value = "0";
            dgv_listOfRooms.Rows[1].Cells[3].Value = "0";
            dgv_listOfRooms.Rows[2].Cells[3].Value = "0";
            dgv_listOfRooms.Rows[3].Cells[3].Value = "0";
            dgv_listOfRooms.Rows[0].Cells[5].Value = "0";
            dgv_listOfRooms.Rows[1].Cells[5].Value = "0";
            dgv_listOfRooms.Rows[2].Cells[5].Value = "0";
            dgv_listOfRooms.Rows[3].Cells[5].Value = "0";
            dgv_listOfRooms.Rows[0].Cells[6].Value = "0";
            dgv_listOfRooms.Rows[1].Cells[6].Value = "0";
            dgv_listOfRooms.Rows[2].Cells[6].Value = "0";
            dgv_listOfRooms.Rows[3].Cells[6].Value = "0";
            dgv_listOfRooms.Rows[0].Cells[7].Value = "0";
            dgv_listOfRooms.Rows[1].Cells[7].Value = "0";
            dgv_listOfRooms.Rows[2].Cells[7].Value = "0";
            dgv_listOfRooms.Rows[3].Cells[7].Value = "0";
            
        }
        private void dtp_arrivalDate_ValueChanged(object sender, EventArgs e)
        {
            TimeManager();
            formUpdate();
        }
        
        

        private void cbox_adults_SelectedIndexChanged(object sender, EventArgs e)
        {
            checker = 1;
            txt_adultsFee.Text = (Decimal.Parse(cbox_extraadults.Text.ToString()) * 750).ToString("N2");
            txt_adultsTotalFee.Text = (Decimal.Parse(txt_adultsFee.Text) * Int32.Parse(lbl_nights.Text)).ToString("N2");
            Total = (((Decimal.Parse(dgv_listOfRooms.Rows[0].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[1].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[2].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[3].Cells[7].Value.ToString()))) + Decimal.Parse(txt_adultsTotalFee.Text) + Decimal.Parse(txt_childTotalFee.Text)) * Int32.Parse(lbl_nights.Text);
            txt_accomo_accomodationTotal.Text = Total.ToString("N2");
            formUpdate();
        }

        private void cbox_children_SelectedIndexChanged(object sender, EventArgs e)
        {
            checker = 1;
            txt_childFee.Text = (Decimal.Parse(cbox_extrachildren.Text.ToString()) * 375).ToString("N2");
            txt_childTotalFee.Text = (Decimal.Parse(txt_childFee.Text) * Int32.Parse(lbl_nights.Text)).ToString("N2");


            Total = (((Decimal.Parse(dgv_listOfRooms.Rows[0].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[1].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[2].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[3].Cells[7].Value.ToString()))) + Decimal.Parse(txt_adultsTotalFee.Text) + Decimal.Parse(txt_childTotalFee.Text)) * Int32.Parse(lbl_nights.Text);
            txt_accomo_accomodationTotal.Text = Total.ToString("N2");
            formUpdate();
        }

        private void cbox_adult_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbox_children.Items.Clear();
            for (int x = 0; x <= Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) * 2; x++)
            {
                cbox_children.Items.Add(x.ToString());
            }
            int ch = cbox_children.Items.Count;
            dgv_listOfRooms.SelectedRows[0].Cells[5].Value = cbox_adult.Text;
            int val = (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString()) - Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString())) * 2;
            cbox_children.Items.Clear();
            for (int x = 0; x < ch - val; x++)
            {
                cbox_children.Items.Add(x.ToString());
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban View Room")
            {
                cbox_children.Items.Clear();
                for (int x = 0; x <= (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString())) * 2; x++)
                {
                    cbox_children.Items.Add(x.ToString());
                }
                int ch1 = cbox_children.Items.Count;
                int val1 = (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString()) - Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString())) * 4;
                cbox_children.Items.Clear();
                for (int x = 0; x < (ch1 - val1); x++)
                {
                    cbox_children.Items.Add(x.ToString());
                }
            }
            formUpdate();
        }

        private void cbox_children_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            cbox_adult.Items.Clear();
            int ch = Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) - ((Int32.Parse(cbox_children.Text) / 2) + Int32.Parse(cbox_children.Text) % 2);
            for (int x = 0; x <= ch; x++)
            {
                cbox_adult.Items.Add(Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + x);
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban View Room")
            {
                cbox_adult.Items.Clear();
                for (int x = 0; x <= ch + Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()); x++)
                {
                    cbox_adult.Items.Add(Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + x);
                }
            }
            dgv_listOfRooms.SelectedRows[0].Cells[6].Value = cbox_children.Text;
            cbox_children.Text = dgv_listOfRooms.SelectedRows[0].Cells[6].Value.ToString();
            formUpdate();
        }

        private void numUD_roomQuantity_MouseUp(object sender, MouseEventArgs e)
        {
            
            cbox_children.Text = "0";
            if(numUD_roomQuantity.Text == "0")
            {
                cbox_adult.Text = "0";
                cbox_children.Text = "0";
            }
            else
            cbox_adult.Items.Clear();
            cbox_children.Items.Clear();
            checker = 1;
            dgv_listOfRooms.SelectedRows[0].Cells[3].Value = numUD_roomQuantity.Text;
            
            if (numUD_roomQuantity.Value >= 1)
            {
                dgv_listOfRooms.SelectedRows[0].Cells[5].Value = numUD_roomQuantity.Text;
                cbox_adult.Text = dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString();
            }
            else
            {
                dgv_listOfRooms.SelectedRows[0].Cells[5].Value = "0";
            }
            
            for(int x = 0;x <= Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString()); x++)
            {
                cbox_adult.Items.Add(Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + x);
            }
            for (int x = 0; x <= Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) * 2; x++)
            {
                cbox_children.Items.Add(x.ToString());
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban View Room")
            {
                cbox_adult.Items.Clear();
                cbox_children.Items.Clear();
                for (int x = 0; x <= Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString()) + (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString())); x++)
                {
                    cbox_adult.Items.Add(Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + x);
                }
                for (int x = 0; x <= (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString())) * 2; x++)
                {
                    cbox_children.Items.Add(x.ToString());
                }
            }

            dgv_listOfRooms.Rows[0].Cells[7].Value = (Int32.Parse(dgv_listOfRooms.Rows[0].Cells[3].Value.ToString()) * Int32.Parse(dgv_listOfRooms.Rows[0].Cells[4].Value.ToString()));
            dgv_listOfRooms.Rows[1].Cells[7].Value = (Int32.Parse(dgv_listOfRooms.Rows[1].Cells[3].Value.ToString()) * Int32.Parse(dgv_listOfRooms.Rows[1].Cells[4].Value.ToString()));
            dgv_listOfRooms.Rows[2].Cells[7].Value = (Int32.Parse(dgv_listOfRooms.Rows[2].Cells[3].Value.ToString()) * Int32.Parse(dgv_listOfRooms.Rows[2].Cells[4].Value.ToString()));
            dgv_listOfRooms.Rows[3].Cells[7].Value = (Int32.Parse(dgv_listOfRooms.Rows[3].Cells[3].Value.ToString()) * Int32.Parse(dgv_listOfRooms.Rows[3].Cells[4].Value.ToString()));
            Total = (((Decimal.Parse(dgv_listOfRooms.Rows[0].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[1].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[2].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[3].Cells[7].Value.ToString()))) + Decimal.Parse(txt_adultsTotalFee.Text) + Decimal.Parse(txt_childTotalFee.Text)) * Int32.Parse(lbl_nights.Text);
            txt_accomo_accomodationTotal.Text = Total.ToString("N2");
            checker = 1;
            formUpdate();
            
        }
        private void txt_guestRemarks_TextChanged(object sender, EventArgs e)
        {
            formUpdate();
        }
        private void dtp_departureDate_MouseUp(object sender, MouseEventArgs e)
        {
            checker = 1;
            double days = Math.Abs((DateTime.Today - dtp_departureDate.Value).TotalDays);
            if (checker == 1)
            {
                nights = Int32.Parse(days.ToString());
                lbl_nights.Text = nights.ToString();
                txt_accomo_accomodationTotal.Text = Total.ToString("N2");
                formUpdate();
            }
        }

        private void dtp_departureDate_ValueChanged(object sender, EventArgs e)
        {
            double days = Math.Abs((DateTime.Today - dtp_departureDate.Value).TotalDays);
            if (checker == 1)
            {
                nights = Int32.Parse(days.ToString());
                lbl_nights.Text = nights.ToString();
                Total = (((Decimal.Parse(dgv_listOfRooms.Rows[0].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[1].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[2].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[3].Cells[7].Value.ToString()))) + Decimal.Parse(txt_adultsTotalFee.Text) + Decimal.Parse(txt_childTotalFee.Text)) * Int32.Parse(lbl_nights.Text);
                txt_accomo_accomodationTotal.Text = Total.ToString("N2");
                formUpdate();
            }
        }

        private void lbl_nights_TextChanged(object sender, EventArgs e)
        {
            if(checker == 1)
            {
                double days = Math.Abs((DateTime.Today - dtp_departureDate.Value).TotalDays);
                nights = Int32.Parse(days.ToString());
                lbl_nights.Text = nights.ToString();
                txt_adultsTotalFee.Text = (Decimal.Parse(txt_adultsFee.Text) * Int32.Parse(lbl_nights.Text)).ToString("N2");
                txt_childTotalFee.Text = (Decimal.Parse(txt_childFee.Text) * Int32.Parse(lbl_nights.Text)).ToString("N2");

                Total = (((Decimal.Parse(dgv_listOfRooms.Rows[0].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[1].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[2].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[3].Cells[7].Value.ToString()))) + Decimal.Parse(txt_adultsTotalFee.Text) + Decimal.Parse(txt_childTotalFee.Text)) * Int32.Parse(lbl_nights.Text);

                txt_accomo_accomodationTotal.Text = Total.ToString("N2");
                formUpdate();
            }
            
        }

        private void btn_viewRoomDetails_Click(object sender, EventArgs e)
        {

        }

        private void dgv_listOfRooms_MouseUp(object sender, MouseEventArgs e)
        {
            checker = 1;
            numUD_roomQuantity.Text = "0";
            formUpdate();
            Total = (((Decimal.Parse(dgv_listOfRooms.Rows[0].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[1].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[2].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[3].Cells[7].Value.ToString()))) + Decimal.Parse(txt_adultsTotalFee.Text) + Decimal.Parse(txt_childTotalFee.Text)) * Int32.Parse(lbl_nights.Text);
            txt_accomo_accomodationTotal.Text =  Total.ToString("N2");
            formUpdate();
            for (int x = 0; x <= Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString()); x++)
            {
                cbox_adult.Items.Add(Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + x);
            }
            for (int x = 0; x <= Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) * 2; x++)
            {
                cbox_children.Items.Add(x.ToString());
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban View Room")
            {
                cbox_adult.Items.Clear();
                cbox_children.Items.Clear();
                for (int x = 0; x <= Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString()) + (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString())); x++)
                {
                    cbox_adult.Items.Add(Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + x);
                }
                for (int x = 0; x <= (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString())) * 2; x++)
                {
                    cbox_children.Items.Add(x.ToString());
                }
            }
            numUD_roomQuantity.Text = dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString();
            cbox_adult.Text = dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString();
            cbox_children.Text = dgv_listOfRooms.SelectedRows[0].Cells[6].Value.ToString();
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban View Room")
            {
                numUD_roomQuantity.Maximum = uvrqty;
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban Room")
            {
                numUD_roomQuantity.Maximum = urqty;
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban Corner Room")
            {
                numUD_roomQuantity.Maximum = ucrqty;
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "PWD Urban Room")
            {
                numUD_roomQuantity.Maximum = pwdqty;
            }
        }

        private void cbox_children_MouseUp(object sender, MouseEventArgs e)
        {
            //cbox_adult.Text = dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString();
            //cbox_children.Text = dgv_listOfRooms.SelectedRows[0].Cells[6].Value.ToString();
            
        }

        private void cbox_adult_MouseUp(object sender, MouseEventArgs e)
        {
            
            //cbox_adult.Text = dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString();
            //cbox_children.Text = dgv_listOfRooms.SelectedRows[0].Cells[6].Value.ToString();

        }

        private void lbl_nights_Click(object sender, EventArgs e)
        {

        }

        private void cbox_adult_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void pnl_accomodationDetails_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_companionName_Click(object sender, EventArgs e)
        {
            CompanionNames_Form CNF = new CompanionNames_Form();
            CNF.ShowDialog();
        }

        private void cbox_adult_KeyDown(object sender, KeyEventArgs e)
        {
            cbox_children.Items.Clear();
            for (int x = 0; x <= Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) * 2; x++)
            {
                cbox_children.Items.Add(x.ToString());
            }
            int ch = cbox_children.Items.Count;
            dgv_listOfRooms.SelectedRows[0].Cells[5].Value = cbox_adult.Text;
            int val = (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString()) - Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString())) * 2;
            cbox_children.Items.Clear();
            for (int x = 0; x < ch - val; x++)
            {
                cbox_children.Items.Add(x.ToString());
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban View Room")
            {
                cbox_children.Items.Clear();
                for (int x = 0; x <= (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString())) * 2; x++)
                {
                    cbox_children.Items.Add(x.ToString());
                }
                int ch1 = cbox_children.Items.Count;
                int val1 = (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString()) - Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString())) * 4;
                cbox_children.Items.Clear();
                for (int x = 0; x < (ch1 - val1); x++)
                {
                    cbox_children.Items.Add(x.ToString());
                }
            }
            formUpdate();
        }

        private void cbox_children_KeyDown(object sender, KeyEventArgs e)
        {
            cbox_adult.Items.Clear();
            int ch = Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) - ((Int32.Parse(cbox_children.Text) / 2) + Int32.Parse(cbox_children.Text) % 2);
            for (int x = 0; x <= ch; x++)
            {
                cbox_adult.Items.Add(Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + x);
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban View Room")
            {
                cbox_adult.Items.Clear();
                for (int x = 0; x <= ch + Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()); x++)
                {
                    cbox_adult.Items.Add(Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + x);
                }
            }
            dgv_listOfRooms.SelectedRows[0].Cells[6].Value = cbox_children.Text;
            cbox_children.Text = dgv_listOfRooms.SelectedRows[0].Cells[6].Value.ToString();
            formUpdate();
        }
    }
}
