﻿namespace maxxis.Walkin_Reservation
{
    partial class Walkin_Reservation_SettlementOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl_settlementOption = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.lbl_modeOfPayment = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btn_apply = new System.Windows.Forms.Button();
            this.txt_discountDescription = new System.Windows.Forms.TextBox();
            this.lbl_discountDescription = new System.Windows.Forms.Label();
            this.lbl_discountAmount = new System.Windows.Forms.Label();
            this.txt_discountAmount = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.rbtn_cash = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_type = new System.Windows.Forms.Label();
            this.cmb_type = new System.Windows.Forms.ComboBox();
            this.lbl_cardNumber = new System.Windows.Forms.Label();
            this.txt_cardNumber = new System.Windows.Forms.TextBox();
            this.lbl_expire = new System.Windows.Forms.Label();
            this.txt_cvv = new System.Windows.Forms.TextBox();
            this.txt_expire = new System.Windows.Forms.TextBox();
            this.lbl_cvv = new System.Windows.Forms.Label();
            this.rbtn_credit = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txt_change = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_cashhanded = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_cashPayment = new System.Windows.Forms.Label();
            this.txt_cashPayment = new System.Windows.Forms.TextBox();
            this.rbtn_partialCredit = new System.Windows.Forms.RadioButton();
            this.settlementOption_panel = new System.Windows.Forms.Panel();
            this.lbl_settlementOption = new System.Windows.Forms.Label();
            this.lbl_requiredPrePayment = new System.Windows.Forms.Label();
            this.txt_requiredPrePayment = new System.Windows.Forms.TextBox();
            this.pnl_settlementOption.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.settlementOption_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_settlementOption
            // 
            this.pnl_settlementOption.BackColor = System.Drawing.Color.Silver;
            this.pnl_settlementOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_settlementOption.Controls.Add(this.panel7);
            this.pnl_settlementOption.Controls.Add(this.settlementOption_panel);
            this.pnl_settlementOption.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_settlementOption.Location = new System.Drawing.Point(0, 0);
            this.pnl_settlementOption.Name = "pnl_settlementOption";
            this.pnl_settlementOption.Size = new System.Drawing.Size(870, 483);
            this.pnl_settlementOption.TabIndex = 19;
            // 
            // panel7
            // 
            this.panel7.AutoScroll = true;
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.lbl_modeOfPayment);
            this.panel7.Controls.Add(this.panel5);
            this.panel7.Controls.Add(this.rbtn_cash);
            this.panel7.Controls.Add(this.panel3);
            this.panel7.Controls.Add(this.rbtn_credit);
            this.panel7.Controls.Add(this.panel1);
            this.panel7.Controls.Add(this.rbtn_partialCredit);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 31);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(868, 450);
            this.panel7.TabIndex = 50;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(837, 481);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 50;
            // 
            // lbl_modeOfPayment
            // 
            this.lbl_modeOfPayment.AutoSize = true;
            this.lbl_modeOfPayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_modeOfPayment.Location = new System.Drawing.Point(27, 14);
            this.lbl_modeOfPayment.Name = "lbl_modeOfPayment";
            this.lbl_modeOfPayment.Size = new System.Drawing.Size(144, 19);
            this.lbl_modeOfPayment.TabIndex = 44;
            this.lbl_modeOfPayment.Text = "Mode Of Payment:";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btn_apply);
            this.panel5.Controls.Add(this.txt_discountDescription);
            this.panel5.Controls.Add(this.lbl_discountDescription);
            this.panel5.Controls.Add(this.lbl_discountAmount);
            this.panel5.Controls.Add(this.txt_discountAmount);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Location = new System.Drawing.Point(32, 307);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(766, 165);
            this.panel5.TabIndex = 49;
            // 
            // btn_apply
            // 
            this.btn_apply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_apply.FlatAppearance.BorderSize = 0;
            this.btn_apply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_apply.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_apply.ForeColor = System.Drawing.Color.White;
            this.btn_apply.Location = new System.Drawing.Point(524, 101);
            this.btn_apply.Name = "btn_apply";
            this.btn_apply.Size = new System.Drawing.Size(139, 35);
            this.btn_apply.TabIndex = 17;
            this.btn_apply.Text = "APPLY";
            this.btn_apply.UseVisualStyleBackColor = false;
            this.btn_apply.Click += new System.EventHandler(this.btn_apply_Click);
            // 
            // txt_discountDescription
            // 
            this.txt_discountDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_discountDescription.Location = new System.Drawing.Point(149, 87);
            this.txt_discountDescription.Multiline = true;
            this.txt_discountDescription.Name = "txt_discountDescription";
            this.txt_discountDescription.Size = new System.Drawing.Size(347, 62);
            this.txt_discountDescription.TabIndex = 8;
            // 
            // lbl_discountDescription
            // 
            this.lbl_discountDescription.AutoSize = true;
            this.lbl_discountDescription.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_discountDescription.Location = new System.Drawing.Point(10, 108);
            this.lbl_discountDescription.Name = "lbl_discountDescription";
            this.lbl_discountDescription.Size = new System.Drawing.Size(116, 19);
            this.lbl_discountDescription.TabIndex = 15;
            this.lbl_discountDescription.Text = "Discount Desc.";
            // 
            // lbl_discountAmount
            // 
            this.lbl_discountAmount.AutoSize = true;
            this.lbl_discountAmount.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_discountAmount.Location = new System.Drawing.Point(10, 52);
            this.lbl_discountAmount.Name = "lbl_discountAmount";
            this.lbl_discountAmount.Size = new System.Drawing.Size(133, 19);
            this.lbl_discountAmount.TabIndex = 14;
            this.lbl_discountAmount.Text = "Discount Amount";
            // 
            // txt_discountAmount
            // 
            this.txt_discountAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_discountAmount.Location = new System.Drawing.Point(149, 51);
            this.txt_discountAmount.Name = "txt_discountAmount";
            this.txt_discountAmount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_discountAmount.Size = new System.Drawing.Size(242, 24);
            this.txt_discountAmount.TabIndex = 7;
            this.txt_discountAmount.Text = "0.00";
            this.txt_discountAmount.TextChanged += new System.EventHandler(this.txt_discountAmount_TextChanged);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel6.Controls.Add(this.label5);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(764, 31);
            this.panel6.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label5.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label5.Location = new System.Drawing.Point(11, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 14);
            this.label5.TabIndex = 0;
            this.label5.Text = "DISCOUNT";
            // 
            // rbtn_cash
            // 
            this.rbtn_cash.AutoSize = true;
            this.rbtn_cash.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_cash.Location = new System.Drawing.Point(191, 13);
            this.rbtn_cash.Name = "rbtn_cash";
            this.rbtn_cash.Size = new System.Drawing.Size(59, 21);
            this.rbtn_cash.TabIndex = 39;
            this.rbtn_cash.Text = "Cash";
            this.rbtn_cash.UseVisualStyleBackColor = true;
            this.rbtn_cash.CheckedChanged += new System.EventHandler(this.rbtn_cash_CheckedChanged);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.lbl_type);
            this.panel3.Controls.Add(this.cmb_type);
            this.panel3.Controls.Add(this.lbl_cardNumber);
            this.panel3.Controls.Add(this.txt_cardNumber);
            this.panel3.Controls.Add(this.lbl_expire);
            this.panel3.Controls.Add(this.txt_cvv);
            this.panel3.Controls.Add(this.txt_expire);
            this.panel3.Controls.Add(this.lbl_cvv);
            this.panel3.Location = new System.Drawing.Point(32, 164);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(767, 127);
            this.panel3.TabIndex = 48;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel4.Controls.Add(this.label2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(765, 31);
            this.panel4.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label2.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label2.Location = new System.Drawing.Point(11, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "CREDIT";
            // 
            // lbl_type
            // 
            this.lbl_type.AutoSize = true;
            this.lbl_type.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_type.Location = new System.Drawing.Point(10, 43);
            this.lbl_type.Name = "lbl_type";
            this.lbl_type.Size = new System.Drawing.Size(44, 19);
            this.lbl_type.TabIndex = 28;
            this.lbl_type.Text = "Type";
            // 
            // cmb_type
            // 
            this.cmb_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_type.FormattingEnabled = true;
            this.cmb_type.Location = new System.Drawing.Point(106, 43);
            this.cmb_type.Name = "cmb_type";
            this.cmb_type.Size = new System.Drawing.Size(272, 26);
            this.cmb_type.TabIndex = 3;
            this.cmb_type.Leave += new System.EventHandler(this.cmb_type_Leave);
            // 
            // lbl_cardNumber
            // 
            this.lbl_cardNumber.AutoSize = true;
            this.lbl_cardNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cardNumber.Location = new System.Drawing.Point(10, 87);
            this.lbl_cardNumber.Name = "lbl_cardNumber";
            this.lbl_cardNumber.Size = new System.Drawing.Size(75, 19);
            this.lbl_cardNumber.TabIndex = 30;
            this.lbl_cardNumber.Text = "Card No*";
            // 
            // txt_cardNumber
            // 
            this.txt_cardNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cardNumber.Location = new System.Drawing.Point(106, 83);
            this.txt_cardNumber.Name = "txt_cardNumber";
            this.txt_cardNumber.Size = new System.Drawing.Size(272, 24);
            this.txt_cardNumber.TabIndex = 4;
            this.txt_cardNumber.Leave += new System.EventHandler(this.txt_cardNumber_Leave);
            // 
            // lbl_expire
            // 
            this.lbl_expire.AutoSize = true;
            this.lbl_expire.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_expire.Location = new System.Drawing.Point(397, 84);
            this.lbl_expire.Name = "lbl_expire";
            this.lbl_expire.Size = new System.Drawing.Size(61, 19);
            this.lbl_expire.TabIndex = 32;
            this.lbl_expire.Text = "Expire*";
            // 
            // txt_cvv
            // 
            this.txt_cvv.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cvv.Location = new System.Drawing.Point(470, 42);
            this.txt_cvv.Name = "txt_cvv";
            this.txt_cvv.Size = new System.Drawing.Size(120, 24);
            this.txt_cvv.TabIndex = 5;
            this.txt_cvv.Leave += new System.EventHandler(this.txt_cvv_Leave);
            // 
            // txt_expire
            // 
            this.txt_expire.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_expire.Location = new System.Drawing.Point(470, 83);
            this.txt_expire.Name = "txt_expire";
            this.txt_expire.Size = new System.Drawing.Size(120, 24);
            this.txt_expire.TabIndex = 6;
            this.txt_expire.Leave += new System.EventHandler(this.txt_expire_Leave);
            // 
            // lbl_cvv
            // 
            this.lbl_cvv.AutoSize = true;
            this.lbl_cvv.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cvv.Location = new System.Drawing.Point(397, 44);
            this.lbl_cvv.Name = "lbl_cvv";
            this.lbl_cvv.Size = new System.Drawing.Size(46, 19);
            this.lbl_cvv.TabIndex = 34;
            this.lbl_cvv.Text = "CVV*";
            // 
            // rbtn_credit
            // 
            this.rbtn_credit.AutoSize = true;
            this.rbtn_credit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_credit.Location = new System.Drawing.Point(293, 13);
            this.rbtn_credit.Name = "rbtn_credit";
            this.rbtn_credit.Size = new System.Drawing.Size(65, 21);
            this.rbtn_credit.TabIndex = 40;
            this.rbtn_credit.Text = "Credit";
            this.rbtn_credit.UseVisualStyleBackColor = true;
            this.rbtn_credit.CheckedChanged += new System.EventHandler(this.rbtn_credit_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txt_change);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txt_cashhanded);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(32, 46);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(767, 99);
            this.panel1.TabIndex = 47;
            // 
            // txt_change
            // 
            this.txt_change.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_change.Location = new System.Drawing.Point(501, 49);
            this.txt_change.Name = "txt_change";
            this.txt_change.ReadOnly = true;
            this.txt_change.Size = new System.Drawing.Size(247, 24);
            this.txt_change.TabIndex = 2;
            this.txt_change.Text = "0.00";
            this.txt_change.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(430, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 19);
            this.label4.TabIndex = 49;
            this.label4.Text = "Change";
            // 
            // txt_cashhanded
            // 
            this.txt_cashhanded.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cashhanded.Location = new System.Drawing.Point(197, 49);
            this.txt_cashhanded.Name = "txt_cashhanded";
            this.txt_cashhanded.Size = new System.Drawing.Size(193, 24);
            this.txt_cashhanded.TabIndex = 1;
            this.txt_cashhanded.Text = "0.00";
            this.txt_cashhanded.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_cashhanded.TextChanged += new System.EventHandler(this.txt_cashhanded_TextChanged);
            this.txt_cashhanded.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_cashhanded_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(178, 19);
            this.label3.TabIndex = 47;
            this.label3.Text = "Cash Handed by Guest";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.lbl_cashPayment);
            this.panel2.Controls.Add(this.txt_cashPayment);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(765, 31);
            this.panel2.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label1.Location = new System.Drawing.Point(11, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "CASH";
            // 
            // lbl_cashPayment
            // 
            this.lbl_cashPayment.AutoSize = true;
            this.lbl_cashPayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cashPayment.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_cashPayment.Location = new System.Drawing.Point(558, 7);
            this.lbl_cashPayment.Name = "lbl_cashPayment";
            this.lbl_cashPayment.Size = new System.Drawing.Size(101, 17);
            this.lbl_cashPayment.TabIndex = 45;
            this.lbl_cashPayment.Text = "Cash Payment";
            // 
            // txt_cashPayment
            // 
            this.txt_cashPayment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_cashPayment.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_cashPayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cashPayment.ForeColor = System.Drawing.Color.White;
            this.txt_cashPayment.Location = new System.Drawing.Point(665, 7);
            this.txt_cashPayment.Name = "txt_cashPayment";
            this.txt_cashPayment.ReadOnly = true;
            this.txt_cashPayment.Size = new System.Drawing.Size(97, 17);
            this.txt_cashPayment.TabIndex = 46;
            this.txt_cashPayment.Text = "0.00";
            this.txt_cashPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_cashPayment.TextChanged += new System.EventHandler(this.txt_cashPayment_TextChanged);
            this.txt_cashPayment.Leave += new System.EventHandler(this.txt_cashPayment_Leave);
            // 
            // rbtn_partialCredit
            // 
            this.rbtn_partialCredit.AutoSize = true;
            this.rbtn_partialCredit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_partialCredit.Location = new System.Drawing.Point(404, 13);
            this.rbtn_partialCredit.Name = "rbtn_partialCredit";
            this.rbtn_partialCredit.Size = new System.Drawing.Size(111, 21);
            this.rbtn_partialCredit.TabIndex = 41;
            this.rbtn_partialCredit.Text = "Partial Credit";
            this.rbtn_partialCredit.UseVisualStyleBackColor = true;
            this.rbtn_partialCredit.CheckedChanged += new System.EventHandler(this.rbtn_partialCredit_CheckedChanged);
            // 
            // settlementOption_panel
            // 
            this.settlementOption_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.settlementOption_panel.Controls.Add(this.lbl_settlementOption);
            this.settlementOption_panel.Controls.Add(this.lbl_requiredPrePayment);
            this.settlementOption_panel.Controls.Add(this.txt_requiredPrePayment);
            this.settlementOption_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.settlementOption_panel.Location = new System.Drawing.Point(0, 0);
            this.settlementOption_panel.Name = "settlementOption_panel";
            this.settlementOption_panel.Size = new System.Drawing.Size(868, 31);
            this.settlementOption_panel.TabIndex = 10;
            // 
            // lbl_settlementOption
            // 
            this.lbl_settlementOption.AutoSize = true;
            this.lbl_settlementOption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_settlementOption.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_settlementOption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_settlementOption.Location = new System.Drawing.Point(11, 8);
            this.lbl_settlementOption.Name = "lbl_settlementOption";
            this.lbl_settlementOption.Size = new System.Drawing.Size(132, 14);
            this.lbl_settlementOption.TabIndex = 0;
            this.lbl_settlementOption.Text = "SETTLEMENT OPTION";
            // 
            // lbl_requiredPrePayment
            // 
            this.lbl_requiredPrePayment.AutoSize = true;
            this.lbl_requiredPrePayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_requiredPrePayment.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_requiredPrePayment.Location = new System.Drawing.Point(582, 4);
            this.lbl_requiredPrePayment.Name = "lbl_requiredPrePayment";
            this.lbl_requiredPrePayment.Size = new System.Drawing.Size(118, 19);
            this.lbl_requiredPrePayment.TabIndex = 42;
            this.lbl_requiredPrePayment.Text = "Total Payment:";
            // 
            // txt_requiredPrePayment
            // 
            this.txt_requiredPrePayment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_requiredPrePayment.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_requiredPrePayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_requiredPrePayment.ForeColor = System.Drawing.Color.White;
            this.txt_requiredPrePayment.Location = new System.Drawing.Point(760, 5);
            this.txt_requiredPrePayment.Name = "txt_requiredPrePayment";
            this.txt_requiredPrePayment.ReadOnly = true;
            this.txt_requiredPrePayment.Size = new System.Drawing.Size(97, 17);
            this.txt_requiredPrePayment.TabIndex = 43;
            this.txt_requiredPrePayment.Text = "0.00";
            this.txt_requiredPrePayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Walkin_Reservation_SettlementOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(870, 479);
            this.Controls.Add(this.pnl_settlementOption);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Walkin_Reservation_SettlementOption";
            this.Text = "Walkin_Reservation_SettlementOption";
            this.Load += new System.EventHandler(this.Walkin_Reservation_SettlementOption_Load);
            this.pnl_settlementOption.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.settlementOption_panel.ResumeLayout(false);
            this.settlementOption_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_settlementOption;
        private System.Windows.Forms.Label lbl_modeOfPayment;
        private System.Windows.Forms.TextBox txt_requiredPrePayment;
        private System.Windows.Forms.Label lbl_requiredPrePayment;
        private System.Windows.Forms.RadioButton rbtn_partialCredit;
        private System.Windows.Forms.RadioButton rbtn_credit;
        private System.Windows.Forms.RadioButton rbtn_cash;
        private System.Windows.Forms.TextBox txt_cvv;
        private System.Windows.Forms.Label lbl_cvv;
        private System.Windows.Forms.TextBox txt_expire;
        private System.Windows.Forms.Label lbl_expire;
        private System.Windows.Forms.TextBox txt_cardNumber;
        private System.Windows.Forms.Label lbl_cardNumber;
        private System.Windows.Forms.ComboBox cmb_type;
        private System.Windows.Forms.Label lbl_type;
        private System.Windows.Forms.Panel settlementOption_panel;
        private System.Windows.Forms.Label lbl_settlementOption;
        private System.Windows.Forms.TextBox txt_cashPayment;
        private System.Windows.Forms.Label lbl_cashPayment;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_cashhanded;
        private System.Windows.Forms.TextBox txt_change;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lbl_discountDescription;
        private System.Windows.Forms.Label lbl_discountAmount;
        private System.Windows.Forms.TextBox txt_discountAmount;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox txt_discountDescription;
        private System.Windows.Forms.Button btn_apply;
        private System.Windows.Forms.Label label6;
    }
}