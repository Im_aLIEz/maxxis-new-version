﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace maxxis.Walkin_Reservation
{
    public partial class Walkin_Reservation_BillingSummary : Form
    {
        string con = System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString;
        //publics
        Walkin_Reservation.Walkin_Reservation_AccomodationDetails WR_AccomodationDetails = new Walkin_Reservation.Walkin_Reservation_AccomodationDetails();
        //variables
        int PWDoverall;
        int URoverall;
        int UCRoverall;
        int UVRoverall;
        string itemUnit;
        decimal amount;
        decimal additional;
        private static string totalpayment;
        private static string subpayment;
        private static string itempayment;

        public static string[] amenity;
        public static string[] quantity;
        public static string[] cost;
        public static string[] amenityamount;
        List<string> amenitylist = new List<string>();
        List<string> quantitylist = new List<string>();
        List<string> costlist = new List<string>();
        List<string> amenityamountlist = new List<string>();
        //publics
        public string SubPayment
        {
            get { return subpayment; }
            set { subpayment = value; }
        }
        public string TotalPayment
        {
            get { return totalpayment; }
            set { totalpayment = value; }
        }
        public string ItemPayment
        {
            get { return itempayment; }
            set { itempayment = value; }
        }
        public Walkin_Reservation_BillingSummary()
        {
            InitializeComponent();
        }

        internal void Walkin_Reservation_BillingSummary_Load(object sender, EventArgs e)
        {
            amount = 0;
            TotalPayment = "0";
            Initialization();
            Amenities();
        }

        //methods
        private void Initialization()
        {
            amount = 0;
            TotalPayment = (Decimal.Parse(WR_AccomodationDetails.TotalPayment) + additional).ToString("N2");


            for (int n = dgv_billingSummary.Rows.Count - 1; n >= 0; n--)
            {
                if (dgv_billingSummary.Rows[n].Cells[2].Value == "ROOM")
                {
                    dgv_billingSummary.Rows.RemoveAt(n);
                }
            }

            if (WR_AccomodationDetails.BillChecker == 1)
            {
                
                if (Int32.Parse(WR_AccomodationDetails.PWDSelect) != 0)
                {
                    dgv_billingSummary.Rows.Add("PWD Urban Room", WR_AccomodationDetails.PWDSelect, "ROOM", WR_AccomodationDetails.PWDRate,Int32.Parse(WR_AccomodationDetails.PWDRate) * Int32.Parse(WR_AccomodationDetails.NumberOfNights));
                }
                if (Int32.Parse(WR_AccomodationDetails.UCRSelect) != 0)
                {
                    dgv_billingSummary.Rows.Add("Urban Corner Room", WR_AccomodationDetails.UCRSelect, "ROOM", WR_AccomodationDetails.UCRRate,Int32.Parse(WR_AccomodationDetails.UCRRate) * Int32.Parse(WR_AccomodationDetails.NumberOfNights));
                }
                if (Int32.Parse(WR_AccomodationDetails.URSelect) != 0)
                {
                    dgv_billingSummary.Rows.Add("Urban Room", WR_AccomodationDetails.URSelect, "ROOM", WR_AccomodationDetails.URRate, Int32.Parse(WR_AccomodationDetails.URRate) * Int32.Parse(WR_AccomodationDetails.NumberOfNights));
                }
                if (Int32.Parse(WR_AccomodationDetails.UVRSelect) != 0)
                {
                    dgv_billingSummary.Rows.Add("Urban View Room", WR_AccomodationDetails.UVRSelect, "ROOM", WR_AccomodationDetails.UVRRate,Int32.Parse(WR_AccomodationDetails.UVRRate) * Int32.Parse(WR_AccomodationDetails.NumberOfNights));
                }
                
                for (int x = 0; x < dgv_billingSummary.Rows.Count; x++)
                {
                    amount = amount + Decimal.Parse(dgv_billingSummary.Rows[x].Cells[4].Value.ToString());
                }
                txt_billingSummary_Total.Text = "Php " + amount.ToString("N2");
            }
            SubPayment = amount.ToString("N2");
            if(ItemPayment==null)
            {
                ItemPayment = "0.00";
            }
            dgv_payments.Rows.Clear();
            txt_billingSummary_Total.Text = TotalPayment;
            dgv_payments.Rows.Add("Total Amount", TotalPayment);
            dgv_payments.Rows.Add("(Less) Discount");
        }
        public void Amenities()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select itemdesc from tbl_amenities where isActive = 1", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                cbox_add.Items.Clear();
                foreach (DataRow item in dt.Rows)
                {
                    cbox_add.Items.Add(item[0]);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void AmenitiesCost()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select cost,itemtype from tbl_amenities where itemdesc = '"+ this.cbox_add.Text +"'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                { 
                    decimal cost = decimal.Parse(item[0].ToString());
                    txt_cost.Text = cost.ToString("N2");
                    itemUnit = item[1].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void AddAmenity()
        {

            for (int n = dgv_billingSummary.Rows.Count - 1; n >= 0; n--)
            {
                if (dgv_billingSummary.Rows[n].Cells[2].Value == "ROOM")
                {
                    dgv_billingSummary.Rows.RemoveAt(n);
                }
            }
            dgv_billingSummary.Rows.Add(cbox_add.Text, numUD_quantity.Text, itemUnit, txt_cost.Text, Int32.Parse(numUD_quantity.Text) * Decimal.Parse(txt_cost.Text)).ToString();
            TotalPayment = (Decimal.Parse(TotalPayment) + Int32.Parse(numUD_quantity.Text) * Decimal.Parse(txt_cost.Text)).ToString();
            amenitylist.Add(cbox_add.Text);
            quantitylist.Add(numUD_quantity.Text);
            costlist.Add(txt_cost.Text);
            amenityamountlist.Add((Int32.Parse(numUD_quantity.Text) * Decimal.Parse(txt_cost.Text)).ToString());
            additional = additional + (Decimal.Parse(txt_cost.Text) * Decimal.Parse(numUD_quantity.Text));
            txt_billingSummary_Total.Text = "Php " + amount.ToString("N2");
            amenity = amenitylist.ToArray();
            quantity = quantitylist.ToArray();
            cost = costlist.ToArray();
            amenityamount = amenityamountlist.ToArray();
            ItemPayment = (Decimal.Parse(ItemPayment) + (Int32.Parse(numUD_quantity.Text) * Decimal.Parse(txt_cost.Text))).ToString("N2");
            Initialization();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            amount = 0;
            AddAmenity();
            Amenities();
            Initialization();
            cbox_add.Text = "";
            numUD_quantity.Text = "";
            txt_cost.Text = "";
        }

        private void cbox_add_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            AmenitiesCost();
            numUD_quantity.Text = "1";
        }

        private void numericUpDown1_MouseUp(object sender, MouseEventArgs e)
        {
            
        }
    }
}
