﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace maxxis
{
    public partial class WalkinReservation_form : Form
    {
        string con = System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString;
        public int check_in_ready = 0;
        public string guest_check_in { get; set; }
        int Checker;

        string hash = "MAXXis";

        string startaccessencrypt;
        string endaccessencrypt;

        DateTime startaccess;
        DateTime endaccess;

        //from guest arrival

        //variables
        string selectedPWD;
        string selectedUR;
        string selectedUCR;
        string selectedUVR;

        string folioNo;

        public static string guestId;
        //Instatiations
        System.Timers.Timer t;
        int h, m, s;
        //Methods
        public void Access()
        {
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select * From tbl_online", conDatabase);
            conDatabase.Open();
            SqlDataAdapter sda = new SqlDataAdapter();
            sda.SelectCommand = cmdDataBase;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            try
            {
                foreach (DataRow item in dt.Rows)
                {
                    startaccessencrypt = item[1].ToString();
                    endaccessencrypt = item[2].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            conDatabase.Close();
        }
        public void AccessEnc()
        {
            byte[] data = Convert.FromBase64String(startaccessencrypt);
            byte[] data1 = Convert.FromBase64String(endaccessencrypt);
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
                using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider() { Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
                {
                    ICryptoTransform transform = tripDes.CreateDecryptor();
                    byte[] results = transform.TransformFinalBlock(data, 0, data.Length);
                    byte[] results1 = transform.TransformFinalBlock(data1, 0, data1.Length);
                    startaccess = Convert.ToDateTime(UTF8Encoding.UTF8.GetString(results));
                    endaccess = Convert.ToDateTime(UTF8Encoding.UTF8.GetString(results1));
                }
            }
        }
        public void CheckIn()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "insert into tbl_guestinfo(title," +
                    "lastname," +
                    "firstname," +
                    "middlename," +
                    "dateofbirth," +
                    "address," +
                    "contact_details," +
                    "email," +
                    "province," +
                    "home_country," +
                    "company_or_agency," +
                    "city," +
                    "checked_in," +
                    "urselect," +
                    "ucrselect," +
                    "uvrselect," +
                    "remarks," +
                    "passport_no," +
                    "visa_no," +
                    "img," +
                    "pwdselect," +
                    "folio_number," +
                    "room_types," +
                    "room_no," +
                    "express_active) values ('"
                    + WR_GuestInfo.Title
                    + "','" + WR_GuestInfo.LastName
                    + "','" + WR_GuestInfo.FirstName
                    + "','" + WR_GuestInfo.MiddleName
                    + "','" + WR_GuestInfo.DateOfBirth
                    + "','" + WR_GuestInfo.Address
                    + "','" + WR_GuestInfo.Contact
                    + "','" + WR_GuestInfo.Email
                    + "','" + WR_GuestInfo.Province
                    + "','" + WR_GuestInfo.HomeCountry
                    + "','" + WR_GuestInfo.CompanyOrAgency
                    + "','" + WR_GuestInfo.City
                    + "','" + 1
                    + "','" + WR_AccomodationDetails.URSelect
                    + "','" + WR_AccomodationDetails.UCRSelect
                    + "','" + WR_AccomodationDetails.UVRSelect
                    + "','" + WR_AccomodationDetails.Remarks
                    + "','" + WR_GuestInfo.Passport
                    + "','" + WR_GuestInfo.Visa
                    + "','" + WR_GuestInfo.GuestImageSaver
                    + "','" + WR_AccomodationDetails.PWDSelect
                    + "','" + folioNo
                    + "','" + WR_FolioSummary.RoomTypes
                    + "','" + WR_FolioSummary.SelectedRooms
                    + "','" + "1" + "')";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                    selectedUR = WR_AccomodationDetails.URSelect;
                    selectedUCR = WR_AccomodationDetails.UCRSelect;
                    selectedUVR = WR_AccomodationDetails.UVRSelect;
                }
                conDatabase.Close();
                MessageBox.Show("Guest Checked In");
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void RetrieveGuestId()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT MAX(id) FROM tbl_guestinfo;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    guestId = item[0].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void RoomPWDCheckIn()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "update tbl_rooms set guest_id = ('" + guestId + "'),occupied = 1,available = 0 where room_type = 'PWD Urban Room' AND room_no = '" + selectedPWD + "';";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void RoomURCheckIn()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "update tbl_rooms set guest_id = ('" + guestId + "'),occupied = 1,available = 0 where room_type = 'Urban Room' AND room_no = '" + selectedUR + "' ;";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void RoomUCRCheckIn()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "update tbl_rooms set guest_id = ('" + guestId + "'),occupied = 1,available = 0 where room_type = 'Urban Corner Room' AND room_no = '" + selectedUCR + "' ;";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void RoomUVRCheckIn()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "update tbl_rooms set guest_id = ('" + guestId + "'),occupied = 1,available = 0 where room_type = 'Urban View Room' AND room_no = '" + selectedUVR + "' ;";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void PWDRoomChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'PWD Urban Room' AND occupied = 0 ORDER BY room_no ASC ;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    selectedPWD = item[0].ToString();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void URRoomupperChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'Urban Room' AND occupied = 0 AND room_no > 70 ORDER BY room_no DESC;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if(dt != null)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        selectedUR = item[0].ToString();

                    }
                }
                else
                {
                    selectedUR = null;
                }
                


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void UCRRoomupperChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'Urban Corner Room' AND occupied = 0 AND room_no > 70 ORDER BY room_no DESC ;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        selectedUCR = item[0].ToString();

                    }
                }
                else
                {
                    selectedUCR = null;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void UVRRoomupperChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'Urban View Room' AND occupied = 0 AND room_no > 70 ORDER BY room_no DESC ;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        selectedUVR = item[0].ToString();

                    }
                }
                else
                {
                    selectedUVR = null;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void URRoomlowerChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'Urban Room' AND occupied = 0 AND room_no < 70 ORDER BY room_no ASC;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    selectedUR = item[0].ToString();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void UCRRoomlowerChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'Urban Corner Room' AND occupied = 0 AND room_no < 70 ORDER BY room_no ASC ;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    selectedUCR = item[0].ToString();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void UVRRoomlowerChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'Urban View Room' AND occupied = 0 AND room_no < 70 ORDER BY room_no ASC ;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    selectedUVR = item[0].ToString();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void StayInfo()
        {
            try
            {

                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "insert into tbl_stayinfo(guest_id,arrival,departure,no_ofnights,totalpayment,adults,children,extraadults,extrachildren) values('"
                    + guestId
                    + "','" + WR_AccomodationDetails.CheckIn
                    + "','" + WR_AccomodationDetails.CheckOut
                    + "','" + WR_AccomodationDetails.NumberOfNights
                    + "','" + WR_AccomodationDetails.TotalPayment
                    + "' ,'" + WR_FolioSummary.OverallAdult
                    + "' ,'" + WR_FolioSummary.OverallChildren
                    + "' ,'" + WR_AccomodationDetails.ExtraAdult
                    + "' ,'" + WR_AccomodationDetails.ExtraChild + "');";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void BusinessSource()
        {
            try
            {

                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "insert into tbl_businesssource(guest_id,marketplace,marketsegment,purposeofstay) values('"
                    + guestId
                    + "','" + WR_BusinessSource.MarketPlace
                    + "','" + WR_BusinessSource.MarketSegment
                    + "','" + WR_BusinessSource.PurposeOfStay + "');";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void Billing()
        {
            try
            {

                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "insert into tbl_billing(guest_id,preauthpayment,modeofpayment,preauthpaid,itempayment,urpayment,ucrpayment,uvrpayment,totalpayment,pwdpayment,balance) values('"
                    + guestId
                    + "','" + WR_BillingSummary.SubPayment
                    + "','" + WR_SettlementOption.Mode
                    + "','" + WR_SettlementOption.CashPaid
                    + "','" + WR_BillingSummary.ItemPayment
                    + "','" + WR_AccomodationDetails.URRate
                    + "','" + WR_AccomodationDetails.UCRRate
                    + "','" + WR_AccomodationDetails.UVRRate
                    + "','" + WR_BillingSummary.TotalPayment
                    + "','" + WR_AccomodationDetails.PWDRate
                    + "','" + (Decimal.Parse(WR_BillingSummary.TotalPayment) - Decimal.Parse(WR_BillingSummary.SubPayment)) + "');";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /*
        public void GuestAmenities()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "insert into tbl_stayinfo(guest_id,arrival,departure,no_ofnights,totalpayment,adults,children,extraadults,extrachildren) values('"
                    + guestId
                    + "','" + WR_AccomodationDetails.CheckIn
                    + "','" + WR_AccomodationDetails.CheckOut
                    + "','" + WR_AccomodationDetails.NumberOfNights
                    + "','" + WR_AccomodationDetails.TotalPayment
                    + "' ,'" + WR_FolioSummary.OverallAdult
                    + "' ,'" + WR_FolioSummary.OverallChildren
                    + "' ,'" + WR_AccomodationDetails.ExtraAdult
                    + "' ,'" + WR_AccomodationDetails.ExtraChild + "');";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        */
        //Guest Information
        Walkin_Reservation.Walkin_Reservation_GuestInfo WR_GuestInfo = new Walkin_Reservation.Walkin_Reservation_GuestInfo();
        //Accomodation Details
        Walkin_Reservation.Walkin_Reservation_AccomodationDetails WR_AccomodationDetails = new Walkin_Reservation.Walkin_Reservation_AccomodationDetails();
        //Hotel Transfer
        Walkin_Reservation.Walkin_Reservation_HotelTransfer WR_HotelTransfer = new Walkin_Reservation.Walkin_Reservation_HotelTransfer();
        //Business Source
        Walkin_Reservation.Walkin_Reservation_BusinessSource WR_BusinessSource = new Walkin_Reservation.Walkin_Reservation_BusinessSource();
        //Visa Information
        Walkin_Reservation.Walkin_Reservation_VisaInformation WR_VisaInfo = new Walkin_Reservation.Walkin_Reservation_VisaInformation();
        //Settlement Options
        Walkin_Reservation.Walkin_Reservation_SettlementOption WR_SettlementOption = new Walkin_Reservation.Walkin_Reservation_SettlementOption();
        //Arrival And Departure
        Walkin_Reservation.Walkin_Reservation_ArrivalDeparture WR_ArrivalDeparture = new Walkin_Reservation.Walkin_Reservation_ArrivalDeparture();
        //Billing Summary
        Walkin_Reservation.Walkin_Reservation_BillingSummary WR_BillingSummary = new Walkin_Reservation.Walkin_Reservation_BillingSummary();
        //Discount
        Walkin_Reservation.Walkin_Reservation_Discount WR_Discount = new Walkin_Reservation.Walkin_Reservation_Discount();
        //Payments
        Walkin_Reservation.Walkin_Reservation_Payments WR_Payments = new Walkin_Reservation.Walkin_Reservation_Payments();
        //Folio Summary
        Walkin_Reservation.Walkin_Reservation_FolioSummary WR_FolioSummary = new Walkin_Reservation.Walkin_Reservation_FolioSummary();

        Main_Form mainfrm = new Main_Form();

        
        public WalkinReservation_form()
        {
            InitializeComponent();
        }

        private void WalkinReservation_form_Load(object sender, EventArgs e)
        {
            WR_AccomodationDetails.URSelect = "0";
            WR_AccomodationDetails.UCRSelect = "0";
            WR_AccomodationDetails.UVRSelect = "0";
            WR_AccomodationDetails.PWDSelect = "0";
            t = new System.Timers.Timer();
            t.Interval = 1000;
            t.Elapsed += OnTimeEvent;
            t.Start();
            WR_GuestInfo.Title = "";
            WR_GuestInfo.LastName = "";
            WR_GuestInfo.FirstName = "";
            WR_GuestInfo.MiddleName = "";
            WR_GuestInfo.City = "";
            WR_GuestInfo.Province = "";
            WR_GuestInfo.Contact = "";
            WR_GuestInfo.Email = "";
            WR_GuestInfo.Address = "";
            WR_GuestInfo.HomeCountry = "";
            WR_GuestInfo.DateOfBirth = "";
            WR_BillingSummary.SubPayment = "";
            RetrieveGuestId();
            if (check_in_ready == 1)
            {

            }

            else
            {
                //changes the yellow selector
                pnl_newReservation_Selector.Height = btn_guestInformation.Height;
                pnl_newReservation_Selector.Top = btn_guestInformation.Top;

                //Guest Information
                WR_GuestInfo.TopLevel = false;
                WR_GuestInfo.Dock = DockStyle.Right;
                lbl_timer.Controls.Add(WR_GuestInfo);
                WR_GuestInfo.Show();

                //Accomodation Details
                WR_AccomodationDetails.TopLevel = false;
                WR_AccomodationDetails.Dock = DockStyle.Right;
                lbl_timer.Controls.Add(WR_AccomodationDetails);
                WR_AccomodationDetails.Hide();

                //Hotel Transfer
                WR_HotelTransfer.TopLevel = false;
                WR_HotelTransfer.Dock = DockStyle.Right;
                lbl_timer.Controls.Add(WR_HotelTransfer);
                WR_HotelTransfer.Hide();

                //Buesiness Source
                WR_BusinessSource.TopLevel = false;
                WR_BusinessSource.Dock = DockStyle.Right;
                lbl_timer.Controls.Add(WR_BusinessSource);
                WR_BusinessSource.Hide();

                //Visa Information
                WR_VisaInfo.TopLevel = false;
                WR_VisaInfo.Dock = DockStyle.Right;
                lbl_timer.Controls.Add(WR_VisaInfo);
                WR_VisaInfo.Hide();

                //Settelement Option
                WR_SettlementOption.TopLevel = false;
                WR_SettlementOption.Dock = DockStyle.Right;
                lbl_timer.Controls.Add(WR_SettlementOption);
                WR_SettlementOption.Hide();

                //Arrival and Departure Information
                WR_ArrivalDeparture.TopLevel = false;
                WR_ArrivalDeparture.Dock = DockStyle.Right;
                lbl_timer.Controls.Add(WR_ArrivalDeparture);
                WR_ArrivalDeparture.Hide();

                //Billing Summary
                WR_BillingSummary.TopLevel = false;
                WR_BillingSummary.Dock = DockStyle.Right;
                lbl_timer.Controls.Add(WR_BillingSummary);
                WR_BillingSummary.Hide();

                //Discount
                WR_Discount.TopLevel = false;
                WR_Discount.Dock = DockStyle.Right;
                lbl_timer.Controls.Add(WR_Discount);
                WR_Discount.Hide();

                //Payments
                WR_Payments.TopLevel = false;
                WR_Payments.Dock = DockStyle.Right;
                lbl_timer.Controls.Add(WR_Payments);
                WR_Payments.Hide();

                //Folio Summary
                WR_FolioSummary.TopLevel = false;
                WR_FolioSummary.Dock = DockStyle.Right;
                lbl_timer.Controls.Add(WR_FolioSummary);
                WR_FolioSummary.Hide();


            }
        }
        private void btn_reserve_Click(object sender, EventArgs e)
        {
            ReservationV2_Form frm = new ReservationV2_Form();
            this.Hide();
            frm.Show();
        }
        private void OnTimeEvent(object sender, EventArgs e)
        {
            Invoke(new Action(() =>
            {
                s += 1;
                if (s == 60)
                {
                    s = 0;
                    m += 1;
                }
                if (m == 60)
                {
                    m = 0;
                    h += 1;
                }
                txt_timer.Text = string.Format("{0}:{1}:{2}", h.ToString().PadLeft(2, '0'), m.ToString().PadLeft(2, '0'), s.ToString().PadLeft(2, '0'));
            }));
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            if (check_in_ready == 1)
            {
                t.Stop();
                this.Close();
                mainfrm.Main_Form_Load(mainfrm, EventArgs.Empty);
                mainfrm.Show();
            }
            else
            {
                t.Stop();
                this.Close();
                mainfrm.Main_Form_Load(mainfrm, EventArgs.Empty);
                mainfrm.Show();
            }
            WR_AccomodationDetails.PWDSelect = "0";
            WR_AccomodationDetails.URSelect = "0";
            WR_AccomodationDetails.UCRSelect = "0";
            WR_AccomodationDetails.UVRSelect = "0";
        }
        private void btn_checkIn_Click(object sender, EventArgs e)
        {
            
            Access();
            AccessEnc();
            if (DateTime.Now < startaccess)
            {
                MessageBox.Show("Error Code: 0239, Please Contact Your Technical Support");
                Application.Exit();
            }
            else if (DateTime.Now > endaccess)
            {
                MessageBox.Show("Error Code: 0239, Please Contact Your Technical Support");
                Application.Exit();
            }
            
            folioNo = "F"+DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
            if (WR_GuestInfo.Title.Equals("")
                || WR_GuestInfo.FirstName.Equals("")
                || WR_GuestInfo.MiddleName.Equals("")
                || WR_GuestInfo.FirstName.Equals("")
                || WR_GuestInfo.Contact.Equals("")
                || WR_GuestInfo.Email.Equals("")
                || WR_GuestInfo.City.Equals("")
                || WR_GuestInfo.Province.Equals("")
                || WR_GuestInfo.HomeCountry.Equals("")
                || WR_GuestInfo.DateOfBirth.Equals("")
                || WR_BillingSummary.SubPayment.Equals("")
                || WR_GuestInfo.GuestImageSaver.Equals(""))
            {
                MessageBox.Show("Please Fill up all the Required Fields");
            }

            else if (Checker == 0)
            {
                MessageBox.Show("Please Check the Folio Summary before you Check In");
            }
            else
            {
                CheckIn();
                RetrieveGuestId();
                if (Int32.Parse(WR_AccomodationDetails.PWDSelect) >= 1)
                {
                    for (int x = 0; x < Int32.Parse(WR_AccomodationDetails.PWDSelect); x++)
                    {
                        PWDRoomChecker();
                        RoomPWDCheckIn();
                    }
                }
                if (Int32.Parse(WR_AccomodationDetails.URSelect) >= 1)
                {
                    for (int x = 0; x < Int32.Parse(WR_AccomodationDetails.URSelect); x++)
                    {
                        URRoomupperChecker();
                        if (selectedUR == null)
                        {
                            URRoomlowerChecker();
                        }
                        RoomURCheckIn();
                    }
                }
                if (Int32.Parse(WR_AccomodationDetails.UCRSelect) >= 1)
                {
                    for (int x = 0; x < Int32.Parse(WR_AccomodationDetails.UCRSelect); x++)
                    {
                        UCRRoomupperChecker();
                        if (selectedUCR == null)
                        {
                            UCRRoomlowerChecker();
                        }
                        RoomUCRCheckIn();


                    }
                }
                if (Int32.Parse(WR_AccomodationDetails.UVRSelect) >= 1)
                {
                    for (int x = 0; x < Int32.Parse(WR_AccomodationDetails.UVRSelect); x++)
                    {
                        UVRRoomupperChecker();
                        if (selectedUVR == null)
                        {
                            UVRRoomlowerChecker();
                        }
                        RoomUVRCheckIn();
                    }
                }
                WR_GuestInfo.GuestImageFile.Save(WR_GuestInfo.GuestImageSaver);
                StayInfo();
                BusinessSource();
                Billing();
                t.Stop();
                this.Close();
                mainfrm.Main_Form_Load(mainfrm, EventArgs.Empty);
                mainfrm.Show();
                WR_AccomodationDetails.PWDSelect = "0";
                WR_AccomodationDetails.URSelect = "0";
                WR_AccomodationDetails.UCRSelect = "0";
                WR_AccomodationDetails.UVRSelect = "0";
            }




        }



        private void btn_guestInformation_Click(object sender, EventArgs e)
        {
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_guestInformation.Height;
            pnl_newReservation_Selector.Top = btn_guestInformation.Top;

            //disables the panels
            WR_GuestInfo.Show();
            WR_AccomodationDetails.Hide();
            WR_HotelTransfer.Hide();
            WR_BusinessSource.Hide();
            WR_VisaInfo.Hide();
            WR_SettlementOption.Hide();
            WR_ArrivalDeparture.Hide();
            WR_BillingSummary.Hide();
            WR_Discount.Hide();
            WR_Payments.Hide();
            WR_FolioSummary.Hide();
            Checker = 0;
        }

        private void btn_accomodationDetails_Click(object sender, EventArgs e)
        {
            //btn_billingSummary.Enabled = true;
            btn_settlementOption.Enabled = false;
            //btn_discounts.Enabled = false;
            //btn_payments.Enabled = false;
            btn_folioSummary.Enabled = false;
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_accomodationDetails.Height;
            pnl_newReservation_Selector.Top = btn_accomodationDetails.Top;


            //disables the panels
            WR_GuestInfo.Hide();
            WR_AccomodationDetails.Show();
            WR_HotelTransfer.Hide();
            WR_BusinessSource.Hide();
            WR_VisaInfo.Hide();
            WR_SettlementOption.Hide();
            WR_ArrivalDeparture.Hide();
            WR_BillingSummary.Hide();
            WR_Discount.Hide();
            WR_Payments.Hide();
            WR_FolioSummary.Hide();
            Checker = 0;
        }



        private void btn_businessSource_Click(object sender, EventArgs e)
        {
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_businessSource.Height;
            pnl_newReservation_Selector.Top = btn_businessSource.Top;

            //disables the panels
            WR_GuestInfo.Hide();
            WR_AccomodationDetails.Hide();
            WR_HotelTransfer.Hide();
            WR_BusinessSource.Show();
            WR_VisaInfo.Hide();
            WR_SettlementOption.Hide();
            WR_ArrivalDeparture.Hide();
            WR_BillingSummary.Hide();
            WR_Discount.Hide();
            WR_Payments.Hide();
            WR_FolioSummary.Hide();
            Checker = 0;
        }



        private void btn_settlementOption_Click(object sender, EventArgs e)
        {
            WR_SettlementOption.Walkin_Reservation_SettlementOption_Load(WR_SettlementOption, EventArgs.Empty);
            //btn_discounts.Enabled = true;
            btn_folioSummary.Enabled = true;
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_settlementOption.Height;
            pnl_newReservation_Selector.Top = btn_settlementOption.Top;

            //disables the panels
            WR_GuestInfo.Hide();
            WR_AccomodationDetails.Hide();
            WR_HotelTransfer.Hide();
            WR_BusinessSource.Hide();
            WR_VisaInfo.Hide();
            WR_SettlementOption.Show();
            WR_ArrivalDeparture.Hide();
            WR_BillingSummary.Hide();
            WR_Discount.Hide();
            WR_Payments.Hide();
            WR_FolioSummary.Hide();
            Checker = 0;
        }

        private void btn_billingSummary_Click(object sender, EventArgs e)
        {
            WR_BillingSummary.Walkin_Reservation_BillingSummary_Load(WR_BillingSummary, EventArgs.Empty);
            btn_settlementOption.Enabled = true;
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_billingSummary.Height;
            pnl_newReservation_Selector.Top = btn_billingSummary.Top;

            //disables the panels

            WR_GuestInfo.Hide();

            WR_AccomodationDetails.Hide();
            WR_HotelTransfer.Hide();
            WR_BusinessSource.Hide();
            WR_VisaInfo.Hide();
            WR_SettlementOption.Hide();
            WR_ArrivalDeparture.Hide();
            WR_BillingSummary.Show();
            WR_BillingSummary.Refresh();
            WR_Discount.Hide();
            WR_Payments.Hide();
            WR_FolioSummary.Hide();
            Checker = 0;
        }

        private void btn_discounts_Click(object sender, EventArgs e)
        {
            //changes the yellow selector
            //pnl_newReservation_Selector.Height = btn_discounts.Height;
            //nl_newReservation_Selector.Top = btn_discounts.Top;

            //disables the panels
            WR_GuestInfo.Hide();
            WR_AccomodationDetails.Hide();
            WR_HotelTransfer.Hide();
            WR_BusinessSource.Hide();
            WR_VisaInfo.Hide();
            WR_SettlementOption.Hide();
            WR_ArrivalDeparture.Hide();
            WR_BillingSummary.Hide();
            WR_Discount.Show();
            WR_Payments.Hide();
            WR_FolioSummary.Hide();
            Checker = 0;
        }

        private void btn_payments_Click(object sender, EventArgs e)
        {
            WR_Payments.Walkin_Reservation_Payments_Load_1(WR_Payments, EventArgs.Empty);
            btn_folioSummary.Enabled = true;
            //changes the yellow selector
            //pnl_newReservation_Selector.Height = btn_payments.Height;
            //pnl_newReservation_Selector.Top = btn_payments.Top;

            //disables the panels
            WR_GuestInfo.Hide();
            WR_AccomodationDetails.Hide();
            WR_HotelTransfer.Hide();
            WR_BusinessSource.Hide();
            WR_VisaInfo.Hide();
            WR_SettlementOption.Hide();
            WR_ArrivalDeparture.Hide();
            WR_BillingSummary.Hide();
            WR_Discount.Hide();
            WR_Payments.Show();
            WR_FolioSummary.Hide();
            Checker = 0;
        }

        private void btn_folioSummary_Click(object sender, EventArgs e)
        {
            WR_FolioSummary.Walkin_Reservation_FolioSummary_Load(WR_FolioSummary, EventArgs.Empty);
            //changes the yellow selector
            pnl_newReservation_Selector.Height = btn_folioSummary.Height;
            pnl_newReservation_Selector.Top = btn_folioSummary.Top;

            //disables the panels
            WR_GuestInfo.Hide();
            WR_AccomodationDetails.Hide();
            WR_HotelTransfer.Hide();
            WR_BusinessSource.Hide();
            WR_VisaInfo.Hide();
            WR_SettlementOption.Hide();
            WR_ArrivalDeparture.Hide();
            WR_BillingSummary.Hide();
            WR_Discount.Hide();
            WR_Payments.Hide();
            WR_FolioSummary.Show();

            Checker = 1;
        }

        private void btn_arrivalAndDeparture_Click(object sender, EventArgs e)
        {
            //changes the yellow selector
            //pnl_newReservation_Selector.Height = btn_arrivalAndDeparture.Height;
            //pnl_newReservation_Selector.Top = btn_arrivalAndDeparture.Top;
            System.Diagnostics.Process.Start("http://maxxhotelmakati.com/booking/reservation.php");

            //disables the panels

        }

        private void txt_timer_TextChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(WR_AccomodationDetails.URSelect)!=0|| Int32.Parse(WR_AccomodationDetails.PWDSelect) != 0|| Int32.Parse(WR_AccomodationDetails.UVRSelect) != 0|| Int32.Parse(WR_AccomodationDetails.UCRSelect) != 0)
            {
                btn_billingSummary.Enabled = true;
            }
            else
            {
                btn_billingSummary.Enabled = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        public void BillingButton()
        {
            btn_billingSummary.Enabled = true;
        }
    }
    
}
