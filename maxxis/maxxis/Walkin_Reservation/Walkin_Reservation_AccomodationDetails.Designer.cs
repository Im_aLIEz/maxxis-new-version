﻿namespace maxxis.Walkin_Reservation
{
    partial class Walkin_Reservation_AccomodationDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnl_accomodationDetails = new System.Windows.Forms.Panel();
            this.btn_companionName = new System.Windows.Forms.Button();
            this.lbl_nights = new System.Windows.Forms.Label();
            this.cbox_children = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbox_adult = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbox_extrachildren = new System.Windows.Forms.ComboBox();
            this.cbox_extraadults = new System.Windows.Forms.ComboBox();
            this.numUD_roomQuantity = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_guestRemarks = new System.Windows.Forms.TextBox();
            this.lbl_guestRemarks = new System.Windows.Forms.Label();
            this.btn_viewRoomDetails = new System.Windows.Forms.Button();
            this.lbl_amount = new System.Windows.Forms.Label();
            this.txt_childTotalFee = new System.Windows.Forms.TextBox();
            this.txt_adultsTotalFee = new System.Windows.Forms.TextBox();
            this.lbl_feePerNight = new System.Windows.Forms.Label();
            this.txt_childFee = new System.Windows.Forms.TextBox();
            this.txt_adultsFee = new System.Windows.Forms.TextBox();
            this.lbl_child = new System.Windows.Forms.Label();
            this.lbl_adults = new System.Windows.Forms.Label();
            this.lbl_additionalCompanion = new System.Windows.Forms.Label();
            this.dgv_listOfRooms = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_RoomNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_RoomType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_RoomRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Adult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Child = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Available = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbl_notification = new System.Windows.Forms.Label();
            this.lbl_numberOfNights = new System.Windows.Forms.Label();
            this.dtp_departureDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_arrivalDate = new System.Windows.Forms.DateTimePicker();
            this.lbl_checkOut = new System.Windows.Forms.Label();
            this.lbl_chekIn = new System.Windows.Forms.Label();
            this.accomo_panel = new System.Windows.Forms.Panel();
            this.txt_accomo_accomodationTotal = new System.Windows.Forms.TextBox();
            this.lbl_accomodationTotal = new System.Windows.Forms.Label();
            this.lbl_accomodationDetails = new System.Windows.Forms.Label();
            this.pnl_accomodationDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_roomQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listOfRooms)).BeginInit();
            this.accomo_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_accomodationDetails
            // 
            this.pnl_accomodationDetails.AutoScroll = true;
            this.pnl_accomodationDetails.BackColor = System.Drawing.Color.Silver;
            this.pnl_accomodationDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_accomodationDetails.Controls.Add(this.btn_companionName);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_nights);
            this.pnl_accomodationDetails.Controls.Add(this.cbox_children);
            this.pnl_accomodationDetails.Controls.Add(this.label3);
            this.pnl_accomodationDetails.Controls.Add(this.cbox_adult);
            this.pnl_accomodationDetails.Controls.Add(this.label2);
            this.pnl_accomodationDetails.Controls.Add(this.cbox_extrachildren);
            this.pnl_accomodationDetails.Controls.Add(this.cbox_extraadults);
            this.pnl_accomodationDetails.Controls.Add(this.numUD_roomQuantity);
            this.pnl_accomodationDetails.Controls.Add(this.label1);
            this.pnl_accomodationDetails.Controls.Add(this.txt_guestRemarks);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_guestRemarks);
            this.pnl_accomodationDetails.Controls.Add(this.btn_viewRoomDetails);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_amount);
            this.pnl_accomodationDetails.Controls.Add(this.txt_childTotalFee);
            this.pnl_accomodationDetails.Controls.Add(this.txt_adultsTotalFee);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_feePerNight);
            this.pnl_accomodationDetails.Controls.Add(this.txt_childFee);
            this.pnl_accomodationDetails.Controls.Add(this.txt_adultsFee);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_child);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_adults);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_additionalCompanion);
            this.pnl_accomodationDetails.Controls.Add(this.dgv_listOfRooms);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_notification);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_numberOfNights);
            this.pnl_accomodationDetails.Controls.Add(this.dtp_departureDate);
            this.pnl_accomodationDetails.Controls.Add(this.dtp_arrivalDate);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_checkOut);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_chekIn);
            this.pnl_accomodationDetails.Controls.Add(this.accomo_panel);
            this.pnl_accomodationDetails.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_accomodationDetails.Location = new System.Drawing.Point(0, 0);
            this.pnl_accomodationDetails.Name = "pnl_accomodationDetails";
            this.pnl_accomodationDetails.Size = new System.Drawing.Size(870, 525);
            this.pnl_accomodationDetails.TabIndex = 15;
            this.pnl_accomodationDetails.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl_accomodationDetails_Paint);
            // 
            // btn_companionName
            // 
            this.btn_companionName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_companionName.FlatAppearance.BorderSize = 0;
            this.btn_companionName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_companionName.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_companionName.ForeColor = System.Drawing.Color.White;
            this.btn_companionName.Location = new System.Drawing.Point(669, 391);
            this.btn_companionName.Name = "btn_companionName";
            this.btn_companionName.Size = new System.Drawing.Size(188, 35);
            this.btn_companionName.TabIndex = 75;
            this.btn_companionName.Text = "ADD COMPANION NAME";
            this.btn_companionName.UseVisualStyleBackColor = false;
            this.btn_companionName.Click += new System.EventHandler(this.btn_companionName_Click);
            // 
            // lbl_nights
            // 
            this.lbl_nights.AutoSize = true;
            this.lbl_nights.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_nights.Location = new System.Drawing.Point(818, 45);
            this.lbl_nights.Name = "lbl_nights";
            this.lbl_nights.Size = new System.Drawing.Size(18, 19);
            this.lbl_nights.TabIndex = 74;
            this.lbl_nights.Text = "0";
            this.lbl_nights.TextChanged += new System.EventHandler(this.lbl_nights_TextChanged);
            this.lbl_nights.Click += new System.EventHandler(this.lbl_nights_Click);
            // 
            // cbox_children
            // 
            this.cbox_children.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_children.FormattingEnabled = true;
            this.cbox_children.Items.AddRange(new object[] {
            "0"});
            this.cbox_children.Location = new System.Drawing.Point(656, 220);
            this.cbox_children.Name = "cbox_children";
            this.cbox_children.Size = new System.Drawing.Size(60, 26);
            this.cbox_children.TabIndex = 5;
            this.cbox_children.SelectedIndexChanged += new System.EventHandler(this.cbox_children_SelectedIndexChanged_1);
            this.cbox_children.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbox_children_KeyDown);
            this.cbox_children.MouseUp += new System.Windows.Forms.MouseEventHandler(this.cbox_children_MouseUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(653, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(180, 17);
            this.label3.TabIndex = 72;
            this.label3.Text = "Number of Child/Children*";
            // 
            // cbox_adult
            // 
            this.cbox_adult.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_adult.Items.AddRange(new object[] {
            "0"});
            this.cbox_adult.Location = new System.Drawing.Point(656, 170);
            this.cbox_adult.Name = "cbox_adult";
            this.cbox_adult.Size = new System.Drawing.Size(60, 26);
            this.cbox_adult.TabIndex = 4;
            this.cbox_adult.SelectedIndexChanged += new System.EventHandler(this.cbox_adult_SelectedIndexChanged);
            this.cbox_adult.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbox_adult_KeyDown);
            this.cbox_adult.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbox_adult_MouseClick);
            this.cbox_adult.MouseUp += new System.Windows.Forms.MouseEventHandler(this.cbox_adult_MouseUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(653, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 17);
            this.label2.TabIndex = 70;
            this.label2.Text = "Number of Adults*";
            // 
            // cbox_extrachildren
            // 
            this.cbox_extrachildren.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_extrachildren.FormattingEnabled = true;
            this.cbox_extrachildren.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.cbox_extrachildren.Location = new System.Drawing.Point(193, 342);
            this.cbox_extrachildren.Name = "cbox_extrachildren";
            this.cbox_extrachildren.Size = new System.Drawing.Size(60, 26);
            this.cbox_extrachildren.TabIndex = 9;
            this.cbox_extrachildren.SelectedIndexChanged += new System.EventHandler(this.cbox_children_SelectedIndexChanged);
            // 
            // cbox_extraadults
            // 
            this.cbox_extraadults.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_extraadults.FormattingEnabled = true;
            this.cbox_extraadults.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.cbox_extraadults.Location = new System.Drawing.Point(193, 309);
            this.cbox_extraadults.Name = "cbox_extraadults";
            this.cbox_extraadults.Size = new System.Drawing.Size(60, 26);
            this.cbox_extraadults.TabIndex = 6;
            this.cbox_extraadults.SelectedIndexChanged += new System.EventHandler(this.cbox_adults_SelectedIndexChanged);
            // 
            // numUD_roomQuantity
            // 
            this.numUD_roomQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numUD_roomQuantity.Location = new System.Drawing.Point(655, 122);
            this.numUD_roomQuantity.Name = "numUD_roomQuantity";
            this.numUD_roomQuantity.Size = new System.Drawing.Size(59, 24);
            this.numUD_roomQuantity.TabIndex = 3;
            this.numUD_roomQuantity.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numUD_roomQuantity_MouseUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(653, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 17);
            this.label1.TabIndex = 66;
            this.label1.Text = "Room Quantity*";
            // 
            // txt_guestRemarks
            // 
            this.txt_guestRemarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_guestRemarks.Location = new System.Drawing.Point(65, 391);
            this.txt_guestRemarks.Multiline = true;
            this.txt_guestRemarks.Name = "txt_guestRemarks";
            this.txt_guestRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_guestRemarks.Size = new System.Drawing.Size(474, 84);
            this.txt_guestRemarks.TabIndex = 12;
            this.txt_guestRemarks.TextChanged += new System.EventHandler(this.txt_guestRemarks_TextChanged);
            // 
            // lbl_guestRemarks
            // 
            this.lbl_guestRemarks.AutoSize = true;
            this.lbl_guestRemarks.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_guestRemarks.Location = new System.Drawing.Point(61, 369);
            this.lbl_guestRemarks.Name = "lbl_guestRemarks";
            this.lbl_guestRemarks.Size = new System.Drawing.Size(122, 19);
            this.lbl_guestRemarks.TabIndex = 60;
            this.lbl_guestRemarks.Text = "Guest Remarks";
            // 
            // btn_viewRoomDetails
            // 
            this.btn_viewRoomDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_viewRoomDetails.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_viewRoomDetails.FlatAppearance.BorderSize = 0;
            this.btn_viewRoomDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_viewRoomDetails.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_viewRoomDetails.ForeColor = System.Drawing.Color.White;
            this.btn_viewRoomDetails.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_viewRoomDetails.Location = new System.Drawing.Point(669, 440);
            this.btn_viewRoomDetails.Name = "btn_viewRoomDetails";
            this.btn_viewRoomDetails.Size = new System.Drawing.Size(188, 35);
            this.btn_viewRoomDetails.TabIndex = 59;
            this.btn_viewRoomDetails.Text = "VIEW ROOM DETAILS";
            this.btn_viewRoomDetails.UseVisualStyleBackColor = false;
            this.btn_viewRoomDetails.Click += new System.EventHandler(this.btn_viewRoomDetails_Click);
            // 
            // lbl_amount
            // 
            this.lbl_amount.AutoSize = true;
            this.lbl_amount.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_amount.Location = new System.Drawing.Point(405, 288);
            this.lbl_amount.Name = "lbl_amount";
            this.lbl_amount.Size = new System.Drawing.Size(57, 17);
            this.lbl_amount.TabIndex = 58;
            this.lbl_amount.Text = "Amount";
            // 
            // txt_childTotalFee
            // 
            this.txt_childTotalFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_childTotalFee.Location = new System.Drawing.Point(396, 340);
            this.txt_childTotalFee.Name = "txt_childTotalFee";
            this.txt_childTotalFee.ReadOnly = true;
            this.txt_childTotalFee.Size = new System.Drawing.Size(122, 24);
            this.txt_childTotalFee.TabIndex = 11;
            this.txt_childTotalFee.Text = "0.00";
            this.txt_childTotalFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_adultsTotalFee
            // 
            this.txt_adultsTotalFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_adultsTotalFee.Location = new System.Drawing.Point(396, 307);
            this.txt_adultsTotalFee.Name = "txt_adultsTotalFee";
            this.txt_adultsTotalFee.ReadOnly = true;
            this.txt_adultsTotalFee.Size = new System.Drawing.Size(122, 24);
            this.txt_adultsTotalFee.TabIndex = 8;
            this.txt_adultsTotalFee.Text = "0.00";
            this.txt_adultsTotalFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbl_feePerNight
            // 
            this.lbl_feePerNight.AutoSize = true;
            this.lbl_feePerNight.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_feePerNight.Location = new System.Drawing.Point(266, 288);
            this.lbl_feePerNight.Name = "lbl_feePerNight";
            this.lbl_feePerNight.Size = new System.Drawing.Size(73, 17);
            this.lbl_feePerNight.TabIndex = 55;
            this.lbl_feePerNight.Text = "Fee/Night";
            // 
            // txt_childFee
            // 
            this.txt_childFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_childFee.Location = new System.Drawing.Point(259, 340);
            this.txt_childFee.Name = "txt_childFee";
            this.txt_childFee.ReadOnly = true;
            this.txt_childFee.Size = new System.Drawing.Size(122, 24);
            this.txt_childFee.TabIndex = 10;
            this.txt_childFee.Text = "0.00";
            this.txt_childFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_adultsFee
            // 
            this.txt_adultsFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_adultsFee.Location = new System.Drawing.Point(259, 307);
            this.txt_adultsFee.Name = "txt_adultsFee";
            this.txt_adultsFee.ReadOnly = true;
            this.txt_adultsFee.Size = new System.Drawing.Size(122, 24);
            this.txt_adultsFee.TabIndex = 7;
            this.txt_adultsFee.Text = "0.00";
            this.txt_adultsFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbl_child
            // 
            this.lbl_child.AutoSize = true;
            this.lbl_child.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_child.Location = new System.Drawing.Point(142, 340);
            this.lbl_child.Name = "lbl_child";
            this.lbl_child.Size = new System.Drawing.Size(47, 17);
            this.lbl_child.TabIndex = 50;
            this.lbl_child.Text = "Child*";
            // 
            // lbl_adults
            // 
            this.lbl_adults.AutoSize = true;
            this.lbl_adults.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_adults.Location = new System.Drawing.Point(136, 310);
            this.lbl_adults.Name = "lbl_adults";
            this.lbl_adults.Size = new System.Drawing.Size(53, 17);
            this.lbl_adults.TabIndex = 49;
            this.lbl_adults.Text = "Adults*";
            // 
            // lbl_additionalCompanion
            // 
            this.lbl_additionalCompanion.AutoSize = true;
            this.lbl_additionalCompanion.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_additionalCompanion.Location = new System.Drawing.Point(63, 269);
            this.lbl_additionalCompanion.Name = "lbl_additionalCompanion";
            this.lbl_additionalCompanion.Size = new System.Drawing.Size(169, 19);
            this.lbl_additionalCompanion.TabIndex = 48;
            this.lbl_additionalCompanion.Text = "Additional Companion";
            // 
            // dgv_listOfRooms
            // 
            this.dgv_listOfRooms.AllowUserToAddRows = false;
            this.dgv_listOfRooms.AllowUserToDeleteRows = false;
            this.dgv_listOfRooms.BackgroundColor = System.Drawing.Color.White;
            this.dgv_listOfRooms.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 7, 0, 7);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_listOfRooms.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_listOfRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_listOfRooms.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.col_RoomNumber,
            this.col_RoomType,
            this.col_Quantity,
            this.col_RoomRate,
            this.col_Adult,
            this.col_Child,
            this.col_Amount,
            this.col_Available});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_listOfRooms.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_listOfRooms.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv_listOfRooms.EnableHeadersVisualStyles = false;
            this.dgv_listOfRooms.Location = new System.Drawing.Point(11, 101);
            this.dgv_listOfRooms.Name = "dgv_listOfRooms";
            this.dgv_listOfRooms.ReadOnly = true;
            this.dgv_listOfRooms.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_listOfRooms.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_listOfRooms.RowHeadersVisible = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            this.dgv_listOfRooms.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_listOfRooms.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_listOfRooms.Size = new System.Drawing.Size(636, 155);
            this.dgv_listOfRooms.TabIndex = 47;
            this.dgv_listOfRooms.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgv_listOfRooms_MouseClick);
            this.dgv_listOfRooms.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dgv_listOfRooms_MouseUp);
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            this.id.Width = 125;
            // 
            // col_RoomNumber
            // 
            this.col_RoomNumber.HeaderText = "Room Number";
            this.col_RoomNumber.Name = "col_RoomNumber";
            this.col_RoomNumber.ReadOnly = true;
            this.col_RoomNumber.Visible = false;
            // 
            // col_RoomType
            // 
            this.col_RoomType.HeaderText = "Room Type";
            this.col_RoomType.Name = "col_RoomType";
            this.col_RoomType.ReadOnly = true;
            this.col_RoomType.Width = 200;
            // 
            // col_Quantity
            // 
            this.col_Quantity.HeaderText = "Qty.";
            this.col_Quantity.Name = "col_Quantity";
            this.col_Quantity.ReadOnly = true;
            this.col_Quantity.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.col_Quantity.Width = 40;
            // 
            // col_RoomRate
            // 
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.col_RoomRate.DefaultCellStyle = dataGridViewCellStyle2;
            this.col_RoomRate.HeaderText = "Room Rate";
            this.col_RoomRate.Name = "col_RoomRate";
            this.col_RoomRate.ReadOnly = true;
            this.col_RoomRate.Width = 150;
            // 
            // col_Adult
            // 
            this.col_Adult.HeaderText = "Adult";
            this.col_Adult.Name = "col_Adult";
            this.col_Adult.ReadOnly = true;
            this.col_Adult.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.col_Adult.Width = 70;
            // 
            // col_Child
            // 
            this.col_Child.HeaderText = "Child";
            this.col_Child.Name = "col_Child";
            this.col_Child.ReadOnly = true;
            this.col_Child.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.col_Child.Width = 70;
            // 
            // col_Amount
            // 
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.col_Amount.DefaultCellStyle = dataGridViewCellStyle3;
            this.col_Amount.HeaderText = "Amount";
            this.col_Amount.Name = "col_Amount";
            this.col_Amount.ReadOnly = true;
            // 
            // col_Available
            // 
            this.col_Available.HeaderText = "Available";
            this.col_Available.Name = "col_Available";
            this.col_Available.ReadOnly = true;
            this.col_Available.Visible = false;
            this.col_Available.Width = 80;
            // 
            // lbl_notification
            // 
            this.lbl_notification.AutoSize = true;
            this.lbl_notification.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_notification.ForeColor = System.Drawing.Color.Red;
            this.lbl_notification.Location = new System.Drawing.Point(21, 81);
            this.lbl_notification.Name = "lbl_notification";
            this.lbl_notification.Size = new System.Drawing.Size(684, 17);
            this.lbl_notification.TabIndex = 46;
            this.lbl_notification.Text = "*Additional companion of 1 adult per room. Additional fee of Php 750.00 for adult" +
    " and Php 375.00 for child";
            // 
            // lbl_numberOfNights
            // 
            this.lbl_numberOfNights.AutoSize = true;
            this.lbl_numberOfNights.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_numberOfNights.Location = new System.Drawing.Point(676, 45);
            this.lbl_numberOfNights.Name = "lbl_numberOfNights";
            this.lbl_numberOfNights.Size = new System.Drawing.Size(118, 19);
            this.lbl_numberOfNights.TabIndex = 44;
            this.lbl_numberOfNights.Text = "No. of Night(s)*";
            // 
            // dtp_departureDate
            // 
            this.dtp_departureDate.CustomFormat = "dd mm, yyyy    -dddd";
            this.dtp_departureDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_departureDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_departureDate.Location = new System.Drawing.Point(419, 43);
            this.dtp_departureDate.Name = "dtp_departureDate";
            this.dtp_departureDate.ShowCheckBox = true;
            this.dtp_departureDate.Size = new System.Drawing.Size(238, 24);
            this.dtp_departureDate.TabIndex = 2;
            this.dtp_departureDate.ValueChanged += new System.EventHandler(this.dtp_departureDate_ValueChanged);
            this.dtp_departureDate.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dtp_departureDate_MouseUp);
            // 
            // dtp_arrivalDate
            // 
            this.dtp_arrivalDate.CustomFormat = "dd mm, yyyy    -dddd";
            this.dtp_arrivalDate.Enabled = false;
            this.dtp_arrivalDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_arrivalDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_arrivalDate.Location = new System.Drawing.Point(86, 43);
            this.dtp_arrivalDate.Name = "dtp_arrivalDate";
            this.dtp_arrivalDate.ShowCheckBox = true;
            this.dtp_arrivalDate.Size = new System.Drawing.Size(228, 24);
            this.dtp_arrivalDate.TabIndex = 1;
            this.dtp_arrivalDate.ValueChanged += new System.EventHandler(this.dtp_arrivalDate_ValueChanged);
            // 
            // lbl_checkOut
            // 
            this.lbl_checkOut.AutoSize = true;
            this.lbl_checkOut.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_checkOut.Location = new System.Drawing.Point(333, 45);
            this.lbl_checkOut.Name = "lbl_checkOut";
            this.lbl_checkOut.Size = new System.Drawing.Size(86, 19);
            this.lbl_checkOut.TabIndex = 41;
            this.lbl_checkOut.Text = "check out*";
            // 
            // lbl_chekIn
            // 
            this.lbl_chekIn.AutoSize = true;
            this.lbl_chekIn.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chekIn.Location = new System.Drawing.Point(10, 44);
            this.lbl_chekIn.Name = "lbl_chekIn";
            this.lbl_chekIn.Size = new System.Drawing.Size(76, 19);
            this.lbl_chekIn.TabIndex = 40;
            this.lbl_chekIn.Text = "check in*";
            // 
            // accomo_panel
            // 
            this.accomo_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.accomo_panel.Controls.Add(this.txt_accomo_accomodationTotal);
            this.accomo_panel.Controls.Add(this.lbl_accomodationTotal);
            this.accomo_panel.Controls.Add(this.lbl_accomodationDetails);
            this.accomo_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.accomo_panel.Location = new System.Drawing.Point(0, 0);
            this.accomo_panel.Name = "accomo_panel";
            this.accomo_panel.Size = new System.Drawing.Size(868, 31);
            this.accomo_panel.TabIndex = 10;
            // 
            // txt_accomo_accomodationTotal
            // 
            this.txt_accomo_accomodationTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_accomo_accomodationTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_accomo_accomodationTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_accomo_accomodationTotal.ForeColor = System.Drawing.Color.White;
            this.txt_accomo_accomodationTotal.Location = new System.Drawing.Point(760, 6);
            this.txt_accomo_accomodationTotal.Name = "txt_accomo_accomodationTotal";
            this.txt_accomo_accomodationTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_accomo_accomodationTotal.Size = new System.Drawing.Size(97, 17);
            this.txt_accomo_accomodationTotal.TabIndex = 74;
            this.txt_accomo_accomodationTotal.Text = "0.00";
            // 
            // lbl_accomodationTotal
            // 
            this.lbl_accomodationTotal.AutoSize = true;
            this.lbl_accomodationTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_accomodationTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_accomodationTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_accomodationTotal.Location = new System.Drawing.Point(568, 8);
            this.lbl_accomodationTotal.Name = "lbl_accomodationTotal";
            this.lbl_accomodationTotal.Size = new System.Drawing.Size(186, 14);
            this.lbl_accomodationTotal.TabIndex = 73;
            this.lbl_accomodationTotal.Text = "ACCOMODATION TOTAL:      Php";
            // 
            // lbl_accomodationDetails
            // 
            this.lbl_accomodationDetails.AutoSize = true;
            this.lbl_accomodationDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_accomodationDetails.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_accomodationDetails.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_accomodationDetails.Location = new System.Drawing.Point(11, 8);
            this.lbl_accomodationDetails.Name = "lbl_accomodationDetails";
            this.lbl_accomodationDetails.Size = new System.Drawing.Size(155, 14);
            this.lbl_accomodationDetails.TabIndex = 0;
            this.lbl_accomodationDetails.Text = "ACCOMODATION DETAILS";
            // 
            // Walkin_Reservation_AccomodationDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(870, 524);
            this.Controls.Add(this.pnl_accomodationDetails);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Walkin_Reservation_AccomodationDetails";
            this.Text = "Walkin_Reservation_AccomodationDetails";
            this.Load += new System.EventHandler(this.Walkin_Reservation_AccomodationDetails_Load);
            this.pnl_accomodationDetails.ResumeLayout(false);
            this.pnl_accomodationDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_roomQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listOfRooms)).EndInit();
            this.accomo_panel.ResumeLayout(false);
            this.accomo_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_accomodationDetails;
        private System.Windows.Forms.Label lbl_guestRemarks;
        private System.Windows.Forms.Button btn_viewRoomDetails;
        private System.Windows.Forms.Label lbl_amount;
        private System.Windows.Forms.TextBox txt_childTotalFee;
        private System.Windows.Forms.TextBox txt_adultsTotalFee;
        private System.Windows.Forms.Label lbl_feePerNight;
        private System.Windows.Forms.TextBox txt_childFee;
        private System.Windows.Forms.TextBox txt_adultsFee;
        private System.Windows.Forms.Label lbl_child;
        private System.Windows.Forms.Label lbl_adults;
        private System.Windows.Forms.Label lbl_additionalCompanion;
        private System.Windows.Forms.DataGridView dgv_listOfRooms;
        private System.Windows.Forms.Label lbl_notification;
        private System.Windows.Forms.Label lbl_numberOfNights;
        private System.Windows.Forms.DateTimePicker dtp_arrivalDate;
        private System.Windows.Forms.Label lbl_checkOut;
        private System.Windows.Forms.Label lbl_chekIn;
        private System.Windows.Forms.Panel accomo_panel;
        private System.Windows.Forms.TextBox txt_accomo_accomodationTotal;
        private System.Windows.Forms.Label lbl_accomodationTotal;
        private System.Windows.Forms.Label lbl_accomodationDetails;
        private System.Windows.Forms.TextBox txt_guestRemarks;
        private System.Windows.Forms.NumericUpDown numUD_roomQuantity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbox_extrachildren;
        private System.Windows.Forms.ComboBox cbox_extraadults;
        private System.Windows.Forms.ComboBox cbox_children;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbox_adult;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_RoomNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_RoomType;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_RoomRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Adult;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Child;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Available;
        private System.Windows.Forms.Label lbl_nights;
        private System.Windows.Forms.Button btn_companionName;
        private System.Windows.Forms.DateTimePicker dtp_departureDate;
    }
}