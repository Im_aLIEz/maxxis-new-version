﻿namespace maxxis.Walkin_Reservation
{
    partial class Walkin_Reservation_BillingStatement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.billingSummary_panel = new System.Windows.Forms.Panel();
            this.lbl_billingStatement = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btn_apply = new System.Windows.Forms.Button();
            this.txt_discountDescription = new System.Windows.Forms.TextBox();
            this.lbl_discountDescription = new System.Windows.Forms.Label();
            this.lbl_discountAmount = new System.Windows.Forms.Label();
            this.txt_discountAmount = new System.Windows.Forms.TextBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.lbl_discount = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.lbl_credit = new System.Windows.Forms.Label();
            this.lbl_type = new System.Windows.Forms.Label();
            this.cmb_type = new System.Windows.Forms.ComboBox();
            this.lbl_cardNumber = new System.Windows.Forms.Label();
            this.txt_cardNumber = new System.Windows.Forms.TextBox();
            this.lbl_expire = new System.Windows.Forms.Label();
            this.txt_cvv = new System.Windows.Forms.TextBox();
            this.txt_expire = new System.Windows.Forms.TextBox();
            this.lbl_cvv = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.txt_change = new System.Windows.Forms.TextBox();
            this.lbl_change = new System.Windows.Forms.Label();
            this.txt_cashhanded = new System.Windows.Forms.TextBox();
            this.lbl_cashHandedByGuest = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lbl_cash = new System.Windows.Forms.Label();
            this.txt_cashPayment = new System.Windows.Forms.TextBox();
            this.lbl_cashPayment = new System.Windows.Forms.Label();
            this.lbl_modeOfPayment = new System.Windows.Forms.Label();
            this.rbtn_cash = new System.Windows.Forms.RadioButton();
            this.rbtn_credit = new System.Windows.Forms.RadioButton();
            this.rbtn_partialCredit = new System.Windows.Forms.RadioButton();
            this.settlementOption_panel = new System.Windows.Forms.Panel();
            this.lbl_settlementOption = new System.Windows.Forms.Label();
            this.lbl_requiredPrePayment = new System.Windows.Forms.Label();
            this.txt_requiredPrePayment = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dgv_payments = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDiscount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.payments_panel = new System.Windows.Forms.Panel();
            this.txt_payments_paymentTotal = new System.Windows.Forms.TextBox();
            this.lbl_payments_paymentTotal = new System.Windows.Forms.Label();
            this.lbl_payments = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgv_billingSummary = new System.Windows.Forms.DataGridView();
            this.col_Particulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Rate_Cost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_TotalAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txt_billingSummary_subTotal = new System.Windows.Forms.TextBox();
            this.lbl_billingSummary_subTotal = new System.Windows.Forms.Label();
            this.lbl_BillingSummary = new System.Windows.Forms.Label();
            this.lbl_add = new System.Windows.Forms.Label();
            this.btn_add = new System.Windows.Forms.Button();
            this.numUD_quantity = new System.Windows.Forms.NumericUpDown();
            this.cbox_add = new System.Windows.Forms.ComboBox();
            this.lbl_cost = new System.Windows.Forms.Label();
            this.txt_cost = new System.Windows.Forms.TextBox();
            this.lbl_quantity = new System.Windows.Forms.Label();
            this.billingSummary_panel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.settlementOption_panel.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_payments)).BeginInit();
            this.payments_panel.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_billingSummary)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_quantity)).BeginInit();
            this.SuspendLayout();
            // 
            // billingSummary_panel
            // 
            this.billingSummary_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.billingSummary_panel.Controls.Add(this.lbl_billingStatement);
            this.billingSummary_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.billingSummary_panel.Location = new System.Drawing.Point(0, 0);
            this.billingSummary_panel.Name = "billingSummary_panel";
            this.billingSummary_panel.Size = new System.Drawing.Size(870, 31);
            this.billingSummary_panel.TabIndex = 12;
            // 
            // lbl_billingStatement
            // 
            this.lbl_billingStatement.AutoSize = true;
            this.lbl_billingStatement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_billingStatement.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_billingStatement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_billingStatement.Location = new System.Drawing.Point(11, 10);
            this.lbl_billingStatement.Name = "lbl_billingStatement";
            this.lbl_billingStatement.Size = new System.Drawing.Size(126, 14);
            this.lbl_billingStatement.TabIndex = 0;
            this.lbl_billingStatement.Text = "BILLING STATEMENT";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(870, 448);
            this.panel1.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(815, 1323);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 13);
            this.label8.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.panel10);
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.lbl_modeOfPayment);
            this.panel5.Controls.Add(this.rbtn_cash);
            this.panel5.Controls.Add(this.rbtn_credit);
            this.panel5.Controls.Add(this.rbtn_partialCredit);
            this.panel5.Controls.Add(this.settlementOption_panel);
            this.panel5.Location = new System.Drawing.Point(14, 755);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(823, 568);
            this.panel5.TabIndex = 2;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.btn_apply);
            this.panel10.Controls.Add(this.txt_discountDescription);
            this.panel10.Controls.Add(this.lbl_discountDescription);
            this.panel10.Controls.Add(this.lbl_discountAmount);
            this.panel10.Controls.Add(this.txt_discountAmount);
            this.panel10.Controls.Add(this.panel11);
            this.panel10.Location = new System.Drawing.Point(26, 376);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(766, 165);
            this.panel10.TabIndex = 51;
            // 
            // btn_apply
            // 
            this.btn_apply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_apply.FlatAppearance.BorderSize = 0;
            this.btn_apply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_apply.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_apply.ForeColor = System.Drawing.Color.White;
            this.btn_apply.Location = new System.Drawing.Point(524, 101);
            this.btn_apply.Name = "btn_apply";
            this.btn_apply.Size = new System.Drawing.Size(139, 35);
            this.btn_apply.TabIndex = 17;
            this.btn_apply.Text = "APPLY";
            this.btn_apply.UseVisualStyleBackColor = false;
            // 
            // txt_discountDescription
            // 
            this.txt_discountDescription.Location = new System.Drawing.Point(149, 87);
            this.txt_discountDescription.Multiline = true;
            this.txt_discountDescription.Name = "txt_discountDescription";
            this.txt_discountDescription.Size = new System.Drawing.Size(347, 62);
            this.txt_discountDescription.TabIndex = 16;
            // 
            // lbl_discountDescription
            // 
            this.lbl_discountDescription.AutoSize = true;
            this.lbl_discountDescription.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_discountDescription.Location = new System.Drawing.Point(10, 108);
            this.lbl_discountDescription.Name = "lbl_discountDescription";
            this.lbl_discountDescription.Size = new System.Drawing.Size(116, 19);
            this.lbl_discountDescription.TabIndex = 15;
            this.lbl_discountDescription.Text = "Discount Desc.";
            // 
            // lbl_discountAmount
            // 
            this.lbl_discountAmount.AutoSize = true;
            this.lbl_discountAmount.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_discountAmount.Location = new System.Drawing.Point(10, 52);
            this.lbl_discountAmount.Name = "lbl_discountAmount";
            this.lbl_discountAmount.Size = new System.Drawing.Size(133, 19);
            this.lbl_discountAmount.TabIndex = 14;
            this.lbl_discountAmount.Text = "Discount Amount";
            // 
            // txt_discountAmount
            // 
            this.txt_discountAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_discountAmount.Location = new System.Drawing.Point(149, 51);
            this.txt_discountAmount.Name = "txt_discountAmount";
            this.txt_discountAmount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_discountAmount.Size = new System.Drawing.Size(242, 23);
            this.txt_discountAmount.TabIndex = 13;
            this.txt_discountAmount.Text = "0.00";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel11.Controls.Add(this.lbl_discount);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(764, 31);
            this.panel11.TabIndex = 12;
            // 
            // lbl_discount
            // 
            this.lbl_discount.AutoSize = true;
            this.lbl_discount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_discount.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_discount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_discount.Location = new System.Drawing.Point(11, 8);
            this.lbl_discount.Name = "lbl_discount";
            this.lbl_discount.Size = new System.Drawing.Size(68, 14);
            this.lbl_discount.TabIndex = 0;
            this.lbl_discount.Text = "DISCOUNT";
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Controls.Add(this.lbl_type);
            this.panel8.Controls.Add(this.cmb_type);
            this.panel8.Controls.Add(this.lbl_cardNumber);
            this.panel8.Controls.Add(this.txt_cardNumber);
            this.panel8.Controls.Add(this.lbl_expire);
            this.panel8.Controls.Add(this.txt_cvv);
            this.panel8.Controls.Add(this.txt_expire);
            this.panel8.Controls.Add(this.lbl_cvv);
            this.panel8.Location = new System.Drawing.Point(26, 233);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(767, 127);
            this.panel8.TabIndex = 50;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel9.Controls.Add(this.lbl_credit);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(765, 31);
            this.panel9.TabIndex = 11;
            // 
            // lbl_credit
            // 
            this.lbl_credit.AutoSize = true;
            this.lbl_credit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_credit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_credit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_credit.Location = new System.Drawing.Point(11, 8);
            this.lbl_credit.Name = "lbl_credit";
            this.lbl_credit.Size = new System.Drawing.Size(50, 14);
            this.lbl_credit.TabIndex = 0;
            this.lbl_credit.Text = "CREDIT";
            // 
            // lbl_type
            // 
            this.lbl_type.AutoSize = true;
            this.lbl_type.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_type.Location = new System.Drawing.Point(10, 43);
            this.lbl_type.Name = "lbl_type";
            this.lbl_type.Size = new System.Drawing.Size(44, 19);
            this.lbl_type.TabIndex = 28;
            this.lbl_type.Text = "Type";
            // 
            // cmb_type
            // 
            this.cmb_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_type.FormattingEnabled = true;
            this.cmb_type.Location = new System.Drawing.Point(106, 43);
            this.cmb_type.Name = "cmb_type";
            this.cmb_type.Size = new System.Drawing.Size(272, 24);
            this.cmb_type.TabIndex = 29;
            // 
            // lbl_cardNumber
            // 
            this.lbl_cardNumber.AutoSize = true;
            this.lbl_cardNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cardNumber.Location = new System.Drawing.Point(10, 87);
            this.lbl_cardNumber.Name = "lbl_cardNumber";
            this.lbl_cardNumber.Size = new System.Drawing.Size(75, 19);
            this.lbl_cardNumber.TabIndex = 30;
            this.lbl_cardNumber.Text = "Card No*";
            // 
            // txt_cardNumber
            // 
            this.txt_cardNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cardNumber.Location = new System.Drawing.Point(106, 83);
            this.txt_cardNumber.Name = "txt_cardNumber";
            this.txt_cardNumber.Size = new System.Drawing.Size(272, 23);
            this.txt_cardNumber.TabIndex = 31;
            // 
            // lbl_expire
            // 
            this.lbl_expire.AutoSize = true;
            this.lbl_expire.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_expire.Location = new System.Drawing.Point(397, 84);
            this.lbl_expire.Name = "lbl_expire";
            this.lbl_expire.Size = new System.Drawing.Size(61, 19);
            this.lbl_expire.TabIndex = 32;
            this.lbl_expire.Text = "Expire*";
            // 
            // txt_cvv
            // 
            this.txt_cvv.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cvv.Location = new System.Drawing.Point(470, 42);
            this.txt_cvv.Name = "txt_cvv";
            this.txt_cvv.Size = new System.Drawing.Size(106, 23);
            this.txt_cvv.TabIndex = 35;
            // 
            // txt_expire
            // 
            this.txt_expire.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_expire.Location = new System.Drawing.Point(470, 83);
            this.txt_expire.Name = "txt_expire";
            this.txt_expire.Size = new System.Drawing.Size(106, 23);
            this.txt_expire.TabIndex = 33;
            // 
            // lbl_cvv
            // 
            this.lbl_cvv.AutoSize = true;
            this.lbl_cvv.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cvv.Location = new System.Drawing.Point(397, 44);
            this.lbl_cvv.Name = "lbl_cvv";
            this.lbl_cvv.Size = new System.Drawing.Size(46, 19);
            this.lbl_cvv.TabIndex = 34;
            this.lbl_cvv.Text = "CVV*";
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.txt_change);
            this.panel6.Controls.Add(this.lbl_change);
            this.panel6.Controls.Add(this.txt_cashhanded);
            this.panel6.Controls.Add(this.lbl_cashHandedByGuest);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.txt_cashPayment);
            this.panel6.Controls.Add(this.lbl_cashPayment);
            this.panel6.Location = new System.Drawing.Point(27, 71);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(767, 145);
            this.panel6.TabIndex = 49;
            // 
            // txt_change
            // 
            this.txt_change.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_change.Location = new System.Drawing.Point(197, 108);
            this.txt_change.Name = "txt_change";
            this.txt_change.ReadOnly = true;
            this.txt_change.Size = new System.Drawing.Size(193, 23);
            this.txt_change.TabIndex = 50;
            this.txt_change.Text = "0.00";
            this.txt_change.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbl_change
            // 
            this.lbl_change.AutoSize = true;
            this.lbl_change.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_change.Location = new System.Drawing.Point(10, 112);
            this.lbl_change.Name = "lbl_change";
            this.lbl_change.Size = new System.Drawing.Size(65, 19);
            this.lbl_change.TabIndex = 49;
            this.lbl_change.Text = "Change";
            // 
            // txt_cashhanded
            // 
            this.txt_cashhanded.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cashhanded.Location = new System.Drawing.Point(197, 75);
            this.txt_cashhanded.Name = "txt_cashhanded";
            this.txt_cashhanded.Size = new System.Drawing.Size(193, 23);
            this.txt_cashhanded.TabIndex = 48;
            this.txt_cashhanded.Text = "0.00";
            this.txt_cashhanded.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbl_cashHandedByGuest
            // 
            this.lbl_cashHandedByGuest.AutoSize = true;
            this.lbl_cashHandedByGuest.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cashHandedByGuest.Location = new System.Drawing.Point(10, 79);
            this.lbl_cashHandedByGuest.Name = "lbl_cashHandedByGuest";
            this.lbl_cashHandedByGuest.Size = new System.Drawing.Size(178, 19);
            this.lbl_cashHandedByGuest.TabIndex = 47;
            this.lbl_cashHandedByGuest.Text = "Cash Handed by Guest";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel7.Controls.Add(this.lbl_cash);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(765, 31);
            this.panel7.TabIndex = 11;
            // 
            // lbl_cash
            // 
            this.lbl_cash.AutoSize = true;
            this.lbl_cash.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_cash.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cash.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_cash.Location = new System.Drawing.Point(11, 8);
            this.lbl_cash.Name = "lbl_cash";
            this.lbl_cash.Size = new System.Drawing.Size(40, 14);
            this.lbl_cash.TabIndex = 0;
            this.lbl_cash.Text = "CASH";
            // 
            // txt_cashPayment
            // 
            this.txt_cashPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cashPayment.Location = new System.Drawing.Point(197, 41);
            this.txt_cashPayment.Name = "txt_cashPayment";
            this.txt_cashPayment.ReadOnly = true;
            this.txt_cashPayment.Size = new System.Drawing.Size(193, 23);
            this.txt_cashPayment.TabIndex = 46;
            this.txt_cashPayment.Text = "0.00";
            this.txt_cashPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbl_cashPayment
            // 
            this.lbl_cashPayment.AutoSize = true;
            this.lbl_cashPayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cashPayment.Location = new System.Drawing.Point(10, 41);
            this.lbl_cashPayment.Name = "lbl_cashPayment";
            this.lbl_cashPayment.Size = new System.Drawing.Size(115, 19);
            this.lbl_cashPayment.TabIndex = 45;
            this.lbl_cashPayment.Text = "Cash Payment";
            // 
            // lbl_modeOfPayment
            // 
            this.lbl_modeOfPayment.AutoSize = true;
            this.lbl_modeOfPayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_modeOfPayment.Location = new System.Drawing.Point(61, 42);
            this.lbl_modeOfPayment.Name = "lbl_modeOfPayment";
            this.lbl_modeOfPayment.Size = new System.Drawing.Size(144, 19);
            this.lbl_modeOfPayment.TabIndex = 48;
            this.lbl_modeOfPayment.Text = "Mode Of Payment:";
            // 
            // rbtn_cash
            // 
            this.rbtn_cash.AutoSize = true;
            this.rbtn_cash.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_cash.Location = new System.Drawing.Point(225, 41);
            this.rbtn_cash.Name = "rbtn_cash";
            this.rbtn_cash.Size = new System.Drawing.Size(59, 21);
            this.rbtn_cash.TabIndex = 45;
            this.rbtn_cash.Text = "Cash";
            this.rbtn_cash.UseVisualStyleBackColor = true;
            // 
            // rbtn_credit
            // 
            this.rbtn_credit.AutoSize = true;
            this.rbtn_credit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_credit.Location = new System.Drawing.Point(327, 41);
            this.rbtn_credit.Name = "rbtn_credit";
            this.rbtn_credit.Size = new System.Drawing.Size(65, 21);
            this.rbtn_credit.TabIndex = 46;
            this.rbtn_credit.Text = "Credit";
            this.rbtn_credit.UseVisualStyleBackColor = true;
            // 
            // rbtn_partialCredit
            // 
            this.rbtn_partialCredit.AutoSize = true;
            this.rbtn_partialCredit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_partialCredit.Location = new System.Drawing.Point(438, 41);
            this.rbtn_partialCredit.Name = "rbtn_partialCredit";
            this.rbtn_partialCredit.Size = new System.Drawing.Size(111, 21);
            this.rbtn_partialCredit.TabIndex = 47;
            this.rbtn_partialCredit.Text = "Partial Credit";
            this.rbtn_partialCredit.UseVisualStyleBackColor = true;
            // 
            // settlementOption_panel
            // 
            this.settlementOption_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.settlementOption_panel.Controls.Add(this.lbl_settlementOption);
            this.settlementOption_panel.Controls.Add(this.lbl_requiredPrePayment);
            this.settlementOption_panel.Controls.Add(this.txt_requiredPrePayment);
            this.settlementOption_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.settlementOption_panel.Location = new System.Drawing.Point(0, 0);
            this.settlementOption_panel.Name = "settlementOption_panel";
            this.settlementOption_panel.Size = new System.Drawing.Size(821, 31);
            this.settlementOption_panel.TabIndex = 11;
            // 
            // lbl_settlementOption
            // 
            this.lbl_settlementOption.AutoSize = true;
            this.lbl_settlementOption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_settlementOption.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_settlementOption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_settlementOption.Location = new System.Drawing.Point(11, 8);
            this.lbl_settlementOption.Name = "lbl_settlementOption";
            this.lbl_settlementOption.Size = new System.Drawing.Size(132, 14);
            this.lbl_settlementOption.TabIndex = 0;
            this.lbl_settlementOption.Text = "SETTLEMENT OPTION";
            // 
            // lbl_requiredPrePayment
            // 
            this.lbl_requiredPrePayment.AutoSize = true;
            this.lbl_requiredPrePayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_requiredPrePayment.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_requiredPrePayment.Location = new System.Drawing.Point(543, 4);
            this.lbl_requiredPrePayment.Name = "lbl_requiredPrePayment";
            this.lbl_requiredPrePayment.Size = new System.Drawing.Size(172, 19);
            this.lbl_requiredPrePayment.TabIndex = 42;
            this.lbl_requiredPrePayment.Text = "Required pre-payment";
            // 
            // txt_requiredPrePayment
            // 
            this.txt_requiredPrePayment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_requiredPrePayment.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_requiredPrePayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_requiredPrePayment.ForeColor = System.Drawing.Color.White;
            this.txt_requiredPrePayment.Location = new System.Drawing.Point(721, 5);
            this.txt_requiredPrePayment.Name = "txt_requiredPrePayment";
            this.txt_requiredPrePayment.ReadOnly = true;
            this.txt_requiredPrePayment.Size = new System.Drawing.Size(97, 17);
            this.txt_requiredPrePayment.TabIndex = 43;
            this.txt_requiredPrePayment.Text = "0.00";
            this.txt_requiredPrePayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.dgv_payments);
            this.panel4.Controls.Add(this.payments_panel);
            this.panel4.Location = new System.Drawing.Point(14, 448);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(823, 277);
            this.panel4.TabIndex = 1;
            // 
            // dgv_payments
            // 
            this.dgv_payments.AllowUserToAddRows = false;
            this.dgv_payments.AllowUserToDeleteRows = false;
            this.dgv_payments.BackgroundColor = System.Drawing.Color.Silver;
            this.dgv_payments.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_payments.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv_payments.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_payments.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_payments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_payments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.colAmount,
            this.colDiscount});
            this.dgv_payments.EnableHeadersVisualStyles = false;
            this.dgv_payments.Location = new System.Drawing.Point(14, 46);
            this.dgv_payments.Name = "dgv_payments";
            this.dgv_payments.ReadOnly = true;
            this.dgv_payments.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_payments.RowHeadersVisible = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_payments.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_payments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_payments.Size = new System.Drawing.Size(789, 203);
            this.dgv_payments.TabIndex = 54;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 200;
            // 
            // colAmount
            // 
            this.colAmount.HeaderText = "Amount";
            this.colAmount.Name = "colAmount";
            this.colAmount.ReadOnly = true;
            this.colAmount.Width = 350;
            // 
            // colDiscount
            // 
            this.colDiscount.HeaderText = "Discount";
            this.colDiscount.Name = "colDiscount";
            this.colDiscount.ReadOnly = true;
            this.colDiscount.Width = 200;
            // 
            // payments_panel
            // 
            this.payments_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.payments_panel.Controls.Add(this.txt_payments_paymentTotal);
            this.payments_panel.Controls.Add(this.lbl_payments_paymentTotal);
            this.payments_panel.Controls.Add(this.lbl_payments);
            this.payments_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.payments_panel.Location = new System.Drawing.Point(0, 0);
            this.payments_panel.Name = "payments_panel";
            this.payments_panel.Size = new System.Drawing.Size(821, 31);
            this.payments_panel.TabIndex = 53;
            // 
            // txt_payments_paymentTotal
            // 
            this.txt_payments_paymentTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_payments_paymentTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_payments_paymentTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_payments_paymentTotal.ForeColor = System.Drawing.Color.White;
            this.txt_payments_paymentTotal.Location = new System.Drawing.Point(715, 5);
            this.txt_payments_paymentTotal.Name = "txt_payments_paymentTotal";
            this.txt_payments_paymentTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_payments_paymentTotal.Size = new System.Drawing.Size(103, 17);
            this.txt_payments_paymentTotal.TabIndex = 74;
            this.txt_payments_paymentTotal.Text = "0.00";
            // 
            // lbl_payments_paymentTotal
            // 
            this.lbl_payments_paymentTotal.AutoSize = true;
            this.lbl_payments_paymentTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_payments_paymentTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_payments_paymentTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_payments_paymentTotal.Location = new System.Drawing.Point(568, 8);
            this.lbl_payments_paymentTotal.Name = "lbl_payments_paymentTotal";
            this.lbl_payments_paymentTotal.Size = new System.Drawing.Size(141, 14);
            this.lbl_payments_paymentTotal.TabIndex = 73;
            this.lbl_payments_paymentTotal.Text = "PAYMENT TOTAL:    Php";
            // 
            // lbl_payments
            // 
            this.lbl_payments.AutoSize = true;
            this.lbl_payments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_payments.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_payments.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_payments.Location = new System.Drawing.Point(11, 8);
            this.lbl_payments.Name = "lbl_payments";
            this.lbl_payments.Size = new System.Drawing.Size(72, 14);
            this.lbl_payments.TabIndex = 0;
            this.lbl_payments.Text = "PAYMENTS";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.dgv_billingSummary);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.lbl_add);
            this.panel2.Controls.Add(this.btn_add);
            this.panel2.Controls.Add(this.numUD_quantity);
            this.panel2.Controls.Add(this.cbox_add);
            this.panel2.Controls.Add(this.lbl_cost);
            this.panel2.Controls.Add(this.txt_cost);
            this.panel2.Controls.Add(this.lbl_quantity);
            this.panel2.Location = new System.Drawing.Point(14, 26);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(823, 393);
            this.panel2.TabIndex = 0;
            // 
            // dgv_billingSummary
            // 
            this.dgv_billingSummary.AllowUserToAddRows = false;
            this.dgv_billingSummary.AllowUserToDeleteRows = false;
            this.dgv_billingSummary.BackgroundColor = System.Drawing.Color.Silver;
            this.dgv_billingSummary.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_billingSummary.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv_billingSummary.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_billingSummary.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_billingSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_billingSummary.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_Particulars,
            this.col_Qty,
            this.col_Unit,
            this.col_Rate_Cost,
            this.col_TotalAmount});
            this.dgv_billingSummary.EnableHeadersVisualStyles = false;
            this.dgv_billingSummary.Location = new System.Drawing.Point(14, 100);
            this.dgv_billingSummary.Name = "dgv_billingSummary";
            this.dgv_billingSummary.ReadOnly = true;
            this.dgv_billingSummary.RowHeadersVisible = false;
            this.dgv_billingSummary.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgv_billingSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_billingSummary.Size = new System.Drawing.Size(789, 277);
            this.dgv_billingSummary.TabIndex = 59;
            // 
            // col_Particulars
            // 
            this.col_Particulars.HeaderText = "Particulars";
            this.col_Particulars.Name = "col_Particulars";
            this.col_Particulars.ReadOnly = true;
            this.col_Particulars.Width = 250;
            // 
            // col_Qty
            // 
            this.col_Qty.HeaderText = "Qty";
            this.col_Qty.Name = "col_Qty";
            this.col_Qty.ReadOnly = true;
            this.col_Qty.Width = 80;
            // 
            // col_Unit
            // 
            this.col_Unit.HeaderText = "Unit";
            this.col_Unit.Name = "col_Unit";
            this.col_Unit.ReadOnly = true;
            // 
            // col_Rate_Cost
            // 
            this.col_Rate_Cost.HeaderText = "Rate/Cost";
            this.col_Rate_Cost.Name = "col_Rate_Cost";
            this.col_Rate_Cost.ReadOnly = true;
            this.col_Rate_Cost.Width = 150;
            // 
            // col_TotalAmount
            // 
            this.col_TotalAmount.HeaderText = "Amount";
            this.col_TotalAmount.Name = "col_TotalAmount";
            this.col_TotalAmount.ReadOnly = true;
            this.col_TotalAmount.Width = 150;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel3.Controls.Add(this.txt_billingSummary_subTotal);
            this.panel3.Controls.Add(this.lbl_billingSummary_subTotal);
            this.panel3.Controls.Add(this.lbl_BillingSummary);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(821, 31);
            this.panel3.TabIndex = 58;
            // 
            // txt_billingSummary_subTotal
            // 
            this.txt_billingSummary_subTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_billingSummary_subTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_billingSummary_subTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_billingSummary_subTotal.ForeColor = System.Drawing.Color.White;
            this.txt_billingSummary_subTotal.Location = new System.Drawing.Point(681, 7);
            this.txt_billingSummary_subTotal.Name = "txt_billingSummary_subTotal";
            this.txt_billingSummary_subTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_billingSummary_subTotal.Size = new System.Drawing.Size(137, 17);
            this.txt_billingSummary_subTotal.TabIndex = 74;
            this.txt_billingSummary_subTotal.Text = "Php 0.00";
            // 
            // lbl_billingSummary_subTotal
            // 
            this.lbl_billingSummary_subTotal.AutoSize = true;
            this.lbl_billingSummary_subTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_billingSummary_subTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_billingSummary_subTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_billingSummary_subTotal.Location = new System.Drawing.Point(600, 10);
            this.lbl_billingSummary_subTotal.Name = "lbl_billingSummary_subTotal";
            this.lbl_billingSummary_subTotal.Size = new System.Drawing.Size(75, 14);
            this.lbl_billingSummary_subTotal.TabIndex = 73;
            this.lbl_billingSummary_subTotal.Text = "SUB TOTAL:";
            // 
            // lbl_BillingSummary
            // 
            this.lbl_BillingSummary.AutoSize = true;
            this.lbl_BillingSummary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_BillingSummary.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_BillingSummary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_BillingSummary.Location = new System.Drawing.Point(11, 10);
            this.lbl_BillingSummary.Name = "lbl_BillingSummary";
            this.lbl_BillingSummary.Size = new System.Drawing.Size(116, 14);
            this.lbl_BillingSummary.TabIndex = 0;
            this.lbl_BillingSummary.Text = "BILLING SUMMARY";
            // 
            // lbl_add
            // 
            this.lbl_add.AutoSize = true;
            this.lbl_add.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_add.Location = new System.Drawing.Point(24, 40);
            this.lbl_add.Name = "lbl_add";
            this.lbl_add.Size = new System.Drawing.Size(33, 17);
            this.lbl_add.TabIndex = 51;
            this.lbl_add.Text = "Add";
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_add.FlatAppearance.BorderSize = 0;
            this.btn_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add.ForeColor = System.Drawing.Color.White;
            this.btn_add.Location = new System.Drawing.Point(578, 54);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(96, 40);
            this.btn_add.TabIndex = 52;
            this.btn_add.Text = "ADD";
            this.btn_add.UseVisualStyleBackColor = false;
            // 
            // numUD_quantity
            // 
            this.numUD_quantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numUD_quantity.Location = new System.Drawing.Point(369, 64);
            this.numUD_quantity.Name = "numUD_quantity";
            this.numUD_quantity.Size = new System.Drawing.Size(71, 23);
            this.numUD_quantity.TabIndex = 57;
            // 
            // cbox_add
            // 
            this.cbox_add.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbox_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_add.FormattingEnabled = true;
            this.cbox_add.Location = new System.Drawing.Point(27, 63);
            this.cbox_add.Name = "cbox_add";
            this.cbox_add.Size = new System.Drawing.Size(299, 24);
            this.cbox_add.TabIndex = 53;
            // 
            // lbl_cost
            // 
            this.lbl_cost.AutoSize = true;
            this.lbl_cost.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cost.Location = new System.Drawing.Point(474, 40);
            this.lbl_cost.Name = "lbl_cost";
            this.lbl_cost.Size = new System.Drawing.Size(37, 17);
            this.lbl_cost.TabIndex = 56;
            this.lbl_cost.Text = "Cost";
            // 
            // txt_cost
            // 
            this.txt_cost.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cost.Location = new System.Drawing.Point(477, 63);
            this.txt_cost.Name = "txt_cost";
            this.txt_cost.ReadOnly = true;
            this.txt_cost.Size = new System.Drawing.Size(71, 23);
            this.txt_cost.TabIndex = 54;
            // 
            // lbl_quantity
            // 
            this.lbl_quantity.AutoSize = true;
            this.lbl_quantity.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_quantity.Location = new System.Drawing.Point(366, 41);
            this.lbl_quantity.Name = "lbl_quantity";
            this.lbl_quantity.Size = new System.Drawing.Size(30, 17);
            this.lbl_quantity.TabIndex = 55;
            this.lbl_quantity.Text = "Qty";
            // 
            // Walkin_Reservation_BillingStatement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(870, 479);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.billingSummary_panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Walkin_Reservation_BillingStatement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Walkin_Reservation_BillingStatement";
            this.billingSummary_panel.ResumeLayout(false);
            this.billingSummary_panel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.settlementOption_panel.ResumeLayout(false);
            this.settlementOption_panel.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_payments)).EndInit();
            this.payments_panel.ResumeLayout(false);
            this.payments_panel.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_billingSummary)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_quantity)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel billingSummary_panel;
        private System.Windows.Forms.Label lbl_billingStatement;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btn_apply;
        private System.Windows.Forms.TextBox txt_discountDescription;
        private System.Windows.Forms.Label lbl_discountDescription;
        private System.Windows.Forms.Label lbl_discountAmount;
        private System.Windows.Forms.TextBox txt_discountAmount;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label lbl_discount;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label lbl_credit;
        private System.Windows.Forms.Label lbl_type;
        private System.Windows.Forms.ComboBox cmb_type;
        private System.Windows.Forms.Label lbl_cardNumber;
        private System.Windows.Forms.TextBox txt_cardNumber;
        private System.Windows.Forms.Label lbl_expire;
        private System.Windows.Forms.TextBox txt_cvv;
        private System.Windows.Forms.TextBox txt_expire;
        private System.Windows.Forms.Label lbl_cvv;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox txt_change;
        private System.Windows.Forms.Label lbl_change;
        private System.Windows.Forms.TextBox txt_cashhanded;
        private System.Windows.Forms.Label lbl_cashHandedByGuest;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lbl_cash;
        private System.Windows.Forms.TextBox txt_cashPayment;
        private System.Windows.Forms.Label lbl_cashPayment;
        private System.Windows.Forms.Label lbl_modeOfPayment;
        private System.Windows.Forms.RadioButton rbtn_cash;
        private System.Windows.Forms.RadioButton rbtn_credit;
        private System.Windows.Forms.RadioButton rbtn_partialCredit;
        private System.Windows.Forms.Panel settlementOption_panel;
        private System.Windows.Forms.Label lbl_settlementOption;
        private System.Windows.Forms.Label lbl_requiredPrePayment;
        private System.Windows.Forms.TextBox txt_requiredPrePayment;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dgv_payments;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDiscount;
        private System.Windows.Forms.Panel payments_panel;
        private System.Windows.Forms.TextBox txt_payments_paymentTotal;
        private System.Windows.Forms.Label lbl_payments_paymentTotal;
        private System.Windows.Forms.Label lbl_payments;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgv_billingSummary;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Particulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Qty;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Rate_Cost;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_TotalAmount;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txt_billingSummary_subTotal;
        private System.Windows.Forms.Label lbl_billingSummary_subTotal;
        private System.Windows.Forms.Label lbl_BillingSummary;
        private System.Windows.Forms.Label lbl_add;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.NumericUpDown numUD_quantity;
        private System.Windows.Forms.ComboBox cbox_add;
        private System.Windows.Forms.Label lbl_cost;
        private System.Windows.Forms.TextBox txt_cost;
        private System.Windows.Forms.Label lbl_quantity;
    }
}