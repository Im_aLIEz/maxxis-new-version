﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace maxxis.Walkin_Reservation
{
    public partial class Walkin_Reservation_Payments : Form
    {
        Walkin_Reservation.Walkin_Reservation_AccomodationDetails WR_AccomodationDetails = new Walkin_Reservation.Walkin_Reservation_AccomodationDetails();
        Walkin_Reservation_BillingSummary WR_billingsummary = new Walkin_Reservation_BillingSummary();
        public Walkin_Reservation_Payments()
        {
            InitializeComponent();
        }
        internal void Walkin_Reservation_Payments_Load_1(object sender, EventArgs e)
        {
            dgv_payments.Rows.Clear();
            txt_payments_paymentTotal.Text = WR_billingsummary.TotalPayment;
            dgv_payments.Rows.Add("Total Amount", WR_billingsummary.TotalPayment);
            dgv_payments.Rows.Add("(Less) Discount");
        }
    }
}
