﻿namespace maxxis.Walkin_Reservation
{
    partial class Walkin_Reservation_VisaInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl_visaInformation = new System.Windows.Forms.Panel();
            this.pnl_visaInformationTwo = new System.Windows.Forms.Panel();
            this.txt_purposeOfVisit = new System.Windows.Forms.TextBox();
            this.txt_departureTransportation = new System.Windows.Forms.TextBox();
            this.dtp_timeOfArrival = new System.Windows.Forms.DateTimePicker();
            this.lbl_departureTransportation = new System.Windows.Forms.Label();
            this.lbl_goingTo = new System.Windows.Forms.Label();
            this.lbl_dateOfArrival = new System.Windows.Forms.Label();
            this.dtp_dateOfArrival = new System.Windows.Forms.DateTimePicker();
            this.txt_goingTo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_timeOfArrival = new System.Windows.Forms.Label();
            this.visaInformation_panel = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_arrivedForm = new System.Windows.Forms.TextBox();
            this.lbl_arrivedForm = new System.Windows.Forms.Label();
            this.txt_serialNumber = new System.Windows.Forms.TextBox();
            this.lbl_vissaIssuePlace = new System.Windows.Forms.Label();
            this.dtp_visaDate = new System.Windows.Forms.DateTimePicker();
            this.lbl_visaDate = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_visaIssuePlace = new System.Windows.Forms.TextBox();
            this.lbl_serialNumber = new System.Windows.Forms.Label();
            this.pnl_visaInformation.SuspendLayout();
            this.pnl_visaInformationTwo.SuspendLayout();
            this.visaInformation_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_visaInformation
            // 
            this.pnl_visaInformation.BackColor = System.Drawing.Color.Silver;
            this.pnl_visaInformation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_visaInformation.Controls.Add(this.pnl_visaInformationTwo);
            this.pnl_visaInformation.Controls.Add(this.visaInformation_panel);
            this.pnl_visaInformation.Controls.Add(this.txt_arrivedForm);
            this.pnl_visaInformation.Controls.Add(this.lbl_arrivedForm);
            this.pnl_visaInformation.Controls.Add(this.txt_serialNumber);
            this.pnl_visaInformation.Controls.Add(this.lbl_vissaIssuePlace);
            this.pnl_visaInformation.Controls.Add(this.dtp_visaDate);
            this.pnl_visaInformation.Controls.Add(this.lbl_visaDate);
            this.pnl_visaInformation.Controls.Add(this.textBox1);
            this.pnl_visaInformation.Controls.Add(this.label1);
            this.pnl_visaInformation.Controls.Add(this.txt_visaIssuePlace);
            this.pnl_visaInformation.Controls.Add(this.lbl_serialNumber);
            this.pnl_visaInformation.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_visaInformation.Location = new System.Drawing.Point(0, 0);
            this.pnl_visaInformation.Name = "pnl_visaInformation";
            this.pnl_visaInformation.Size = new System.Drawing.Size(870, 267);
            this.pnl_visaInformation.TabIndex = 18;
            // 
            // pnl_visaInformationTwo
            // 
            this.pnl_visaInformationTwo.AutoScroll = true;
            this.pnl_visaInformationTwo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_visaInformationTwo.Controls.Add(this.txt_purposeOfVisit);
            this.pnl_visaInformationTwo.Controls.Add(this.txt_departureTransportation);
            this.pnl_visaInformationTwo.Controls.Add(this.dtp_timeOfArrival);
            this.pnl_visaInformationTwo.Controls.Add(this.lbl_departureTransportation);
            this.pnl_visaInformationTwo.Controls.Add(this.lbl_goingTo);
            this.pnl_visaInformationTwo.Controls.Add(this.lbl_dateOfArrival);
            this.pnl_visaInformationTwo.Controls.Add(this.dtp_dateOfArrival);
            this.pnl_visaInformationTwo.Controls.Add(this.txt_goingTo);
            this.pnl_visaInformationTwo.Controls.Add(this.label2);
            this.pnl_visaInformationTwo.Controls.Add(this.lbl_timeOfArrival);
            this.pnl_visaInformationTwo.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnl_visaInformationTwo.Location = new System.Drawing.Point(416, 31);
            this.pnl_visaInformationTwo.Name = "pnl_visaInformationTwo";
            this.pnl_visaInformationTwo.Size = new System.Drawing.Size(452, 234);
            this.pnl_visaInformationTwo.TabIndex = 45;
            // 
            // txt_purposeOfVisit
            // 
            this.txt_purposeOfVisit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_purposeOfVisit.Location = new System.Drawing.Point(178, 65);
            this.txt_purposeOfVisit.Name = "txt_purposeOfVisit";
            this.txt_purposeOfVisit.Size = new System.Drawing.Size(249, 23);
            this.txt_purposeOfVisit.TabIndex = 62;
            // 
            // txt_departureTransportation
            // 
            this.txt_departureTransportation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_departureTransportation.Location = new System.Drawing.Point(178, 123);
            this.txt_departureTransportation.Name = "txt_departureTransportation";
            this.txt_departureTransportation.Size = new System.Drawing.Size(249, 23);
            this.txt_departureTransportation.TabIndex = 60;
            // 
            // dtp_timeOfArrival
            // 
            this.dtp_timeOfArrival.CustomFormat = "  hh:mm:ss tt";
            this.dtp_timeOfArrival.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_timeOfArrival.Location = new System.Drawing.Point(178, 36);
            this.dtp_timeOfArrival.Name = "dtp_timeOfArrival";
            this.dtp_timeOfArrival.ShowCheckBox = true;
            this.dtp_timeOfArrival.ShowUpDown = true;
            this.dtp_timeOfArrival.Size = new System.Drawing.Size(148, 20);
            this.dtp_timeOfArrival.TabIndex = 61;
            // 
            // lbl_departureTransportation
            // 
            this.lbl_departureTransportation.AutoSize = true;
            this.lbl_departureTransportation.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_departureTransportation.Location = new System.Drawing.Point(3, 127);
            this.lbl_departureTransportation.Name = "lbl_departureTransportation";
            this.lbl_departureTransportation.Size = new System.Drawing.Size(172, 19);
            this.lbl_departureTransportation.TabIndex = 57;
            this.lbl_departureTransportation.Text = "Depart. Transportation";
            // 
            // lbl_goingTo
            // 
            this.lbl_goingTo.AutoSize = true;
            this.lbl_goingTo.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_goingTo.Location = new System.Drawing.Point(3, 98);
            this.lbl_goingTo.Name = "lbl_goingTo";
            this.lbl_goingTo.Size = new System.Drawing.Size(71, 19);
            this.lbl_goingTo.TabIndex = 56;
            this.lbl_goingTo.Text = "Going to";
            // 
            // lbl_dateOfArrival
            // 
            this.lbl_dateOfArrival.AutoSize = true;
            this.lbl_dateOfArrival.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dateOfArrival.Location = new System.Drawing.Point(3, 11);
            this.lbl_dateOfArrival.Name = "lbl_dateOfArrival";
            this.lbl_dateOfArrival.Size = new System.Drawing.Size(114, 19);
            this.lbl_dateOfArrival.TabIndex = 53;
            this.lbl_dateOfArrival.Text = "Date of Arrival";
            // 
            // dtp_dateOfArrival
            // 
            this.dtp_dateOfArrival.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtp_dateOfArrival.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtp_dateOfArrival.CustomFormat = "  dd/MM/yyyy     - dddd";
            this.dtp_dateOfArrival.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dateOfArrival.Location = new System.Drawing.Point(178, 7);
            this.dtp_dateOfArrival.Name = "dtp_dateOfArrival";
            this.dtp_dateOfArrival.ShowCheckBox = true;
            this.dtp_dateOfArrival.Size = new System.Drawing.Size(207, 20);
            this.dtp_dateOfArrival.TabIndex = 58;
            // 
            // txt_goingTo
            // 
            this.txt_goingTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_goingTo.Location = new System.Drawing.Point(178, 94);
            this.txt_goingTo.Name = "txt_goingTo";
            this.txt_goingTo.Size = new System.Drawing.Size(249, 23);
            this.txt_goingTo.TabIndex = 59;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 19);
            this.label2.TabIndex = 55;
            this.label2.Text = "Purpose of Visit";
            // 
            // lbl_timeOfArrival
            // 
            this.lbl_timeOfArrival.AutoSize = true;
            this.lbl_timeOfArrival.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_timeOfArrival.Location = new System.Drawing.Point(3, 40);
            this.lbl_timeOfArrival.Name = "lbl_timeOfArrival";
            this.lbl_timeOfArrival.Size = new System.Drawing.Size(116, 19);
            this.lbl_timeOfArrival.TabIndex = 54;
            this.lbl_timeOfArrival.Text = "Time of Arrival";
            // 
            // visaInformation_panel
            // 
            this.visaInformation_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.visaInformation_panel.Controls.Add(this.label4);
            this.visaInformation_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.visaInformation_panel.Location = new System.Drawing.Point(0, 0);
            this.visaInformation_panel.Name = "visaInformation_panel";
            this.visaInformation_panel.Size = new System.Drawing.Size(868, 31);
            this.visaInformation_panel.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label4.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label4.Location = new System.Drawing.Point(11, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "VISA INFORMATION";
            // 
            // txt_arrivedForm
            // 
            this.txt_arrivedForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_arrivedForm.Location = new System.Drawing.Point(153, 159);
            this.txt_arrivedForm.Name = "txt_arrivedForm";
            this.txt_arrivedForm.Size = new System.Drawing.Size(257, 23);
            this.txt_arrivedForm.TabIndex = 52;
            // 
            // lbl_arrivedForm
            // 
            this.lbl_arrivedForm.AutoSize = true;
            this.lbl_arrivedForm.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_arrivedForm.Location = new System.Drawing.Point(3, 163);
            this.lbl_arrivedForm.Name = "lbl_arrivedForm";
            this.lbl_arrivedForm.Size = new System.Drawing.Size(104, 19);
            this.lbl_arrivedForm.TabIndex = 47;
            this.lbl_arrivedForm.Text = "Arrived Form";
            // 
            // txt_serialNumber
            // 
            this.txt_serialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_serialNumber.Location = new System.Drawing.Point(153, 43);
            this.txt_serialNumber.Name = "txt_serialNumber";
            this.txt_serialNumber.Size = new System.Drawing.Size(257, 23);
            this.txt_serialNumber.TabIndex = 48;
            // 
            // lbl_vissaIssuePlace
            // 
            this.lbl_vissaIssuePlace.AutoSize = true;
            this.lbl_vissaIssuePlace.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_vissaIssuePlace.Location = new System.Drawing.Point(3, 134);
            this.lbl_vissaIssuePlace.Name = "lbl_vissaIssuePlace";
            this.lbl_vissaIssuePlace.Size = new System.Drawing.Size(137, 19);
            this.lbl_vissaIssuePlace.TabIndex = 46;
            this.lbl_vissaIssuePlace.Text = "Vissa Issue Place";
            // 
            // dtp_visaDate
            // 
            this.dtp_visaDate.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtp_visaDate.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtp_visaDate.CustomFormat = "  dd/MM/yyyy     - dddd";
            this.dtp_visaDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_visaDate.Location = new System.Drawing.Point(153, 101);
            this.dtp_visaDate.Name = "dtp_visaDate";
            this.dtp_visaDate.ShowCheckBox = true;
            this.dtp_visaDate.Size = new System.Drawing.Size(207, 20);
            this.dtp_visaDate.TabIndex = 49;
            // 
            // lbl_visaDate
            // 
            this.lbl_visaDate.AutoSize = true;
            this.lbl_visaDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_visaDate.Location = new System.Drawing.Point(3, 105);
            this.lbl_visaDate.Name = "lbl_visaDate";
            this.lbl_visaDate.Size = new System.Drawing.Size(79, 19);
            this.lbl_visaDate.TabIndex = 45;
            this.lbl_visaDate.Text = "Visa Date";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(153, 72);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(257, 23);
            this.textBox1.TabIndex = 50;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 19);
            this.label1.TabIndex = 44;
            this.label1.Text = "Visa No";
            // 
            // txt_visaIssuePlace
            // 
            this.txt_visaIssuePlace.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_visaIssuePlace.Location = new System.Drawing.Point(153, 130);
            this.txt_visaIssuePlace.Name = "txt_visaIssuePlace";
            this.txt_visaIssuePlace.Size = new System.Drawing.Size(257, 23);
            this.txt_visaIssuePlace.TabIndex = 51;
            // 
            // lbl_serialNumber
            // 
            this.lbl_serialNumber.AutoSize = true;
            this.lbl_serialNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_serialNumber.Location = new System.Drawing.Point(3, 47);
            this.lbl_serialNumber.Name = "lbl_serialNumber";
            this.lbl_serialNumber.Size = new System.Drawing.Size(77, 19);
            this.lbl_serialNumber.TabIndex = 43;
            this.lbl_serialNumber.Text = "Serial No";
            // 
            // Walkin_Reservation_VisaInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(870, 479);
            this.Controls.Add(this.pnl_visaInformation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Walkin_Reservation_VisaInformation";
            this.Text = "Walkin_Reservation_VisaInformation";
            this.pnl_visaInformation.ResumeLayout(false);
            this.pnl_visaInformation.PerformLayout();
            this.pnl_visaInformationTwo.ResumeLayout(false);
            this.pnl_visaInformationTwo.PerformLayout();
            this.visaInformation_panel.ResumeLayout(false);
            this.visaInformation_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_visaInformation;
        private System.Windows.Forms.Panel pnl_visaInformationTwo;
        private System.Windows.Forms.TextBox txt_purposeOfVisit;
        private System.Windows.Forms.TextBox txt_departureTransportation;
        private System.Windows.Forms.DateTimePicker dtp_timeOfArrival;
        private System.Windows.Forms.Label lbl_departureTransportation;
        private System.Windows.Forms.Label lbl_goingTo;
        private System.Windows.Forms.Label lbl_dateOfArrival;
        private System.Windows.Forms.DateTimePicker dtp_dateOfArrival;
        private System.Windows.Forms.TextBox txt_goingTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_timeOfArrival;
        private System.Windows.Forms.Panel visaInformation_panel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_arrivedForm;
        private System.Windows.Forms.Label lbl_arrivedForm;
        private System.Windows.Forms.TextBox txt_serialNumber;
        private System.Windows.Forms.Label lbl_vissaIssuePlace;
        private System.Windows.Forms.DateTimePicker dtp_visaDate;
        private System.Windows.Forms.Label lbl_visaDate;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_visaIssuePlace;
        private System.Windows.Forms.Label lbl_serialNumber;
    }
}