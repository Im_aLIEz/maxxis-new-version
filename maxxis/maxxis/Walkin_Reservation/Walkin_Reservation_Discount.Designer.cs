﻿namespace maxxis.Walkin_Reservation
{
    partial class Walkin_Reservation_Discount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnl_discount = new System.Windows.Forms.Panel();
            this.dgv_discount = new System.Windows.Forms.DataGridView();
            this.col_Date_discountDGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Type_discountDGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Discount_discountDGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.discount_panel = new System.Windows.Forms.Panel();
            this.txt_discount_discountTotal = new System.Windows.Forms.TextBox();
            this.lbl_discount_discountTotal = new System.Windows.Forms.Label();
            this.lbl_discount = new System.Windows.Forms.Label();
            this.pnl_discount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_discount)).BeginInit();
            this.discount_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_discount
            // 
            this.pnl_discount.AutoScroll = true;
            this.pnl_discount.BackColor = System.Drawing.Color.Silver;
            this.pnl_discount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_discount.Controls.Add(this.dgv_discount);
            this.pnl_discount.Controls.Add(this.discount_panel);
            this.pnl_discount.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_discount.Location = new System.Drawing.Point(0, 0);
            this.pnl_discount.Name = "pnl_discount";
            this.pnl_discount.Size = new System.Drawing.Size(870, 267);
            this.pnl_discount.TabIndex = 21;
            // 
            // dgv_discount
            // 
            this.dgv_discount.AllowUserToAddRows = false;
            this.dgv_discount.AllowUserToDeleteRows = false;
            this.dgv_discount.BackgroundColor = System.Drawing.Color.Silver;
            this.dgv_discount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_discount.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv_discount.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_discount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_discount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_discount.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_Date_discountDGV,
            this.col_Type_discountDGV,
            this.col_Discount_discountDGV});
            this.dgv_discount.EnableHeadersVisualStyles = false;
            this.dgv_discount.Location = new System.Drawing.Point(38, 37);
            this.dgv_discount.Name = "dgv_discount";
            this.dgv_discount.ReadOnly = true;
            this.dgv_discount.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_discount.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_discount.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_discount.Size = new System.Drawing.Size(804, 203);
            this.dgv_discount.TabIndex = 11;
            // 
            // col_Date_discountDGV
            // 
            this.col_Date_discountDGV.HeaderText = "Date";
            this.col_Date_discountDGV.Name = "col_Date_discountDGV";
            this.col_Date_discountDGV.ReadOnly = true;
            this.col_Date_discountDGV.Width = 200;
            // 
            // col_Type_discountDGV
            // 
            this.col_Type_discountDGV.HeaderText = "Type";
            this.col_Type_discountDGV.Name = "col_Type_discountDGV";
            this.col_Type_discountDGV.ReadOnly = true;
            this.col_Type_discountDGV.Width = 350;
            // 
            // col_Discount_discountDGV
            // 
            this.col_Discount_discountDGV.HeaderText = "Discount";
            this.col_Discount_discountDGV.Name = "col_Discount_discountDGV";
            this.col_Discount_discountDGV.ReadOnly = true;
            this.col_Discount_discountDGV.Width = 250;
            // 
            // discount_panel
            // 
            this.discount_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.discount_panel.Controls.Add(this.txt_discount_discountTotal);
            this.discount_panel.Controls.Add(this.lbl_discount_discountTotal);
            this.discount_panel.Controls.Add(this.lbl_discount);
            this.discount_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.discount_panel.Location = new System.Drawing.Point(0, 0);
            this.discount_panel.Name = "discount_panel";
            this.discount_panel.Size = new System.Drawing.Size(868, 31);
            this.discount_panel.TabIndex = 10;
            // 
            // txt_discount_discountTotal
            // 
            this.txt_discount_discountTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_discount_discountTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_discount_discountTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_discount_discountTotal.ForeColor = System.Drawing.Color.White;
            this.txt_discount_discountTotal.Location = new System.Drawing.Point(720, 5);
            this.txt_discount_discountTotal.Name = "txt_discount_discountTotal";
            this.txt_discount_discountTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_discount_discountTotal.Size = new System.Drawing.Size(137, 17);
            this.txt_discount_discountTotal.TabIndex = 74;
            this.txt_discount_discountTotal.Text = "Php 0.00";
            // 
            // lbl_discount_discountTotal
            // 
            this.lbl_discount_discountTotal.AutoSize = true;
            this.lbl_discount_discountTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_discount_discountTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_discount_discountTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_discount_discountTotal.Location = new System.Drawing.Point(593, 8);
            this.lbl_discount_discountTotal.Name = "lbl_discount_discountTotal";
            this.lbl_discount_discountTotal.Size = new System.Drawing.Size(111, 14);
            this.lbl_discount_discountTotal.TabIndex = 73;
            this.lbl_discount_discountTotal.Text = "DISCOUNT TOTAL:";
            // 
            // lbl_discount
            // 
            this.lbl_discount.AutoSize = true;
            this.lbl_discount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_discount.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_discount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_discount.Location = new System.Drawing.Point(11, 8);
            this.lbl_discount.Name = "lbl_discount";
            this.lbl_discount.Size = new System.Drawing.Size(68, 14);
            this.lbl_discount.TabIndex = 0;
            this.lbl_discount.Text = "DISCOUNT";
            // 
            // Walkin_Reservation_Discount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(870, 479);
            this.Controls.Add(this.pnl_discount);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Walkin_Reservation_Discount";
            this.Text = "Walkin_Reservation_Discount";
            this.pnl_discount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_discount)).EndInit();
            this.discount_panel.ResumeLayout(false);
            this.discount_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_discount;
        private System.Windows.Forms.DataGridView dgv_discount;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Date_discountDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Type_discountDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Discount_discountDGV;
        private System.Windows.Forms.Panel discount_panel;
        private System.Windows.Forms.TextBox txt_discount_discountTotal;
        private System.Windows.Forms.Label lbl_discount_discountTotal;
        private System.Windows.Forms.Label lbl_discount;
    }
}