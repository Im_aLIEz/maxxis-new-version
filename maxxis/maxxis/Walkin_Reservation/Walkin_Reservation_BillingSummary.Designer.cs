﻿namespace maxxis.Walkin_Reservation
{
    partial class Walkin_Reservation_BillingSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnl_billingSummary = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.lbl_folioNumber = new System.Windows.Forms.Label();
            this.payments_panel = new System.Windows.Forms.Panel();
            this.lbl_payments = new System.Windows.Forms.Label();
            this.dgv_payments = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDiscount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_billingSummary = new System.Windows.Forms.DataGridView();
            this.col_Particulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Rate_Cost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_TotalAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_add = new System.Windows.Forms.Button();
            this.numUD_quantity = new System.Windows.Forms.NumericUpDown();
            this.cbox_add = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_cost = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.billingSummary_panel = new System.Windows.Forms.Panel();
            this.txt_billingSummary_Total = new System.Windows.Forms.TextBox();
            this.lbl_billingSummary_subTotal = new System.Windows.Forms.Label();
            this.lbl_billingSummary = new System.Windows.Forms.Label();
            this.pnl_billingSummary.SuspendLayout();
            this.panel1.SuspendLayout();
            this.payments_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_payments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_billingSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_quantity)).BeginInit();
            this.billingSummary_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_billingSummary
            // 
            this.pnl_billingSummary.BackColor = System.Drawing.Color.Silver;
            this.pnl_billingSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_billingSummary.Controls.Add(this.panel1);
            this.pnl_billingSummary.Controls.Add(this.billingSummary_panel);
            this.pnl_billingSummary.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_billingSummary.Location = new System.Drawing.Point(0, 0);
            this.pnl_billingSummary.Name = "pnl_billingSummary";
            this.pnl_billingSummary.Size = new System.Drawing.Size(870, 487);
            this.pnl_billingSummary.TabIndex = 20;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lbl_folioNumber);
            this.panel1.Controls.Add(this.payments_panel);
            this.panel1.Controls.Add(this.dgv_payments);
            this.panel1.Controls.Add(this.dgv_billingSummary);
            this.panel1.Controls.Add(this.btn_add);
            this.panel1.Controls.Add(this.numUD_quantity);
            this.panel1.Controls.Add(this.cbox_add);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txt_cost);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(868, 451);
            this.panel1.TabIndex = 53;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(843, 600);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 53;
            // 
            // lbl_folioNumber
            // 
            this.lbl_folioNumber.AutoSize = true;
            this.lbl_folioNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_folioNumber.Location = new System.Drawing.Point(35, 11);
            this.lbl_folioNumber.Name = "lbl_folioNumber";
            this.lbl_folioNumber.Size = new System.Drawing.Size(33, 17);
            this.lbl_folioNumber.TabIndex = 43;
            this.lbl_folioNumber.Text = "Add";
            // 
            // payments_panel
            // 
            this.payments_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.payments_panel.Controls.Add(this.lbl_payments);
            this.payments_panel.Location = new System.Drawing.Point(14, 353);
            this.payments_panel.Name = "payments_panel";
            this.payments_panel.Size = new System.Drawing.Size(820, 31);
            this.payments_panel.TabIndex = 52;
            // 
            // lbl_payments
            // 
            this.lbl_payments.AutoSize = true;
            this.lbl_payments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_payments.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_payments.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_payments.Location = new System.Drawing.Point(11, 8);
            this.lbl_payments.Name = "lbl_payments";
            this.lbl_payments.Size = new System.Drawing.Size(72, 14);
            this.lbl_payments.TabIndex = 0;
            this.lbl_payments.Text = "PAYMENTS";
            // 
            // dgv_payments
            // 
            this.dgv_payments.AllowUserToAddRows = false;
            this.dgv_payments.AllowUserToDeleteRows = false;
            this.dgv_payments.BackgroundColor = System.Drawing.Color.Silver;
            this.dgv_payments.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv_payments.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_payments.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_payments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_payments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.colAmount,
            this.colDiscount});
            this.dgv_payments.EnableHeadersVisualStyles = false;
            this.dgv_payments.Location = new System.Drawing.Point(14, 387);
            this.dgv_payments.Name = "dgv_payments";
            this.dgv_payments.ReadOnly = true;
            this.dgv_payments.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_payments.RowHeadersVisible = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_payments.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_payments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_payments.Size = new System.Drawing.Size(820, 203);
            this.dgv_payments.TabIndex = 51;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 200;
            // 
            // colAmount
            // 
            this.colAmount.HeaderText = "Amount";
            this.colAmount.Name = "colAmount";
            this.colAmount.ReadOnly = true;
            this.colAmount.Width = 350;
            // 
            // colDiscount
            // 
            this.colDiscount.HeaderText = "Discount";
            this.colDiscount.Name = "colDiscount";
            this.colDiscount.ReadOnly = true;
            this.colDiscount.Width = 200;
            // 
            // dgv_billingSummary
            // 
            this.dgv_billingSummary.AllowUserToAddRows = false;
            this.dgv_billingSummary.AllowUserToDeleteRows = false;
            this.dgv_billingSummary.BackgroundColor = System.Drawing.Color.Silver;
            this.dgv_billingSummary.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv_billingSummary.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_billingSummary.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_billingSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_billingSummary.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_Particulars,
            this.col_Qty,
            this.col_Unit,
            this.col_Rate_Cost,
            this.col_TotalAmount});
            this.dgv_billingSummary.EnableHeadersVisualStyles = false;
            this.dgv_billingSummary.Location = new System.Drawing.Point(14, 70);
            this.dgv_billingSummary.Name = "dgv_billingSummary";
            this.dgv_billingSummary.ReadOnly = true;
            this.dgv_billingSummary.RowHeadersVisible = false;
            this.dgv_billingSummary.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgv_billingSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_billingSummary.Size = new System.Drawing.Size(820, 277);
            this.dgv_billingSummary.TabIndex = 41;
            // 
            // col_Particulars
            // 
            this.col_Particulars.HeaderText = "Particulars";
            this.col_Particulars.Name = "col_Particulars";
            this.col_Particulars.ReadOnly = true;
            this.col_Particulars.Width = 250;
            // 
            // col_Qty
            // 
            this.col_Qty.HeaderText = "Qty";
            this.col_Qty.Name = "col_Qty";
            this.col_Qty.ReadOnly = true;
            this.col_Qty.Width = 80;
            // 
            // col_Unit
            // 
            this.col_Unit.HeaderText = "Unit";
            this.col_Unit.Name = "col_Unit";
            this.col_Unit.ReadOnly = true;
            // 
            // col_Rate_Cost
            // 
            this.col_Rate_Cost.HeaderText = "Rate/Cost";
            this.col_Rate_Cost.Name = "col_Rate_Cost";
            this.col_Rate_Cost.ReadOnly = true;
            this.col_Rate_Cost.Width = 150;
            // 
            // col_TotalAmount
            // 
            this.col_TotalAmount.HeaderText = "Amount";
            this.col_TotalAmount.Name = "col_TotalAmount";
            this.col_TotalAmount.ReadOnly = true;
            this.col_TotalAmount.Width = 150;
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_add.FlatAppearance.BorderSize = 0;
            this.btn_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add.ForeColor = System.Drawing.Color.White;
            this.btn_add.Location = new System.Drawing.Point(589, 20);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(96, 40);
            this.btn_add.TabIndex = 44;
            this.btn_add.Text = "ADD";
            this.btn_add.UseVisualStyleBackColor = false;
            this.btn_add.Click += new System.EventHandler(this.button1_Click);
            // 
            // numUD_quantity
            // 
            this.numUD_quantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numUD_quantity.Location = new System.Drawing.Point(373, 32);
            this.numUD_quantity.Name = "numUD_quantity";
            this.numUD_quantity.Size = new System.Drawing.Size(71, 24);
            this.numUD_quantity.TabIndex = 2;
            this.numUD_quantity.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown1_MouseUp);
            // 
            // cbox_add
            // 
            this.cbox_add.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbox_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_add.FormattingEnabled = true;
            this.cbox_add.Location = new System.Drawing.Point(38, 31);
            this.cbox_add.Name = "cbox_add";
            this.cbox_add.Size = new System.Drawing.Size(299, 26);
            this.cbox_add.TabIndex = 1;
            this.cbox_add.SelectedIndexChanged += new System.EventHandler(this.cbox_add_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(485, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 17);
            this.label2.TabIndex = 49;
            this.label2.Text = "Cost";
            // 
            // txt_cost
            // 
            this.txt_cost.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cost.Location = new System.Drawing.Point(488, 29);
            this.txt_cost.Name = "txt_cost";
            this.txt_cost.ReadOnly = true;
            this.txt_cost.Size = new System.Drawing.Size(71, 24);
            this.txt_cost.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(370, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 17);
            this.label1.TabIndex = 48;
            this.label1.Text = "Qty";
            // 
            // billingSummary_panel
            // 
            this.billingSummary_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.billingSummary_panel.Controls.Add(this.txt_billingSummary_Total);
            this.billingSummary_panel.Controls.Add(this.lbl_billingSummary_subTotal);
            this.billingSummary_panel.Controls.Add(this.lbl_billingSummary);
            this.billingSummary_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.billingSummary_panel.Location = new System.Drawing.Point(0, 0);
            this.billingSummary_panel.Name = "billingSummary_panel";
            this.billingSummary_panel.Size = new System.Drawing.Size(868, 31);
            this.billingSummary_panel.TabIndex = 10;
            // 
            // txt_billingSummary_Total
            // 
            this.txt_billingSummary_Total.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_billingSummary_Total.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_billingSummary_Total.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_billingSummary_Total.ForeColor = System.Drawing.Color.White;
            this.txt_billingSummary_Total.Location = new System.Drawing.Point(720, 7);
            this.txt_billingSummary_Total.Name = "txt_billingSummary_Total";
            this.txt_billingSummary_Total.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_billingSummary_Total.Size = new System.Drawing.Size(137, 17);
            this.txt_billingSummary_Total.TabIndex = 74;
            this.txt_billingSummary_Total.Text = "Php 0.00";
            // 
            // lbl_billingSummary_subTotal
            // 
            this.lbl_billingSummary_subTotal.AutoSize = true;
            this.lbl_billingSummary_subTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_billingSummary_subTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_billingSummary_subTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_billingSummary_subTotal.Location = new System.Drawing.Point(599, 10);
            this.lbl_billingSummary_subTotal.Name = "lbl_billingSummary_subTotal";
            this.lbl_billingSummary_subTotal.Size = new System.Drawing.Size(115, 14);
            this.lbl_billingSummary_subTotal.TabIndex = 73;
            this.lbl_billingSummary_subTotal.Text = "TOTAL PAYMENTS:";
            // 
            // lbl_billingSummary
            // 
            this.lbl_billingSummary.AutoSize = true;
            this.lbl_billingSummary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_billingSummary.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_billingSummary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_billingSummary.Location = new System.Drawing.Point(11, 10);
            this.lbl_billingSummary.Name = "lbl_billingSummary";
            this.lbl_billingSummary.Size = new System.Drawing.Size(116, 14);
            this.lbl_billingSummary.TabIndex = 0;
            this.lbl_billingSummary.Text = "BILLING SUMMARY";
            // 
            // Walkin_Reservation_BillingSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(870, 479);
            this.Controls.Add(this.pnl_billingSummary);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Walkin_Reservation_BillingSummary";
            this.Text = "Walkin_Reservation_BillingSummary";
            this.Load += new System.EventHandler(this.Walkin_Reservation_BillingSummary_Load);
            this.pnl_billingSummary.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.payments_panel.ResumeLayout(false);
            this.payments_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_payments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_billingSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_quantity)).EndInit();
            this.billingSummary_panel.ResumeLayout(false);
            this.billingSummary_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_billingSummary;
        private System.Windows.Forms.DataGridView dgv_billingSummary;
        private System.Windows.Forms.Panel billingSummary_panel;
        private System.Windows.Forms.TextBox txt_billingSummary_Total;
        private System.Windows.Forms.Label lbl_billingSummary;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Particulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Qty;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Rate_Cost;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_TotalAmount;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_cost;
        private System.Windows.Forms.Label lbl_folioNumber;
        private System.Windows.Forms.ComboBox cbox_add;
        private System.Windows.Forms.Label lbl_billingSummary_subTotal;
        private System.Windows.Forms.NumericUpDown numUD_quantity;
        private System.Windows.Forms.DataGridView dgv_payments;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDiscount;
        private System.Windows.Forms.Panel payments_panel;
        private System.Windows.Forms.Label lbl_payments;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
    }
}