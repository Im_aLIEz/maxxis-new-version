﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace maxxis.Walkin_Reservation
{
    public partial class Walkin_Reservation_FolioSummary : Form
    {
        string con = System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString;

        Walkin_Reservation_AccomodationDetails WR_Accomodationdetails = new Walkin_Reservation_AccomodationDetails();
        Walkin_Reservation_BillingSummary WR_billingsummary = new Walkin_Reservation_BillingSummary();
        Walkin_Reservation_SettlementOption WR_settlement = new Walkin_Reservation_SettlementOption();

        static string overalladult;
        static string overallchildren;

        private static string selectedrooms;
        private static string roomtypes;

        public string SelectedRooms
        {
            get { return selectedrooms; }
            set { selectedrooms = value; }
        }
        public string RoomTypes
        {
            get { return roomtypes; }
            set { roomtypes = value; }
        }

        string selectedPWD;
        string selectedUR;
        string selectedUCR;
        string selectedUVR;

        string roomsselect;

        int pwdcounter = 0;
        int urcounter = 0;
        int ucrcounter = 0;
        int uvrcounter = 0;

        List<string> ucrrooms = new List<string>();
        List<string> urrooms = new List<string>();
        List<string> uvrrooms = new List<string>();

        string[] arrurrooms;
        string[] arrucrrooms;
        string[] arruvrrooms;

        string rooms = "";

        public string OverallAdult
        {
            get { return overalladult; }
            set { overalladult = value; }
        }
        public string OverallChildren
        {
            get { return overallchildren; }
            set { overallchildren = value; }
        }

        public Walkin_Reservation_FolioSummary()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        public void PWDRoomChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'PWD Urban Room' AND occupied = 0 ORDER BY room_no ASC ;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    selectedPWD = item[pwdcounter].ToString();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void URRoomupperChecker()
        {
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'Urban Room' AND occupied = 0 AND room_no > 70 ORDER BY room_no ASC ;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        selectedUR = item[0].ToString();
                        urrooms.Add(item[0].ToString());
                    }
                }
                else
                {
                    selectedUCR = null;
                }

                arrurrooms = urrooms.ToArray();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void UCRRoomupperChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'Urban Corner Room' AND occupied = 0 AND room_no > 70 ORDER BY room_no ASC ;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        selectedUCR = item[0].ToString();
                        ucrrooms.Add(item[0].ToString());
                    }
                }
                else
                {
                    selectedUCR = null;
                }
                arrucrrooms = ucrrooms.ToArray();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void UVRRoomupperChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'Urban View Room' AND occupied = 0 AND room_no > 70 ORDER BY room_no ASC ;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt != null)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        selectedUVR = item[0].ToString();
                        uvrrooms.Add(item[0].ToString());
                    }
                }
                else
                {
                    selectedUVR = null;
                }
                arruvrrooms = uvrrooms.ToArray();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void URRoomlowerChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'Urban Room' AND occupied = 0 AND room_no < 70 ORDER BY room_no DESC;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    selectedUR = item[0].ToString();
                    urrooms.Add(item[0].ToString());
                }

                arrurrooms = urrooms.ToArray();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void UCRRoomlowerChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'Urban Corner Room' AND occupied = 0 AND room_no < 70 ORDER BY room_no DESC ;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    selectedUCR = item[0].ToString();
                    ucrrooms.Add(item[0].ToString());
                }

                arrucrrooms = ucrrooms.ToArray();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void UVRRoomlowerChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'Urban View Room' AND occupied = 0 AND room_no < 70 ORDER BY room_no ASC ;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    selectedUVR = item[0].ToString();
                    uvrrooms.Add(item[0].ToString());
                }

                arruvrrooms = uvrrooms.ToArray();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        

        internal void Walkin_Reservation_FolioSummary_Load(object sender, EventArgs e)
        {
            if(WR_Accomodationdetails.BillChecker==1)
            {
                OverallAdult = (Int32.Parse(WR_Accomodationdetails.URadult) + Int32.Parse(WR_Accomodationdetails.UCRadult) + Int32.Parse(WR_Accomodationdetails.UVRadult)).ToString();
                OverallChildren = (Int32.Parse(WR_Accomodationdetails.URchild) + Int32.Parse(WR_Accomodationdetails.UCRchild) + Int32.Parse(WR_Accomodationdetails.UVRchild)).ToString();
                //txt_subTotal.Text = "Php " + WR_billingsummary.SubPayment;
                txt_totalNumberOfAdults.Text = OverallAdult;
                total_numberOfChildren.Text = OverallChildren;
                lbl_additionalCompanionAdults.Text = "Adult/s(" + WR_Accomodationdetails.ExtraAdult +")";
                lbl_additionalCompanionChildren.Text = "Child/ren(" + WR_Accomodationdetails.ExtraChild + ")";
                txt_totalPayment.Text = "Php " + WR_billingsummary.TotalPayment;
                txt_balance.Text = "Php " + (Decimal.Parse(WR_billingsummary.TotalPayment) - Decimal.Parse(WR_settlement.CashPaid));
                txt_folioSummary_balanceDue.Text = "Php " + WR_billingsummary.TotalPayment;
                txt_otherCharges.Text = "Php " + WR_billingsummary.ItemPayment.ToString();
                txt_fee_additionalAdultsAndChildren.Text = "Php " + WR_Accomodationdetails.ExtraPayments;
                txt_totalDiscount.Text = WR_settlement.Discount;
            }

            if (Int32.Parse(WR_Accomodationdetails.PWDSelect) >= 1)
            {
                for (int x = 0; x < Int32.Parse(WR_Accomodationdetails.PWDSelect); x++)
                {
                    PWDRoomChecker();
                    rooms = rooms + selectedPWD +" ";
                    selectedPWD = "";
                }
                roomsselect = roomsselect + "PWD Urban Room /";
            }
            if (Int32.Parse(WR_Accomodationdetails.URSelect) >= 1)
            {
                for (int x = 0; x < Int32.Parse(WR_Accomodationdetails.URSelect); x++)
                {
                    URRoomupperChecker();
                    if (selectedUR == null)
                    {
                        URRoomlowerChecker();
                    }
                    rooms = rooms + urrooms[urcounter] + " ";
                    selectedUR = "";
                    urcounter = urcounter + 1;
                }
                roomsselect = roomsselect + "Urban Room /";
            }
            if (Int32.Parse(WR_Accomodationdetails.UCRSelect) >= 1)
            {
                for (int x = 0; x < Int32.Parse(WR_Accomodationdetails.UCRSelect); x++)
                {
                    UCRRoomupperChecker();
                    if (selectedUCR == null)
                    {
                        UCRRoomlowerChecker();
                    }
                    rooms = rooms + ucrrooms[ucrcounter] + " ";
                    selectedUCR = "";
                    ucrcounter = ucrcounter + 1;
                }
                roomsselect = roomsselect + "Urban Corner Room /";
            }
            if (Int32.Parse(WR_Accomodationdetails.UVRSelect) >= 1)
            {
                for (int x = 0; x < Int32.Parse(WR_Accomodationdetails.UVRSelect); x++)
                {
                    UVRRoomupperChecker();
                    if (selectedUVR == null)
                    {
                        URRoomlowerChecker();
                    }
                    rooms = rooms + uvrrooms[uvrcounter] + " ";
                    selectedUVR = "";
                    uvrcounter = uvrcounter + 1;
                }
                roomsselect = roomsselect + "Urban View Room /";
            }
            lbl_rooms.Text = rooms;
            SelectedRooms = rooms;
            RoomTypes = roomsselect;
            rooms = "";
            roomsselect = "";
            urcounter = 0;
            ucrcounter = 0;
            uvrcounter = 0;
        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void txt_fee_specialArrangements_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbl_specialArrangements_Click(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void txt_otherCharges_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void txt_balance_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_totalPayment_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_totalDiscount_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_subTotal_TextChanged(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }
    }
}
