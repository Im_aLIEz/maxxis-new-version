﻿namespace maxxis
{
    partial class WalkinReservation_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.newWalkinReservation_Panel = new System.Windows.Forms.Panel();
            this.lbl_newWalkinReservation = new System.Windows.Forms.Label();
            this.pcb_newWalkinReservation = new System.Windows.Forms.PictureBox();
            this.pnl_hotelInfo = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_hotelAddress = new System.Windows.Forms.Label();
            this.lbl_hotelStreet = new System.Windows.Forms.Label();
            this.lbl_maxxHotelMakati = new System.Windows.Forms.Label();
            this.pcb_maxxLogo = new System.Windows.Forms.PictureBox();
            this.lbl_timer = new System.Windows.Forms.Panel();
            this.pnl_guestInformation = new System.Windows.Forms.Panel();
            this.lbl_dateOfBirth = new System.Windows.Forms.Label();
            this.lbl_province = new System.Windows.Forms.Label();
            this.txt_province = new System.Windows.Forms.TextBox();
            this.cmb_homeCountry = new System.Windows.Forms.ComboBox();
            this.lbl_homeCountry = new System.Windows.Forms.Label();
            this.dtb_dateOfBirth = new System.Windows.Forms.DateTimePicker();
            this.lbl_visaNumber = new System.Windows.Forms.Label();
            this.txt_visaNumber = new System.Windows.Forms.TextBox();
            this.lbl_city = new System.Windows.Forms.Label();
            this.txt_city = new System.Windows.Forms.TextBox();
            this.lbl_passportNumber = new System.Windows.Forms.Label();
            this.txt_passportNumber = new System.Windows.Forms.TextBox();
            this.lbl_companyAgency = new System.Windows.Forms.Label();
            this.txt_companyAgency = new System.Windows.Forms.TextBox();
            this.lbl_address = new System.Windows.Forms.Label();
            this.txt_address = new System.Windows.Forms.TextBox();
            this.lbl_email = new System.Windows.Forms.Label();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.lbl_contactDetails = new System.Windows.Forms.Label();
            this.txt_contactDetails = new System.Windows.Forms.TextBox();
            this.lbl_middlename = new System.Windows.Forms.Label();
            this.txt_middleName = new System.Windows.Forms.TextBox();
            this.lbl_firstname = new System.Windows.Forms.Label();
            this.txt_firstName = new System.Windows.Forms.TextBox();
            this.lbl_lastname = new System.Windows.Forms.Label();
            this.cmb_title = new System.Windows.Forms.ComboBox();
            this.txt_lastName = new System.Windows.Forms.TextBox();
            this.lbl_title = new System.Windows.Forms.Label();
            this.info_panel = new System.Windows.Forms.Panel();
            this.lbl_guestInformation = new System.Windows.Forms.Label();
            this.pnl_newWalkinGuest = new System.Windows.Forms.Panel();
            this.txt_timer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pnl_newReservation_Selector = new System.Windows.Forms.Panel();
            this.btn_folioSummary = new System.Windows.Forms.Button();
            this.btn_businessSource = new System.Windows.Forms.Button();
            this.btn_accomodationDetails = new System.Windows.Forms.Button();
            this.btn_guestInformation = new System.Windows.Forms.Button();
            this.lbl_newWalkinGuest = new System.Windows.Forms.Label();
            this.btn_settlementOption = new System.Windows.Forms.Button();
            this.btn_billingSummary = new System.Windows.Forms.Button();
            this.btn_checkIn = new System.Windows.Forms.Button();
            this.btn_reserve = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.tblguestinfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tblguestinfoBindingSource7 = new System.Windows.Forms.BindingSource(this.components);
            this.tblguestinfoBindingSource9 = new System.Windows.Forms.BindingSource(this.components);
            this.tblguestinfoBindingSource10 = new System.Windows.Forms.BindingSource(this.components);
            this.tblguestinfoBindingSource8 = new System.Windows.Forms.BindingSource(this.components);
            this.tblguestinfoBindingSource6 = new System.Windows.Forms.BindingSource(this.components);
            this.tblguestinfoBindingSource5 = new System.Windows.Forms.BindingSource(this.components);
            this.tblguestinfoBindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.tblguestinfoBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.tblguestinfoBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.tblguestinfoBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.tblguestinfoBindingSource11 = new System.Windows.Forms.BindingSource(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.newWalkinReservation_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_newWalkinReservation)).BeginInit();
            this.pnl_hotelInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_maxxLogo)).BeginInit();
            this.lbl_timer.SuspendLayout();
            this.pnl_guestInformation.SuspendLayout();
            this.info_panel.SuspendLayout();
            this.pnl_newWalkinGuest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource11)).BeginInit();
            this.SuspendLayout();
            // 
            // newWalkinReservation_Panel
            // 
            this.newWalkinReservation_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.newWalkinReservation_Panel.Controls.Add(this.lbl_newWalkinReservation);
            this.newWalkinReservation_Panel.Controls.Add(this.pcb_newWalkinReservation);
            this.newWalkinReservation_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.newWalkinReservation_Panel.Location = new System.Drawing.Point(0, 0);
            this.newWalkinReservation_Panel.Name = "newWalkinReservation_Panel";
            this.newWalkinReservation_Panel.Size = new System.Drawing.Size(1250, 43);
            this.newWalkinReservation_Panel.TabIndex = 9;
            // 
            // lbl_newWalkinReservation
            // 
            this.lbl_newWalkinReservation.AutoSize = true;
            this.lbl_newWalkinReservation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_newWalkinReservation.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_newWalkinReservation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_newWalkinReservation.Location = new System.Drawing.Point(38, 13);
            this.lbl_newWalkinReservation.Name = "lbl_newWalkinReservation";
            this.lbl_newWalkinReservation.Size = new System.Drawing.Size(169, 19);
            this.lbl_newWalkinReservation.TabIndex = 0;
            this.lbl_newWalkinReservation.Text = "New Walk-in Check in";
            // 
            // pcb_newWalkinReservation
            // 
            this.pcb_newWalkinReservation.BackgroundImage = global::maxxis.Properties.Resources.New_walk_in_2;
            this.pcb_newWalkinReservation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_newWalkinReservation.Dock = System.Windows.Forms.DockStyle.Left;
            this.pcb_newWalkinReservation.Location = new System.Drawing.Point(0, 0);
            this.pcb_newWalkinReservation.Name = "pcb_newWalkinReservation";
            this.pcb_newWalkinReservation.Size = new System.Drawing.Size(41, 43);
            this.pcb_newWalkinReservation.TabIndex = 1;
            this.pcb_newWalkinReservation.TabStop = false;
            // 
            // pnl_hotelInfo
            // 
            this.pnl_hotelInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.pnl_hotelInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_hotelInfo.Controls.Add(this.label1);
            this.pnl_hotelInfo.Controls.Add(this.lbl_hotelAddress);
            this.pnl_hotelInfo.Controls.Add(this.lbl_hotelStreet);
            this.pnl_hotelInfo.Controls.Add(this.lbl_maxxHotelMakati);
            this.pnl_hotelInfo.Controls.Add(this.pcb_maxxLogo);
            this.pnl_hotelInfo.Location = new System.Drawing.Point(88, 49);
            this.pnl_hotelInfo.Name = "pnl_hotelInfo";
            this.pnl_hotelInfo.Size = new System.Drawing.Size(1076, 143);
            this.pnl_hotelInfo.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label1.Location = new System.Drawing.Point(505, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(566, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "All fields that is marked with an Asterisk (*) is required and must be not left e" +
    "mpty";
            // 
            // lbl_hotelAddress
            // 
            this.lbl_hotelAddress.AutoSize = true;
            this.lbl_hotelAddress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_hotelAddress.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_hotelAddress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_hotelAddress.Location = new System.Drawing.Point(179, 81);
            this.lbl_hotelAddress.Name = "lbl_hotelAddress";
            this.lbl_hotelAddress.Size = new System.Drawing.Size(207, 17);
            this.lbl_hotelAddress.TabIndex = 3;
            this.lbl_hotelAddress.Text = "Makati City, 1210 PHILIPPINES";
            // 
            // lbl_hotelStreet
            // 
            this.lbl_hotelStreet.AutoSize = true;
            this.lbl_hotelStreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_hotelStreet.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_hotelStreet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_hotelStreet.Location = new System.Drawing.Point(179, 65);
            this.lbl_hotelStreet.Name = "lbl_hotelStreet";
            this.lbl_hotelStreet.Size = new System.Drawing.Size(246, 17);
            this.lbl_hotelStreet.TabIndex = 2;
            this.lbl_hotelStreet.Text = "Makati Avenue corner Singian Street.";
            // 
            // lbl_maxxHotelMakati
            // 
            this.lbl_maxxHotelMakati.AutoSize = true;
            this.lbl_maxxHotelMakati.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_maxxHotelMakati.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxxHotelMakati.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_maxxHotelMakati.Location = new System.Drawing.Point(178, 23);
            this.lbl_maxxHotelMakati.Name = "lbl_maxxHotelMakati";
            this.lbl_maxxHotelMakati.Size = new System.Drawing.Size(288, 32);
            this.lbl_maxxHotelMakati.TabIndex = 1;
            this.lbl_maxxHotelMakati.Text = "MAXX HOTEL MAKATI";
            // 
            // pcb_maxxLogo
            // 
            this.pcb_maxxLogo.BackgroundImage = global::maxxis.Properties.Resources.maxxLogo_5;
            this.pcb_maxxLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pcb_maxxLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcb_maxxLogo.Location = new System.Drawing.Point(15, 13);
            this.pcb_maxxLogo.Name = "pcb_maxxLogo";
            this.pcb_maxxLogo.Size = new System.Drawing.Size(157, 114);
            this.pcb_maxxLogo.TabIndex = 0;
            this.pcb_maxxLogo.TabStop = false;
            // 
            // lbl_timer
            // 
            this.lbl_timer.Controls.Add(this.pnl_guestInformation);
            this.lbl_timer.Controls.Add(this.pnl_newWalkinGuest);
            this.lbl_timer.Location = new System.Drawing.Point(88, 198);
            this.lbl_timer.Name = "lbl_timer";
            this.lbl_timer.Size = new System.Drawing.Size(1076, 479);
            this.lbl_timer.TabIndex = 12;
            // 
            // pnl_guestInformation
            // 
            this.pnl_guestInformation.AutoScroll = true;
            this.pnl_guestInformation.BackColor = System.Drawing.Color.Silver;
            this.pnl_guestInformation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_guestInformation.Controls.Add(this.lbl_dateOfBirth);
            this.pnl_guestInformation.Controls.Add(this.lbl_province);
            this.pnl_guestInformation.Controls.Add(this.txt_province);
            this.pnl_guestInformation.Controls.Add(this.cmb_homeCountry);
            this.pnl_guestInformation.Controls.Add(this.lbl_homeCountry);
            this.pnl_guestInformation.Controls.Add(this.dtb_dateOfBirth);
            this.pnl_guestInformation.Controls.Add(this.lbl_visaNumber);
            this.pnl_guestInformation.Controls.Add(this.txt_visaNumber);
            this.pnl_guestInformation.Controls.Add(this.lbl_city);
            this.pnl_guestInformation.Controls.Add(this.txt_city);
            this.pnl_guestInformation.Controls.Add(this.lbl_passportNumber);
            this.pnl_guestInformation.Controls.Add(this.txt_passportNumber);
            this.pnl_guestInformation.Controls.Add(this.lbl_companyAgency);
            this.pnl_guestInformation.Controls.Add(this.txt_companyAgency);
            this.pnl_guestInformation.Controls.Add(this.lbl_address);
            this.pnl_guestInformation.Controls.Add(this.txt_address);
            this.pnl_guestInformation.Controls.Add(this.lbl_email);
            this.pnl_guestInformation.Controls.Add(this.txt_email);
            this.pnl_guestInformation.Controls.Add(this.lbl_contactDetails);
            this.pnl_guestInformation.Controls.Add(this.txt_contactDetails);
            this.pnl_guestInformation.Controls.Add(this.lbl_middlename);
            this.pnl_guestInformation.Controls.Add(this.txt_middleName);
            this.pnl_guestInformation.Controls.Add(this.lbl_firstname);
            this.pnl_guestInformation.Controls.Add(this.txt_firstName);
            this.pnl_guestInformation.Controls.Add(this.lbl_lastname);
            this.pnl_guestInformation.Controls.Add(this.cmb_title);
            this.pnl_guestInformation.Controls.Add(this.txt_lastName);
            this.pnl_guestInformation.Controls.Add(this.lbl_title);
            this.pnl_guestInformation.Controls.Add(this.info_panel);
            this.pnl_guestInformation.Location = new System.Drawing.Point(1116, 72);
            this.pnl_guestInformation.Name = "pnl_guestInformation";
            this.pnl_guestInformation.Size = new System.Drawing.Size(870, 300);
            this.pnl_guestInformation.TabIndex = 14;
            // 
            // lbl_dateOfBirth
            // 
            this.lbl_dateOfBirth.AutoSize = true;
            this.lbl_dateOfBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dateOfBirth.Location = new System.Drawing.Point(562, 191);
            this.lbl_dateOfBirth.Name = "lbl_dateOfBirth";
            this.lbl_dateOfBirth.Size = new System.Drawing.Size(87, 17);
            this.lbl_dateOfBirth.TabIndex = 45;
            this.lbl_dateOfBirth.Text = "Date of Birth";
            // 
            // lbl_province
            // 
            this.lbl_province.AutoSize = true;
            this.lbl_province.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_province.Location = new System.Drawing.Point(562, 144);
            this.lbl_province.Name = "lbl_province";
            this.lbl_province.Size = new System.Drawing.Size(63, 17);
            this.lbl_province.TabIndex = 44;
            this.lbl_province.Text = "Province";
            // 
            // txt_province
            // 
            this.txt_province.Location = new System.Drawing.Point(565, 163);
            this.txt_province.Name = "txt_province";
            this.txt_province.Size = new System.Drawing.Size(208, 20);
            this.txt_province.TabIndex = 43;
            // 
            // cmb_homeCountry
            // 
            this.cmb_homeCountry.FormattingEnabled = true;
            this.cmb_homeCountry.Location = new System.Drawing.Point(331, 209);
            this.cmb_homeCountry.Name = "cmb_homeCountry";
            this.cmb_homeCountry.Size = new System.Drawing.Size(218, 21);
            this.cmb_homeCountry.TabIndex = 42;
            // 
            // lbl_homeCountry
            // 
            this.lbl_homeCountry.AutoSize = true;
            this.lbl_homeCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_homeCountry.Location = new System.Drawing.Point(328, 191);
            this.lbl_homeCountry.Name = "lbl_homeCountry";
            this.lbl_homeCountry.Size = new System.Drawing.Size(98, 17);
            this.lbl_homeCountry.TabIndex = 41;
            this.lbl_homeCountry.Text = "Home Country";
            // 
            // dtb_dateOfBirth
            // 
            this.dtb_dateOfBirth.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtb_dateOfBirth.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtb_dateOfBirth.Cursor = System.Windows.Forms.Cursors.No;
            this.dtb_dateOfBirth.CustomFormat = "MMMM dd, yyyy - dddd";
            this.dtb_dateOfBirth.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtb_dateOfBirth.Location = new System.Drawing.Point(565, 210);
            this.dtb_dateOfBirth.Name = "dtb_dateOfBirth";
            this.dtb_dateOfBirth.Size = new System.Drawing.Size(208, 20);
            this.dtb_dateOfBirth.TabIndex = 40;
            // 
            // lbl_visaNumber
            // 
            this.lbl_visaNumber.AutoSize = true;
            this.lbl_visaNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_visaNumber.Location = new System.Drawing.Point(328, 239);
            this.lbl_visaNumber.Name = "lbl_visaNumber";
            this.lbl_visaNumber.Size = new System.Drawing.Size(62, 17);
            this.lbl_visaNumber.TabIndex = 32;
            this.lbl_visaNumber.Text = "VISA no.";
            // 
            // txt_visaNumber
            // 
            this.txt_visaNumber.Location = new System.Drawing.Point(331, 258);
            this.txt_visaNumber.Name = "txt_visaNumber";
            this.txt_visaNumber.Size = new System.Drawing.Size(300, 20);
            this.txt_visaNumber.TabIndex = 31;
            // 
            // lbl_city
            // 
            this.lbl_city.AutoSize = true;
            this.lbl_city.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_city.Location = new System.Drawing.Point(328, 144);
            this.lbl_city.Name = "lbl_city";
            this.lbl_city.Size = new System.Drawing.Size(31, 17);
            this.lbl_city.TabIndex = 30;
            this.lbl_city.Text = "City";
            // 
            // txt_city
            // 
            this.txt_city.Location = new System.Drawing.Point(331, 163);
            this.txt_city.Name = "txt_city";
            this.txt_city.Size = new System.Drawing.Size(218, 20);
            this.txt_city.TabIndex = 29;
            // 
            // lbl_passportNumber
            // 
            this.lbl_passportNumber.AutoSize = true;
            this.lbl_passportNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_passportNumber.Location = new System.Drawing.Point(11, 239);
            this.lbl_passportNumber.Name = "lbl_passportNumber";
            this.lbl_passportNumber.Size = new System.Drawing.Size(90, 17);
            this.lbl_passportNumber.TabIndex = 28;
            this.lbl_passportNumber.Text = "Passport No.";
            // 
            // txt_passportNumber
            // 
            this.txt_passportNumber.Location = new System.Drawing.Point(14, 258);
            this.txt_passportNumber.Name = "txt_passportNumber";
            this.txt_passportNumber.Size = new System.Drawing.Size(300, 20);
            this.txt_passportNumber.TabIndex = 27;
            // 
            // lbl_companyAgency
            // 
            this.lbl_companyAgency.AutoSize = true;
            this.lbl_companyAgency.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_companyAgency.Location = new System.Drawing.Point(11, 191);
            this.lbl_companyAgency.Name = "lbl_companyAgency";
            this.lbl_companyAgency.Size = new System.Drawing.Size(118, 17);
            this.lbl_companyAgency.TabIndex = 26;
            this.lbl_companyAgency.Text = "Company/Agency";
            // 
            // txt_companyAgency
            // 
            this.txt_companyAgency.Location = new System.Drawing.Point(14, 210);
            this.txt_companyAgency.Name = "txt_companyAgency";
            this.txt_companyAgency.Size = new System.Drawing.Size(300, 20);
            this.txt_companyAgency.TabIndex = 25;
            // 
            // lbl_address
            // 
            this.lbl_address.AutoSize = true;
            this.lbl_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_address.Location = new System.Drawing.Point(11, 144);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(60, 17);
            this.lbl_address.TabIndex = 24;
            this.lbl_address.Text = "Address";
            // 
            // txt_address
            // 
            this.txt_address.Location = new System.Drawing.Point(14, 163);
            this.txt_address.Name = "txt_address";
            this.txt_address.Size = new System.Drawing.Size(300, 20);
            this.txt_address.TabIndex = 23;
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_email.Location = new System.Drawing.Point(246, 97);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(42, 17);
            this.lbl_email.TabIndex = 22;
            this.lbl_email.Text = "Email";
            // 
            // txt_email
            // 
            this.txt_email.Location = new System.Drawing.Point(249, 116);
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(300, 20);
            this.txt_email.TabIndex = 21;
            // 
            // lbl_contactDetails
            // 
            this.lbl_contactDetails.AutoSize = true;
            this.lbl_contactDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_contactDetails.Location = new System.Drawing.Point(11, 97);
            this.lbl_contactDetails.Name = "lbl_contactDetails";
            this.lbl_contactDetails.Size = new System.Drawing.Size(103, 17);
            this.lbl_contactDetails.TabIndex = 20;
            this.lbl_contactDetails.Text = "Contact Details";
            // 
            // txt_contactDetails
            // 
            this.txt_contactDetails.Location = new System.Drawing.Point(14, 116);
            this.txt_contactDetails.Name = "txt_contactDetails";
            this.txt_contactDetails.Size = new System.Drawing.Size(201, 20);
            this.txt_contactDetails.TabIndex = 19;
            // 
            // lbl_middlename
            // 
            this.lbl_middlename.AutoSize = true;
            this.lbl_middlename.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_middlename.Location = new System.Drawing.Point(562, 49);
            this.lbl_middlename.Name = "lbl_middlename";
            this.lbl_middlename.Size = new System.Drawing.Size(90, 17);
            this.lbl_middlename.TabIndex = 18;
            this.lbl_middlename.Text = "Middle Name";
            // 
            // txt_middleName
            // 
            this.txt_middleName.Location = new System.Drawing.Point(565, 68);
            this.txt_middleName.Name = "txt_middleName";
            this.txt_middleName.Size = new System.Drawing.Size(208, 20);
            this.txt_middleName.TabIndex = 17;
            // 
            // lbl_firstname
            // 
            this.lbl_firstname.AutoSize = true;
            this.lbl_firstname.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_firstname.Location = new System.Drawing.Point(345, 50);
            this.lbl_firstname.Name = "lbl_firstname";
            this.lbl_firstname.Size = new System.Drawing.Size(76, 17);
            this.lbl_firstname.TabIndex = 16;
            this.lbl_firstname.Text = "First Name";
            // 
            // txt_firstName
            // 
            this.txt_firstName.Location = new System.Drawing.Point(348, 69);
            this.txt_firstName.Name = "txt_firstName";
            this.txt_firstName.Size = new System.Drawing.Size(201, 20);
            this.txt_firstName.TabIndex = 15;
            // 
            // lbl_lastname
            // 
            this.lbl_lastname.AutoSize = true;
            this.lbl_lastname.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lastname.Location = new System.Drawing.Point(129, 49);
            this.lbl_lastname.Name = "lbl_lastname";
            this.lbl_lastname.Size = new System.Drawing.Size(76, 17);
            this.lbl_lastname.TabIndex = 14;
            this.lbl_lastname.Text = "Last Name";
            // 
            // cmb_title
            // 
            this.cmb_title.FormattingEnabled = true;
            this.cmb_title.Items.AddRange(new object[] {
            "Mr.",
            "Mrs.",
            "Dr."});
            this.cmb_title.Location = new System.Drawing.Point(14, 68);
            this.cmb_title.Name = "cmb_title";
            this.cmb_title.Size = new System.Drawing.Size(103, 21);
            this.cmb_title.TabIndex = 13;
            // 
            // txt_lastName
            // 
            this.txt_lastName.Location = new System.Drawing.Point(132, 68);
            this.txt_lastName.Name = "txt_lastName";
            this.txt_lastName.Size = new System.Drawing.Size(201, 20);
            this.txt_lastName.TabIndex = 12;
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title.Location = new System.Drawing.Point(11, 49);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(35, 17);
            this.lbl_title.TabIndex = 11;
            this.lbl_title.Text = "Title";
            // 
            // info_panel
            // 
            this.info_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.info_panel.Controls.Add(this.lbl_guestInformation);
            this.info_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.info_panel.Location = new System.Drawing.Point(0, 0);
            this.info_panel.Name = "info_panel";
            this.info_panel.Size = new System.Drawing.Size(868, 31);
            this.info_panel.TabIndex = 10;
            // 
            // lbl_guestInformation
            // 
            this.lbl_guestInformation.AutoSize = true;
            this.lbl_guestInformation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_guestInformation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_guestInformation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_guestInformation.Location = new System.Drawing.Point(11, 8);
            this.lbl_guestInformation.Name = "lbl_guestInformation";
            this.lbl_guestInformation.Size = new System.Drawing.Size(134, 15);
            this.lbl_guestInformation.TabIndex = 0;
            this.lbl_guestInformation.Text = "GUEST INFORMATION";
            // 
            // pnl_newWalkinGuest
            // 
            this.pnl_newWalkinGuest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.pnl_newWalkinGuest.Controls.Add(this.txt_timer);
            this.pnl_newWalkinGuest.Controls.Add(this.label2);
            this.pnl_newWalkinGuest.Controls.Add(this.pnl_newReservation_Selector);
            this.pnl_newWalkinGuest.Controls.Add(this.btn_folioSummary);
            this.pnl_newWalkinGuest.Controls.Add(this.btn_businessSource);
            this.pnl_newWalkinGuest.Controls.Add(this.btn_accomodationDetails);
            this.pnl_newWalkinGuest.Controls.Add(this.btn_guestInformation);
            this.pnl_newWalkinGuest.Controls.Add(this.lbl_newWalkinGuest);
            this.pnl_newWalkinGuest.Controls.Add(this.btn_settlementOption);
            this.pnl_newWalkinGuest.Controls.Add(this.btn_billingSummary);
            this.pnl_newWalkinGuest.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnl_newWalkinGuest.Location = new System.Drawing.Point(0, 0);
            this.pnl_newWalkinGuest.Name = "pnl_newWalkinGuest";
            this.pnl_newWalkinGuest.Size = new System.Drawing.Size(200, 479);
            this.pnl_newWalkinGuest.TabIndex = 13;
            // 
            // txt_timer
            // 
            this.txt_timer.Location = new System.Drawing.Point(84, 427);
            this.txt_timer.Name = "txt_timer";
            this.txt_timer.ReadOnly = true;
            this.txt_timer.Size = new System.Drawing.Size(89, 20);
            this.txt_timer.TabIndex = 12;
            this.txt_timer.TextChanged += new System.EventHandler(this.txt_timer_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ Extended", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(3, 428);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 19);
            this.label2.TabIndex = 11;
            this.label2.Text = "Timer:";
            // 
            // pnl_newReservation_Selector
            // 
            this.pnl_newReservation_Selector.BackColor = System.Drawing.Color.Yellow;
            this.pnl_newReservation_Selector.Location = new System.Drawing.Point(-6, 44);
            this.pnl_newReservation_Selector.Name = "pnl_newReservation_Selector";
            this.pnl_newReservation_Selector.Size = new System.Drawing.Size(10, 38);
            this.pnl_newReservation_Selector.TabIndex = 0;
            // 
            // btn_folioSummary
            // 
            this.btn_folioSummary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_folioSummary.Enabled = false;
            this.btn_folioSummary.FlatAppearance.BorderSize = 0;
            this.btn_folioSummary.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_folioSummary.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_folioSummary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_folioSummary.Location = new System.Drawing.Point(0, 234);
            this.btn_folioSummary.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn_folioSummary.Name = "btn_folioSummary";
            this.btn_folioSummary.Size = new System.Drawing.Size(200, 38);
            this.btn_folioSummary.TabIndex = 10;
            this.btn_folioSummary.Text = "Folio Summary";
            this.btn_folioSummary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_folioSummary.UseVisualStyleBackColor = false;
            this.btn_folioSummary.Click += new System.EventHandler(this.btn_folioSummary_Click);
            // 
            // btn_businessSource
            // 
            this.btn_businessSource.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_businessSource.FlatAppearance.BorderSize = 0;
            this.btn_businessSource.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_businessSource.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_businessSource.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_businessSource.Location = new System.Drawing.Point(0, 120);
            this.btn_businessSource.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn_businessSource.Name = "btn_businessSource";
            this.btn_businessSource.Size = new System.Drawing.Size(200, 38);
            this.btn_businessSource.TabIndex = 5;
            this.btn_businessSource.Text = "Business Source";
            this.btn_businessSource.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_businessSource.UseVisualStyleBackColor = false;
            this.btn_businessSource.Click += new System.EventHandler(this.btn_businessSource_Click);
            // 
            // btn_accomodationDetails
            // 
            this.btn_accomodationDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_accomodationDetails.FlatAppearance.BorderSize = 0;
            this.btn_accomodationDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_accomodationDetails.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_accomodationDetails.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_accomodationDetails.Location = new System.Drawing.Point(0, 82);
            this.btn_accomodationDetails.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn_accomodationDetails.Name = "btn_accomodationDetails";
            this.btn_accomodationDetails.Size = new System.Drawing.Size(200, 38);
            this.btn_accomodationDetails.TabIndex = 3;
            this.btn_accomodationDetails.Text = "Accomodation Details";
            this.btn_accomodationDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_accomodationDetails.UseVisualStyleBackColor = false;
            this.btn_accomodationDetails.Click += new System.EventHandler(this.btn_accomodationDetails_Click);
            // 
            // btn_guestInformation
            // 
            this.btn_guestInformation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_guestInformation.FlatAppearance.BorderSize = 0;
            this.btn_guestInformation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_guestInformation.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_guestInformation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_guestInformation.Location = new System.Drawing.Point(0, 44);
            this.btn_guestInformation.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn_guestInformation.Name = "btn_guestInformation";
            this.btn_guestInformation.Size = new System.Drawing.Size(200, 38);
            this.btn_guestInformation.TabIndex = 2;
            this.btn_guestInformation.Text = "Guest Information";
            this.btn_guestInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_guestInformation.UseVisualStyleBackColor = false;
            this.btn_guestInformation.Click += new System.EventHandler(this.btn_guestInformation_Click);
            // 
            // lbl_newWalkinGuest
            // 
            this.lbl_newWalkinGuest.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_newWalkinGuest.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.lbl_newWalkinGuest.Location = new System.Drawing.Point(0, -1);
            this.lbl_newWalkinGuest.Name = "lbl_newWalkinGuest";
            this.lbl_newWalkinGuest.Size = new System.Drawing.Size(200, 36);
            this.lbl_newWalkinGuest.TabIndex = 0;
            this.lbl_newWalkinGuest.Text = "New Walk-in Guest";
            this.lbl_newWalkinGuest.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_settlementOption
            // 
            this.btn_settlementOption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_settlementOption.Enabled = false;
            this.btn_settlementOption.FlatAppearance.BorderSize = 0;
            this.btn_settlementOption.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_settlementOption.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_settlementOption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_settlementOption.Location = new System.Drawing.Point(0, 196);
            this.btn_settlementOption.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn_settlementOption.Name = "btn_settlementOption";
            this.btn_settlementOption.Size = new System.Drawing.Size(200, 38);
            this.btn_settlementOption.TabIndex = 6;
            this.btn_settlementOption.Text = "Settlement Option";
            this.btn_settlementOption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_settlementOption.UseVisualStyleBackColor = false;
            this.btn_settlementOption.Click += new System.EventHandler(this.btn_settlementOption_Click);
            // 
            // btn_billingSummary
            // 
            this.btn_billingSummary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_billingSummary.Enabled = false;
            this.btn_billingSummary.FlatAppearance.BorderSize = 0;
            this.btn_billingSummary.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_billingSummary.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_billingSummary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_billingSummary.Location = new System.Drawing.Point(0, 158);
            this.btn_billingSummary.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn_billingSummary.Name = "btn_billingSummary";
            this.btn_billingSummary.Size = new System.Drawing.Size(200, 38);
            this.btn_billingSummary.TabIndex = 7;
            this.btn_billingSummary.Text = "Billing Summary";
            this.btn_billingSummary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_billingSummary.UseVisualStyleBackColor = false;
            this.btn_billingSummary.Click += new System.EventHandler(this.btn_billingSummary_Click);
            // 
            // btn_checkIn
            // 
            this.btn_checkIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_checkIn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_checkIn.FlatAppearance.BorderSize = 0;
            this.btn_checkIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_checkIn.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_checkIn.ForeColor = System.Drawing.Color.White;
            this.btn_checkIn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_checkIn.Location = new System.Drawing.Point(652, 683);
            this.btn_checkIn.Name = "btn_checkIn";
            this.btn_checkIn.Size = new System.Drawing.Size(128, 35);
            this.btn_checkIn.TabIndex = 14;
            this.btn_checkIn.Text = "CHECK IN";
            this.btn_checkIn.UseVisualStyleBackColor = false;
            this.btn_checkIn.Click += new System.EventHandler(this.btn_checkIn_Click);
            // 
            // btn_reserve
            // 
            this.btn_reserve.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_reserve.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_reserve.FlatAppearance.BorderSize = 0;
            this.btn_reserve.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reserve.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reserve.ForeColor = System.Drawing.Color.White;
            this.btn_reserve.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reserve.Location = new System.Drawing.Point(467, 683);
            this.btn_reserve.Name = "btn_reserve";
            this.btn_reserve.Size = new System.Drawing.Size(167, 35);
            this.btn_reserve.TabIndex = 15;
            this.btn_reserve.Text = "TO RESERVATION";
            this.btn_reserve.UseVisualStyleBackColor = false;
            this.btn_reserve.Click += new System.EventHandler(this.btn_reserve_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_cancel.FlatAppearance.BorderSize = 0;
            this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cancel.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cancel.ForeColor = System.Drawing.Color.White;
            this.btn_cancel.Image = global::maxxis.Properties.Resources.Go_Back;
            this.btn_cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancel.Location = new System.Drawing.Point(798, 683);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(128, 35);
            this.btn_cancel.TabIndex = 13;
            this.btn_cancel.Text = "GO BACK";
            this.btn_cancel.UseVisualStyleBackColor = false;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // WalkinReservation_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(1250, 730);
            this.Controls.Add(this.btn_reserve);
            this.Controls.Add(this.btn_checkIn);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.lbl_timer);
            this.Controls.Add(this.pnl_hotelInfo);
            this.Controls.Add(this.newWalkinReservation_Panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "WalkinReservation_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.WalkinReservation_form_Load);
            this.newWalkinReservation_Panel.ResumeLayout(false);
            this.newWalkinReservation_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_newWalkinReservation)).EndInit();
            this.pnl_hotelInfo.ResumeLayout(false);
            this.pnl_hotelInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_maxxLogo)).EndInit();
            this.lbl_timer.ResumeLayout(false);
            this.pnl_guestInformation.ResumeLayout(false);
            this.pnl_guestInformation.PerformLayout();
            this.info_panel.ResumeLayout(false);
            this.info_panel.PerformLayout();
            this.pnl_newWalkinGuest.ResumeLayout(false);
            this.pnl_newWalkinGuest.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblguestinfoBindingSource11)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel newWalkinReservation_Panel;
        private System.Windows.Forms.Label lbl_newWalkinReservation;
        private System.Windows.Forms.PictureBox pcb_newWalkinReservation;
        private System.Windows.Forms.Panel pnl_hotelInfo;
        private System.Windows.Forms.Label lbl_hotelAddress;
        private System.Windows.Forms.Label lbl_hotelStreet;
        private System.Windows.Forms.Label lbl_maxxHotelMakati;
        private System.Windows.Forms.PictureBox pcb_maxxLogo;
        private System.Windows.Forms.Panel lbl_timer;
        private System.Windows.Forms.Panel pnl_newWalkinGuest;
        private System.Windows.Forms.Panel pnl_newReservation_Selector;
        private System.Windows.Forms.Button btn_folioSummary;
        private System.Windows.Forms.Button btn_settlementOption;
        private System.Windows.Forms.Button btn_businessSource;
        private System.Windows.Forms.Button btn_accomodationDetails;
        private System.Windows.Forms.Button btn_guestInformation;
        private System.Windows.Forms.Label lbl_newWalkinGuest;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_checkIn;
        private System.Windows.Forms.Button btn_reserve;
        private System.Windows.Forms.BindingSource tblguestinfoBindingSource;
        private System.Windows.Forms.BindingSource tblguestinfoBindingSource1;
        private System.Windows.Forms.BindingSource tblguestinfoBindingSource2;
        private System.Windows.Forms.BindingSource tblguestinfoBindingSource3;
        private System.Windows.Forms.BindingSource tblguestinfoBindingSource4;
        private System.Windows.Forms.BindingSource tblguestinfoBindingSource5;
        private System.Windows.Forms.BindingSource tblguestinfoBindingSource6;
        private System.Windows.Forms.BindingSource tblguestinfoBindingSource8;
        private System.Windows.Forms.BindingSource tblguestinfoBindingSource7;
        private System.Windows.Forms.BindingSource tblguestinfoBindingSource9;
        private System.Windows.Forms.BindingSource tblguestinfoBindingSource10;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn middlenameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateofbirthDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contactdetailsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn emailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn provinceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn homecountryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn companyoragencyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblguestfolioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblsettlementDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblstayinfoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblvisainfoDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource tblguestinfoBindingSource11;
        private System.Windows.Forms.Panel pnl_guestInformation;
        private System.Windows.Forms.Label lbl_dateOfBirth;
        private System.Windows.Forms.Label lbl_province;
        private System.Windows.Forms.TextBox txt_province;
        private System.Windows.Forms.ComboBox cmb_homeCountry;
        private System.Windows.Forms.Label lbl_homeCountry;
        private System.Windows.Forms.DateTimePicker dtb_dateOfBirth;
        private System.Windows.Forms.Label lbl_visaNumber;
        private System.Windows.Forms.TextBox txt_visaNumber;
        private System.Windows.Forms.Label lbl_city;
        private System.Windows.Forms.TextBox txt_city;
        private System.Windows.Forms.Label lbl_passportNumber;
        private System.Windows.Forms.TextBox txt_passportNumber;
        private System.Windows.Forms.Label lbl_companyAgency;
        private System.Windows.Forms.TextBox txt_companyAgency;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.TextBox txt_address;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.Label lbl_contactDetails;
        private System.Windows.Forms.TextBox txt_contactDetails;
        private System.Windows.Forms.Label lbl_middlename;
        private System.Windows.Forms.TextBox txt_middleName;
        private System.Windows.Forms.Label lbl_firstname;
        private System.Windows.Forms.TextBox txt_firstName;
        private System.Windows.Forms.Label lbl_lastname;
        private System.Windows.Forms.ComboBox cmb_title;
        private System.Windows.Forms.TextBox txt_lastName;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.Panel info_panel;
        private System.Windows.Forms.Label lbl_guestInformation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_timer;
        public System.Windows.Forms.Button btn_billingSummary;
    }
}