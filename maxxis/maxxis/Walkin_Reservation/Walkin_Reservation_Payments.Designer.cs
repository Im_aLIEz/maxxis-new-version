﻿namespace maxxis.Walkin_Reservation
{
    partial class Walkin_Reservation_Payments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnl_payments = new System.Windows.Forms.Panel();
            this.dgv_payments = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDiscount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.payments_panel = new System.Windows.Forms.Panel();
            this.txt_payments_paymentTotal = new System.Windows.Forms.TextBox();
            this.lbl_payments_paymentTotal = new System.Windows.Forms.Label();
            this.lbl_payments = new System.Windows.Forms.Label();
            this.pnl_payments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_payments)).BeginInit();
            this.payments_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_payments
            // 
            this.pnl_payments.AutoScroll = true;
            this.pnl_payments.BackColor = System.Drawing.Color.Silver;
            this.pnl_payments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_payments.Controls.Add(this.dgv_payments);
            this.pnl_payments.Controls.Add(this.payments_panel);
            this.pnl_payments.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_payments.Location = new System.Drawing.Point(0, 0);
            this.pnl_payments.Name = "pnl_payments";
            this.pnl_payments.Size = new System.Drawing.Size(870, 267);
            this.pnl_payments.TabIndex = 22;
            // 
            // dgv_payments
            // 
            this.dgv_payments.AllowUserToAddRows = false;
            this.dgv_payments.AllowUserToDeleteRows = false;
            this.dgv_payments.BackgroundColor = System.Drawing.Color.Silver;
            this.dgv_payments.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_payments.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv_payments.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_payments.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_payments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_payments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.colAmount,
            this.colDiscount});
            this.dgv_payments.EnableHeadersVisualStyles = false;
            this.dgv_payments.Location = new System.Drawing.Point(3, 37);
            this.dgv_payments.Name = "dgv_payments";
            this.dgv_payments.ReadOnly = true;
            this.dgv_payments.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_payments.RowHeadersVisible = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_payments.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_payments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_payments.Size = new System.Drawing.Size(838, 203);
            this.dgv_payments.TabIndex = 11;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 200;
            // 
            // colAmount
            // 
            this.colAmount.HeaderText = "Amount";
            this.colAmount.Name = "colAmount";
            this.colAmount.ReadOnly = true;
            this.colAmount.Width = 350;
            // 
            // colDiscount
            // 
            this.colDiscount.HeaderText = "Discount";
            this.colDiscount.Name = "colDiscount";
            this.colDiscount.ReadOnly = true;
            this.colDiscount.Width = 200;
            // 
            // payments_panel
            // 
            this.payments_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.payments_panel.Controls.Add(this.txt_payments_paymentTotal);
            this.payments_panel.Controls.Add(this.lbl_payments_paymentTotal);
            this.payments_panel.Controls.Add(this.lbl_payments);
            this.payments_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.payments_panel.Location = new System.Drawing.Point(0, 0);
            this.payments_panel.Name = "payments_panel";
            this.payments_panel.Size = new System.Drawing.Size(868, 31);
            this.payments_panel.TabIndex = 10;
            // 
            // txt_payments_paymentTotal
            // 
            this.txt_payments_paymentTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_payments_paymentTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_payments_paymentTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_payments_paymentTotal.ForeColor = System.Drawing.Color.White;
            this.txt_payments_paymentTotal.Location = new System.Drawing.Point(754, 6);
            this.txt_payments_paymentTotal.Name = "txt_payments_paymentTotal";
            this.txt_payments_paymentTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_payments_paymentTotal.Size = new System.Drawing.Size(103, 17);
            this.txt_payments_paymentTotal.TabIndex = 74;
            this.txt_payments_paymentTotal.Text = "0.00";
            // 
            // lbl_payments_paymentTotal
            // 
            this.lbl_payments_paymentTotal.AutoSize = true;
            this.lbl_payments_paymentTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_payments_paymentTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_payments_paymentTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_payments_paymentTotal.Location = new System.Drawing.Point(607, 8);
            this.lbl_payments_paymentTotal.Name = "lbl_payments_paymentTotal";
            this.lbl_payments_paymentTotal.Size = new System.Drawing.Size(141, 14);
            this.lbl_payments_paymentTotal.TabIndex = 73;
            this.lbl_payments_paymentTotal.Text = "PAYMENT TOTAL:    Php";
            // 
            // lbl_payments
            // 
            this.lbl_payments.AutoSize = true;
            this.lbl_payments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_payments.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_payments.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_payments.Location = new System.Drawing.Point(11, 8);
            this.lbl_payments.Name = "lbl_payments";
            this.lbl_payments.Size = new System.Drawing.Size(72, 14);
            this.lbl_payments.TabIndex = 0;
            this.lbl_payments.Text = "PAYMENTS";
            // 
            // Walkin_Reservation_Payments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(870, 479);
            this.Controls.Add(this.pnl_payments);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Walkin_Reservation_Payments";
            this.Text = "Walkin_Reservation_Payments";
            this.Load += new System.EventHandler(this.Walkin_Reservation_Payments_Load_1);
            this.pnl_payments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_payments)).EndInit();
            this.payments_panel.ResumeLayout(false);
            this.payments_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_payments;
        private System.Windows.Forms.DataGridView dgv_payments;
        private System.Windows.Forms.Panel payments_panel;
        private System.Windows.Forms.TextBox txt_payments_paymentTotal;
        private System.Windows.Forms.Label lbl_payments_paymentTotal;
        private System.Windows.Forms.Label lbl_payments;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDiscount;
    }
}