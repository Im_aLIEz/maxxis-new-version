﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace maxxis.Walkin_Reservation
{
    public partial class Walkin_Reservation_SettlementOption : Form
    {
        Walkin_Reservation.Walkin_Reservation_BillingSummary WR_BillingSummary = new Walkin_Reservation.Walkin_Reservation_BillingSummary();

        private static string card_type;
        private static string card_number;
        private static string card_ccv;
        private static string card_expire;
        private static string cash_given;
        private static string requiredPayment;
        private static string mode;
        private static string discount;
        private Boolean cash;
        private Boolean credit;
        private Boolean partial_credit;

        private static string cashpaid;
        public string CashPaid
        {
            get { return cashpaid; }
            set { cashpaid = value; }
        }
        public string Mode
        {
            get { return mode; }
            set { mode = value; }
        }
        public string Discount
        {
            get { return discount; }
            set { discount = value; }
        }

        public Walkin_Reservation_SettlementOption()
        {
            InitializeComponent();
        }
        

        internal void Walkin_Reservation_SettlementOption_Load(object sender, EventArgs e)
        {
            Discount = decimal.Parse(txt_discountAmount.Text).ToString("N2");
            WR_BillingSummary.TotalPayment = (Decimal.Parse(WR_BillingSummary.TotalPayment) - Decimal.Parse(Discount)).ToString();
            rbtn_cash.Checked = true;
            cashSelected();
            Initialization();
        }

        private void rbtn_cash_CheckedChanged(object sender, EventArgs e)
        {
            Initialization();
            cashSelected();
            TextTyped();
        }
        
        private void rbtn_credit_CheckedChanged(object sender, EventArgs e)
        {
            creditSelected();
            TextTyped();
        }
        private void rbtn_partialCredit_CheckedChanged(object sender, EventArgs e)
        {
            partialCreditSelected();
            TextTyped();
        }
        private void cmb_type_Leave(object sender, EventArgs e)
        {
            TextTyped();
        }

        private void txt_cardNumber_Leave(object sender, EventArgs e)
        {
            TextTyped();
        }
        private void txt_cvv_Leave(object sender, EventArgs e)
        {
            TextTyped();
        }
        private void txt_expire_Leave(object sender, EventArgs e)
        {
            TextTyped();
        }
        private void txt_cashPayment_Leave(object sender, EventArgs e)
        {
            TextTyped();
        }
        //methods
        public void cashSelected()
        {
            cmb_type.Enabled = false;
            txt_cardNumber.Enabled = false;
            txt_cvv.Enabled = false;
            txt_expire.Enabled = false;
            txt_cashPayment.Enabled = true;

            cmb_type.Text = "";
            txt_cardNumber.Clear();
            txt_cvv.Clear();
            txt_expire.Clear();
            txt_cashPayment.Clear();

            Cash = true;
            Credit = false;
            PartialCredit = false;
        }
        public void creditSelected()
        {
            cmb_type.Enabled = true;
            txt_cardNumber.Enabled = true;
            txt_cvv.Enabled = true;
            txt_expire.Enabled = true;
            txt_cashPayment.Enabled = false;

            cmb_type.Text = "";
            txt_cardNumber.Clear();
            txt_cvv.Clear();
            txt_expire.Clear();
            txt_cashPayment.Clear();

            Cash = false;
            Credit = true;
            PartialCredit = false;
        }
        public void partialCreditSelected()
        {
            cmb_type.Enabled = true;
            txt_cardNumber.Enabled = true;
            txt_cvv.Enabled = true;
            txt_expire.Enabled = true;
            txt_cashPayment.Enabled = true;

            cmb_type.Text = "";
            txt_cardNumber.Clear();
            txt_cvv.Clear();
            txt_expire.Clear();

            Cash = false;
            Credit = false;
            PartialCredit = true;
        }
        public void TextTyped()
        {
            Card_Type = cmb_type.Text;
            Card_Number = txt_cardNumber.Text;
            Card_CVV = txt_cvv.Text;
            Card_Expire = txt_expire.Text;
            Cash_Given = txt_cashPayment.Text;
            RequiredPrePayment = txt_requiredPrePayment.Text;

            CashPaid = txt_cashPayment.Text;
            if(rbtn_cash.Checked=true)
            {
                Mode = "Cash";
            }
            else if(rbtn_credit.Checked=true)
            {
                Mode = "Credit";
            }
            else if(rbtn_partialCredit.Checked=true)
            {
                Mode = "Partial Credit";
            }
        }
        public void Initialization()
        {
            txt_requiredPrePayment.Text = WR_BillingSummary.TotalPayment;
            txt_cashPayment.Text = WR_BillingSummary.TotalPayment;
            
        }
        public string Card_Type
        {
            get { return card_type; }
            set { card_type = value; }
        }

        public string Card_Number
        {
            get { return card_number; }
            set { card_number = value; }
        }

        public string Card_CVV
        {
            get { return card_type; }
            set { card_type = value; }
        }

        public string Card_Expire
        {
            get { return card_expire; }
            set { card_expire = value; }
        }

        public string Cash_Given
        {
            get { return cash_given; }
            set { cash_given = value; }
        }

        public string RequiredPrePayment
        {
            get { return requiredPayment; }
            set { requiredPayment = value; }
        }

        public Boolean Cash
        {
            get { return cash; }
            set { cash = value;  }
        }

        public Boolean Credit
        {
            get { return credit; }
            set { credit = value;  }
        }

        public Boolean PartialCredit
        {
            get { return partial_credit;  }
            set { partial_credit = value;  }
        }

        private void txt_cashPayment_TextChanged(object sender, EventArgs e)
        {
            TextTyped();
        }

        private void txt_cashhanded_TextChanged(object sender, EventArgs e)
        {
            if (txt_cashhanded.Text != "")
            {
                txt_change.Text = (decimal.Parse(txt_cashhanded.Text) - decimal.Parse(txt_cashPayment.Text)).ToString("N2");
            }
        }

        private void txt_cashhanded_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)8;

            if(e.KeyChar==(char)13)
            {
                txt_cashhanded.Text = string.Format("{0:n0}",double.Parse(txt_cashhanded.Text));
            }

            
        }

        private void btn_apply_Click(object sender, EventArgs e)
        {

            Discount = decimal.Parse(txt_discountAmount.Text).ToString("N2");
            WR_BillingSummary.TotalPayment = (Decimal.Parse(WR_BillingSummary.TotalPayment) - Decimal.Parse(Discount)).ToString();
            rbtn_cash.Checked = true;
            cashSelected();
            Initialization();
        }

        private void txt_discountAmount_TextChanged(object sender, EventArgs e)
        {
            if(txt_discountAmount.Text=="")
            {
                btn_apply.Enabled = false;
            }
            else
            {
                btn_apply.Enabled = true;
            }
        }
    }
}
