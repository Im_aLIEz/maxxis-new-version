﻿namespace maxxis.Walkin_Reservation
{
    partial class Walkin_Reservation_HotelTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl_hotelTransfer = new System.Windows.Forms.Panel();
            this.cmb_hotelTransferVia = new System.Windows.Forms.ComboBox();
            this.lbl_hotelTransferVia = new System.Windows.Forms.Label();
            this.hotelTransfer_panel = new System.Windows.Forms.Panel();
            this.txt_hotelTransfer_transferFeeTotal = new System.Windows.Forms.TextBox();
            this.lbl_hotelTransfer_transferFeeTotal = new System.Windows.Forms.Label();
            this.lbl_hotelTransfer = new System.Windows.Forms.Label();
            this.pnl_hotelTransfer.SuspendLayout();
            this.hotelTransfer_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_hotelTransfer
            // 
            this.pnl_hotelTransfer.AutoScroll = true;
            this.pnl_hotelTransfer.BackColor = System.Drawing.Color.Silver;
            this.pnl_hotelTransfer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_hotelTransfer.Controls.Add(this.cmb_hotelTransferVia);
            this.pnl_hotelTransfer.Controls.Add(this.lbl_hotelTransferVia);
            this.pnl_hotelTransfer.Controls.Add(this.hotelTransfer_panel);
            this.pnl_hotelTransfer.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_hotelTransfer.Location = new System.Drawing.Point(0, 0);
            this.pnl_hotelTransfer.Name = "pnl_hotelTransfer";
            this.pnl_hotelTransfer.Size = new System.Drawing.Size(870, 114);
            this.pnl_hotelTransfer.TabIndex = 16;
            // 
            // cmb_hotelTransferVia
            // 
            this.cmb_hotelTransferVia.FormattingEnabled = true;
            this.cmb_hotelTransferVia.Location = new System.Drawing.Point(129, 58);
            this.cmb_hotelTransferVia.Name = "cmb_hotelTransferVia";
            this.cmb_hotelTransferVia.Size = new System.Drawing.Size(690, 21);
            this.cmb_hotelTransferVia.TabIndex = 15;
            // 
            // lbl_hotelTransferVia
            // 
            this.lbl_hotelTransferVia.AutoSize = true;
            this.lbl_hotelTransferVia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_hotelTransferVia.Location = new System.Drawing.Point(11, 60);
            this.lbl_hotelTransferVia.Name = "lbl_hotelTransferVia";
            this.lbl_hotelTransferVia.Size = new System.Drawing.Size(120, 17);
            this.lbl_hotelTransferVia.TabIndex = 14;
            this.lbl_hotelTransferVia.Text = "Hotel transfer via:";
            // 
            // hotelTransfer_panel
            // 
            this.hotelTransfer_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.hotelTransfer_panel.Controls.Add(this.txt_hotelTransfer_transferFeeTotal);
            this.hotelTransfer_panel.Controls.Add(this.lbl_hotelTransfer_transferFeeTotal);
            this.hotelTransfer_panel.Controls.Add(this.lbl_hotelTransfer);
            this.hotelTransfer_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.hotelTransfer_panel.Location = new System.Drawing.Point(0, 0);
            this.hotelTransfer_panel.Name = "hotelTransfer_panel";
            this.hotelTransfer_panel.Size = new System.Drawing.Size(868, 31);
            this.hotelTransfer_panel.TabIndex = 10;
            // 
            // txt_hotelTransfer_transferFeeTotal
            // 
            this.txt_hotelTransfer_transferFeeTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_hotelTransfer_transferFeeTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_hotelTransfer_transferFeeTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_hotelTransfer_transferFeeTotal.ForeColor = System.Drawing.Color.White;
            this.txt_hotelTransfer_transferFeeTotal.Location = new System.Drawing.Point(720, 7);
            this.txt_hotelTransfer_transferFeeTotal.Name = "txt_hotelTransfer_transferFeeTotal";
            this.txt_hotelTransfer_transferFeeTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_hotelTransfer_transferFeeTotal.Size = new System.Drawing.Size(137, 17);
            this.txt_hotelTransfer_transferFeeTotal.TabIndex = 74;
            this.txt_hotelTransfer_transferFeeTotal.Text = "Php 0.00";
            // 
            // lbl_hotelTransfer_transferFeeTotal
            // 
            this.lbl_hotelTransfer_transferFeeTotal.AutoSize = true;
            this.lbl_hotelTransfer_transferFeeTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_hotelTransfer_transferFeeTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_hotelTransfer_transferFeeTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_hotelTransfer_transferFeeTotal.Location = new System.Drawing.Point(570, 8);
            this.lbl_hotelTransfer_transferFeeTotal.Name = "lbl_hotelTransfer_transferFeeTotal";
            this.lbl_hotelTransfer_transferFeeTotal.Size = new System.Drawing.Size(144, 15);
            this.lbl_hotelTransfer_transferFeeTotal.TabIndex = 73;
            this.lbl_hotelTransfer_transferFeeTotal.Text = "TRANSFER FEE TOTAL:";
            // 
            // lbl_hotelTransfer
            // 
            this.lbl_hotelTransfer.AutoSize = true;
            this.lbl_hotelTransfer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_hotelTransfer.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_hotelTransfer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_hotelTransfer.Location = new System.Drawing.Point(11, 9);
            this.lbl_hotelTransfer.Name = "lbl_hotelTransfer";
            this.lbl_hotelTransfer.Size = new System.Drawing.Size(116, 15);
            this.lbl_hotelTransfer.TabIndex = 0;
            this.lbl_hotelTransfer.Text = "HOTEL TRANSFER";
            // 
            // Walkin_Reservation_HotelTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(870, 479);
            this.Controls.Add(this.pnl_hotelTransfer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Walkin_Reservation_HotelTransfer";
            this.Text = "Walkin_Reservation_HotelTransfer";
            this.pnl_hotelTransfer.ResumeLayout(false);
            this.pnl_hotelTransfer.PerformLayout();
            this.hotelTransfer_panel.ResumeLayout(false);
            this.hotelTransfer_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_hotelTransfer;
        private System.Windows.Forms.ComboBox cmb_hotelTransferVia;
        private System.Windows.Forms.Label lbl_hotelTransferVia;
        private System.Windows.Forms.Panel hotelTransfer_panel;
        private System.Windows.Forms.TextBox txt_hotelTransfer_transferFeeTotal;
        private System.Windows.Forms.Label lbl_hotelTransfer_transferFeeTotal;
        private System.Windows.Forms.Label lbl_hotelTransfer;
    }
}