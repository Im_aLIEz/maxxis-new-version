﻿namespace maxxis.Walkin_Reservation
{
    partial class Walkin_Reservation_GuestInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl_guestInformation = new System.Windows.Forms.Panel();
            this.btn_scanImage = new System.Windows.Forms.Button();
            this.btn_attachImage = new System.Windows.Forms.Button();
            this.pcb_image = new System.Windows.Forms.PictureBox();
            this.lbl_dateOfBirth = new System.Windows.Forms.Label();
            this.lbl_province = new System.Windows.Forms.Label();
            this.txt_province = new System.Windows.Forms.TextBox();
            this.cmb_homeCountry = new System.Windows.Forms.ComboBox();
            this.lbl_homeCountry = new System.Windows.Forms.Label();
            this.dtb_dateOfBirth = new System.Windows.Forms.DateTimePicker();
            this.lbl_visaNumber = new System.Windows.Forms.Label();
            this.txt_visaNumber = new System.Windows.Forms.TextBox();
            this.lbl_city = new System.Windows.Forms.Label();
            this.txt_city = new System.Windows.Forms.TextBox();
            this.lbl_passportNumber = new System.Windows.Forms.Label();
            this.txt_passportNumber = new System.Windows.Forms.TextBox();
            this.lbl_companyAgency = new System.Windows.Forms.Label();
            this.txt_companyAgency = new System.Windows.Forms.TextBox();
            this.lbl_address = new System.Windows.Forms.Label();
            this.txt_address = new System.Windows.Forms.TextBox();
            this.lbl_email = new System.Windows.Forms.Label();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.lbl_contactDetails = new System.Windows.Forms.Label();
            this.txt_contactDetails = new System.Windows.Forms.TextBox();
            this.lbl_middlename = new System.Windows.Forms.Label();
            this.txt_middleName = new System.Windows.Forms.TextBox();
            this.lbl_firstname = new System.Windows.Forms.Label();
            this.txt_firstName = new System.Windows.Forms.TextBox();
            this.lbl_lastname = new System.Windows.Forms.Label();
            this.cmb_title = new System.Windows.Forms.ComboBox();
            this.txt_lastName = new System.Windows.Forms.TextBox();
            this.lbl_title = new System.Windows.Forms.Label();
            this.info_panel = new System.Windows.Forms.Panel();
            this.lbl_guestInformation = new System.Windows.Forms.Label();
            this.pnl_guestInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_image)).BeginInit();
            this.info_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_guestInformation
            // 
            this.pnl_guestInformation.AutoScroll = true;
            this.pnl_guestInformation.BackColor = System.Drawing.Color.Silver;
            this.pnl_guestInformation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_guestInformation.Controls.Add(this.btn_scanImage);
            this.pnl_guestInformation.Controls.Add(this.btn_attachImage);
            this.pnl_guestInformation.Controls.Add(this.pcb_image);
            this.pnl_guestInformation.Controls.Add(this.lbl_dateOfBirth);
            this.pnl_guestInformation.Controls.Add(this.lbl_province);
            this.pnl_guestInformation.Controls.Add(this.txt_province);
            this.pnl_guestInformation.Controls.Add(this.cmb_homeCountry);
            this.pnl_guestInformation.Controls.Add(this.lbl_homeCountry);
            this.pnl_guestInformation.Controls.Add(this.dtb_dateOfBirth);
            this.pnl_guestInformation.Controls.Add(this.lbl_visaNumber);
            this.pnl_guestInformation.Controls.Add(this.txt_visaNumber);
            this.pnl_guestInformation.Controls.Add(this.lbl_city);
            this.pnl_guestInformation.Controls.Add(this.txt_city);
            this.pnl_guestInformation.Controls.Add(this.lbl_passportNumber);
            this.pnl_guestInformation.Controls.Add(this.txt_passportNumber);
            this.pnl_guestInformation.Controls.Add(this.lbl_companyAgency);
            this.pnl_guestInformation.Controls.Add(this.txt_companyAgency);
            this.pnl_guestInformation.Controls.Add(this.lbl_address);
            this.pnl_guestInformation.Controls.Add(this.txt_address);
            this.pnl_guestInformation.Controls.Add(this.lbl_email);
            this.pnl_guestInformation.Controls.Add(this.txt_email);
            this.pnl_guestInformation.Controls.Add(this.lbl_contactDetails);
            this.pnl_guestInformation.Controls.Add(this.txt_contactDetails);
            this.pnl_guestInformation.Controls.Add(this.lbl_middlename);
            this.pnl_guestInformation.Controls.Add(this.txt_middleName);
            this.pnl_guestInformation.Controls.Add(this.lbl_firstname);
            this.pnl_guestInformation.Controls.Add(this.txt_firstName);
            this.pnl_guestInformation.Controls.Add(this.lbl_lastname);
            this.pnl_guestInformation.Controls.Add(this.cmb_title);
            this.pnl_guestInformation.Controls.Add(this.txt_lastName);
            this.pnl_guestInformation.Controls.Add(this.lbl_title);
            this.pnl_guestInformation.Controls.Add(this.info_panel);
            this.pnl_guestInformation.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_guestInformation.Location = new System.Drawing.Point(0, 0);
            this.pnl_guestInformation.Name = "pnl_guestInformation";
            this.pnl_guestInformation.Size = new System.Drawing.Size(870, 309);
            this.pnl_guestInformation.TabIndex = 12;
            this.pnl_guestInformation.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl_guestInformation_Paint);
            // 
            // btn_scanImage
            // 
            this.btn_scanImage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_scanImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_scanImage.FlatAppearance.BorderSize = 0;
            this.btn_scanImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_scanImage.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_scanImage.ForeColor = System.Drawing.Color.White;
            this.btn_scanImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_scanImage.Location = new System.Drawing.Point(29, 237);
            this.btn_scanImage.Name = "btn_scanImage";
            this.btn_scanImage.Size = new System.Drawing.Size(128, 32);
            this.btn_scanImage.TabIndex = 48;
            this.btn_scanImage.Text = "SCAN IMAGE";
            this.btn_scanImage.UseVisualStyleBackColor = false;
            this.btn_scanImage.Click += new System.EventHandler(this.btn_scanImage_Click);
            // 
            // btn_attachImage
            // 
            this.btn_attachImage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_attachImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_attachImage.FlatAppearance.BorderSize = 0;
            this.btn_attachImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_attachImage.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_attachImage.ForeColor = System.Drawing.Color.White;
            this.btn_attachImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_attachImage.Location = new System.Drawing.Point(29, 191);
            this.btn_attachImage.Name = "btn_attachImage";
            this.btn_attachImage.Size = new System.Drawing.Size(128, 32);
            this.btn_attachImage.TabIndex = 47;
            this.btn_attachImage.Text = "ATTACH IMAGE";
            this.btn_attachImage.UseVisualStyleBackColor = false;
            this.btn_attachImage.Click += new System.EventHandler(this.btn_attachImage_Click);
            // 
            // pcb_image
            // 
            this.pcb_image.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pcb_image.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcb_image.Location = new System.Drawing.Point(16, 42);
            this.pcb_image.Name = "pcb_image";
            this.pcb_image.Size = new System.Drawing.Size(154, 134);
            this.pcb_image.TabIndex = 46;
            this.pcb_image.TabStop = false;
            // 
            // lbl_dateOfBirth
            // 
            this.lbl_dateOfBirth.AutoSize = true;
            this.lbl_dateOfBirth.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dateOfBirth.Location = new System.Drawing.Point(641, 95);
            this.lbl_dateOfBirth.Name = "lbl_dateOfBirth";
            this.lbl_dateOfBirth.Size = new System.Drawing.Size(94, 17);
            this.lbl_dateOfBirth.TabIndex = 45;
            this.lbl_dateOfBirth.Text = "Date of Birth*";
            // 
            // lbl_province
            // 
            this.lbl_province.AutoSize = true;
            this.lbl_province.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_province.Location = new System.Drawing.Point(641, 148);
            this.lbl_province.Name = "lbl_province";
            this.lbl_province.Size = new System.Drawing.Size(69, 17);
            this.lbl_province.TabIndex = 44;
            this.lbl_province.Text = "Province*";
            // 
            // txt_province
            // 
            this.txt_province.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_province.Location = new System.Drawing.Point(644, 167);
            this.txt_province.Name = "txt_province";
            this.txt_province.Size = new System.Drawing.Size(213, 24);
            this.txt_province.TabIndex = 10;
            this.txt_province.TextChanged += new System.EventHandler(this.txt_province_TextChanged);
            // 
            // cmb_homeCountry
            // 
            this.cmb_homeCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_homeCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_homeCountry.FormattingEnabled = true;
            this.cmb_homeCountry.Items.AddRange(new object[] {
            "Afghanistan",
            "Albania",
            "Algeria",
            "Andorra",
            "Angola",
            "Anguilla",
            "Antigua & Barbuda",
            "Argentina",
            "Armenia",
            "Australia",
            "Austria",
            "Azerbaijan",
            "Bahamas",
            "Bahrain",
            "Bangladesh",
            "Barbados",
            "Belarus",
            "Belgium",
            "Belize",
            "Benin",
            "Bermuda",
            "Bhutan",
            "Bolivia",
            "Bosnia & Herzegovina",
            "Botswana",
            "Brazil",
            "Brunei Darussalam",
            "Bulgaria",
            "Burkina Faso",
            "Myanmar/Burma",
            "Burundi",
            "Cambodia",
            "Cameroon",
            "Canada",
            "Cape Verde",
            "Cayman Islands",
            "Central African Republic",
            "Chad",
            "Chile",
            "China",
            "Colombia",
            "Comoros",
            "Congo",
            "Costa Rica",
            "Croatia",
            "Cuba",
            "Cyprus",
            "Czech Republic",
            "Democratic Republic of the Congo",
            "Denmark",
            "Djibouti",
            "Dominican Republic",
            "Dominica",
            "Ecuador",
            "Egypt",
            "El Salvador",
            "Equatorial Guinea",
            "Eritrea",
            "Estonia",
            "Ethiopia",
            "Fiji",
            "Finland",
            "France",
            "French Guiana",
            "Gabon",
            "Gambia",
            "Georgia",
            "Germany",
            "Ghana",
            "Great Britain",
            "Greece",
            "Grenada",
            "Guadeloupe",
            "Guatemala",
            "Guinea",
            "Guinea-Bissau",
            "Guyana",
            "Haiti",
            "Honduras",
            "Hungary",
            "Iceland",
            "India",
            "Indonesia",
            "Iran",
            "Iraq",
            "Israel and the Occupied Territories",
            "Italy",
            "Ivory Coast (Cote d\'Ivoire)",
            "Jamaica",
            "Japan",
            "Jordan",
            "Kazakhstan",
            "Kenya",
            "Kosovo",
            "Kuwait",
            "Kyrgyz Republic (Kyrgyzstan)",
            "Laos",
            "Latvia",
            "Lebanon",
            "Lesotho",
            "Liberia",
            "Libya",
            "Liechtenstein",
            "Lithuania",
            "Luxembourg",
            "Republic of Macedonia",
            "Madagascar",
            "Malawi",
            "Malaysia",
            "Maldives",
            "Mali",
            "Malta",
            "Martinique",
            "Mauritania",
            "Mauritius",
            "Mayotte",
            "Mexico",
            "Moldova, Republic of",
            "Monaco",
            "Mongolia",
            "Montenegro",
            "Montserrat",
            "Morocco",
            "Mozambique",
            "Namibia",
            "Nepal",
            "Netherlands",
            "New Zealand",
            "Nicaragua",
            "Niger",
            "Nigeria",
            "Korea, Democratic Republic of (North Korea)",
            "Norway",
            "Oman",
            "Pacific Islands",
            "Pakistan",
            "Panama",
            "Papua New Guinea",
            "Paraguay",
            "Peru",
            "Philippines",
            "Poland",
            "Portugal",
            "Puerto Rico",
            "Qatar",
            "Reunion",
            "Romania",
            "Russian Federation",
            "Rwanda",
            "Saint Kitts and Nevis",
            "Saint Lucia",
            "Saint Vincent\'s & Grenadines",
            "Samoa",
            "Sao Tome and Principe",
            "Saudi Arabia",
            "Senegal",
            "Serbia",
            "Seychelles",
            "Sierra Leone",
            "Singapore",
            "Slovak Republic (Slovakia)",
            "Slovenia",
            "Solomon Islands",
            "Somalia",
            "South Africa",
            "Korea, Republic of (South Korea)",
            "South Sudan",
            "Spain",
            "Sri Lanka",
            "Sudan",
            "Suriname",
            "Swaziland",
            "Sweden",
            "Switzerland",
            "Syria",
            "Tajikistan",
            "Tanzania",
            "Thailand",
            "Timor Leste",
            "Togo",
            "Trinidad & Tobago",
            "Tunisia",
            "Turkey",
            "Turkmenistan",
            "Turks & Caicos Islands",
            "Uganda",
            "Ukraine",
            "United Arab Emirates",
            "United States of America (USA)",
            "Uruguay",
            "Uzbekistan",
            "Venezuela",
            "Vietnam",
            "Virgin Islands (UK)",
            "Virgin Islands (US)",
            "Yemen",
            "Zambia",
            "Zimbabwe"});
            this.cmb_homeCountry.Location = new System.Drawing.Point(492, 218);
            this.cmb_homeCountry.Name = "cmb_homeCountry";
            this.cmb_homeCountry.Size = new System.Drawing.Size(218, 26);
            this.cmb_homeCountry.TabIndex = 12;
            this.cmb_homeCountry.SelectedIndexChanged += new System.EventHandler(this.cmb_homeCountry_SelectedIndexChanged);
            // 
            // lbl_homeCountry
            // 
            this.lbl_homeCountry.AutoSize = true;
            this.lbl_homeCountry.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_homeCountry.Location = new System.Drawing.Point(489, 200);
            this.lbl_homeCountry.Name = "lbl_homeCountry";
            this.lbl_homeCountry.Size = new System.Drawing.Size(105, 17);
            this.lbl_homeCountry.TabIndex = 41;
            this.lbl_homeCountry.Text = "Home Country*";
            // 
            // dtb_dateOfBirth
            // 
            this.dtb_dateOfBirth.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtb_dateOfBirth.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtb_dateOfBirth.Cursor = System.Windows.Forms.Cursors.No;
            this.dtb_dateOfBirth.CustomFormat = "MMMM dd, yyyy - dddd";
            this.dtb_dateOfBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtb_dateOfBirth.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtb_dateOfBirth.Location = new System.Drawing.Point(644, 114);
            this.dtb_dateOfBirth.Name = "dtb_dateOfBirth";
            this.dtb_dateOfBirth.Size = new System.Drawing.Size(213, 24);
            this.dtb_dateOfBirth.TabIndex = 7;
            this.dtb_dateOfBirth.ValueChanged += new System.EventHandler(this.dtb_dateOfBirth_ValueChanged);
            // 
            // lbl_visaNumber
            // 
            this.lbl_visaNumber.AutoSize = true;
            this.lbl_visaNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_visaNumber.Location = new System.Drawing.Point(489, 251);
            this.lbl_visaNumber.Name = "lbl_visaNumber";
            this.lbl_visaNumber.Size = new System.Drawing.Size(61, 17);
            this.lbl_visaNumber.TabIndex = 32;
            this.lbl_visaNumber.Text = "VISA no.";
            // 
            // txt_visaNumber
            // 
            this.txt_visaNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_visaNumber.Location = new System.Drawing.Point(492, 270);
            this.txt_visaNumber.Name = "txt_visaNumber";
            this.txt_visaNumber.Size = new System.Drawing.Size(300, 24);
            this.txt_visaNumber.TabIndex = 14;
            this.txt_visaNumber.TextChanged += new System.EventHandler(this.txt_visaNumber_TextChanged);
            // 
            // lbl_city
            // 
            this.lbl_city.AutoSize = true;
            this.lbl_city.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_city.Location = new System.Drawing.Point(413, 148);
            this.lbl_city.Name = "lbl_city";
            this.lbl_city.Size = new System.Drawing.Size(38, 17);
            this.lbl_city.TabIndex = 30;
            this.lbl_city.Text = "City*";
            // 
            // txt_city
            // 
            this.txt_city.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_city.Location = new System.Drawing.Point(416, 167);
            this.txt_city.Name = "txt_city";
            this.txt_city.Size = new System.Drawing.Size(222, 24);
            this.txt_city.TabIndex = 9;
            this.txt_city.TextChanged += new System.EventHandler(this.txt_city_TextChanged);
            // 
            // lbl_passportNumber
            // 
            this.lbl_passportNumber.AutoSize = true;
            this.lbl_passportNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_passportNumber.Location = new System.Drawing.Point(183, 251);
            this.lbl_passportNumber.Name = "lbl_passportNumber";
            this.lbl_passportNumber.Size = new System.Drawing.Size(89, 17);
            this.lbl_passportNumber.TabIndex = 28;
            this.lbl_passportNumber.Text = "Passport No.";
            // 
            // txt_passportNumber
            // 
            this.txt_passportNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_passportNumber.Location = new System.Drawing.Point(186, 270);
            this.txt_passportNumber.Name = "txt_passportNumber";
            this.txt_passportNumber.Size = new System.Drawing.Size(300, 24);
            this.txt_passportNumber.TabIndex = 13;
            this.txt_passportNumber.TextChanged += new System.EventHandler(this.txt_passportNumber_TextChanged);
            // 
            // lbl_companyAgency
            // 
            this.lbl_companyAgency.AutoSize = true;
            this.lbl_companyAgency.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_companyAgency.Location = new System.Drawing.Point(183, 199);
            this.lbl_companyAgency.Name = "lbl_companyAgency";
            this.lbl_companyAgency.Size = new System.Drawing.Size(123, 17);
            this.lbl_companyAgency.TabIndex = 26;
            this.lbl_companyAgency.Text = "Company/Agency";
            // 
            // txt_companyAgency
            // 
            this.txt_companyAgency.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_companyAgency.Location = new System.Drawing.Point(186, 218);
            this.txt_companyAgency.Name = "txt_companyAgency";
            this.txt_companyAgency.Size = new System.Drawing.Size(300, 24);
            this.txt_companyAgency.TabIndex = 11;
            this.txt_companyAgency.TextChanged += new System.EventHandler(this.txt_companyAgency_TextChanged);
            // 
            // lbl_address
            // 
            this.lbl_address.AutoSize = true;
            this.lbl_address.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_address.Location = new System.Drawing.Point(183, 148);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(65, 17);
            this.lbl_address.TabIndex = 24;
            this.lbl_address.Text = "Address*";
            // 
            // txt_address
            // 
            this.txt_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_address.Location = new System.Drawing.Point(186, 167);
            this.txt_address.Name = "txt_address";
            this.txt_address.Size = new System.Drawing.Size(224, 24);
            this.txt_address.TabIndex = 8;
            this.txt_address.TextChanged += new System.EventHandler(this.txt_address_TextChanged);
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_email.Location = new System.Drawing.Point(413, 95);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(50, 17);
            this.lbl_email.TabIndex = 22;
            this.lbl_email.Text = "Email*";
            // 
            // txt_email
            // 
            this.txt_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_email.Location = new System.Drawing.Point(416, 114);
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(222, 24);
            this.txt_email.TabIndex = 6;
            this.txt_email.TextChanged += new System.EventHandler(this.txt_email_TextChanged);
            // 
            // lbl_contactDetails
            // 
            this.lbl_contactDetails.AutoSize = true;
            this.lbl_contactDetails.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_contactDetails.Location = new System.Drawing.Point(183, 95);
            this.lbl_contactDetails.Name = "lbl_contactDetails";
            this.lbl_contactDetails.Size = new System.Drawing.Size(111, 17);
            this.lbl_contactDetails.TabIndex = 20;
            this.lbl_contactDetails.Text = "Contact Details*";
            // 
            // txt_contactDetails
            // 
            this.txt_contactDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_contactDetails.Location = new System.Drawing.Point(186, 114);
            this.txt_contactDetails.Name = "txt_contactDetails";
            this.txt_contactDetails.Size = new System.Drawing.Size(224, 24);
            this.txt_contactDetails.TabIndex = 5;
            this.txt_contactDetails.TextChanged += new System.EventHandler(this.txt_contactDetails_TextChanged);
            // 
            // lbl_middlename
            // 
            this.lbl_middlename.AutoSize = true;
            this.lbl_middlename.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_middlename.Location = new System.Drawing.Point(706, 43);
            this.lbl_middlename.Name = "lbl_middlename";
            this.lbl_middlename.Size = new System.Drawing.Size(98, 17);
            this.lbl_middlename.TabIndex = 18;
            this.lbl_middlename.Text = "Middle Name*";
            // 
            // txt_middleName
            // 
            this.txt_middleName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_middleName.Location = new System.Drawing.Point(709, 62);
            this.txt_middleName.Name = "txt_middleName";
            this.txt_middleName.Size = new System.Drawing.Size(148, 24);
            this.txt_middleName.TabIndex = 4;
            this.txt_middleName.TextChanged += new System.EventHandler(this.txt_middleName_TextChanged);
            // 
            // lbl_firstname
            // 
            this.lbl_firstname.AutoSize = true;
            this.lbl_firstname.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_firstname.Location = new System.Drawing.Point(499, 43);
            this.lbl_firstname.Name = "lbl_firstname";
            this.lbl_firstname.Size = new System.Drawing.Size(83, 17);
            this.lbl_firstname.TabIndex = 16;
            this.lbl_firstname.Text = "First Name*";
            // 
            // txt_firstName
            // 
            this.txt_firstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_firstName.Location = new System.Drawing.Point(502, 62);
            this.txt_firstName.Name = "txt_firstName";
            this.txt_firstName.Size = new System.Drawing.Size(201, 24);
            this.txt_firstName.TabIndex = 3;
            this.txt_firstName.TextChanged += new System.EventHandler(this.txt_firstName_TextChanged);
            // 
            // lbl_lastname
            // 
            this.lbl_lastname.AutoSize = true;
            this.lbl_lastname.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lastname.Location = new System.Drawing.Point(292, 43);
            this.lbl_lastname.Name = "lbl_lastname";
            this.lbl_lastname.Size = new System.Drawing.Size(81, 17);
            this.lbl_lastname.TabIndex = 14;
            this.lbl_lastname.Text = "Last Name*";
            // 
            // cmb_title
            // 
            this.cmb_title.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_title.FormattingEnabled = true;
            this.cmb_title.Items.AddRange(new object[] {
            "Mr.",
            "Mrs.",
            "Ms.",
            "Madam",
            "Sir",
            "Dr.",
            "Atty.",
            "Engr."});
            this.cmb_title.Location = new System.Drawing.Point(186, 61);
            this.cmb_title.Name = "cmb_title";
            this.cmb_title.Size = new System.Drawing.Size(103, 26);
            this.cmb_title.TabIndex = 1;
            this.cmb_title.SelectedIndexChanged += new System.EventHandler(this.cmb_title_SelectedIndexChanged);
            // 
            // txt_lastName
            // 
            this.txt_lastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_lastName.Location = new System.Drawing.Point(295, 62);
            this.txt_lastName.Name = "txt_lastName";
            this.txt_lastName.Size = new System.Drawing.Size(201, 24);
            this.txt_lastName.TabIndex = 2;
            this.txt_lastName.TextChanged += new System.EventHandler(this.txt_lastName_TextChanged);
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title.Location = new System.Drawing.Point(183, 42);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(41, 17);
            this.lbl_title.TabIndex = 11;
            this.lbl_title.Text = "Title*";
            // 
            // info_panel
            // 
            this.info_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.info_panel.Controls.Add(this.lbl_guestInformation);
            this.info_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.info_panel.Location = new System.Drawing.Point(0, 0);
            this.info_panel.Name = "info_panel";
            this.info_panel.Size = new System.Drawing.Size(868, 31);
            this.info_panel.TabIndex = 10;
            // 
            // lbl_guestInformation
            // 
            this.lbl_guestInformation.AutoSize = true;
            this.lbl_guestInformation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_guestInformation.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_guestInformation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_guestInformation.Location = new System.Drawing.Point(11, 8);
            this.lbl_guestInformation.Name = "lbl_guestInformation";
            this.lbl_guestInformation.Size = new System.Drawing.Size(133, 14);
            this.lbl_guestInformation.TabIndex = 0;
            this.lbl_guestInformation.Text = "GUEST INFORMATION";
            // 
            // Walkin_Reservation_GuestInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(870, 479);
            this.Controls.Add(this.pnl_guestInformation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Walkin_Reservation_GuestInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Walkin_Reservation_GuestInfo";
            this.Load += new System.EventHandler(this.Walkin_Reservation_GuestInfo_Load);
            this.pnl_guestInformation.ResumeLayout(false);
            this.pnl_guestInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_image)).EndInit();
            this.info_panel.ResumeLayout(false);
            this.info_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_guestInformation;
        private System.Windows.Forms.Label lbl_dateOfBirth;
        private System.Windows.Forms.Label lbl_province;
        private System.Windows.Forms.TextBox txt_province;
        private System.Windows.Forms.ComboBox cmb_homeCountry;
        private System.Windows.Forms.Label lbl_homeCountry;
        private System.Windows.Forms.DateTimePicker dtb_dateOfBirth;
        private System.Windows.Forms.Label lbl_visaNumber;
        private System.Windows.Forms.TextBox txt_visaNumber;
        private System.Windows.Forms.Label lbl_city;
        private System.Windows.Forms.TextBox txt_city;
        private System.Windows.Forms.Label lbl_passportNumber;
        private System.Windows.Forms.TextBox txt_passportNumber;
        private System.Windows.Forms.Label lbl_companyAgency;
        private System.Windows.Forms.TextBox txt_companyAgency;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.TextBox txt_address;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.Label lbl_contactDetails;
        private System.Windows.Forms.TextBox txt_contactDetails;
        private System.Windows.Forms.Label lbl_middlename;
        private System.Windows.Forms.TextBox txt_middleName;
        private System.Windows.Forms.Label lbl_firstname;
        private System.Windows.Forms.TextBox txt_firstName;
        private System.Windows.Forms.Label lbl_lastname;
        private System.Windows.Forms.ComboBox cmb_title;
        private System.Windows.Forms.TextBox txt_lastName;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.Panel info_panel;
        private System.Windows.Forms.Label lbl_guestInformation;
        private System.Windows.Forms.Button btn_scanImage;
        private System.Windows.Forms.Button btn_attachImage;
        private System.Windows.Forms.PictureBox pcb_image;
    }
}