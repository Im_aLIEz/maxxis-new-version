﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace maxxis.Walkin_Reservation
{
    public partial class Walkin_Reservation_GuestInfo : Form
    {

        private string title;
        private string lastname;
        private string firstname;
        private string middlename;
        private string dateofbirth;
        private string contact;
        private string address;
        private string email;
        private string province;
        private string homecountry;
        private string companyoragency;
        private string city;
        private string passport;
        private string visa;
        private static string guestimagesaver;
        private static Image GuestImage;

        public Walkin_Reservation_GuestInfo()
        {
            InitializeComponent();
        }
        

        private void Walkin_Reservation_GuestInfo_Load(object sender, EventArgs e)
        {
            cmb_homeCountry.Text = "Philippines";
            pcb_image.SizeMode = PictureBoxSizeMode.StretchImage;
            dtb_dateOfBirth.MaxDate = DateTime.Today.AddYears(-18);
            dtb_dateOfBirth.CustomFormat = "MM-dd-yyyy";
            GuestImageSaver = "";
            TextTyped();
        }

        private void cmb_title_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextTyped();
        }
        
        private void txt_lastName_TextChanged(object sender, EventArgs e)
        {
            TextTyped();
        }

        private void txt_firstName_TextChanged(object sender, EventArgs e)
        {
            TextTyped();
        }

        private void txt_middleName_TextChanged(object sender, EventArgs e)
        {
            TextTyped();
        }

        private void txt_contactDetails_TextChanged(object sender, EventArgs e)
        {
            TextTyped();
        }

        private void pnl_guestInformation_Paint(object sender, PaintEventArgs e)
        {

        }
        
        private void txt_email_TextChanged(object sender, EventArgs e)
        {
            TextTyped();
        }

        private void txt_address_TextChanged(object sender, EventArgs e)
        {
            TextTyped();
        }

        private void txt_city_TextChanged(object sender, EventArgs e)
        {
            TextTyped();
        }
        

        private void txt_province_TextChanged(object sender, EventArgs e)
        {
            TextTyped();
        }

        private void txt_companyAgency_TextChanged(object sender, EventArgs e)
        {
            TextTyped();
        }

        private void cmb_homeCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextTyped();
        }

        private void dtb_dateOfBirth_ValueChanged(object sender, EventArgs e)
        {
            TextTyped();
        }

        private void txt_passportNumber_TextChanged(object sender, EventArgs e)
        {
            TextTyped();
        }

        private void txt_visaNumber_TextChanged(object sender, EventArgs e)
        {
            TextTyped();
        }
        
        public void TextTyped()
        {
            Title = cmb_title.Text;
            LastName = txt_lastName.Text;
            FirstName = txt_firstName.Text;
            MiddleName = txt_middleName.Text;
            DateOfBirth = dtb_dateOfBirth.Text;
            Contact = txt_contactDetails.Text;
            Address = txt_address.Text;
            Email = txt_email.Text;
            Province = txt_province.Text;
            HomeCountry = cmb_homeCountry.Text;
            CompanyOrAgency = txt_companyAgency.Text;
            City = txt_city.Text;
            Passport = txt_passportNumber.Text;
            Visa = txt_visaNumber.Text;
            
        }
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string LastName
        {
            get { return lastname; }
            set { lastname = value; }
        }
        public string FirstName
        {
            get { return firstname; }
            set { firstname = value; }
        }
        public string MiddleName
        {
            get { return middlename; }
            set { middlename = value; }
        }
        public string DateOfBirth
        {
            get { return dateofbirth; }
            set { dateofbirth = value; }
        }
        public string Contact
        {
            get { return contact; }
            set { contact = value; }
        }
        public string Address
        {
            get { return address; }
            set { address = value; }
        }
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public string Province
        {
            get { return province; }
            set { province = value; }
        }
        public string HomeCountry
        {
            get { return homecountry; }
            set { homecountry = value; }
        }
        public string CompanyOrAgency
        {
            get { return companyoragency; }
            set { companyoragency = value; }
        }
        public string City
        {
            get { return city; }
            set { city = value; }
        }
        public string Passport
        {
            get { return passport; }
            set { passport = value; }
        }
        public string Visa
        {
            get { return visa; }
            set { visa = value; }
        }
        public string GuestImageSaver
        {
            get { return guestimagesaver; }
            set { guestimagesaver = value; }
        }
        public Image GuestImageFile
        {
            get { return GuestImage; }
            set { GuestImage = value; }
        }
        
        private void btn_attachImage_Click(object sender, EventArgs e)
        {
            WalkinReservation_form frm = new WalkinReservation_form();
            OpenFileDialog f = new OpenFileDialog();
            f.Filter = "Image Files|*.jpg;*.jpeg;*.png;...";
            if (f.ShowDialog() == DialogResult.OK)
            {
                GuestImageFile = Image.FromFile(f.FileName);
                pcb_image.BackgroundImage = GuestImageFile;
                pcb_image.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            GuestImageSaver = @"C:\Maxxis Guests\" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff") + ".jpg";
            TextTyped();
            
        }

        private void btn_scanImage_Click(object sender, EventArgs e)
        {
            
            
        }
    }
}
