﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace maxxis
{
    public partial class ReservationV2_Form : Form
    {
        string con = System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString;

        float Occupancy;
        float occupancyRate;
        
        int urqty;
        int ucrqty;
        int uvrqty;
        int checker;
        int nights;

        decimal Total;
        decimal Subtotal;

        string guestId;

        string folioNo;
        string resNo;
        string title;
        string lname;
        string fname;
        string mname;
        string contact;
        string email;
        string dateofbirth;
        string address;
        string city;
        string province;
        string company;
        string homecountry;
        string passport;
        string visa;
        string checkin;
        string checkout;
        string pwdselect;
        string urselect;
        string ucrselect;
        string uvrselect;
        string noofnights;
        string adults;
        string children;
        string extraadults;
        string extrachidlren;
        string purposeofstay;
        string cashhanded;
        string totalpayment;
        string preauthpayment;
        string change;
        string balance;
        string remarks;
        string urrate;
        string ucrrate;
        string uvrrate;

        string tnxdate = DateTime.Today.ToString("dd-MM-yyyy");
        string refno = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");

        private static string guestimagesaver;
        private static Image GuestImage;


        public ReservationV2_Form()
        {
            InitializeComponent();
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Main_Form frm = new Main_Form();
            this.Hide();
            frm.Show();
        }
        private void ReservationV2_Form_Load_1(object sender, EventArgs e)
        {
            dtb_dateOfBirth.MaxDate = DateTime.Today.AddYears(-18);
            dtp_arrivalDate.MinDate = DateTime.Today;
            dtp_departureDate.MinDate = dtp_arrivalDate.Value.AddDays(1);
            RoomLoadOccupy();
            Display();
            urqtycheck();
            ucrqtycheck();
            uvrqtycheck();
            if (ucrqty == 0)
            {
                dgv_listOfRooms.Rows[0].DefaultCellStyle.BackColor = Color.Red;
            }
            if (urqty == 0)
            {
                dgv_listOfRooms.Rows[1].DefaultCellStyle.BackColor = Color.Red;
            }
            if (uvrqty == 0)
            {
                dgv_listOfRooms.Rows[2].DefaultCellStyle.BackColor = Color.Red;
            }
            dtp_departureDate.MinDate = DateTime.Today.AddDays(1);
            dgv_listOfRooms.Rows[0].Cells[3].Value = "0";
            dgv_listOfRooms.Rows[1].Cells[3].Value = "0";
            dgv_listOfRooms.Rows[2].Cells[3].Value = "0";
            dgv_listOfRooms.Rows[0].Cells[5].Value = "0";
            dgv_listOfRooms.Rows[1].Cells[5].Value = "0";
            dgv_listOfRooms.Rows[2].Cells[5].Value = "0";
            dgv_listOfRooms.Rows[0].Cells[6].Value = "0";
            dgv_listOfRooms.Rows[1].Cells[6].Value = "0";
            dgv_listOfRooms.Rows[2].Cells[6].Value = "0";
            dgv_listOfRooms.Rows[0].Cells[7].Value = "0";
            dgv_listOfRooms.Rows[1].Cells[7].Value = "0";
            dgv_listOfRooms.Rows[2].Cells[7].Value = "0";
            lbl_nights.Text = "1";
        }

        public void Formupdate()
        {
            title = cmb_title.Text;
            fname = txt_firstName.Text;
            lname = txt_lastName.Text;
            mname = txt_middleName.Text;
            contact = txt_contactDetails.Text;
            email = txt_email.Text;
            dateofbirth = dtb_dateOfBirth.Text;
            address = txt_address.Text;
            city = txt_city.Text;
            province = txt_province.Text;
            company = txt_companyAgency.Text;
            homecountry = cmb_homeCountry.Text;
            passport = txt_passportNumber.Text;
            visa = txt_visaNumber.Text;
            checkin = dtp_arrivalDate.Text;
            checkout = dtp_departureDate.Text;
            noofnights = lbl_nights.Text;
            if(checker==1)
            {
                adults = (Int32.Parse(dgv_listOfRooms.Rows[0].Cells[5].Value.ToString()) + Int32.Parse(dgv_listOfRooms.Rows[1].Cells[5].Value.ToString()) + Int32.Parse(dgv_listOfRooms.Rows[2].Cells[5].Value.ToString())).ToString();
                children = (Int32.Parse(dgv_listOfRooms.Rows[0].Cells[6].Value.ToString()) + Int32.Parse(dgv_listOfRooms.Rows[1].Cells[6].Value.ToString()) + Int32.Parse(dgv_listOfRooms.Rows[2].Cells[6].Value.ToString())).ToString();
                urselect = dgv_listOfRooms.Rows[1].Cells[3].Value.ToString();
                ucrselect = dgv_listOfRooms.Rows[0].Cells[3].Value.ToString();
                uvrselect = dgv_listOfRooms.Rows[2].Cells[3].Value.ToString();
                urrate = dgv_listOfRooms.Rows[1].Cells[7].Value.ToString();
                ucrrate = dgv_listOfRooms.Rows[0].Cells[7].Value.ToString();
                uvrrate = dgv_listOfRooms.Rows[2].Cells[7].Value.ToString();
                extraadults = cbox_extraadults.Text;
                extrachidlren = cbox_extrachildren.Text;
                totalpayment = txt_accomo_accomodationTotal.Text;
                preauthpayment = txt_requiredPrePayment.Text;
                cashhanded = txt_cashhanded.Text;
                change = txt_change.Text;
                remarks = txt_guestRemarks.Text;
                balance = (decimal.Parse(totalpayment) - decimal.Parse(preauthpayment)).ToString();
            }
        }
        public void RetrieveGuestId()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT MAX(id) FROM tbl_guestinfo;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    guestId = item[0].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void Display()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT Distinct dbo.tbl_rooms.room_type, dbo.tbl_roomrate.rate - (dbo.tbl_roomrate.rate * dbo.tbl_currentoccupancy.occupancy) + dbo.tbl_rooms.roomrate AS Total FROM dbo.tbl_rooms CROSS JOIN dbo.tbl_roomrate Cross Join dbo.tbl_currentoccupancy where Not dbo.tbl_rooms.room_type = 'PWD Urban Room'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dgv_listOfRooms.Rows.Clear();
                foreach (DataRow item in dt.Rows)
                {
                    int n = dgv_listOfRooms.Rows.Add();
                    dgv_listOfRooms.Rows[n].Cells[2].Value = item[0].ToString();
                    dgv_listOfRooms.Rows[n].Cells[4].Value = item[1].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void RoomLoadOccupy()
        {
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select Sum(occupied/48 * 100)from tbl_rooms ", conDatabase);
            conDatabase.Open();
            SqlDataAdapter sda = new SqlDataAdapter();
            sda.SelectCommand = cmdDataBase;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            try
            {
                foreach (DataRow item in dt.Rows)
                {
                    Occupancy = float.Parse(item[0].ToString());
                }
                if (Occupancy < 59.99)
                {
                    occupancyRate = 0.40f;
                }
                else if (Occupancy < 69.99)
                {
                    occupancyRate = 0.30f;
                }
                else if (Occupancy < 79.99)
                {
                    occupancyRate = 0.20f;
                }
                else if (Occupancy < 89.99)
                {
                    occupancyRate = 0.10f;
                }
                else
                {
                    occupancyRate = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            conDatabase.Close();
        }
        public void urqtycheck()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select sum(available) as Quantity From tbl_rooms where room_type = 'Urban Room'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    urqty = Int32.Parse(item[0].ToString());
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void ucrqtycheck()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select sum(available) as Quantity From tbl_rooms where room_type = 'Urban Corner Room'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    ucrqty = Int32.Parse(item[0].ToString());
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void uvrqtycheck()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select sum(available) as Quantity From tbl_rooms where room_type = 'Urban View Room'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    uvrqty = Int32.Parse(item[0].ToString());
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void Reserve()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "insert into tbl_guestinfo(title," +
                    "lastname," +
                    "firstname," +
                    "middlename," +
                    "dateofbirth," +
                    "address," +
                    "contact_details," +
                    "email," +
                    "province," +
                    "home_country," +
                    "company_or_agency," +
                    "city," +
                    "reserved," +
                    "urselect," +
                    "ucrselect," +
                    "uvrselect," +
                    "remarks," +
                    "passport_no," +
                    "visa_no," +
                    "img," +
                    "reservation_no," +
                    "folio_number) values ('"
                    + title
                    + "','" + lname
                    + "','" + fname
                    + "','" + mname
                    + "','" + dateofbirth
                    + "','" + address
                    + "','" + contact
                    + "','" + email
                    + "','" + province
                    + "','" + homecountry
                    + "','" + company
                    + "','" + city
                    + "','" + 1
                    + "','" + urselect
                    + "','" + ucrselect
                    + "','" + uvrselect
                    + "','" + remarks
                    + "','" + passport
                    + "','" + visa
                    + "','" + guestimagesaver 
                    + "','" + resNo
                    + "','" + folioNo + "')";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
                MessageBox.Show("Guest Reserved");
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void StayInfo()
        {
            try
            {

                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "insert into tbl_stayinfo(guest_id,arrival,departure,no_ofnights,totalpayment,adults,children,extraadults,extrachildren) values('"
                    + guestId
                    + "','" + checkin
                    + "','" + checkout
                    + "','" + noofnights
                    + "','" + totalpayment
                    + "' ,'" + adults
                    + "' ,'" + children
                    + "' ,'" + extraadults
                    + "' ,'" + extrachidlren + "');";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void Billing()
        {
            try
            {

                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "insert into tbl_billing(guest_id,preauthpayment,modeofpayment,urpayment,ucrpayment,uvrpayment,totalpayment) values('"
                    + guestId
                    + "','" + Subtotal
                    + "','" + "Walk in Reservation"
                    + "','" + urrate
                    + "','" + ucrrate
                    + "','" + uvrrate
                    + "','" + totalpayment + "');";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void cmb_title_SelectedIndexChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void txt_lastName_TextChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void txt_firstName_TextChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void txt_middleName_TextChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void txt_contactDetails_TextChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void txt_email_TextChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void dtb_dateOfBirth_ValueChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void txt_address_TextChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void txt_city_TextChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void txt_province_TextChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void txt_companyAgency_TextChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void cmb_homeCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void txt_passportNumber_TextChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void txt_visaNumber_TextChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void dtp_checkIn_ValueChanged(object sender, EventArgs e)
        {
            dtp_departureDate.MinDate = dtp_arrivalDate.Value.AddDays(1);
            Formupdate();
        }

        private void dtp_checkOut_ValueChanged(object sender, EventArgs e)
        {
            double days = Math.Abs((dtp_departureDate.Value - dtp_arrivalDate.Value).TotalDays);
            lbl_nights.Text = (Convert.ToInt32(days)).ToString();
            Formupdate();
        }

        private void cmb_add_urbanRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void cmb_addAdult_UR_SelectedIndexChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void cmd_addChild_UR_SelectedIndexChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void cmb_add_urbanCornerRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void cmb_addAdult_UCR_SelectedIndexChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void cmb_addChild_UCR_SelectedIndexChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void cmd_add_urbanViewRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void cmb_addAdult_UVR_SelectedIndexChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void cmb_addChild_UVR_SelectedIndexChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void cmb_addAdult_additionalComapinon_SelectedIndexChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void cmb_addChild_additionalCompanion_SelectedIndexChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void cmb_purposeOfStay_SelectedIndexChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void txt_cashhanded_TextChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void txt_change_TextChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void txt_requiredPrePayment_TextChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void txt_cashPayment_TextChanged(object sender, EventArgs e)
        {
            Formupdate();
        }

        private void cbox_adult_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbox_children.Items.Clear();
            for (int x = 0; x <= Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) * 2; x++)
            {
                cbox_children.Items.Add(x.ToString());
            }
            int ch = cbox_children.Items.Count;
            dgv_listOfRooms.SelectedRows[0].Cells[5].Value = cbox_adult.Text;
            int val = (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString()) - Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString())) * 2;
            cbox_children.Items.Clear();
            for (int x = 0; x < ch - val; x++)
            {
                cbox_children.Items.Add(x.ToString());
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban View Room")
            {
                cbox_children.Items.Clear();
                for (int x = 0; x <= (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString())) * 2; x++)
                {
                    cbox_children.Items.Add(x.ToString());
                }
                int ch1 = cbox_children.Items.Count;
                int val1 = (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString()) - Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString())) * 4;
                cbox_children.Items.Clear();
                for (int x = 0; x < (ch1 - val1); x++)
                {
                    cbox_children.Items.Add(x.ToString());
                }
            }
            checker = 1;
            Formupdate();
        }

        private void cbox_children_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbox_adult.Items.Clear();
            int ch = Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) - ((Int32.Parse(cbox_children.Text) / 2) + Int32.Parse(cbox_children.Text) % 2);
            for (int x = 0; x <= ch; x++)
            {
                cbox_adult.Items.Add(Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + x);
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban View Room")
            {
                cbox_adult.Items.Clear();
                for (int x = 0; x <= ch + Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()); x++)
                {
                    cbox_adult.Items.Add(Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + x);
                }
            }
            dgv_listOfRooms.SelectedRows[0].Cells[6].Value = cbox_children.Text;
            cbox_children.Text = dgv_listOfRooms.SelectedRows[0].Cells[6].Value.ToString();
            checker = 1;
            Formupdate();
        }

        private void numUD_roomQuantity_MouseUp(object sender, MouseEventArgs e)
        {
            cbox_children.Text = "0";
            if (numUD_roomQuantity.Text == "0")
            {
                cbox_adult.Text = "0";
                cbox_children.Text = "0";
            }
            else
                cbox_adult.Items.Clear();
            cbox_children.Items.Clear();
            dgv_listOfRooms.SelectedRows[0].Cells[3].Value = numUD_roomQuantity.Text;

            if (numUD_roomQuantity.Value >= 1)
            {
                dgv_listOfRooms.SelectedRows[0].Cells[5].Value = numUD_roomQuantity.Text;
                cbox_adult.Text = dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString();
            }
            else
            {
                dgv_listOfRooms.SelectedRows[0].Cells[5].Value = "0";
            }

            for (int x = 0; x <= Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString()); x++)
            {
                cbox_adult.Items.Add(Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + x);
            }
            for (int x = 0; x <= Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) * 2; x++)
            {
                cbox_children.Items.Add(x.ToString());
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban View Room")
            {
                cbox_adult.Items.Clear();
                cbox_children.Items.Clear();
                for (int x = 0; x <= Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString()) + (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString())); x++)
                {
                    cbox_adult.Items.Add(Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + x);
                }
                for (int x = 0; x <= (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString())) * 2; x++)
                {
                    cbox_children.Items.Add(x.ToString());
                }
            }

            dgv_listOfRooms.Rows[0].Cells[7].Value = (Int32.Parse(dgv_listOfRooms.Rows[0].Cells[3].Value.ToString()) * Int32.Parse(dgv_listOfRooms.Rows[0].Cells[4].Value.ToString()));
            dgv_listOfRooms.Rows[1].Cells[7].Value = (Int32.Parse(dgv_listOfRooms.Rows[1].Cells[3].Value.ToString()) * Int32.Parse(dgv_listOfRooms.Rows[1].Cells[4].Value.ToString()));
            dgv_listOfRooms.Rows[2].Cells[7].Value = (Int32.Parse(dgv_listOfRooms.Rows[2].Cells[3].Value.ToString()) * Int32.Parse(dgv_listOfRooms.Rows[2].Cells[4].Value.ToString()));
            Total = (((Decimal.Parse(dgv_listOfRooms.Rows[0].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[1].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[2].Cells[7].Value.ToString())))) + Decimal.Parse(txt_adultsTotalFee.Text) + Decimal.Parse(txt_childTotalFee.Text) * Int32.Parse(lbl_nights.Text);
            Subtotal = (((Decimal.Parse(dgv_listOfRooms.Rows[0].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[1].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[2].Cells[7].Value.ToString())))) + Decimal.Parse(txt_adultsTotalFee.Text) + Decimal.Parse(txt_childTotalFee.Text);
            txt_accomo_accomodationTotal.Text = Total.ToString("N2");
            txt_requiredPrePayment.Text = Subtotal.ToString("N2");
            Formupdate();
        }

        private void btn_reserveNow_Click(object sender, EventArgs e)
        {
            folioNo = "F" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
            resNo = "R" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
            Reserve();
            RetrieveGuestId();
            StayInfo();
            Billing();
            Main_Form mainfrm = new Main_Form();
            this.Hide();
            mainfrm.Show();
        }

        private void dgv_listOfRooms_MouseUp(object sender, MouseEventArgs e)
        {
            numUD_roomQuantity.Text = "0";
            Formupdate();
            Total = (((Decimal.Parse(dgv_listOfRooms.Rows[0].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[1].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[2].Cells[7].Value.ToString())))) + Decimal.Parse(txt_adultsTotalFee.Text) + Decimal.Parse(txt_childTotalFee.Text) * Int32.Parse(lbl_nights.Text);
            Subtotal = (((Decimal.Parse(dgv_listOfRooms.Rows[0].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[1].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[2].Cells[7].Value.ToString())))) + Decimal.Parse(txt_adultsTotalFee.Text) + Decimal.Parse(txt_childTotalFee.Text);
            txt_requiredPrePayment.Text = Subtotal.ToString("N2");
            txt_accomo_accomodationTotal.Text = Total.ToString("N2");
            Formupdate();
            for (int x = 0; x <= Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString()); x++)
            {
                cbox_adult.Items.Add(Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + x);
            }
            for (int x = 0; x <= Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) * 2; x++)
            {
                cbox_children.Items.Add(x.ToString());
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban View Room")
            {
                cbox_adult.Items.Clear();
                cbox_children.Items.Clear();
                for (int x = 0; x <= Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString()) + (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString())); x++)
                {
                    cbox_adult.Items.Add(Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + x);
                }
                for (int x = 0; x <= (Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString()) + Int32.Parse(dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString())) * 2; x++)
                {
                    cbox_children.Items.Add(x.ToString());
                }
            }
            numUD_roomQuantity.Text = dgv_listOfRooms.SelectedRows[0].Cells[3].Value.ToString();
            cbox_adult.Text = dgv_listOfRooms.SelectedRows[0].Cells[5].Value.ToString();
            cbox_children.Text = dgv_listOfRooms.SelectedRows[0].Cells[6].Value.ToString();
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban View Room")
            {
                numUD_roomQuantity.Maximum = uvrqty;
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban Room")
            {
                numUD_roomQuantity.Maximum = urqty;
            }
            if (dgv_listOfRooms.SelectedRows[0].Cells[2].Value.ToString() == "Urban Corner Room")
            {
                numUD_roomQuantity.Maximum = ucrqty;
            }
        }

        private void numUD_roomQuantity_ValueChanged(object sender, EventArgs e)
        {
            checker = 1;
        }

        private void dtp_arrivalDate_ValueChanged(object sender, EventArgs e)
        {
            if(checker == 1)
            {
                dtp_arrivalDate.MinDate = DateTime.Today;
                dtp_departureDate.MinDate = dtp_arrivalDate.Value.AddDays(1);
                double days = Math.Abs((dtp_arrivalDate.Value - dtp_departureDate.Value).TotalDays);
                lbl_nights.Text = Math.Truncate(days).ToString();
                Total = (((Decimal.Parse(dgv_listOfRooms.Rows[0].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[1].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[2].Cells[7].Value.ToString())))) + Decimal.Parse(txt_adultsTotalFee.Text) + Decimal.Parse(txt_childTotalFee.Text) * Int32.Parse(lbl_nights.Text);
                Subtotal = (((Decimal.Parse(dgv_listOfRooms.Rows[0].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[1].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[2].Cells[7].Value.ToString())))) + Decimal.Parse(txt_adultsTotalFee.Text) + Decimal.Parse(txt_childTotalFee.Text);
                txt_accomo_accomodationTotal.Text = Total.ToString("N2");
                txt_requiredPrePayment.Text = Subtotal.ToString("N2");
                Formupdate();
            }
            
        }

        private void dtp_departureDate_ValueChanged(object sender, EventArgs e)
        {
            double days = Math.Abs((dtp_arrivalDate.Value - dtp_departureDate.Value).TotalDays);
            if (checker == 1)
            {
                lbl_nights.Text = Math.Truncate(days).ToString();
                Total = (((Decimal.Parse(dgv_listOfRooms.Rows[0].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[1].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[2].Cells[7].Value.ToString())))) + Decimal.Parse(txt_adultsTotalFee.Text) + Decimal.Parse(txt_childTotalFee.Text) * Int32.Parse(lbl_nights.Text);
                Subtotal = (((Decimal.Parse(dgv_listOfRooms.Rows[0].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[1].Cells[7].Value.ToString()) + Decimal.Parse(dgv_listOfRooms.Rows[2].Cells[7].Value.ToString())))) + Decimal.Parse(txt_adultsTotalFee.Text) + Decimal.Parse(txt_childTotalFee.Text);
                txt_accomo_accomodationTotal.Text = Total.ToString("N2");
                txt_requiredPrePayment.Text = Subtotal.ToString("N2");
                Formupdate();
            }
            else
            {
                lbl_nights.Text = Math.Truncate(days+1).ToString();
            }
        }

        private void dtp_departureDate_MouseUp(object sender, MouseEventArgs e)
        {
            
        }

        private void dtp_arrivalDate_MouseUp(object sender, MouseEventArgs e)
        {
            checker = 1;
        }

        private void btn_attachImage_Click(object sender, EventArgs e)
        {
            WalkinReservation_form frm = new WalkinReservation_form();
            OpenFileDialog f = new OpenFileDialog();
            f.Filter = "Image Files|*.jpg;*.jpeg;*.png;...";
            if (f.ShowDialog() == DialogResult.OK)
            {
                GuestImage= Image.FromFile(f.FileName);
                pcb_image.BackgroundImage = GuestImage;

            }
            guestimagesaver = @"C:\Users\Kiko\Pictures\Maxxis Guests\" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff") + ".jpg";
        }
    }
}
