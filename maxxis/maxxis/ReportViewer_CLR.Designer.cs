﻿namespace maxxis
{
    partial class ReportViewer_CLR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource4 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.tbl_cancellationListReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CLR_DataReport = new maxxis.CLR_DataReport();
            this.rv_reportViewer_CLR = new Microsoft.Reporting.WinForms.ReportViewer();
            this.tbl_cancellationListReportTableAdapter = new maxxis.CLR_DataReportTableAdapters.tbl_cancellationListReportTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.dtp_fromDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_toDate = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_loadReport = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tbl_cancellationListReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CLR_DataReport)).BeginInit();
            this.SuspendLayout();
            // 
            // tbl_cancellationListReportBindingSource
            // 
            this.tbl_cancellationListReportBindingSource.DataMember = "tbl_cancellationListReport";
            this.tbl_cancellationListReportBindingSource.DataSource = this.CLR_DataReport;
            // 
            // CLR_DataReport
            // 
            this.CLR_DataReport.DataSetName = "CLR_DataReport";
            this.CLR_DataReport.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rv_reportViewer_CLR
            // 
            reportDataSource4.Name = "RDLC_cancellationList";
            reportDataSource4.Value = this.tbl_cancellationListReportBindingSource;
            this.rv_reportViewer_CLR.LocalReport.DataSources.Add(reportDataSource4);
            this.rv_reportViewer_CLR.LocalReport.ReportEmbeddedResource = "maxxis.Report_RDLC_cancellationList.rdlc";
            this.rv_reportViewer_CLR.Location = new System.Drawing.Point(12, 63);
            this.rv_reportViewer_CLR.Name = "rv_reportViewer_CLR";
            this.rv_reportViewer_CLR.ServerReport.BearerToken = null;
            this.rv_reportViewer_CLR.Size = new System.Drawing.Size(1260, 616);
            this.rv_reportViewer_CLR.TabIndex = 0;
            // 
            // tbl_cancellationListReportTableAdapter
            // 
            this.tbl_cancellationListReportTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 17);
            this.label1.TabIndex = 92;
            this.label1.Text = "From Date:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtp_fromDate
            // 
            this.dtp_fromDate.CalendarFont = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_fromDate.CalendarTitleBackColor = System.Drawing.Color.White;
            this.dtp_fromDate.CustomFormat = "MM/dd/yyyy";
            this.dtp_fromDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_fromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_fromDate.Location = new System.Drawing.Point(94, 20);
            this.dtp_fromDate.Name = "dtp_fromDate";
            this.dtp_fromDate.Size = new System.Drawing.Size(107, 22);
            this.dtp_fromDate.TabIndex = 93;
            this.dtp_fromDate.Value = new System.DateTime(2018, 4, 23, 0, 0, 0, 0);
            // 
            // dtp_toDate
            // 
            this.dtp_toDate.CalendarFont = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_toDate.CalendarTitleBackColor = System.Drawing.Color.White;
            this.dtp_toDate.CustomFormat = "MM/dd/yyyy";
            this.dtp_toDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_toDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_toDate.Location = new System.Drawing.Point(294, 20);
            this.dtp_toDate.Name = "dtp_toDate";
            this.dtp_toDate.Size = new System.Drawing.Size(113, 22);
            this.dtp_toDate.TabIndex = 94;
            this.dtp_toDate.Value = new System.DateTime(2018, 4, 23, 0, 0, 0, 0);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(226, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 17);
            this.label9.TabIndex = 95;
            this.label9.Text = "To Date:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_loadReport
            // 
            this.btn_loadReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_loadReport.FlatAppearance.BorderSize = 0;
            this.btn_loadReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_loadReport.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_loadReport.ForeColor = System.Drawing.Color.White;
            this.btn_loadReport.Location = new System.Drawing.Point(432, 15);
            this.btn_loadReport.Name = "btn_loadReport";
            this.btn_loadReport.Size = new System.Drawing.Size(108, 30);
            this.btn_loadReport.TabIndex = 96;
            this.btn_loadReport.Text = "LOAD";
            this.btn_loadReport.UseVisualStyleBackColor = false;
            this.btn_loadReport.Click += new System.EventHandler(this.btn_loadReport_Click);
            // 
            // ReportViewer_CLR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 691);
            this.Controls.Add(this.btn_loadReport);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dtp_toDate);
            this.Controls.Add(this.dtp_fromDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rv_reportViewer_CLR);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ReportViewer_CLR";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ReportViewer_CLR";
            this.Load += new System.EventHandler(this.ReportViewer_CLR_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbl_cancellationListReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CLR_DataReport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rv_reportViewer_CLR;
        private System.Windows.Forms.BindingSource tbl_cancellationListReportBindingSource;
        private CLR_DataReport CLR_DataReport;
        private CLR_DataReportTableAdapters.tbl_cancellationListReportTableAdapter tbl_cancellationListReportTableAdapter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtp_fromDate;
        private System.Windows.Forms.DateTimePicker dtp_toDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_loadReport;
    }
}