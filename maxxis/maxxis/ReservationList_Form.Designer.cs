﻿namespace maxxis
{
    partial class ReservationList_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.guestFolio_panel = new System.Windows.Forms.Panel();
            this.lbl_reservationList = new System.Windows.Forms.Label();
            this.pcb_reservationList = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.dtp_cancellationEnd = new System.Windows.Forms.DateTimePicker();
            this.dtp_cancellation_start = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.dtp_reservationDate_end = new System.Windows.Forms.DateTimePicker();
            this.dtp_reservationDate_start = new System.Windows.Forms.DateTimePicker();
            this.dtp_arrivalDate = new System.Windows.Forms.DateTimePicker();
            this.chb_cancelationDate = new System.Windows.Forms.CheckBox();
            this.chb_reservationDate = new System.Windows.Forms.CheckBox();
            this.chb_arrivalDate = new System.Windows.Forms.CheckBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.cmb_source = new System.Windows.Forms.ComboBox();
            this.cmb_roomType = new System.Windows.Forms.ComboBox();
            this.txt_lastName = new System.Windows.Forms.TextBox();
            this.lbl_source = new System.Windows.Forms.Label();
            this.lbl_roomType = new System.Windows.Forms.Label();
            this.lbl_firstNameAndLastName = new System.Windows.Forms.Label();
            this.txt_firstName = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lbl_cancellationNumber = new System.Windows.Forms.Label();
            this.txt_cancellationNumber = new System.Windows.Forms.TextBox();
            this.lbl_voucherNumber = new System.Windows.Forms.Label();
            this.txt_voucherNumber = new System.Windows.Forms.TextBox();
            this.lbl_reservationNumber = new System.Windows.Forms.Label();
            this.txt_reservationNumber = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.rbt_waitForPayments = new System.Windows.Forms.RadioButton();
            this.rbt_optioned = new System.Windows.Forms.RadioButton();
            this.rbt_all = new System.Windows.Forms.RadioButton();
            this.rbt_void = new System.Windows.Forms.RadioButton();
            this.rbt_noShow = new System.Windows.Forms.RadioButton();
            this.rbt_cancelled = new System.Windows.Forms.RadioButton();
            this.rbt_active = new System.Windows.Forms.RadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lbl_searchCriteria = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgv_reservationList = new System.Windows.Forms.DataGridView();
            this.colReservationNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRoom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGuestName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReservationDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colArrival = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeparture = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_close = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_new = new System.Windows.Forms.Button();
            this.btn_edit = new System.Windows.Forms.Button();
            this.btn_void = new System.Windows.Forms.Button();
            this.guestFolio_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_reservationList)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_reservationList)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // guestFolio_panel
            // 
            this.guestFolio_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.guestFolio_panel.Controls.Add(this.lbl_reservationList);
            this.guestFolio_panel.Controls.Add(this.pcb_reservationList);
            this.guestFolio_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.guestFolio_panel.Location = new System.Drawing.Point(0, 0);
            this.guestFolio_panel.Name = "guestFolio_panel";
            this.guestFolio_panel.Size = new System.Drawing.Size(1300, 35);
            this.guestFolio_panel.TabIndex = 2;
            // 
            // lbl_reservationList
            // 
            this.lbl_reservationList.AutoSize = true;
            this.lbl_reservationList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_reservationList.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_reservationList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_reservationList.Location = new System.Drawing.Point(37, 9);
            this.lbl_reservationList.Name = "lbl_reservationList";
            this.lbl_reservationList.Size = new System.Drawing.Size(144, 18);
            this.lbl_reservationList.TabIndex = 0;
            this.lbl_reservationList.Text = "RESERVATION LIST";
            // 
            // pcb_reservationList
            // 
            this.pcb_reservationList.BackgroundImage = global::maxxis.Properties.Resources.Reservation_List;
            this.pcb_reservationList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_reservationList.Dock = System.Windows.Forms.DockStyle.Left;
            this.pcb_reservationList.Location = new System.Drawing.Point(0, 0);
            this.pcb_reservationList.Name = "pcb_reservationList";
            this.pcb_reservationList.Size = new System.Drawing.Size(41, 35);
            this.pcb_reservationList.TabIndex = 1;
            this.pcb_reservationList.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Location = new System.Drawing.Point(12, 41);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1276, 211);
            this.panel1.TabIndex = 3;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.dtp_cancellationEnd);
            this.panel7.Controls.Add(this.dtp_cancellation_start);
            this.panel7.Controls.Add(this.label9);
            this.panel7.Controls.Add(this.dtp_reservationDate_end);
            this.panel7.Controls.Add(this.dtp_reservationDate_start);
            this.panel7.Controls.Add(this.dtp_arrivalDate);
            this.panel7.Controls.Add(this.chb_cancelationDate);
            this.panel7.Controls.Add(this.chb_reservationDate);
            this.panel7.Controls.Add(this.chb_arrivalDate);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(809, 35);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(465, 136);
            this.panel7.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(283, 95);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 17);
            this.label10.TabIndex = 76;
            this.label10.Text = "To";
            // 
            // dtp_cancellationEnd
            // 
            this.dtp_cancellationEnd.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtp_cancellationEnd.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtp_cancellationEnd.CustomFormat = "   dd/MM/yyyy";
            this.dtp_cancellationEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_cancellationEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_cancellationEnd.Location = new System.Drawing.Point(327, 88);
            this.dtp_cancellationEnd.Name = "dtp_cancellationEnd";
            this.dtp_cancellationEnd.Size = new System.Drawing.Size(117, 23);
            this.dtp_cancellationEnd.TabIndex = 75;
            // 
            // dtp_cancellation_start
            // 
            this.dtp_cancellation_start.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtp_cancellation_start.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtp_cancellation_start.CustomFormat = "   dd/MM/yyyy";
            this.dtp_cancellation_start.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_cancellation_start.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_cancellation_start.Location = new System.Drawing.Point(144, 89);
            this.dtp_cancellation_start.Name = "dtp_cancellation_start";
            this.dtp_cancellation_start.Size = new System.Drawing.Size(120, 23);
            this.dtp_cancellation_start.TabIndex = 74;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(283, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 17);
            this.label9.TabIndex = 73;
            this.label9.Text = "To";
            // 
            // dtp_reservationDate_end
            // 
            this.dtp_reservationDate_end.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtp_reservationDate_end.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtp_reservationDate_end.CustomFormat = "   dd/MM/yyyy";
            this.dtp_reservationDate_end.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_reservationDate_end.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_reservationDate_end.Location = new System.Drawing.Point(327, 47);
            this.dtp_reservationDate_end.Name = "dtp_reservationDate_end";
            this.dtp_reservationDate_end.Size = new System.Drawing.Size(117, 23);
            this.dtp_reservationDate_end.TabIndex = 72;
            // 
            // dtp_reservationDate_start
            // 
            this.dtp_reservationDate_start.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtp_reservationDate_start.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtp_reservationDate_start.CustomFormat = "   dd/MM/yyyy";
            this.dtp_reservationDate_start.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_reservationDate_start.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_reservationDate_start.Location = new System.Drawing.Point(144, 48);
            this.dtp_reservationDate_start.Name = "dtp_reservationDate_start";
            this.dtp_reservationDate_start.Size = new System.Drawing.Size(120, 23);
            this.dtp_reservationDate_start.TabIndex = 71;
            // 
            // dtp_arrivalDate
            // 
            this.dtp_arrivalDate.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtp_arrivalDate.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtp_arrivalDate.CustomFormat = "          MMMM dd, yyyy    -dddd";
            this.dtp_arrivalDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_arrivalDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_arrivalDate.Location = new System.Drawing.Point(144, 9);
            this.dtp_arrivalDate.Name = "dtp_arrivalDate";
            this.dtp_arrivalDate.Size = new System.Drawing.Size(300, 23);
            this.dtp_arrivalDate.TabIndex = 68;
            this.dtp_arrivalDate.ValueChanged += new System.EventHandler(this.dtp_arrivalDate_ValueChanged);
            // 
            // chb_cancelationDate
            // 
            this.chb_cancelationDate.AutoSize = true;
            this.chb_cancelationDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chb_cancelationDate.Location = new System.Drawing.Point(25, 91);
            this.chb_cancelationDate.Name = "chb_cancelationDate";
            this.chb_cancelationDate.Size = new System.Drawing.Size(90, 21);
            this.chb_cancelationDate.TabIndex = 31;
            this.chb_cancelationDate.Text = "Can. Date";
            this.chb_cancelationDate.UseVisualStyleBackColor = true;
            // 
            // chb_reservationDate
            // 
            this.chb_reservationDate.AutoSize = true;
            this.chb_reservationDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chb_reservationDate.Location = new System.Drawing.Point(25, 50);
            this.chb_reservationDate.Name = "chb_reservationDate";
            this.chb_reservationDate.Size = new System.Drawing.Size(88, 21);
            this.chb_reservationDate.TabIndex = 30;
            this.chb_reservationDate.Text = "Res. Date";
            this.chb_reservationDate.UseVisualStyleBackColor = true;
            // 
            // chb_arrivalDate
            // 
            this.chb_arrivalDate.AutoSize = true;
            this.chb_arrivalDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chb_arrivalDate.Location = new System.Drawing.Point(25, 11);
            this.chb_arrivalDate.Name = "chb_arrivalDate";
            this.chb_arrivalDate.Size = new System.Drawing.Size(103, 21);
            this.chb_arrivalDate.TabIndex = 29;
            this.chb_arrivalDate.Text = "Arrival Date";
            this.chb_arrivalDate.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.cmb_source);
            this.panel6.Controls.Add(this.cmb_roomType);
            this.panel6.Controls.Add(this.txt_lastName);
            this.panel6.Controls.Add(this.lbl_source);
            this.panel6.Controls.Add(this.lbl_roomType);
            this.panel6.Controls.Add(this.lbl_firstNameAndLastName);
            this.panel6.Controls.Add(this.txt_firstName);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(348, 35);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(461, 136);
            this.panel6.TabIndex = 14;
            // 
            // cmb_source
            // 
            this.cmb_source.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_source.FormattingEnabled = true;
            this.cmb_source.Location = new System.Drawing.Point(146, 88);
            this.cmb_source.Name = "cmb_source";
            this.cmb_source.Size = new System.Drawing.Size(298, 24);
            this.cmb_source.TabIndex = 13;
            // 
            // cmb_roomType
            // 
            this.cmb_roomType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_roomType.FormattingEnabled = true;
            this.cmb_roomType.Location = new System.Drawing.Point(146, 47);
            this.cmb_roomType.Name = "cmb_roomType";
            this.cmb_roomType.Size = new System.Drawing.Size(298, 24);
            this.cmb_roomType.TabIndex = 12;
            // 
            // txt_lastName
            // 
            this.txt_lastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_lastName.Location = new System.Drawing.Point(282, 9);
            this.txt_lastName.Name = "txt_lastName";
            this.txt_lastName.Size = new System.Drawing.Size(162, 23);
            this.txt_lastName.TabIndex = 11;
            // 
            // lbl_source
            // 
            this.lbl_source.AutoSize = true;
            this.lbl_source.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_source.Location = new System.Drawing.Point(12, 93);
            this.lbl_source.Name = "lbl_source";
            this.lbl_source.Size = new System.Drawing.Size(53, 17);
            this.lbl_source.TabIndex = 9;
            this.lbl_source.Text = "Source";
            // 
            // lbl_roomType
            // 
            this.lbl_roomType.AutoSize = true;
            this.lbl_roomType.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_roomType.Location = new System.Drawing.Point(12, 52);
            this.lbl_roomType.Name = "lbl_roomType";
            this.lbl_roomType.Size = new System.Drawing.Size(80, 17);
            this.lbl_roomType.TabIndex = 7;
            this.lbl_roomType.Text = "Room Type";
            // 
            // lbl_firstNameAndLastName
            // 
            this.lbl_firstNameAndLastName.AutoSize = true;
            this.lbl_firstNameAndLastName.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_firstNameAndLastName.Location = new System.Drawing.Point(12, 13);
            this.lbl_firstNameAndLastName.Name = "lbl_firstNameAndLastName";
            this.lbl_firstNameAndLastName.Size = new System.Drawing.Size(111, 17);
            this.lbl_firstNameAndLastName.TabIndex = 5;
            this.lbl_firstNameAndLastName.Text = "First, Last Name";
            // 
            // txt_firstName
            // 
            this.txt_firstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_firstName.Location = new System.Drawing.Point(146, 9);
            this.txt_firstName.Name = "txt_firstName";
            this.txt_firstName.Size = new System.Drawing.Size(130, 23);
            this.txt_firstName.TabIndex = 6;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.lbl_cancellationNumber);
            this.panel5.Controls.Add(this.txt_cancellationNumber);
            this.panel5.Controls.Add(this.lbl_voucherNumber);
            this.panel5.Controls.Add(this.txt_voucherNumber);
            this.panel5.Controls.Add(this.lbl_reservationNumber);
            this.panel5.Controls.Add(this.txt_reservationNumber);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(0, 35);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(348, 136);
            this.panel5.TabIndex = 13;
            // 
            // lbl_cancellationNumber
            // 
            this.lbl_cancellationNumber.AutoSize = true;
            this.lbl_cancellationNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cancellationNumber.Location = new System.Drawing.Point(20, 94);
            this.lbl_cancellationNumber.Name = "lbl_cancellationNumber";
            this.lbl_cancellationNumber.Size = new System.Drawing.Size(114, 17);
            this.lbl_cancellationNumber.TabIndex = 9;
            this.lbl_cancellationNumber.Text = "Cancellation No.";
            // 
            // txt_cancellationNumber
            // 
            this.txt_cancellationNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cancellationNumber.Location = new System.Drawing.Point(154, 90);
            this.txt_cancellationNumber.Name = "txt_cancellationNumber";
            this.txt_cancellationNumber.Size = new System.Drawing.Size(177, 23);
            this.txt_cancellationNumber.TabIndex = 10;
            // 
            // lbl_voucherNumber
            // 
            this.lbl_voucherNumber.AutoSize = true;
            this.lbl_voucherNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_voucherNumber.Location = new System.Drawing.Point(20, 53);
            this.lbl_voucherNumber.Name = "lbl_voucherNumber";
            this.lbl_voucherNumber.Size = new System.Drawing.Size(85, 17);
            this.lbl_voucherNumber.TabIndex = 7;
            this.lbl_voucherNumber.Text = "Voucher No.";
            // 
            // txt_voucherNumber
            // 
            this.txt_voucherNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_voucherNumber.Location = new System.Drawing.Point(154, 49);
            this.txt_voucherNumber.Name = "txt_voucherNumber";
            this.txt_voucherNumber.Size = new System.Drawing.Size(177, 23);
            this.txt_voucherNumber.TabIndex = 8;
            // 
            // lbl_reservationNumber
            // 
            this.lbl_reservationNumber.AutoSize = true;
            this.lbl_reservationNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_reservationNumber.Location = new System.Drawing.Point(20, 14);
            this.lbl_reservationNumber.Name = "lbl_reservationNumber";
            this.lbl_reservationNumber.Size = new System.Drawing.Size(109, 17);
            this.lbl_reservationNumber.TabIndex = 5;
            this.lbl_reservationNumber.Text = "Reservation No.";
            // 
            // txt_reservationNumber
            // 
            this.txt_reservationNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_reservationNumber.Location = new System.Drawing.Point(154, 10);
            this.txt_reservationNumber.Name = "txt_reservationNumber";
            this.txt_reservationNumber.Size = new System.Drawing.Size(177, 23);
            this.txt_reservationNumber.TabIndex = 6;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.rbt_waitForPayments);
            this.panel8.Controls.Add(this.rbt_optioned);
            this.panel8.Controls.Add(this.rbt_all);
            this.panel8.Controls.Add(this.rbt_void);
            this.panel8.Controls.Add(this.rbt_noShow);
            this.panel8.Controls.Add(this.rbt_cancelled);
            this.panel8.Controls.Add(this.rbt_active);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel8.Location = new System.Drawing.Point(0, 171);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1274, 38);
            this.panel8.TabIndex = 5;
            // 
            // rbt_waitForPayments
            // 
            this.rbt_waitForPayments.AutoSize = true;
            this.rbt_waitForPayments.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbt_waitForPayments.Location = new System.Drawing.Point(1064, 5);
            this.rbt_waitForPayments.Name = "rbt_waitForPayments";
            this.rbt_waitForPayments.Size = new System.Drawing.Size(151, 22);
            this.rbt_waitForPayments.TabIndex = 5;
            this.rbt_waitForPayments.TabStop = true;
            this.rbt_waitForPayments.Text = "Wait For Payments";
            this.rbt_waitForPayments.UseVisualStyleBackColor = true;
            // 
            // rbt_optioned
            // 
            this.rbt_optioned.AutoSize = true;
            this.rbt_optioned.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbt_optioned.Location = new System.Drawing.Point(866, 5);
            this.rbt_optioned.Name = "rbt_optioned";
            this.rbt_optioned.Size = new System.Drawing.Size(90, 22);
            this.rbt_optioned.TabIndex = 4;
            this.rbt_optioned.TabStop = true;
            this.rbt_optioned.Text = "Optioned";
            this.rbt_optioned.UseVisualStyleBackColor = true;
            // 
            // rbt_all
            // 
            this.rbt_all.AutoSize = true;
            this.rbt_all.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbt_all.Location = new System.Drawing.Point(680, 5);
            this.rbt_all.Name = "rbt_all";
            this.rbt_all.Size = new System.Drawing.Size(44, 22);
            this.rbt_all.TabIndex = 4;
            this.rbt_all.TabStop = true;
            this.rbt_all.Text = "All";
            this.rbt_all.UseVisualStyleBackColor = true;
            this.rbt_all.CheckedChanged += new System.EventHandler(this.rbt_all_CheckedChanged);
            // 
            // rbt_void
            // 
            this.rbt_void.AutoSize = true;
            this.rbt_void.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbt_void.Location = new System.Drawing.Point(523, 5);
            this.rbt_void.Name = "rbt_void";
            this.rbt_void.Size = new System.Drawing.Size(57, 22);
            this.rbt_void.TabIndex = 3;
            this.rbt_void.TabStop = true;
            this.rbt_void.Text = "Void";
            this.rbt_void.UseVisualStyleBackColor = true;
            // 
            // rbt_noShow
            // 
            this.rbt_noShow.AutoSize = true;
            this.rbt_noShow.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbt_noShow.Location = new System.Drawing.Point(347, 5);
            this.rbt_noShow.Name = "rbt_noShow";
            this.rbt_noShow.Size = new System.Drawing.Size(89, 22);
            this.rbt_noShow.TabIndex = 2;
            this.rbt_noShow.TabStop = true;
            this.rbt_noShow.Text = "No Show";
            this.rbt_noShow.UseVisualStyleBackColor = true;
            // 
            // rbt_cancelled
            // 
            this.rbt_cancelled.AutoSize = true;
            this.rbt_cancelled.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbt_cancelled.Location = new System.Drawing.Point(178, 5);
            this.rbt_cancelled.Name = "rbt_cancelled";
            this.rbt_cancelled.Size = new System.Drawing.Size(93, 22);
            this.rbt_cancelled.TabIndex = 1;
            this.rbt_cancelled.TabStop = true;
            this.rbt_cancelled.Text = "Cancelled";
            this.rbt_cancelled.UseVisualStyleBackColor = true;
            // 
            // rbt_active
            // 
            this.rbt_active.AutoSize = true;
            this.rbt_active.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbt_active.Location = new System.Drawing.Point(23, 5);
            this.rbt_active.Name = "rbt_active";
            this.rbt_active.Size = new System.Drawing.Size(68, 22);
            this.rbt_active.TabIndex = 0;
            this.rbt_active.TabStop = true;
            this.rbt_active.Text = "Active";
            this.rbt_active.UseVisualStyleBackColor = true;
            this.rbt_active.CheckedChanged += new System.EventHandler(this.rbt_active_CheckedChanged);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.lbl_searchCriteria);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1274, 35);
            this.panel4.TabIndex = 4;
            // 
            // lbl_searchCriteria
            // 
            this.lbl_searchCriteria.AutoSize = true;
            this.lbl_searchCriteria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_searchCriteria.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_searchCriteria.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_searchCriteria.Location = new System.Drawing.Point(3, 6);
            this.lbl_searchCriteria.Name = "lbl_searchCriteria";
            this.lbl_searchCriteria.Size = new System.Drawing.Size(120, 19);
            this.lbl_searchCriteria.TabIndex = 1;
            this.lbl_searchCriteria.Text = "Search Criteria";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgv_reservationList);
            this.panel2.Location = new System.Drawing.Point(12, 269);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1276, 381);
            this.panel2.TabIndex = 4;
            // 
            // dgv_reservationList
            // 
            this.dgv_reservationList.AllowUserToAddRows = false;
            this.dgv_reservationList.AllowUserToDeleteRows = false;
            this.dgv_reservationList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_reservationList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colReservationNumber,
            this.colRoom,
            this.colGuestName,
            this.colReservationDate,
            this.colArrival,
            this.colDeparture,
            this.Column1,
            this.Column2});
            this.dgv_reservationList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_reservationList.Location = new System.Drawing.Point(0, 0);
            this.dgv_reservationList.Name = "dgv_reservationList";
            this.dgv_reservationList.ReadOnly = true;
            this.dgv_reservationList.RowHeadersVisible = false;
            this.dgv_reservationList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_reservationList.Size = new System.Drawing.Size(1276, 381);
            this.dgv_reservationList.TabIndex = 0;
            this.dgv_reservationList.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgv_reservationList_MouseClick);
            // 
            // colReservationNumber
            // 
            this.colReservationNumber.HeaderText = "Res. No";
            this.colReservationNumber.Name = "colReservationNumber";
            this.colReservationNumber.ReadOnly = true;
            this.colReservationNumber.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colReservationNumber.Width = 200;
            // 
            // colRoom
            // 
            this.colRoom.HeaderText = "Purpose of Stay";
            this.colRoom.Name = "colRoom";
            this.colRoom.ReadOnly = true;
            this.colRoom.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colRoom.Width = 200;
            // 
            // colGuestName
            // 
            this.colGuestName.HeaderText = "Guest Name";
            this.colGuestName.Name = "colGuestName";
            this.colGuestName.ReadOnly = true;
            this.colGuestName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colGuestName.Width = 200;
            // 
            // colReservationDate
            // 
            this.colReservationDate.HeaderText = "Res. Date";
            this.colReservationDate.Name = "colReservationDate";
            this.colReservationDate.ReadOnly = true;
            this.colReservationDate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colReservationDate.Width = 200;
            // 
            // colArrival
            // 
            this.colArrival.HeaderText = "Arrival";
            this.colArrival.Name = "colArrival";
            this.colArrival.ReadOnly = true;
            this.colArrival.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colArrival.Width = 200;
            // 
            // colDeparture
            // 
            this.colDeparture.HeaderText = "Departure";
            this.colDeparture.Name = "colDeparture";
            this.colDeparture.ReadOnly = true;
            this.colDeparture.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colDeparture.Width = 200;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "fname";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "lname";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Visible = false;
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_close.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_close.FlatAppearance.BorderSize = 0;
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_close.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_close.ForeColor = System.Drawing.Color.White;
            this.btn_close.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_close.Location = new System.Drawing.Point(1136, 13);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(128, 35);
            this.btn_close.TabIndex = 5;
            this.btn_close.Text = "Close";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.btn_new);
            this.panel3.Controls.Add(this.btn_edit);
            this.panel3.Controls.Add(this.btn_void);
            this.panel3.Controls.Add(this.btn_close);
            this.panel3.Location = new System.Drawing.Point(12, 656);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1276, 62);
            this.panel3.TabIndex = 6;
            // 
            // btn_new
            // 
            this.btn_new.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_new.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_new.FlatAppearance.BorderSize = 0;
            this.btn_new.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_new.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_new.ForeColor = System.Drawing.Color.White;
            this.btn_new.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_new.Location = new System.Drawing.Point(680, 13);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(128, 35);
            this.btn_new.TabIndex = 8;
            this.btn_new.Text = "New";
            this.btn_new.UseVisualStyleBackColor = false;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // btn_edit
            // 
            this.btn_edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_edit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_edit.FlatAppearance.BorderSize = 0;
            this.btn_edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_edit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_edit.ForeColor = System.Drawing.Color.White;
            this.btn_edit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_edit.Location = new System.Drawing.Point(834, 13);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(128, 35);
            this.btn_edit.TabIndex = 7;
            this.btn_edit.Text = "Edit";
            this.btn_edit.UseVisualStyleBackColor = false;
            // 
            // btn_void
            // 
            this.btn_void.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_void.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_void.FlatAppearance.BorderSize = 0;
            this.btn_void.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_void.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_void.ForeColor = System.Drawing.Color.White;
            this.btn_void.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_void.Location = new System.Drawing.Point(990, 13);
            this.btn_void.Name = "btn_void";
            this.btn_void.Size = new System.Drawing.Size(128, 35);
            this.btn_void.TabIndex = 6;
            this.btn_void.Text = "Void";
            this.btn_void.UseVisualStyleBackColor = false;
            // 
            // ReservationList_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.ClientSize = new System.Drawing.Size(1300, 730);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.guestFolio_panel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ReservationList_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ReservationList_Form";
            this.Load += new System.EventHandler(this.ReservationList_Form_Load);
            this.guestFolio_panel.ResumeLayout(false);
            this.guestFolio_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_reservationList)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_reservationList)).EndInit();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel guestFolio_panel;
        private System.Windows.Forms.Label lbl_reservationList;
        private System.Windows.Forms.PictureBox pcb_reservationList;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.Button btn_edit;
        private System.Windows.Forms.Button btn_void;
        private System.Windows.Forms.DataGridView dgv_reservationList;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lbl_searchCriteria;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dtp_cancellationEnd;
        private System.Windows.Forms.DateTimePicker dtp_cancellation_start;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtp_reservationDate_end;
        private System.Windows.Forms.DateTimePicker dtp_reservationDate_start;
        private System.Windows.Forms.DateTimePicker dtp_arrivalDate;
        private System.Windows.Forms.CheckBox chb_cancelationDate;
        private System.Windows.Forms.CheckBox chb_reservationDate;
        private System.Windows.Forms.CheckBox chb_arrivalDate;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ComboBox cmb_source;
        private System.Windows.Forms.ComboBox cmb_roomType;
        private System.Windows.Forms.TextBox txt_lastName;
        private System.Windows.Forms.Label lbl_source;
        private System.Windows.Forms.Label lbl_roomType;
        private System.Windows.Forms.Label lbl_firstNameAndLastName;
        private System.Windows.Forms.TextBox txt_firstName;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lbl_cancellationNumber;
        private System.Windows.Forms.TextBox txt_cancellationNumber;
        private System.Windows.Forms.Label lbl_voucherNumber;
        private System.Windows.Forms.TextBox txt_voucherNumber;
        private System.Windows.Forms.Label lbl_reservationNumber;
        private System.Windows.Forms.TextBox txt_reservationNumber;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.RadioButton rbt_waitForPayments;
        private System.Windows.Forms.RadioButton rbt_optioned;
        private System.Windows.Forms.RadioButton rbt_all;
        private System.Windows.Forms.RadioButton rbt_void;
        private System.Windows.Forms.RadioButton rbt_noShow;
        private System.Windows.Forms.RadioButton rbt_cancelled;
        private System.Windows.Forms.RadioButton rbt_active;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReservationNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRoom;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGuestName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReservationDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colArrival;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeparture;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    }
}