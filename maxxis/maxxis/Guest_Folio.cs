﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace maxxis
{
    public partial class Guest_Folio : Form
    {
        string con = System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString;

        //variables
        string pwdselect;
        string urselect;
        string ucrselect;
        string uvrselect;
        int numbernights;
        //methods

        public void DisplayBilling()
        {
            Main_Form mainfrm = new Main_Form();
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT * FROM dbo.tbl_billing Where guest_id = '"+ mainfrm.SelectedGuest +"'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    txt_requiredPrePayment.Text = item[2].ToString();
                    txt_cashPayment.Text = item[5].ToString();
                    if (Int32.Parse(pwdselect) != 0)
                    {
                        dgv_billingSummary.Rows.Add("PWD Urban Room", pwdselect, "ROOM", item[12].ToString(), Int32.Parse(pwdselect) * Int32.Parse(item[12].ToString())  * numbernights);
                    }
                    if (Int32.Parse(ucrselect) != 0)
                    {
                        dgv_billingSummary.Rows.Add("Urban Corner Room", ucrselect, "ROOM", item[9].ToString(), Int32.Parse(ucrselect) * Int32.Parse(item[9].ToString()) * numbernights);
                    }
                    if (Int32.Parse(urselect) != 0)
                    {
                        dgv_billingSummary.Rows.Add("Urban Room", urselect, "ROOM",item[8].ToString(), Int32.Parse(urselect) * Int32.Parse(item[8].ToString()) * numbernights);
                    }
                    if (Int32.Parse(uvrselect) != 0)
                    {
                        dgv_billingSummary.Rows.Add("Urban View Room", uvrselect, "ROOM", item[10].ToString(), Int32.Parse(uvrselect) * Int32.Parse(item[10].ToString()) * numbernights);
                    }
                    if (Decimal.Parse(item[7].ToString()) != 0)
                    {
                        dgv_billingSummary.Rows.Add("Items/Services", "", "Item/Services", "", item[7].ToString());
                    }
                    dgv_payments.Rows.Clear();
                    dgv_payments.Rows.Add("Total Amount", item[2]);
                    dgv_payments.Rows.Add("(Less) Discount");
                    dgv_payments.Rows.Add("(Less) Pre-Authorized Amount", item[1]);
                    txt_otherCharges.Text = item[7].ToString();
                    //txt_subTotal.Text = item[1].ToString();
                    txt_totalPayment.Text = item[2].ToString();
                    txt_balance.Text = (Decimal.Parse(item[2].ToString()) - Decimal.Parse(item[5].ToString())).ToString("N2");
                    txt_cashPayment.Text = item[11].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void DisplayGuestInfo()
        {
            Main_Form mainfrm = new Main_Form();
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT * FROM dbo.tbl_guestinfo where id = '" + mainfrm.SelectedGuest + "'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    cbox_title.Text = item[1].ToString();
                    txt_lastname.Text = item[2].ToString();
                    txt_firstname.Text = item[3].ToString();
                    txt_middleName.Text = item[4].ToString();
                    dtb_dateOfBirth.Text = item[5].ToString();
                    txt_contactDetails.Text = item[6].ToString();
                    txt_address.Text = item[7].ToString();
                    txt_email.Text = item[8].ToString();
                    txt_province.Text = item[9].ToString();
                    cmb_homeCountry.Text = item[10].ToString();
                    txt_companyAgency.Text = item[11].ToString();
                    txt_city.Text = item[12].ToString();
                    txt_guestRemarks.Text = item[20].ToString();
                    txt_serialNumber.Text =  item[21].ToString();
                    urselect = item[17].ToString();
                    ucrselect = item[18].ToString();
                    uvrselect = item[19].ToString();
                    pcb_image.Image = Image.FromFile(item[22].ToString());
                    pcb_image.SizeMode = PictureBoxSizeMode.StretchImage;
                    pwdselect = item[23].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void DisplayPayments()
        {
            Main_Form mainfrm = new Main_Form();
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT * FROM dbo.tbl_billing Where guest_id = '" + mainfrm.SelectedGuest + "'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void DisplayBusiness()
        {
            Main_Form mainfrm = new Main_Form();
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT * FROM dbo.tbl_billing Where guest_id = '" + mainfrm.SelectedGuest + "'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void DisplayStayInfo()
        {
            Main_Form mainfrm = new Main_Form();
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT * FROM dbo.tbl_stayinfo Where guest_id = '" + mainfrm.SelectedGuest + "'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    dtp_arrivalDate.Text = item[2].ToString();
                    dtp_departureDate.Text = item[3].ToString();
                    txt_noofnights.Text = item[4].ToString();
                    numbernights = Int32.Parse(item[4].ToString());
                    txt_adults.Text = item[6].ToString();
                    txt_child.Text = item[7].ToString();
                    txt_totalNumberOfAdults.Text = item[6].ToString();
                    total_numberOfChildren.Text = item[7].ToString();
                    lbl_additionalCompanionAdults.Text = "Adult/s(" + item[8] +")";
                    lbl_additionalCompanionChildren.Text = "Child/ren(" + item[9] + ")";
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void Checkout()
        {
            Main_Form mainfrm = new Main_Form();
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "update tbl_rooms set occupied = 0, dirty = 1,guest_id = '',available = 1 where room_no = '" + mainfrm.SelectedRoom + "' ;";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void CheckoutGuest()
        {
            Main_Form mainfrm = new Main_Form();
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "update tbl_guestinfo set express_checkout_active = 1 where id = '" + mainfrm.SelectedGuest + "' ;";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public Guest_Folio()
        {
            InitializeComponent();
        }
        

        private void lbl_showRates_Click(object sender, EventArgs e)
        {
            this.Hide();
            CompanionNames_Form CNF = new CompanionNames_Form();
            CNF.ShowDialog();
            this.Close();
        }
        internal void Guest_Folio_Load(object sender, EventArgs e)
        {
            Main_Form mainfrm = new Main_Form();
            DisplayGuestInfo();
            DisplayBusiness();
            DisplayPayments();
            DisplayStayInfo();
            DisplayBilling();
        }

        private void btn_GoBack_Click(object sender, EventArgs e)
        {
            Main_Form mainfrm = new Main_Form();
            this.Hide();
            mainfrm.Main_Form_Load(mainfrm, EventArgs.Empty);
            mainfrm.Show();
        }

        private void btn_checkOut_Click(object sender, EventArgs e)
        {
            if((decimal.Parse(txt_cashhanded.Text) - decimal.Parse(txt_cashPayment.Text))>=0)
            {
                Main_Form mainfrm = new Main_Form();
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to check out?", "Confirmation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    Checkout();
                    CheckoutGuest();
                    this.Hide();
                    mainfrm.Show();
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
            else
            {
                MessageBox.Show("Please Transact Remaining Balance Before Checking Out");
            }
            
        }

        private void pcb_search_Click(object sender, EventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txt_cashhanded_TextChanged(object sender, EventArgs e)
        {
            if (txt_cashhanded.Text != "")
            {
                txt_change.Text = (decimal.Parse(txt_cashhanded.Text) - decimal.Parse(txt_cashPayment.Text)).ToString("N2");
            }
        }
    }
}
