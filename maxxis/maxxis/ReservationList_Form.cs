﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace maxxis
{
    public partial class ReservationList_Form : Form
    {
        //SQL
        string con = System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString;
        Main_Form mainfrm = new Main_Form();
        //variables
        string fName;
        string Lname;

        string guestId;

        string lname;
        string fname;
        string mname;
        string email;
        string contact;
        string ur;
        string urchild;
        string uradult;
        string uc;
        string ucchild;
        string ucadult;
        string uv;
        string uvchild;
        string uvadult;
        string extraadult;
        string extrachildren;
        string checkin;
        string checkout;
        string purposeofstay;
        string preauth;
        string total;
        string noofnights;
        string title;
        string urrate;
        string ucrate;
        string uvrate;

        string selectedUR;
        string selectedUCR;
        string selectedUVR;

        public ReservationList_Form()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            Main_Form frm = new Main_Form();
            this.Hide();
            frm.Show();
        }

        //methods
        public void Display()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select refno, fname + '" + " "+"' + lname AS GuestName, tnxdate,cin,cout,fname,lname,purpose_stay from tbl_booking", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dgv_reservationList.Rows.Clear();
                foreach (DataRow item in dt.Rows)
                {
                    int n = dgv_reservationList.Rows.Add();
                    dgv_reservationList.Rows[n].Cells[0].Value = item[0].ToString();
                    dgv_reservationList.Rows[n].Cells[1].Value = item[7].ToString();
                    dgv_reservationList.Rows[n].Cells[2].Value = item[1].ToString();
                    dgv_reservationList.Rows[n].Cells[3].Value = item[2].ToString();
                    dgv_reservationList.Rows[n].Cells[4].Value = item[3].ToString();
                    dgv_reservationList.Rows[n].Cells[5].Value = item[4].ToString();
                    dgv_reservationList.Rows[n].Cells[6].Value = item[5].ToString();
                    dgv_reservationList.Rows[n].Cells[7].Value = item[6].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void DisplayWalkin()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT dbo.tbl_guestinfo.res_no, dbo.tbl_guestinfo.firstname + dbo.tbl_guestinfo.lastname AS FullName,dbo.tbl_guestinfo.res_date, dbo.tbl_stayinfo.arrival, dbo.tbl_stayinfo.departure,dbo.tbl_guestinfo.firstname,dbo.tbl_guestinfo.lastname, dbo.tbl_businesssource.purposeofstay FROM dbo.tbl_guestinfo INNER JOIN dbo.tbl_stayinfo ON dbo.tbl_guestinfo.id = dbo.tbl_stayinfo.guest_id INNER JOIN dbo.tbl_businesssource ON dbo.tbl_guestinfo.id = dbo.tbl_businesssource.guest_id WHERE dbo.tbl_guestinfo.reserved = 1;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    int n = dgv_reservationList.Rows.Add();
                    dgv_reservationList.Rows[n].Cells[0].Value = item[0].ToString();
                    dgv_reservationList.Rows[n].Cells[1].Value = item[7].ToString();
                    dgv_reservationList.Rows[n].Cells[2].Value = item[1].ToString();
                    dgv_reservationList.Rows[n].Cells[3].Value = item[2].ToString();
                    dgv_reservationList.Rows[n].Cells[4].Value = item[3].ToString();
                    dgv_reservationList.Rows[n].Cells[5].Value = item[4].ToString();
                    dgv_reservationList.Rows[n].Cells[6].Value = item[5].ToString();
                    dgv_reservationList.Rows[n].Cells[7].Value = item[6].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void ArrivalDisplay()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select refno, fname + '" + " " + "' + lname AS GuestName, tnxdate,cin,cout,fname,lname,purpose_stay from tbl_booking where cin = '"+ dtp_arrivalDate.Text +"'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dgv_reservationList.Rows.Clear();
                foreach (DataRow item in dt.Rows)
                {
                    int n = dgv_reservationList.Rows.Add();
                    dgv_reservationList.Rows[n].Cells[0].Value = item[0].ToString();
                    dgv_reservationList.Rows[n].Cells[1].Value = item[7].ToString();
                    dgv_reservationList.Rows[n].Cells[2].Value = item[1].ToString();
                    dgv_reservationList.Rows[n].Cells[3].Value = item[2].ToString();
                    dgv_reservationList.Rows[n].Cells[4].Value = item[3].ToString();
                    dgv_reservationList.Rows[n].Cells[5].Value = item[4].ToString();
                    dgv_reservationList.Rows[n].Cells[6].Value = item[5].ToString();
                    dgv_reservationList.Rows[n].Cells[7].Value = item[6].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void GuestChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select * from tbl_booking where refno = '" + dgv_reservationList.SelectedRows[0].Cells[0].Value.ToString() + "'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    lname = item[2].ToString();
                    fname = item[3].ToString();
                    mname = item[4].ToString();
                    email = item[5].ToString();
                    contact = item[6].ToString();
                    ur = item[7].ToString();
                    uradult = item[8].ToString();
                    urchild = item[9].ToString();
                    uc = item[10].ToString();
                    ucadult = item[11].ToString();
                    ucchild = item[12].ToString();
                    uv = item[13].ToString();
                    uvadult = item[14].ToString();
                    uvchild = item[15].ToString();
                    urrate = item[16].ToString();
                    ucrate = item[17].ToString();
                    uvrate = item[18].ToString();
                    extraadult = item[19].ToString();
                    extrachildren = item[20].ToString();
                    checkin = item[21].ToString();
                    checkout = item[22].ToString();
                    purposeofstay = item[26].ToString();
                    preauth = item[32].ToString();
                    title = item[30].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void CheckIn()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "insert into tbl_guestinfo(title," +
                    "lastname," +
                    "firstname," +
                    "middlename," +
                    "contact_details," +
                    "email," +
                    "urselect," +
                    "ucrselect," +
                    "uvrselect) values ('"
                    + title
                    + "','" + lname
                    + "','" + fname
                    + "','" + mname
                    + "','" + contact
                    + "','" + email
                    + "','" + ur
                    + "','" + uc
                    + "','" + uv + "')";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
                MessageBox.Show("Guest Checked In");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void RetrieveGuestId()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT MAX(id) FROM tbl_guestinfo;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    guestId = item[0].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void RoomURCheckIn()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "update tbl_rooms set guest_id = ('" + guestId + "'),occupied = 1 where room_type = 'Urban Room' AND room_no = '" + selectedUR + "' ;";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void RoomUCRCheckIn()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "update tbl_rooms set guest_id = ('" + guestId + "'),occupied = 1 where room_type = 'Urban Corner Room' AND room_no = '" + selectedUCR + "' ;";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void RoomUVRCheckIn()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "update tbl_rooms set guest_id = ('" + guestId + "'),occupied = 1 where room_type = 'Urban View Room' AND room_no = '" + selectedUVR + "' ;";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void URRoomChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'Urban Room' AND occupied = 0 ORDER BY room_no ASC ;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    selectedUR = item[0].ToString();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void UCRRoomChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'Urban Corner Room' AND occupied = 0 ORDER BY room_no ASC ;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    selectedUCR = item[0].ToString();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void UVRRoomChecker()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT room_no FROM tbl_rooms Where room_type = 'Urban View Room' AND occupied = 0 ORDER BY room_no ASC ;", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    selectedUVR = item[0].ToString();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void StayInfo()
        {
            try
            {

                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "insert into tbl_stayinfo(guest_id,arrival,departure,no_ofnights,totalpayment,adults,children,extraadults,extrachildren) values('"
                    + guestId
                    + "','" + checkin
                    + "','" + checkout
                    + "','" + noofnights
                    + "','" + total
                    + "' ,'" + (Int32.Parse(uradult) + Int32.Parse(ucadult) + Int32.Parse(uvadult))
                    + "' ,'" + (Int32.Parse(urchild) + Int32.Parse(ucchild) + Int32.Parse(uvchild))
                    + "' ,'" + extraadult
                    + "' ,'" + extrachildren + "');";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void BusinessSource()
        {
            try
            {

                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "insert into tbl_businesssource(guest_id,purposeofstay) values('"
                    + guestId
                    + "','" + purposeofstay + "');";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void Billing()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "insert into tbl_billing(guest_id,preauthpayment,urpayment,ucrpayment,uvrpayment,totalpayment) values('"
                    + guestId
                    + "','" + preauth
                    + "','" + Int32.Parse(ur) * Int32.Parse(urrate)
                    + "','" + Int32.Parse(uc) * Int32.Parse(ucrate)
                    + "','" + Int32.Parse(uv) * Int32.Parse(uvrate)
                    + "','" + total + "');";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void Delete()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "delete tbl_booking where";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                conDatabase.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }




        private void ReservationList_Form_Load(object sender, EventArgs e)
        {
            dtp_arrivalDate.Format = DateTimePickerFormat.Custom;
            dtp_arrivalDate.CustomFormat = "dd/MM/yyyy";
            Display();
            DisplayWalkin();
        }

        private void dgv_reservationList_MouseClick(object sender, MouseEventArgs e)
        {
            txt_reservationNumber.Text = dgv_reservationList.SelectedRows[0].Cells[0].Value.ToString();
            txt_firstName.Text = dgv_reservationList.SelectedRows[0].Cells[6].Value.ToString();
            txt_lastName.Text = dgv_reservationList.SelectedRows[0].Cells[7].Value.ToString();
            GuestChecker();
            noofnights = (Math.Abs((Convert.ToDateTime(checkout) - Convert.ToDateTime(checkin)).TotalDays)).ToString();
            total = (Int32.Parse(preauth) * Int32.Parse(noofnights)).ToString();
        }

        private void dtp_arrivalDate_ValueChanged(object sender, EventArgs e)
        {
            ArrivalDisplay();
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://maxxhotelmakati.com/booking/reservation.php");
        }

        private void rbt_all_CheckedChanged(object sender, EventArgs e)
        {
            Display();
        }

        private void rbt_active_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btn_checkin_Click(object sender, EventArgs e)
        {
            CheckIn();
            RetrieveGuestId();
            if (Int32.Parse(ur) >= 1)
            {
                for (int x = 0; x < Int32.Parse(ur); x++)
                {
                    URRoomChecker();
                    RoomURCheckIn();
                }
            }
            if (Int32.Parse(uc) >= 1)
            {
                for (int x = 0; x < Int32.Parse(uc); x++)
                {
                    UCRRoomChecker();
                    RoomUCRCheckIn();
                }
            }
            if (Int32.Parse(uv) >= 1)
            {
                for (int x = 0; x < Int32.Parse(uv); x++)
                {
                    UVRRoomChecker();
                    RoomUVRCheckIn();
                }
            }

            StayInfo();
            BusinessSource();
            Billing();
            this.Close();
            mainfrm.Main_Form_Load(mainfrm, EventArgs.Empty);
            mainfrm.Show();
        }
    }
}
