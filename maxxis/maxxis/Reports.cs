﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;

namespace maxxis
{
    public partial class Reports : Form
    {
        public Reports()
        {
            InitializeComponent();
        }

        private static string operatorName;
        private static string operatorPosition;

        public string OperatorName
        {
            get { return operatorName; }
            set { operatorName = value; }
        }

        public string OperatorPosition
        {
            get { return operatorPosition; }
            set { operatorPosition = value; }
        }

        public void load_accomodationList()
        {
            try
            {
                using (IDbConnection connection_con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString))
                {

                    connection_con.Open();

                    string query = "SELECT "
                                   + "folioNumber,"
                                   + "fullName,"
                                   + "reservationDate,"
                                   + "checkInDate,"
                                   + "dueOut,"
                                   + "numberOfNights,"
                                   + "preAuthorizedAmount"
                                   + " FROM tbl_accomodationDetailsReport"
                                   + $" WHERE checkInDate between '{dtp_fromDate.Value.ToString("MM/dd/yyyy")}' AND '{dtp_toDate.Value.ToString("MM/dd/yyyy")}'";

                    accomodationListReportBindingSource.DataSource = connection_con.Query<guestArrival_Report>(query, commandType: CommandType.Text);


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void load_guestArrivalReport()
        {
            try
            {
                using (IDbConnection connection_con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString))
                {

                    connection_con.Open();

                    string query = "SELECT "
                                   + "reservationNumber,"
                                   + "fullName,"
                                   + "booking,"
                                   + "roomNumbers,"
                                   + "roomType,"
                                   + "reservationDate,"
                                   + "checkInDate,"
                                   + "dueOut,"
                                   + "transportation,"
                                   + "flightNumber,"
                                   + "pickUpTime,"
                                   + "purposeOfStay,"
                                   + "adults,"
                                   + "children"
                                   + " FROM tbl_guestArrivalReports"
                                   + $" WHERE checkInDate between '{dtp_fromDate.Value.ToString("MM/dd/yyyy")}' AND '{dtp_toDate.Value.ToString("MM/dd/yyyy")}'";

                    guestArrivalReportBindingSource.DataSource = connection_con.Query<guestArrival_Report>(query, commandType: CommandType.Text);


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void load_reservationListReport()
        {
            try
            {
                using (IDbConnection connection_con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString))
                {

                    connection_con.Open();

                    string query = "SELECT "
                                   + "reservationNumber,"
                                   + "fullName,"
                                   + "booking,"
                                   + "roomNumbers,"
                                   + "roomType,"
                                   + "reservationDate,"
                                   + "checkInDate,"
                                   + "dueOut,"
                                   + "transportation,"
                                   + "flightNumber,"
                                   + "pickUpTime,"
                                   + "purposeOfStay,"
                                   + "adults,"
                                   + "children"
                                   + " FROM tbl_reservationListReport"
                                   + $" WHERE reservationDate between '{dtp_fromDate.Value.ToString("MM/dd/yyyy")}' AND '{dtp_toDate.Value.ToString("MM/dd/yyyy")}'";

                    reservationListReportBindingSource.DataSource = connection_con.Query<reservationList_Report>(query, commandType: CommandType.Text);


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void load_cancellationListReport()
        {
            try
            {
                using (IDbConnection connection_con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString))
                {

                    connection_con.Open();

                    string query = "SELECT "
                                    + "reservationNumber,"
                                    + "fullName,"
                                    + "booking,"
                                    + "roomNumbers,"
                                    + "roomType,"
                                    + "reservationDate,"
                                    + "cancellationDate,"
                                    + "checkInDate,"
                                    + "dueOut,"
                                    + "transportation,"
                                    + "flightNumber,"
                                    + "pickUpTime,"
                                    + "purposeOfStay,"
                                    + "adults,"
                                    + "children"
                                    + " FROM tbl_cancellationListReport"
                                   + $" WHERE cancellationDate between '{dtp_fromDate.Value.ToString("MM/dd/yyyy")}' AND '{dtp_toDate.Value.ToString("MM/dd/yyyy")}'";

                    cancellationListReportBindingSource.DataSource = connection_con.Query<CancellationList_Report>(query, commandType: CommandType.Text);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void load_nightCutOffReport()
        {
            try
            {
                using (IDbConnection connection_con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString))
                {

                    connection_con.Open();

                    string query = "SELECT "
                                   + "folioNumber,"
                                   + "reservationNumber,"
                                   + "fullName,"
                                   + "booking,"
                                   + "roomNumbers,"
                                   + "roomTypes,"
                                   + "reservationDate,"
                                   + "cancellationDate,"
                                   + "checkInDate,"
                                   + "dueOut,"
                                   + "checkOutDate,"
                                   + "numberOfNights,"
                                   + "cancellationPenalty,"
                                   + "totalPriceOfAmenities,"
                                   + "totalPriceForLossBreakageFines,"
                                   + "totalBillAccumulated"
                                   + " FROM tbl_nightCutOffReport"
                                   + $" WHERE checkOutDate between '{dtp_fromDate.Value.ToString("MM/dd/yyyy")}' AND '{dtp_toDate.Value.ToString("MM/dd/yyyy")}'";

                    nightCutOffReportBindingSource.DataSource = connection_con.Query<CancellationList_Report>(query, commandType: CommandType.Text);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_loadReport_Click(object sender, EventArgs e)
        {
            load_accomodationList();
            load_guestArrivalReport();
            load_reservationListReport();
            load_cancellationListReport();
            load_nightCutOffReport();
        }

        private void Reports_Load(object sender, EventArgs e)
        {
            operatorName = "Rylle";
            operatorPosition = "Admin";

            dtp_fromDate.Value = DateTime.Today.AddDays(-14);
            dtp_toDate.Value = DateTime.Today;
            dtp_toDate.MaxDate = DateTime.Today;

            load_accomodationList();
            load_guestArrivalReport();
            load_reservationListReport();
            load_cancellationListReport();
            load_nightCutOffReport();
        }

        private void btn_print_GAR_Click(object sender, EventArgs e)
        {
            ReportViewer RV_GAR = new ReportViewer();
            this.Hide();
            RV_GAR.ShowDialog();
            this.Show();
        }

        private void btn_print_RLR_Click(object sender, EventArgs e)
        {
            ReportViewer_RLR RV_RLR = new ReportViewer_RLR();
            this.Hide();
            RV_RLR.ShowDialog();
            this.Show();
        }

        private void btn_print_CLR_Click(object sender, EventArgs e)
        {
            ReportViewer_CLR RV_CLR = new ReportViewer_CLR();
            this.Hide();
            RV_CLR.ShowDialog();
            this.Show();
        }

        private void btn_print_NCO_Click(object sender, EventArgs e)
        {
            ReportViewer_NCO RV_NCO = new ReportViewer_NCO();
            this.Hide();
            RV_NCO.ShowDialog();
            this.Show();
        }

        private void btn_print_AL_Click(object sender, EventArgs e)
        {
            ReportViewer_ALV2 RV_AL = new ReportViewer_ALV2();
            this.Hide();
            RV_AL.ShowDialog();
            this.Show();
        }
    }
}
