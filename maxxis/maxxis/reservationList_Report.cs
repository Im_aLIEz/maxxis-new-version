﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace maxxis
{
    class reservationList_Report
    {
        public string ReservationNumber { get; set; }

        public string FullName { get; set; }

        public string Booking { get; set; }

        public string RoomNumbers { get; set; }

        public string RoomType { get; set; }

        public string ReservationDate { get; set; }

        public string CheckInDate { get; set; }

        public string DueOut { get; set; }

        public string Transportation { get; set; }

        public string FlightNumber { get; set; }

        public string PickUpTime { get; set; }

        public string PurposeOfStay { get; set; }

        public int Adults { get; set; }

        public int Children { get; set; }

    }
}
