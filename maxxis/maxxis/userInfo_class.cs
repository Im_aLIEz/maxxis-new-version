﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace maxxis
{
    class userInfo_class
    {
        /*******************************************************************************
       * Variables and Instantiations                                                 *
       *******************************************************************************/
        //Connection String
        string con;

        //Database connection and reader
        SqlConnection conDatabase;
        SqlCommand cmdDataBase;
        SqlDataReader dataReader;

        //Instantiations
        auditTrail_class ATC;
        login_class LC;

        //Values
        bool userExist;
        string userName;

        /*******************************************************************************
        * Protected Properties                                                         *
        *******************************************************************************/



        /*******************************************************************************
        * Private Properties                                                           *
        *******************************************************************************/
        private static string userId;
        private static string fullName;
        private static string shiftNumber;
        private static string position;


        /*******************************************************************************
        * Public Properties                                                            *
        *******************************************************************************/
        public string UserID
        {
            get { return userId; }
            set { userId = value; }
        }

        public string FullName
        {
            get { return fullName; }
            set { userName = value; }
        }

        public string ShiftNumber
        {
            get { return shiftNumber; }
            set { shiftNumber = value; }
        }

        public string Position
        {
            get { return position; }
            set { position = value; }
        }


        /*******************************************************************************
        * Protected Methods                                                            *
        *******************************************************************************/


        /*******************************************************************************
        * Private Methods                                                              *
        *******************************************************************************/
        protected void defaultValues()
        {
            userExist = false;
            userId = "";
        }


        /*******************************************************************************
        * Public Methods                                                               *
        *******************************************************************************/
        public void getUserInfo_login(string UserId_param)
        {
            //Returns variables to original value
            defaultValues();

            //call instantiations
            ATC = new auditTrail_class();

            userId = UserId_param;

            //connection string
            con = System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString;

            conDatabase = new SqlConnection(con);

            conDatabase.Open();

            //Query
            cmdDataBase = new SqlCommand("SELECT dbo.tbl_login.user_id, dbo.tbl_login.username, dbo.tbl_employee.fullName, dbo.tbl_employee.shiftNumber, dbo.tbl_login.position " +
                                         "FROM dbo.tbl_login INNER JOIN " +
                                         "dbo.tbl_employee ON dbo.tbl_login.user_id = dbo.tbl_employee.user_id " +
                                         "WHERE (dbo.tbl_login.user_id = @User_id)", conDatabase);

            cmdDataBase.Parameters.AddWithValue("@User_id", UserId_param);

            //Execute the query
            dataReader = cmdDataBase.ExecuteReader();

            //Checks if the query returned a value
            while (dataReader.Read())
            {
                if (dataReader["user_id"].ToString().Equals(UserId_param))
                {
                    userId = dataReader["user_id"].ToString();
                    userName = dataReader["username"].ToString();
                    fullName = dataReader["fullName"].ToString();
                    position = dataReader["position"].ToString();
                    shiftNumber = dataReader["shiftNumber"].ToString();
                }
            }

            ATC.recordLogin(userId, userName, fullName, shiftNumber, position);

            conDatabase.Close();
        }


        public void getUserInfo_logOut(string UserId_param)
        {
            //Returns variables to original value
            defaultValues();

            //call instantiations
            ATC = new auditTrail_class();

            userId = UserId_param;

            //connection string
            con = System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString;

            conDatabase = new SqlConnection(con);

            conDatabase.Open();

            //Query
            cmdDataBase = new SqlCommand("SELECT dbo.tbl_login.user_id, dbo.tbl_login.username, dbo.tbl_employee.fullName, dbo.tbl_login.position " +
                                         "FROM dbo.tbl_login INNER JOIN " +
                                         "dbo.tbl_employee ON dbo.tbl_login.user_id = dbo.tbl_employee.user_id " +
                                         "WHERE (dbo.tbl_login.user_id = @User_id)", conDatabase);

            cmdDataBase.Parameters.AddWithValue("@User_id", UserId_param);

            //Execute the query
            dataReader = cmdDataBase.ExecuteReader();

            //Checks if the query returned a value
            while (dataReader.Read())
            {
                if (dataReader["user_id"].ToString().Equals(UserId_param))
                {
                    userId = dataReader["user_id"].ToString();
                    userName = dataReader["username"].ToString();
                    fullName = dataReader["fullName"].ToString();
                    position = dataReader["position"].ToString();
                }
            }

            ATC.recordLogout(userId, userName, fullName, shiftNumber, position);

            conDatabase.Close();
        }
    }
}
