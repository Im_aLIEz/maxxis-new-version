﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace maxxis
{
    public partial class ReportViewer_ALV2 : Form
    {
        public ReportViewer_ALV2()
        {
            InitializeComponent();
        }

        private void ReportViewer_ALV2_Load(object sender, EventArgs e)
        {
            Reports rp = new Reports();


            dtp_fromDate.Value = DateTime.Today.AddDays(-14);

            dtp_toDate.Value = DateTime.Today;
            dtp_toDate.MaxDate = DateTime.Today;

            ReportParameter[] param = new ReportParameter[4];
            param[0] = new ReportParameter("fromDate", dtp_fromDate.Value.ToString("MMM dd, yyyy"));
            param[1] = new ReportParameter("toDate", dtp_toDate.Value.ToString("MMM dd, yyyy"));
            param[2] = new ReportParameter("operatorName", rp.OperatorName);
            param[3] = new ReportParameter("operatorPosition", rp.OperatorPosition);

            this.rv_reportViewer_AL.LocalReport.SetParameters(param);

            // TODO: This line of code loads data into the 'AL_DataReportV2.tbl_accomodationDetailsReport' table. You can move, or remove it, as needed.
            this.tbl_accomodationDetailsReportTableAdapter.Fill(this.AL_DataReportV2.tbl_accomodationDetailsReport, dtp_fromDate.Text, dtp_toDate.Text);

            this.rv_reportViewer_AL.RefreshReport();
        }

        private void btn_loadReport_Click(object sender, EventArgs e)
        {
            Reports rp = new Reports();
            ReportParameter[] param = new ReportParameter[2];
            param[0] = new ReportParameter("fromDate", dtp_fromDate.Value.ToString("MMM dd, yyyy"));
            param[1] = new ReportParameter("toDate", dtp_toDate.Value.ToString("MMM dd, yyyy"));
            this.rv_reportViewer_AL.LocalReport.SetParameters(param);

            // TODO: This line of code loads data into the 'AL_DataReportV2.tbl_accomodationDetailsReport' table. You can move, or remove it, as needed.
            this.tbl_accomodationDetailsReportTableAdapter.Fill(this.AL_DataReportV2.tbl_accomodationDetailsReport, dtp_fromDate.Text, dtp_toDate.Text);

            this.rv_reportViewer_AL.RefreshReport();
        }
    }
}
