﻿namespace maxxis
{
    partial class ReservationV2_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.newWalkinReservation_Panel = new System.Windows.Forms.Panel();
            this.lbl_newWalkinReservation = new System.Windows.Forms.Label();
            this.pcb_newWalkinReservation = new System.Windows.Forms.PictureBox();
            this.pnl_hotelInfo = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_hotelAddress = new System.Windows.Forms.Label();
            this.lbl_hotelStreet = new System.Windows.Forms.Label();
            this.lbl_maxxHotelMakati = new System.Windows.Forms.Label();
            this.pcb_maxxLogo = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnl_accomodationDetails = new System.Windows.Forms.Panel();
            this.lbl_nights = new System.Windows.Forms.Label();
            this.lbl_numberOfNights = new System.Windows.Forms.Label();
            this.btn_companionName = new System.Windows.Forms.Button();
            this.cbox_children = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbox_adult = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbox_extrachildren = new System.Windows.Forms.ComboBox();
            this.cbox_extraadults = new System.Windows.Forms.ComboBox();
            this.numUD_roomQuantity = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_guestRemarks = new System.Windows.Forms.TextBox();
            this.lbl_guestRemarks = new System.Windows.Forms.Label();
            this.btn_viewRoomDetails = new System.Windows.Forms.Button();
            this.lbl_amount = new System.Windows.Forms.Label();
            this.txt_childTotalFee = new System.Windows.Forms.TextBox();
            this.txt_adultsTotalFee = new System.Windows.Forms.TextBox();
            this.lbl_feePerNight = new System.Windows.Forms.Label();
            this.txt_childFee = new System.Windows.Forms.TextBox();
            this.txt_adultsFee = new System.Windows.Forms.TextBox();
            this.lbl_child = new System.Windows.Forms.Label();
            this.lbl_adults = new System.Windows.Forms.Label();
            this.lbl_additionalCompanion = new System.Windows.Forms.Label();
            this.dgv_listOfRooms = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_RoomNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_RoomType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_RoomRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Adult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Child = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Available = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbl_notification = new System.Windows.Forms.Label();
            this.dtp_departureDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_arrivalDate = new System.Windows.Forms.DateTimePicker();
            this.lbl_checkOut = new System.Windows.Forms.Label();
            this.lbl_chekIn = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txt_accomo_accomodationTotal = new System.Windows.Forms.TextBox();
            this.lbl_accomodationTotal = new System.Windows.Forms.Label();
            this.lbl_accomodationDetails = new System.Windows.Forms.Label();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_reserveNow = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btn_apply = new System.Windows.Forms.Button();
            this.txt_discountDescription = new System.Windows.Forms.TextBox();
            this.lbl_discountDescription = new System.Windows.Forms.Label();
            this.lbl_discountAmount = new System.Windows.Forms.Label();
            this.txt_discountAmount = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lbl_discount = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.lbl_credit = new System.Windows.Forms.Label();
            this.lbl_type = new System.Windows.Forms.Label();
            this.cmb_type = new System.Windows.Forms.ComboBox();
            this.lbl_cardNumber = new System.Windows.Forms.Label();
            this.txt_cardNumber = new System.Windows.Forms.TextBox();
            this.lbl_expire = new System.Windows.Forms.Label();
            this.txt_cvv = new System.Windows.Forms.TextBox();
            this.txt_expire = new System.Windows.Forms.TextBox();
            this.lbl_cvv = new System.Windows.Forms.Label();
            this.panel35 = new System.Windows.Forms.Panel();
            this.txt_change = new System.Windows.Forms.TextBox();
            this.lbl_change = new System.Windows.Forms.Label();
            this.txt_cashhanded = new System.Windows.Forms.TextBox();
            this.lbl_cashHandedByGuest = new System.Windows.Forms.Label();
            this.panel36 = new System.Windows.Forms.Panel();
            this.lbl_cash = new System.Windows.Forms.Label();
            this.txt_cashPayment = new System.Windows.Forms.TextBox();
            this.lbl_cashPayment = new System.Windows.Forms.Label();
            this.settlementOption_panel = new System.Windows.Forms.Panel();
            this.lbl_settlementOption = new System.Windows.Forms.Label();
            this.lbl_requiredPrePayment = new System.Windows.Forms.Label();
            this.txt_requiredPrePayment = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_visaNumber = new System.Windows.Forms.Label();
            this.txt_visaNumber = new System.Windows.Forms.TextBox();
            this.lbl_passportNumber = new System.Windows.Forms.Label();
            this.txt_passportNumber = new System.Windows.Forms.TextBox();
            this.cmb_homeCountry = new System.Windows.Forms.ComboBox();
            this.lbl_homeCountry = new System.Windows.Forms.Label();
            this.lbl_companyAgency = new System.Windows.Forms.Label();
            this.txt_companyAgency = new System.Windows.Forms.TextBox();
            this.lbl_province = new System.Windows.Forms.Label();
            this.txt_province = new System.Windows.Forms.TextBox();
            this.lbl_city = new System.Windows.Forms.Label();
            this.txt_city = new System.Windows.Forms.TextBox();
            this.lbl_address = new System.Windows.Forms.Label();
            this.txt_address = new System.Windows.Forms.TextBox();
            this.lbl_dateOfBirth = new System.Windows.Forms.Label();
            this.dtb_dateOfBirth = new System.Windows.Forms.DateTimePicker();
            this.btn_scanImage = new System.Windows.Forms.Button();
            this.btn_attachImage = new System.Windows.Forms.Button();
            this.pcb_image = new System.Windows.Forms.PictureBox();
            this.txt_contactDetails = new System.Windows.Forms.TextBox();
            this.lbl_contactDetails = new System.Windows.Forms.Label();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.lbl_email = new System.Windows.Forms.Label();
            this.txt_lastName = new System.Windows.Forms.TextBox();
            this.lbl_lastName = new System.Windows.Forms.Label();
            this.txt_middleName = new System.Windows.Forms.TextBox();
            this.lbl_middleName = new System.Windows.Forms.Label();
            this.txt_firstName = new System.Windows.Forms.TextBox();
            this.lbl_firstName = new System.Windows.Forms.Label();
            this.cmb_title = new System.Windows.Forms.ComboBox();
            this.lbl_title = new System.Windows.Forms.Label();
            this.accomo_panel = new System.Windows.Forms.Panel();
            this.lbl_guestInformation = new System.Windows.Forms.Label();
            this.newWalkinReservation_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_newWalkinReservation)).BeginInit();
            this.pnl_hotelInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_maxxLogo)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnl_accomodationDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_roomQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listOfRooms)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel36.SuspendLayout();
            this.settlementOption_panel.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_image)).BeginInit();
            this.accomo_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // newWalkinReservation_Panel
            // 
            this.newWalkinReservation_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.newWalkinReservation_Panel.Controls.Add(this.lbl_newWalkinReservation);
            this.newWalkinReservation_Panel.Controls.Add(this.pcb_newWalkinReservation);
            this.newWalkinReservation_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.newWalkinReservation_Panel.Location = new System.Drawing.Point(0, 0);
            this.newWalkinReservation_Panel.Name = "newWalkinReservation_Panel";
            this.newWalkinReservation_Panel.Size = new System.Drawing.Size(1300, 43);
            this.newWalkinReservation_Panel.TabIndex = 10;
            // 
            // lbl_newWalkinReservation
            // 
            this.lbl_newWalkinReservation.AutoSize = true;
            this.lbl_newWalkinReservation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_newWalkinReservation.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_newWalkinReservation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_newWalkinReservation.Location = new System.Drawing.Point(38, 13);
            this.lbl_newWalkinReservation.Name = "lbl_newWalkinReservation";
            this.lbl_newWalkinReservation.Size = new System.Drawing.Size(191, 19);
            this.lbl_newWalkinReservation.TabIndex = 0;
            this.lbl_newWalkinReservation.Text = "New Walk-in Reservation";
            // 
            // pcb_newWalkinReservation
            // 
            this.pcb_newWalkinReservation.BackgroundImage = global::maxxis.Properties.Resources.New_walk_in_2;
            this.pcb_newWalkinReservation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_newWalkinReservation.Dock = System.Windows.Forms.DockStyle.Left;
            this.pcb_newWalkinReservation.Location = new System.Drawing.Point(0, 0);
            this.pcb_newWalkinReservation.Name = "pcb_newWalkinReservation";
            this.pcb_newWalkinReservation.Size = new System.Drawing.Size(41, 43);
            this.pcb_newWalkinReservation.TabIndex = 1;
            this.pcb_newWalkinReservation.TabStop = false;
            // 
            // pnl_hotelInfo
            // 
            this.pnl_hotelInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.pnl_hotelInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_hotelInfo.Controls.Add(this.label1);
            this.pnl_hotelInfo.Controls.Add(this.lbl_hotelAddress);
            this.pnl_hotelInfo.Controls.Add(this.lbl_hotelStreet);
            this.pnl_hotelInfo.Controls.Add(this.lbl_maxxHotelMakati);
            this.pnl_hotelInfo.Controls.Add(this.pcb_maxxLogo);
            this.pnl_hotelInfo.Location = new System.Drawing.Point(158, 6);
            this.pnl_hotelInfo.Name = "pnl_hotelInfo";
            this.pnl_hotelInfo.Size = new System.Drawing.Size(995, 143);
            this.pnl_hotelInfo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label1.Location = new System.Drawing.Point(424, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(566, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "All fields that is marked with an Asterisk (*) is required and must be not left e" +
    "mpty";
            // 
            // lbl_hotelAddress
            // 
            this.lbl_hotelAddress.AutoSize = true;
            this.lbl_hotelAddress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_hotelAddress.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_hotelAddress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_hotelAddress.Location = new System.Drawing.Point(179, 81);
            this.lbl_hotelAddress.Name = "lbl_hotelAddress";
            this.lbl_hotelAddress.Size = new System.Drawing.Size(207, 17);
            this.lbl_hotelAddress.TabIndex = 3;
            this.lbl_hotelAddress.Text = "Makati City, 1210 PHILIPPINES";
            // 
            // lbl_hotelStreet
            // 
            this.lbl_hotelStreet.AutoSize = true;
            this.lbl_hotelStreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_hotelStreet.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_hotelStreet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_hotelStreet.Location = new System.Drawing.Point(179, 65);
            this.lbl_hotelStreet.Name = "lbl_hotelStreet";
            this.lbl_hotelStreet.Size = new System.Drawing.Size(246, 17);
            this.lbl_hotelStreet.TabIndex = 2;
            this.lbl_hotelStreet.Text = "Makati Avenue corner Singian Street.";
            // 
            // lbl_maxxHotelMakati
            // 
            this.lbl_maxxHotelMakati.AutoSize = true;
            this.lbl_maxxHotelMakati.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_maxxHotelMakati.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxxHotelMakati.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_maxxHotelMakati.Location = new System.Drawing.Point(178, 23);
            this.lbl_maxxHotelMakati.Name = "lbl_maxxHotelMakati";
            this.lbl_maxxHotelMakati.Size = new System.Drawing.Size(288, 32);
            this.lbl_maxxHotelMakati.TabIndex = 1;
            this.lbl_maxxHotelMakati.Text = "MAXX HOTEL MAKATI";
            // 
            // pcb_maxxLogo
            // 
            this.pcb_maxxLogo.BackgroundImage = global::maxxis.Properties.Resources.maxxLogo_5;
            this.pcb_maxxLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pcb_maxxLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcb_maxxLogo.Location = new System.Drawing.Point(15, 13);
            this.pcb_maxxLogo.Name = "pcb_maxxLogo";
            this.pcb_maxxLogo.Size = new System.Drawing.Size(157, 114);
            this.pcb_maxxLogo.TabIndex = 0;
            this.pcb_maxxLogo.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pnl_accomodationDetails);
            this.panel1.Controls.Add(this.btn_cancel);
            this.panel1.Controls.Add(this.btn_reserveNow);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.pnl_hotelInfo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 43);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1300, 707);
            this.panel1.TabIndex = 12;
            // 
            // pnl_accomodationDetails
            // 
            this.pnl_accomodationDetails.AutoScroll = true;
            this.pnl_accomodationDetails.BackColor = System.Drawing.Color.Silver;
            this.pnl_accomodationDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_accomodationDetails.Controls.Add(this.lbl_nights);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_numberOfNights);
            this.pnl_accomodationDetails.Controls.Add(this.btn_companionName);
            this.pnl_accomodationDetails.Controls.Add(this.cbox_children);
            this.pnl_accomodationDetails.Controls.Add(this.label3);
            this.pnl_accomodationDetails.Controls.Add(this.cbox_adult);
            this.pnl_accomodationDetails.Controls.Add(this.label2);
            this.pnl_accomodationDetails.Controls.Add(this.cbox_extrachildren);
            this.pnl_accomodationDetails.Controls.Add(this.cbox_extraadults);
            this.pnl_accomodationDetails.Controls.Add(this.numUD_roomQuantity);
            this.pnl_accomodationDetails.Controls.Add(this.label4);
            this.pnl_accomodationDetails.Controls.Add(this.txt_guestRemarks);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_guestRemarks);
            this.pnl_accomodationDetails.Controls.Add(this.btn_viewRoomDetails);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_amount);
            this.pnl_accomodationDetails.Controls.Add(this.txt_childTotalFee);
            this.pnl_accomodationDetails.Controls.Add(this.txt_adultsTotalFee);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_feePerNight);
            this.pnl_accomodationDetails.Controls.Add(this.txt_childFee);
            this.pnl_accomodationDetails.Controls.Add(this.txt_adultsFee);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_child);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_adults);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_additionalCompanion);
            this.pnl_accomodationDetails.Controls.Add(this.dgv_listOfRooms);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_notification);
            this.pnl_accomodationDetails.Controls.Add(this.dtp_departureDate);
            this.pnl_accomodationDetails.Controls.Add(this.dtp_arrivalDate);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_checkOut);
            this.pnl_accomodationDetails.Controls.Add(this.lbl_chekIn);
            this.pnl_accomodationDetails.Controls.Add(this.panel3);
            this.pnl_accomodationDetails.Location = new System.Drawing.Point(156, 508);
            this.pnl_accomodationDetails.Name = "pnl_accomodationDetails";
            this.pnl_accomodationDetails.Size = new System.Drawing.Size(998, 547);
            this.pnl_accomodationDetails.TabIndex = 20;
            // 
            // lbl_nights
            // 
            this.lbl_nights.AutoSize = true;
            this.lbl_nights.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_nights.Location = new System.Drawing.Point(860, 45);
            this.lbl_nights.Name = "lbl_nights";
            this.lbl_nights.Size = new System.Drawing.Size(18, 19);
            this.lbl_nights.TabIndex = 74;
            this.lbl_nights.Text = "1";
            // 
            // lbl_numberOfNights
            // 
            this.lbl_numberOfNights.AutoSize = true;
            this.lbl_numberOfNights.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_numberOfNights.Location = new System.Drawing.Point(721, 45);
            this.lbl_numberOfNights.Name = "lbl_numberOfNights";
            this.lbl_numberOfNights.Size = new System.Drawing.Size(116, 19);
            this.lbl_numberOfNights.TabIndex = 44;
            this.lbl_numberOfNights.Text = "No. of Night(s):";
            // 
            // btn_companionName
            // 
            this.btn_companionName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_companionName.FlatAppearance.BorderSize = 0;
            this.btn_companionName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_companionName.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_companionName.ForeColor = System.Drawing.Color.White;
            this.btn_companionName.Location = new System.Drawing.Point(699, 408);
            this.btn_companionName.Name = "btn_companionName";
            this.btn_companionName.Size = new System.Drawing.Size(188, 35);
            this.btn_companionName.TabIndex = 75;
            this.btn_companionName.Text = "ADD COMPANION NAME";
            this.btn_companionName.UseVisualStyleBackColor = false;
            // 
            // cbox_children
            // 
            this.cbox_children.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_children.FormattingEnabled = true;
            this.cbox_children.Items.AddRange(new object[] {
            "0"});
            this.cbox_children.Location = new System.Drawing.Point(777, 220);
            this.cbox_children.Name = "cbox_children";
            this.cbox_children.Size = new System.Drawing.Size(60, 26);
            this.cbox_children.TabIndex = 5;
            this.cbox_children.SelectedIndexChanged += new System.EventHandler(this.cbox_children_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(774, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(180, 17);
            this.label3.TabIndex = 72;
            this.label3.Text = "Number of Child/Children*";
            // 
            // cbox_adult
            // 
            this.cbox_adult.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_adult.Items.AddRange(new object[] {
            "0"});
            this.cbox_adult.Location = new System.Drawing.Point(777, 170);
            this.cbox_adult.Name = "cbox_adult";
            this.cbox_adult.Size = new System.Drawing.Size(60, 26);
            this.cbox_adult.TabIndex = 4;
            this.cbox_adult.SelectedIndexChanged += new System.EventHandler(this.cbox_adult_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(774, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 17);
            this.label2.TabIndex = 70;
            this.label2.Text = "Number of Adults*";
            // 
            // cbox_extrachildren
            // 
            this.cbox_extrachildren.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_extrachildren.FormattingEnabled = true;
            this.cbox_extrachildren.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.cbox_extrachildren.Location = new System.Drawing.Point(258, 342);
            this.cbox_extrachildren.Name = "cbox_extrachildren";
            this.cbox_extrachildren.Size = new System.Drawing.Size(60, 26);
            this.cbox_extrachildren.TabIndex = 9;
            // 
            // cbox_extraadults
            // 
            this.cbox_extraadults.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_extraadults.FormattingEnabled = true;
            this.cbox_extraadults.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.cbox_extraadults.Location = new System.Drawing.Point(258, 309);
            this.cbox_extraadults.Name = "cbox_extraadults";
            this.cbox_extraadults.Size = new System.Drawing.Size(60, 26);
            this.cbox_extraadults.TabIndex = 6;
            // 
            // numUD_roomQuantity
            // 
            this.numUD_roomQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numUD_roomQuantity.Location = new System.Drawing.Point(776, 122);
            this.numUD_roomQuantity.Name = "numUD_roomQuantity";
            this.numUD_roomQuantity.Size = new System.Drawing.Size(59, 24);
            this.numUD_roomQuantity.TabIndex = 3;
            this.numUD_roomQuantity.ValueChanged += new System.EventHandler(this.numUD_roomQuantity_ValueChanged);
            this.numUD_roomQuantity.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numUD_roomQuantity_MouseUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(774, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 17);
            this.label4.TabIndex = 66;
            this.label4.Text = "Room Quantity*";
            // 
            // txt_guestRemarks
            // 
            this.txt_guestRemarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_guestRemarks.Location = new System.Drawing.Point(64, 408);
            this.txt_guestRemarks.Multiline = true;
            this.txt_guestRemarks.Name = "txt_guestRemarks";
            this.txt_guestRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_guestRemarks.Size = new System.Drawing.Size(582, 113);
            this.txt_guestRemarks.TabIndex = 12;
            // 
            // lbl_guestRemarks
            // 
            this.lbl_guestRemarks.AutoSize = true;
            this.lbl_guestRemarks.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_guestRemarks.Location = new System.Drawing.Point(60, 386);
            this.lbl_guestRemarks.Name = "lbl_guestRemarks";
            this.lbl_guestRemarks.Size = new System.Drawing.Size(122, 19);
            this.lbl_guestRemarks.TabIndex = 60;
            this.lbl_guestRemarks.Text = "Guest Remarks";
            // 
            // btn_viewRoomDetails
            // 
            this.btn_viewRoomDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_viewRoomDetails.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_viewRoomDetails.FlatAppearance.BorderSize = 0;
            this.btn_viewRoomDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_viewRoomDetails.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_viewRoomDetails.ForeColor = System.Drawing.Color.White;
            this.btn_viewRoomDetails.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_viewRoomDetails.Location = new System.Drawing.Point(699, 457);
            this.btn_viewRoomDetails.Name = "btn_viewRoomDetails";
            this.btn_viewRoomDetails.Size = new System.Drawing.Size(188, 35);
            this.btn_viewRoomDetails.TabIndex = 59;
            this.btn_viewRoomDetails.Text = "VIEW ROOM DETAILS";
            this.btn_viewRoomDetails.UseVisualStyleBackColor = false;
            // 
            // lbl_amount
            // 
            this.lbl_amount.AutoSize = true;
            this.lbl_amount.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_amount.Location = new System.Drawing.Point(470, 288);
            this.lbl_amount.Name = "lbl_amount";
            this.lbl_amount.Size = new System.Drawing.Size(57, 17);
            this.lbl_amount.TabIndex = 58;
            this.lbl_amount.Text = "Amount";
            // 
            // txt_childTotalFee
            // 
            this.txt_childTotalFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_childTotalFee.Location = new System.Drawing.Point(461, 340);
            this.txt_childTotalFee.Name = "txt_childTotalFee";
            this.txt_childTotalFee.ReadOnly = true;
            this.txt_childTotalFee.Size = new System.Drawing.Size(122, 24);
            this.txt_childTotalFee.TabIndex = 11;
            this.txt_childTotalFee.Text = "0.00";
            this.txt_childTotalFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_adultsTotalFee
            // 
            this.txt_adultsTotalFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_adultsTotalFee.Location = new System.Drawing.Point(461, 307);
            this.txt_adultsTotalFee.Name = "txt_adultsTotalFee";
            this.txt_adultsTotalFee.ReadOnly = true;
            this.txt_adultsTotalFee.Size = new System.Drawing.Size(122, 24);
            this.txt_adultsTotalFee.TabIndex = 8;
            this.txt_adultsTotalFee.Text = "0.00";
            this.txt_adultsTotalFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbl_feePerNight
            // 
            this.lbl_feePerNight.AutoSize = true;
            this.lbl_feePerNight.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_feePerNight.Location = new System.Drawing.Point(331, 288);
            this.lbl_feePerNight.Name = "lbl_feePerNight";
            this.lbl_feePerNight.Size = new System.Drawing.Size(73, 17);
            this.lbl_feePerNight.TabIndex = 55;
            this.lbl_feePerNight.Text = "Fee/Night";
            // 
            // txt_childFee
            // 
            this.txt_childFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_childFee.Location = new System.Drawing.Point(324, 340);
            this.txt_childFee.Name = "txt_childFee";
            this.txt_childFee.ReadOnly = true;
            this.txt_childFee.Size = new System.Drawing.Size(122, 24);
            this.txt_childFee.TabIndex = 10;
            this.txt_childFee.Text = "0.00";
            this.txt_childFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_adultsFee
            // 
            this.txt_adultsFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_adultsFee.Location = new System.Drawing.Point(324, 307);
            this.txt_adultsFee.Name = "txt_adultsFee";
            this.txt_adultsFee.ReadOnly = true;
            this.txt_adultsFee.Size = new System.Drawing.Size(122, 24);
            this.txt_adultsFee.TabIndex = 7;
            this.txt_adultsFee.Text = "0.00";
            this.txt_adultsFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbl_child
            // 
            this.lbl_child.AutoSize = true;
            this.lbl_child.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_child.Location = new System.Drawing.Point(207, 340);
            this.lbl_child.Name = "lbl_child";
            this.lbl_child.Size = new System.Drawing.Size(47, 17);
            this.lbl_child.TabIndex = 50;
            this.lbl_child.Text = "Child*";
            // 
            // lbl_adults
            // 
            this.lbl_adults.AutoSize = true;
            this.lbl_adults.BackColor = System.Drawing.Color.Silver;
            this.lbl_adults.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_adults.ForeColor = System.Drawing.Color.Black;
            this.lbl_adults.Location = new System.Drawing.Point(199, 315);
            this.lbl_adults.Name = "lbl_adults";
            this.lbl_adults.Size = new System.Drawing.Size(53, 17);
            this.lbl_adults.TabIndex = 49;
            this.lbl_adults.Text = "Adults*";
            // 
            // lbl_additionalCompanion
            // 
            this.lbl_additionalCompanion.AutoSize = true;
            this.lbl_additionalCompanion.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_additionalCompanion.Location = new System.Drawing.Point(128, 269);
            this.lbl_additionalCompanion.Name = "lbl_additionalCompanion";
            this.lbl_additionalCompanion.Size = new System.Drawing.Size(169, 19);
            this.lbl_additionalCompanion.TabIndex = 48;
            this.lbl_additionalCompanion.Text = "Additional Companion";
            // 
            // dgv_listOfRooms
            // 
            this.dgv_listOfRooms.AllowUserToAddRows = false;
            this.dgv_listOfRooms.AllowUserToDeleteRows = false;
            this.dgv_listOfRooms.BackgroundColor = System.Drawing.Color.White;
            this.dgv_listOfRooms.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 7, 0, 7);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_listOfRooms.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_listOfRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_listOfRooms.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.col_RoomNumber,
            this.col_RoomType,
            this.col_Quantity,
            this.col_RoomRate,
            this.col_Adult,
            this.col_Child,
            this.col_Amount,
            this.col_Available});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_listOfRooms.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_listOfRooms.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv_listOfRooms.EnableHeadersVisualStyles = false;
            this.dgv_listOfRooms.Location = new System.Drawing.Point(60, 101);
            this.dgv_listOfRooms.Name = "dgv_listOfRooms";
            this.dgv_listOfRooms.ReadOnly = true;
            this.dgv_listOfRooms.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_listOfRooms.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_listOfRooms.RowHeadersVisible = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            this.dgv_listOfRooms.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_listOfRooms.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_listOfRooms.Size = new System.Drawing.Size(708, 155);
            this.dgv_listOfRooms.TabIndex = 47;
            this.dgv_listOfRooms.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dgv_listOfRooms_MouseUp);
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            this.id.Width = 125;
            // 
            // col_RoomNumber
            // 
            this.col_RoomNumber.HeaderText = "Room Number";
            this.col_RoomNumber.Name = "col_RoomNumber";
            this.col_RoomNumber.ReadOnly = true;
            this.col_RoomNumber.Visible = false;
            // 
            // col_RoomType
            // 
            this.col_RoomType.HeaderText = "Room Type";
            this.col_RoomType.Name = "col_RoomType";
            this.col_RoomType.ReadOnly = true;
            this.col_RoomType.Width = 210;
            // 
            // col_Quantity
            // 
            this.col_Quantity.HeaderText = "Qty.";
            this.col_Quantity.Name = "col_Quantity";
            this.col_Quantity.ReadOnly = true;
            this.col_Quantity.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.col_Quantity.Width = 50;
            // 
            // col_RoomRate
            // 
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.col_RoomRate.DefaultCellStyle = dataGridViewCellStyle2;
            this.col_RoomRate.HeaderText = "Room Rate";
            this.col_RoomRate.Name = "col_RoomRate";
            this.col_RoomRate.ReadOnly = true;
            this.col_RoomRate.Width = 160;
            // 
            // col_Adult
            // 
            this.col_Adult.HeaderText = "Adult";
            this.col_Adult.Name = "col_Adult";
            this.col_Adult.ReadOnly = true;
            this.col_Adult.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.col_Adult.Width = 80;
            // 
            // col_Child
            // 
            this.col_Child.HeaderText = "Child";
            this.col_Child.Name = "col_Child";
            this.col_Child.ReadOnly = true;
            this.col_Child.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.col_Child.Width = 80;
            // 
            // col_Amount
            // 
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.col_Amount.DefaultCellStyle = dataGridViewCellStyle3;
            this.col_Amount.HeaderText = "Amount";
            this.col_Amount.Name = "col_Amount";
            this.col_Amount.ReadOnly = true;
            this.col_Amount.Width = 110;
            // 
            // col_Available
            // 
            this.col_Available.HeaderText = "Available";
            this.col_Available.Name = "col_Available";
            this.col_Available.ReadOnly = true;
            this.col_Available.Visible = false;
            this.col_Available.Width = 90;
            // 
            // lbl_notification
            // 
            this.lbl_notification.AutoSize = true;
            this.lbl_notification.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_notification.ForeColor = System.Drawing.Color.Red;
            this.lbl_notification.Location = new System.Drawing.Point(129, 81);
            this.lbl_notification.Name = "lbl_notification";
            this.lbl_notification.Size = new System.Drawing.Size(684, 17);
            this.lbl_notification.TabIndex = 46;
            this.lbl_notification.Text = "*Additional companion of 1 adult per room. Additional fee of Php 750.00 for adult" +
    " and Php 375.00 for child";
            // 
            // dtp_departureDate
            // 
            this.dtp_departureDate.CustomFormat = "dd mm, yyyy    -dddd";
            this.dtp_departureDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_departureDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_departureDate.Location = new System.Drawing.Point(419, 43);
            this.dtp_departureDate.Name = "dtp_departureDate";
            this.dtp_departureDate.ShowCheckBox = true;
            this.dtp_departureDate.Size = new System.Drawing.Size(238, 24);
            this.dtp_departureDate.TabIndex = 2;
            this.dtp_departureDate.ValueChanged += new System.EventHandler(this.dtp_departureDate_ValueChanged);
            this.dtp_departureDate.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dtp_departureDate_MouseUp);
            // 
            // dtp_arrivalDate
            // 
            this.dtp_arrivalDate.CustomFormat = "dd mm, yyyy    -dddd";
            this.dtp_arrivalDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_arrivalDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_arrivalDate.Location = new System.Drawing.Point(86, 43);
            this.dtp_arrivalDate.Name = "dtp_arrivalDate";
            this.dtp_arrivalDate.ShowCheckBox = true;
            this.dtp_arrivalDate.Size = new System.Drawing.Size(228, 24);
            this.dtp_arrivalDate.TabIndex = 1;
            this.dtp_arrivalDate.ValueChanged += new System.EventHandler(this.dtp_arrivalDate_ValueChanged);
            this.dtp_arrivalDate.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dtp_arrivalDate_MouseUp);
            // 
            // lbl_checkOut
            // 
            this.lbl_checkOut.AutoSize = true;
            this.lbl_checkOut.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_checkOut.Location = new System.Drawing.Point(333, 45);
            this.lbl_checkOut.Name = "lbl_checkOut";
            this.lbl_checkOut.Size = new System.Drawing.Size(86, 19);
            this.lbl_checkOut.TabIndex = 41;
            this.lbl_checkOut.Text = "check out*";
            // 
            // lbl_chekIn
            // 
            this.lbl_chekIn.AutoSize = true;
            this.lbl_chekIn.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chekIn.Location = new System.Drawing.Point(10, 44);
            this.lbl_chekIn.Name = "lbl_chekIn";
            this.lbl_chekIn.Size = new System.Drawing.Size(76, 19);
            this.lbl_chekIn.TabIndex = 40;
            this.lbl_chekIn.Text = "check in*";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.txt_accomo_accomodationTotal);
            this.panel3.Controls.Add(this.lbl_accomodationTotal);
            this.panel3.Controls.Add(this.lbl_accomodationDetails);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(996, 31);
            this.panel3.TabIndex = 10;
            // 
            // txt_accomo_accomodationTotal
            // 
            this.txt_accomo_accomodationTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_accomo_accomodationTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_accomo_accomodationTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_accomo_accomodationTotal.ForeColor = System.Drawing.Color.White;
            this.txt_accomo_accomodationTotal.Location = new System.Drawing.Point(889, 6);
            this.txt_accomo_accomodationTotal.Name = "txt_accomo_accomodationTotal";
            this.txt_accomo_accomodationTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_accomo_accomodationTotal.Size = new System.Drawing.Size(97, 17);
            this.txt_accomo_accomodationTotal.TabIndex = 74;
            this.txt_accomo_accomodationTotal.Text = "0.00";
            // 
            // lbl_accomodationTotal
            // 
            this.lbl_accomodationTotal.AutoSize = true;
            this.lbl_accomodationTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_accomodationTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_accomodationTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_accomodationTotal.Location = new System.Drawing.Point(697, 8);
            this.lbl_accomodationTotal.Name = "lbl_accomodationTotal";
            this.lbl_accomodationTotal.Size = new System.Drawing.Size(186, 14);
            this.lbl_accomodationTotal.TabIndex = 73;
            this.lbl_accomodationTotal.Text = "ACCOMODATION TOTAL:      Php";
            // 
            // lbl_accomodationDetails
            // 
            this.lbl_accomodationDetails.AutoSize = true;
            this.lbl_accomodationDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_accomodationDetails.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_accomodationDetails.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_accomodationDetails.Location = new System.Drawing.Point(11, 8);
            this.lbl_accomodationDetails.Name = "lbl_accomodationDetails";
            this.lbl_accomodationDetails.Size = new System.Drawing.Size(155, 14);
            this.lbl_accomodationDetails.TabIndex = 0;
            this.lbl_accomodationDetails.Text = "ACCOMODATION DETAILS";
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_cancel.FlatAppearance.BorderSize = 0;
            this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cancel.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cancel.ForeColor = System.Drawing.Color.White;
            this.btn_cancel.Location = new System.Drawing.Point(717, 1614);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(191, 35);
            this.btn_cancel.TabIndex = 19;
            this.btn_cancel.Text = "CANCEL";
            this.btn_cancel.UseVisualStyleBackColor = false;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_reserveNow
            // 
            this.btn_reserveNow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_reserveNow.FlatAppearance.BorderSize = 0;
            this.btn_reserveNow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reserveNow.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reserveNow.ForeColor = System.Drawing.Color.White;
            this.btn_reserveNow.Location = new System.Drawing.Point(506, 1614);
            this.btn_reserveNow.Name = "btn_reserveNow";
            this.btn_reserveNow.Size = new System.Drawing.Size(191, 35);
            this.btn_reserveNow.TabIndex = 18;
            this.btn_reserveNow.Text = "RESERVE NOW";
            this.btn_reserveNow.UseVisualStyleBackColor = false;
            this.btn_reserveNow.Click += new System.EventHandler(this.btn_reserveNow_Click);
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.panel10);
            this.panel6.Controls.Add(this.panel8);
            this.panel6.Controls.Add(this.panel35);
            this.panel6.Controls.Add(this.settlementOption_panel);
            this.panel6.Location = new System.Drawing.Point(162, 1084);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(995, 503);
            this.panel6.TabIndex = 15;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.btn_apply);
            this.panel10.Controls.Add(this.txt_discountDescription);
            this.panel10.Controls.Add(this.lbl_discountDescription);
            this.panel10.Controls.Add(this.lbl_discountAmount);
            this.panel10.Controls.Add(this.txt_discountAmount);
            this.panel10.Controls.Add(this.panel7);
            this.panel10.Location = new System.Drawing.Point(55, 300);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(887, 165);
            this.panel10.TabIndex = 51;
            // 
            // btn_apply
            // 
            this.btn_apply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_apply.FlatAppearance.BorderSize = 0;
            this.btn_apply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_apply.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_apply.ForeColor = System.Drawing.Color.White;
            this.btn_apply.Location = new System.Drawing.Point(524, 101);
            this.btn_apply.Name = "btn_apply";
            this.btn_apply.Size = new System.Drawing.Size(139, 35);
            this.btn_apply.TabIndex = 17;
            this.btn_apply.Text = "APPLY";
            this.btn_apply.UseVisualStyleBackColor = false;
            // 
            // txt_discountDescription
            // 
            this.txt_discountDescription.Location = new System.Drawing.Point(149, 87);
            this.txt_discountDescription.Multiline = true;
            this.txt_discountDescription.Name = "txt_discountDescription";
            this.txt_discountDescription.Size = new System.Drawing.Size(347, 62);
            this.txt_discountDescription.TabIndex = 16;
            // 
            // lbl_discountDescription
            // 
            this.lbl_discountDescription.AutoSize = true;
            this.lbl_discountDescription.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_discountDescription.Location = new System.Drawing.Point(10, 108);
            this.lbl_discountDescription.Name = "lbl_discountDescription";
            this.lbl_discountDescription.Size = new System.Drawing.Size(116, 19);
            this.lbl_discountDescription.TabIndex = 15;
            this.lbl_discountDescription.Text = "Discount Desc.";
            // 
            // lbl_discountAmount
            // 
            this.lbl_discountAmount.AutoSize = true;
            this.lbl_discountAmount.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_discountAmount.Location = new System.Drawing.Point(10, 52);
            this.lbl_discountAmount.Name = "lbl_discountAmount";
            this.lbl_discountAmount.Size = new System.Drawing.Size(133, 19);
            this.lbl_discountAmount.TabIndex = 14;
            this.lbl_discountAmount.Text = "Discount Amount";
            // 
            // txt_discountAmount
            // 
            this.txt_discountAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_discountAmount.Location = new System.Drawing.Point(149, 51);
            this.txt_discountAmount.Name = "txt_discountAmount";
            this.txt_discountAmount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_discountAmount.Size = new System.Drawing.Size(242, 23);
            this.txt_discountAmount.TabIndex = 13;
            this.txt_discountAmount.Text = "0.00";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel7.Controls.Add(this.lbl_discount);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(885, 31);
            this.panel7.TabIndex = 12;
            // 
            // lbl_discount
            // 
            this.lbl_discount.AutoSize = true;
            this.lbl_discount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_discount.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_discount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_discount.Location = new System.Drawing.Point(11, 8);
            this.lbl_discount.Name = "lbl_discount";
            this.lbl_discount.Size = new System.Drawing.Size(68, 14);
            this.lbl_discount.TabIndex = 0;
            this.lbl_discount.Text = "DISCOUNT";
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Controls.Add(this.lbl_type);
            this.panel8.Controls.Add(this.cmb_type);
            this.panel8.Controls.Add(this.lbl_cardNumber);
            this.panel8.Controls.Add(this.txt_cardNumber);
            this.panel8.Controls.Add(this.lbl_expire);
            this.panel8.Controls.Add(this.txt_cvv);
            this.panel8.Controls.Add(this.txt_expire);
            this.panel8.Controls.Add(this.lbl_cvv);
            this.panel8.Location = new System.Drawing.Point(55, 157);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(888, 127);
            this.panel8.TabIndex = 50;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel9.Controls.Add(this.lbl_credit);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(886, 31);
            this.panel9.TabIndex = 11;
            // 
            // lbl_credit
            // 
            this.lbl_credit.AutoSize = true;
            this.lbl_credit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_credit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_credit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_credit.Location = new System.Drawing.Point(11, 8);
            this.lbl_credit.Name = "lbl_credit";
            this.lbl_credit.Size = new System.Drawing.Size(50, 14);
            this.lbl_credit.TabIndex = 0;
            this.lbl_credit.Text = "CREDIT";
            // 
            // lbl_type
            // 
            this.lbl_type.AutoSize = true;
            this.lbl_type.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_type.Location = new System.Drawing.Point(10, 43);
            this.lbl_type.Name = "lbl_type";
            this.lbl_type.Size = new System.Drawing.Size(44, 19);
            this.lbl_type.TabIndex = 28;
            this.lbl_type.Text = "Type";
            // 
            // cmb_type
            // 
            this.cmb_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_type.FormattingEnabled = true;
            this.cmb_type.Location = new System.Drawing.Point(106, 43);
            this.cmb_type.Name = "cmb_type";
            this.cmb_type.Size = new System.Drawing.Size(272, 24);
            this.cmb_type.TabIndex = 29;
            // 
            // lbl_cardNumber
            // 
            this.lbl_cardNumber.AutoSize = true;
            this.lbl_cardNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cardNumber.Location = new System.Drawing.Point(10, 87);
            this.lbl_cardNumber.Name = "lbl_cardNumber";
            this.lbl_cardNumber.Size = new System.Drawing.Size(75, 19);
            this.lbl_cardNumber.TabIndex = 30;
            this.lbl_cardNumber.Text = "Card No*";
            // 
            // txt_cardNumber
            // 
            this.txt_cardNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cardNumber.Location = new System.Drawing.Point(106, 83);
            this.txt_cardNumber.Name = "txt_cardNumber";
            this.txt_cardNumber.Size = new System.Drawing.Size(272, 23);
            this.txt_cardNumber.TabIndex = 31;
            // 
            // lbl_expire
            // 
            this.lbl_expire.AutoSize = true;
            this.lbl_expire.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_expire.Location = new System.Drawing.Point(397, 84);
            this.lbl_expire.Name = "lbl_expire";
            this.lbl_expire.Size = new System.Drawing.Size(61, 19);
            this.lbl_expire.TabIndex = 32;
            this.lbl_expire.Text = "Expire*";
            // 
            // txt_cvv
            // 
            this.txt_cvv.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cvv.Location = new System.Drawing.Point(470, 42);
            this.txt_cvv.Name = "txt_cvv";
            this.txt_cvv.Size = new System.Drawing.Size(135, 23);
            this.txt_cvv.TabIndex = 35;
            // 
            // txt_expire
            // 
            this.txt_expire.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_expire.Location = new System.Drawing.Point(470, 83);
            this.txt_expire.Name = "txt_expire";
            this.txt_expire.Size = new System.Drawing.Size(135, 23);
            this.txt_expire.TabIndex = 33;
            // 
            // lbl_cvv
            // 
            this.lbl_cvv.AutoSize = true;
            this.lbl_cvv.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cvv.Location = new System.Drawing.Point(397, 44);
            this.lbl_cvv.Name = "lbl_cvv";
            this.lbl_cvv.Size = new System.Drawing.Size(46, 19);
            this.lbl_cvv.TabIndex = 34;
            this.lbl_cvv.Text = "CVV*";
            // 
            // panel35
            // 
            this.panel35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel35.Controls.Add(this.txt_change);
            this.panel35.Controls.Add(this.lbl_change);
            this.panel35.Controls.Add(this.txt_cashhanded);
            this.panel35.Controls.Add(this.lbl_cashHandedByGuest);
            this.panel35.Controls.Add(this.panel36);
            this.panel35.Location = new System.Drawing.Point(54, 53);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(888, 86);
            this.panel35.TabIndex = 49;
            // 
            // txt_change
            // 
            this.txt_change.Enabled = false;
            this.txt_change.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_change.Location = new System.Drawing.Point(514, 44);
            this.txt_change.Name = "txt_change";
            this.txt_change.ReadOnly = true;
            this.txt_change.Size = new System.Drawing.Size(193, 23);
            this.txt_change.TabIndex = 50;
            this.txt_change.Text = "0.00";
            this.txt_change.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_change.TextChanged += new System.EventHandler(this.txt_change_TextChanged);
            // 
            // lbl_change
            // 
            this.lbl_change.AutoSize = true;
            this.lbl_change.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_change.Location = new System.Drawing.Point(443, 48);
            this.lbl_change.Name = "lbl_change";
            this.lbl_change.Size = new System.Drawing.Size(65, 19);
            this.lbl_change.TabIndex = 49;
            this.lbl_change.Text = "Change";
            // 
            // txt_cashhanded
            // 
            this.txt_cashhanded.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cashhanded.Location = new System.Drawing.Point(197, 44);
            this.txt_cashhanded.Name = "txt_cashhanded";
            this.txt_cashhanded.Size = new System.Drawing.Size(193, 23);
            this.txt_cashhanded.TabIndex = 48;
            this.txt_cashhanded.Text = "0.00";
            this.txt_cashhanded.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_cashhanded.TextChanged += new System.EventHandler(this.txt_cashhanded_TextChanged);
            // 
            // lbl_cashHandedByGuest
            // 
            this.lbl_cashHandedByGuest.AutoSize = true;
            this.lbl_cashHandedByGuest.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cashHandedByGuest.Location = new System.Drawing.Point(10, 48);
            this.lbl_cashHandedByGuest.Name = "lbl_cashHandedByGuest";
            this.lbl_cashHandedByGuest.Size = new System.Drawing.Size(178, 19);
            this.lbl_cashHandedByGuest.TabIndex = 47;
            this.lbl_cashHandedByGuest.Text = "Cash Handed by Guest";
            // 
            // panel36
            // 
            this.panel36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel36.Controls.Add(this.lbl_cash);
            this.panel36.Controls.Add(this.txt_cashPayment);
            this.panel36.Controls.Add(this.lbl_cashPayment);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel36.Location = new System.Drawing.Point(0, 0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(886, 31);
            this.panel36.TabIndex = 11;
            // 
            // lbl_cash
            // 
            this.lbl_cash.AutoSize = true;
            this.lbl_cash.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_cash.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cash.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_cash.Location = new System.Drawing.Point(11, 8);
            this.lbl_cash.Name = "lbl_cash";
            this.lbl_cash.Size = new System.Drawing.Size(40, 14);
            this.lbl_cash.TabIndex = 0;
            this.lbl_cash.Text = "CASH";
            // 
            // txt_cashPayment
            // 
            this.txt_cashPayment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_cashPayment.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_cashPayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cashPayment.ForeColor = System.Drawing.Color.White;
            this.txt_cashPayment.Location = new System.Drawing.Point(781, 7);
            this.txt_cashPayment.Name = "txt_cashPayment";
            this.txt_cashPayment.ReadOnly = true;
            this.txt_cashPayment.Size = new System.Drawing.Size(97, 17);
            this.txt_cashPayment.TabIndex = 46;
            this.txt_cashPayment.Text = "0.00";
            this.txt_cashPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_cashPayment.TextChanged += new System.EventHandler(this.txt_cashPayment_TextChanged);
            // 
            // lbl_cashPayment
            // 
            this.lbl_cashPayment.AutoSize = true;
            this.lbl_cashPayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cashPayment.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_cashPayment.Location = new System.Drawing.Point(660, 5);
            this.lbl_cashPayment.Name = "lbl_cashPayment";
            this.lbl_cashPayment.Size = new System.Drawing.Size(115, 19);
            this.lbl_cashPayment.TabIndex = 45;
            this.lbl_cashPayment.Text = "Cash Payment";
            // 
            // settlementOption_panel
            // 
            this.settlementOption_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.settlementOption_panel.Controls.Add(this.lbl_settlementOption);
            this.settlementOption_panel.Controls.Add(this.lbl_requiredPrePayment);
            this.settlementOption_panel.Controls.Add(this.txt_requiredPrePayment);
            this.settlementOption_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.settlementOption_panel.Location = new System.Drawing.Point(0, 0);
            this.settlementOption_panel.Name = "settlementOption_panel";
            this.settlementOption_panel.Size = new System.Drawing.Size(993, 31);
            this.settlementOption_panel.TabIndex = 11;
            // 
            // lbl_settlementOption
            // 
            this.lbl_settlementOption.AutoSize = true;
            this.lbl_settlementOption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_settlementOption.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_settlementOption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_settlementOption.Location = new System.Drawing.Point(11, 8);
            this.lbl_settlementOption.Name = "lbl_settlementOption";
            this.lbl_settlementOption.Size = new System.Drawing.Size(132, 14);
            this.lbl_settlementOption.TabIndex = 0;
            this.lbl_settlementOption.Text = "SETTLEMENT OPTION";
            // 
            // lbl_requiredPrePayment
            // 
            this.lbl_requiredPrePayment.AutoSize = true;
            this.lbl_requiredPrePayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_requiredPrePayment.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_requiredPrePayment.Location = new System.Drawing.Point(710, 6);
            this.lbl_requiredPrePayment.Name = "lbl_requiredPrePayment";
            this.lbl_requiredPrePayment.Size = new System.Drawing.Size(172, 19);
            this.lbl_requiredPrePayment.TabIndex = 42;
            this.lbl_requiredPrePayment.Text = "Required pre-payment";
            // 
            // txt_requiredPrePayment
            // 
            this.txt_requiredPrePayment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_requiredPrePayment.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_requiredPrePayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_requiredPrePayment.ForeColor = System.Drawing.Color.White;
            this.txt_requiredPrePayment.Location = new System.Drawing.Point(888, 7);
            this.txt_requiredPrePayment.Name = "txt_requiredPrePayment";
            this.txt_requiredPrePayment.ReadOnly = true;
            this.txt_requiredPrePayment.Size = new System.Drawing.Size(97, 17);
            this.txt_requiredPrePayment.TabIndex = 43;
            this.txt_requiredPrePayment.Text = "0.00";
            this.txt_requiredPrePayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_requiredPrePayment.TextChanged += new System.EventHandler(this.txt_requiredPrePayment_TextChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Silver;
            this.label29.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(1186, 1656);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(0, 19);
            this.label29.TabIndex = 14;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lbl_visaNumber);
            this.panel2.Controls.Add(this.txt_visaNumber);
            this.panel2.Controls.Add(this.lbl_passportNumber);
            this.panel2.Controls.Add(this.txt_passportNumber);
            this.panel2.Controls.Add(this.cmb_homeCountry);
            this.panel2.Controls.Add(this.lbl_homeCountry);
            this.panel2.Controls.Add(this.lbl_companyAgency);
            this.panel2.Controls.Add(this.txt_companyAgency);
            this.panel2.Controls.Add(this.lbl_province);
            this.panel2.Controls.Add(this.txt_province);
            this.panel2.Controls.Add(this.lbl_city);
            this.panel2.Controls.Add(this.txt_city);
            this.panel2.Controls.Add(this.lbl_address);
            this.panel2.Controls.Add(this.txt_address);
            this.panel2.Controls.Add(this.lbl_dateOfBirth);
            this.panel2.Controls.Add(this.dtb_dateOfBirth);
            this.panel2.Controls.Add(this.btn_scanImage);
            this.panel2.Controls.Add(this.btn_attachImage);
            this.panel2.Controls.Add(this.pcb_image);
            this.panel2.Controls.Add(this.txt_contactDetails);
            this.panel2.Controls.Add(this.lbl_contactDetails);
            this.panel2.Controls.Add(this.txt_email);
            this.panel2.Controls.Add(this.lbl_email);
            this.panel2.Controls.Add(this.txt_lastName);
            this.panel2.Controls.Add(this.lbl_lastName);
            this.panel2.Controls.Add(this.txt_middleName);
            this.panel2.Controls.Add(this.lbl_middleName);
            this.panel2.Controls.Add(this.txt_firstName);
            this.panel2.Controls.Add(this.lbl_firstName);
            this.panel2.Controls.Add(this.cmb_title);
            this.panel2.Controls.Add(this.lbl_title);
            this.panel2.Controls.Add(this.accomo_panel);
            this.panel2.Location = new System.Drawing.Point(159, 155);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(995, 330);
            this.panel2.TabIndex = 12;
            // 
            // lbl_visaNumber
            // 
            this.lbl_visaNumber.AutoSize = true;
            this.lbl_visaNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_visaNumber.Location = new System.Drawing.Point(540, 271);
            this.lbl_visaNumber.Name = "lbl_visaNumber";
            this.lbl_visaNumber.Size = new System.Drawing.Size(61, 17);
            this.lbl_visaNumber.TabIndex = 67;
            this.lbl_visaNumber.Text = "VISA no.";
            // 
            // txt_visaNumber
            // 
            this.txt_visaNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_visaNumber.Location = new System.Drawing.Point(543, 290);
            this.txt_visaNumber.Name = "txt_visaNumber";
            this.txt_visaNumber.Size = new System.Drawing.Size(349, 24);
            this.txt_visaNumber.TabIndex = 66;
            this.txt_visaNumber.TextChanged += new System.EventHandler(this.txt_visaNumber_TextChanged);
            // 
            // lbl_passportNumber
            // 
            this.lbl_passportNumber.AutoSize = true;
            this.lbl_passportNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_passportNumber.Location = new System.Drawing.Point(181, 271);
            this.lbl_passportNumber.Name = "lbl_passportNumber";
            this.lbl_passportNumber.Size = new System.Drawing.Size(89, 17);
            this.lbl_passportNumber.TabIndex = 65;
            this.lbl_passportNumber.Text = "Passport No.";
            // 
            // txt_passportNumber
            // 
            this.txt_passportNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_passportNumber.Location = new System.Drawing.Point(184, 290);
            this.txt_passportNumber.Name = "txt_passportNumber";
            this.txt_passportNumber.Size = new System.Drawing.Size(353, 24);
            this.txt_passportNumber.TabIndex = 64;
            this.txt_passportNumber.TextChanged += new System.EventHandler(this.txt_passportNumber_TextChanged);
            // 
            // cmb_homeCountry
            // 
            this.cmb_homeCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_homeCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_homeCountry.FormattingEnabled = true;
            this.cmb_homeCountry.Items.AddRange(new object[] {
            "Afghanistan",
            "Albania",
            "Algeria",
            "Andorra",
            "Angola",
            "Anguilla",
            "Antigua & Barbuda",
            "Argentina",
            "Armenia",
            "Australia",
            "Austria",
            "Azerbaijan",
            "Bahamas",
            "Bahrain",
            "Bangladesh",
            "Barbados",
            "Belarus",
            "Belgium",
            "Belize",
            "Benin",
            "Bermuda",
            "Bhutan",
            "Bolivia",
            "Bosnia & Herzegovina",
            "Botswana",
            "Brazil",
            "Brunei Darussalam",
            "Bulgaria",
            "Burkina Faso",
            "Myanmar/Burma",
            "Burundi",
            "Cambodia",
            "Cameroon",
            "Canada",
            "Cape Verde",
            "Cayman Islands",
            "Central African Republic",
            "Chad",
            "Chile",
            "China",
            "Colombia",
            "Comoros",
            "Congo",
            "Costa Rica",
            "Croatia",
            "Cuba",
            "Cyprus",
            "Czech Republic",
            "Democratic Republic of the Congo",
            "Denmark",
            "Djibouti",
            "Dominican Republic",
            "Dominica",
            "Ecuador",
            "Egypt",
            "El Salvador",
            "Equatorial Guinea",
            "Eritrea",
            "Estonia",
            "Ethiopia",
            "Fiji",
            "Finland",
            "France",
            "French Guiana",
            "Gabon",
            "Gambia",
            "Georgia",
            "Germany",
            "Ghana",
            "Great Britain",
            "Greece",
            "Grenada",
            "Guadeloupe",
            "Guatemala",
            "Guinea",
            "Guinea-Bissau",
            "Guyana",
            "Haiti",
            "Honduras",
            "Hungary",
            "Iceland",
            "India",
            "Indonesia",
            "Iran",
            "Iraq",
            "Israel and the Occupied Territories",
            "Italy",
            "Ivory Coast (Cote d\'Ivoire)",
            "Jamaica",
            "Japan",
            "Jordan",
            "Kazakhstan",
            "Kenya",
            "Kosovo",
            "Kuwait",
            "Kyrgyz Republic (Kyrgyzstan)",
            "Laos",
            "Latvia",
            "Lebanon",
            "Lesotho",
            "Liberia",
            "Libya",
            "Liechtenstein",
            "Lithuania",
            "Luxembourg",
            "Republic of Macedonia",
            "Madagascar",
            "Malawi",
            "Malaysia",
            "Maldives",
            "Mali",
            "Malta",
            "Martinique",
            "Mauritania",
            "Mauritius",
            "Mayotte",
            "Mexico",
            "Moldova, Republic of",
            "Monaco",
            "Mongolia",
            "Montenegro",
            "Montserrat",
            "Morocco",
            "Mozambique",
            "Namibia",
            "Nepal",
            "Netherlands",
            "New Zealand",
            "Nicaragua",
            "Niger",
            "Nigeria",
            "Korea, Democratic Republic of (North Korea)",
            "Norway",
            "Oman",
            "Pacific Islands",
            "Pakistan",
            "Panama",
            "Papua New Guinea",
            "Paraguay",
            "Peru",
            "Philippines",
            "Poland",
            "Portugal",
            "Puerto Rico",
            "Qatar",
            "Reunion",
            "Romania",
            "Russian Federation",
            "Rwanda",
            "Saint Kitts and Nevis",
            "Saint Lucia",
            "Saint Vincent\'s & Grenadines",
            "Samoa",
            "Sao Tome and Principe",
            "Saudi Arabia",
            "Senegal",
            "Serbia",
            "Seychelles",
            "Sierra Leone",
            "Singapore",
            "Slovak Republic (Slovakia)",
            "Slovenia",
            "Solomon Islands",
            "Somalia",
            "South Africa",
            "Korea, Republic of (South Korea)",
            "South Sudan",
            "Spain",
            "Sri Lanka",
            "Sudan",
            "Suriname",
            "Swaziland",
            "Sweden",
            "Switzerland",
            "Syria",
            "Tajikistan",
            "Tanzania",
            "Thailand",
            "Timor Leste",
            "Togo",
            "Trinidad & Tobago",
            "Tunisia",
            "Turkey",
            "Turkmenistan",
            "Turks & Caicos Islands",
            "Uganda",
            "Ukraine",
            "United Arab Emirates",
            "United States of America (USA)",
            "Uruguay",
            "Uzbekistan",
            "Venezuela",
            "Vietnam",
            "Virgin Islands (UK)",
            "Virgin Islands (US)",
            "Yemen",
            "Zambia",
            "Zimbabwe"});
            this.cmb_homeCountry.Location = new System.Drawing.Point(543, 235);
            this.cmb_homeCountry.Name = "cmb_homeCountry";
            this.cmb_homeCountry.Size = new System.Drawing.Size(250, 26);
            this.cmb_homeCountry.TabIndex = 63;
            this.cmb_homeCountry.SelectedIndexChanged += new System.EventHandler(this.cmb_homeCountry_SelectedIndexChanged);
            // 
            // lbl_homeCountry
            // 
            this.lbl_homeCountry.AutoSize = true;
            this.lbl_homeCountry.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_homeCountry.Location = new System.Drawing.Point(540, 217);
            this.lbl_homeCountry.Name = "lbl_homeCountry";
            this.lbl_homeCountry.Size = new System.Drawing.Size(113, 17);
            this.lbl_homeCountry.TabIndex = 62;
            this.lbl_homeCountry.Text = "Home Country: *";
            // 
            // lbl_companyAgency
            // 
            this.lbl_companyAgency.AutoSize = true;
            this.lbl_companyAgency.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_companyAgency.Location = new System.Drawing.Point(181, 216);
            this.lbl_companyAgency.Name = "lbl_companyAgency";
            this.lbl_companyAgency.Size = new System.Drawing.Size(123, 17);
            this.lbl_companyAgency.TabIndex = 61;
            this.lbl_companyAgency.Text = "Company/Agency";
            // 
            // txt_companyAgency
            // 
            this.txt_companyAgency.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_companyAgency.Location = new System.Drawing.Point(184, 235);
            this.txt_companyAgency.Name = "txt_companyAgency";
            this.txt_companyAgency.Size = new System.Drawing.Size(353, 24);
            this.txt_companyAgency.TabIndex = 60;
            this.txt_companyAgency.TextChanged += new System.EventHandler(this.txt_companyAgency_TextChanged);
            // 
            // lbl_province
            // 
            this.lbl_province.AutoSize = true;
            this.lbl_province.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_province.Location = new System.Drawing.Point(764, 164);
            this.lbl_province.Name = "lbl_province";
            this.lbl_province.Size = new System.Drawing.Size(77, 17);
            this.lbl_province.TabIndex = 59;
            this.lbl_province.Text = "Province: *";
            // 
            // txt_province
            // 
            this.txt_province.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_province.Location = new System.Drawing.Point(767, 183);
            this.txt_province.Name = "txt_province";
            this.txt_province.Size = new System.Drawing.Size(213, 24);
            this.txt_province.TabIndex = 58;
            this.txt_province.TextChanged += new System.EventHandler(this.txt_province_TextChanged);
            // 
            // lbl_city
            // 
            this.lbl_city.AutoSize = true;
            this.lbl_city.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_city.Location = new System.Drawing.Point(468, 164);
            this.lbl_city.Name = "lbl_city";
            this.lbl_city.Size = new System.Drawing.Size(46, 17);
            this.lbl_city.TabIndex = 57;
            this.lbl_city.Text = "City: *";
            // 
            // txt_city
            // 
            this.txt_city.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_city.Location = new System.Drawing.Point(471, 183);
            this.txt_city.Name = "txt_city";
            this.txt_city.Size = new System.Drawing.Size(290, 24);
            this.txt_city.TabIndex = 56;
            this.txt_city.TextChanged += new System.EventHandler(this.txt_city_TextChanged);
            // 
            // lbl_address
            // 
            this.lbl_address.AutoSize = true;
            this.lbl_address.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_address.Location = new System.Drawing.Point(181, 164);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(73, 17);
            this.lbl_address.TabIndex = 55;
            this.lbl_address.Text = "Address: *";
            // 
            // txt_address
            // 
            this.txt_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_address.Location = new System.Drawing.Point(184, 183);
            this.txt_address.Name = "txt_address";
            this.txt_address.Size = new System.Drawing.Size(281, 24);
            this.txt_address.TabIndex = 54;
            this.txt_address.TextChanged += new System.EventHandler(this.txt_address_TextChanged);
            // 
            // lbl_dateOfBirth
            // 
            this.lbl_dateOfBirth.AutoSize = true;
            this.lbl_dateOfBirth.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dateOfBirth.Location = new System.Drawing.Point(764, 109);
            this.lbl_dateOfBirth.Name = "lbl_dateOfBirth";
            this.lbl_dateOfBirth.Size = new System.Drawing.Size(102, 17);
            this.lbl_dateOfBirth.TabIndex = 53;
            this.lbl_dateOfBirth.Text = "Date of Birth: *";
            // 
            // dtb_dateOfBirth
            // 
            this.dtb_dateOfBirth.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtb_dateOfBirth.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtb_dateOfBirth.Cursor = System.Windows.Forms.Cursors.No;
            this.dtb_dateOfBirth.CustomFormat = "MMMM dd, yyyy - dddd";
            this.dtb_dateOfBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtb_dateOfBirth.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtb_dateOfBirth.Location = new System.Drawing.Point(767, 128);
            this.dtb_dateOfBirth.Name = "dtb_dateOfBirth";
            this.dtb_dateOfBirth.Size = new System.Drawing.Size(213, 24);
            this.dtb_dateOfBirth.TabIndex = 52;
            this.dtb_dateOfBirth.ValueChanged += new System.EventHandler(this.dtb_dateOfBirth_ValueChanged);
            // 
            // btn_scanImage
            // 
            this.btn_scanImage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_scanImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_scanImage.FlatAppearance.BorderSize = 0;
            this.btn_scanImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_scanImage.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_scanImage.ForeColor = System.Drawing.Color.White;
            this.btn_scanImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_scanImage.Location = new System.Drawing.Point(32, 247);
            this.btn_scanImage.Name = "btn_scanImage";
            this.btn_scanImage.Size = new System.Drawing.Size(128, 32);
            this.btn_scanImage.TabIndex = 51;
            this.btn_scanImage.Text = "SCAN IMAGE";
            this.btn_scanImage.UseVisualStyleBackColor = false;
            // 
            // btn_attachImage
            // 
            this.btn_attachImage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_attachImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_attachImage.FlatAppearance.BorderSize = 0;
            this.btn_attachImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_attachImage.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_attachImage.ForeColor = System.Drawing.Color.White;
            this.btn_attachImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_attachImage.Location = new System.Drawing.Point(32, 201);
            this.btn_attachImage.Name = "btn_attachImage";
            this.btn_attachImage.Size = new System.Drawing.Size(128, 32);
            this.btn_attachImage.TabIndex = 50;
            this.btn_attachImage.Text = "ATTACH IMAGE";
            this.btn_attachImage.UseVisualStyleBackColor = false;
            this.btn_attachImage.Click += new System.EventHandler(this.btn_attachImage_Click);
            // 
            // pcb_image
            // 
            this.pcb_image.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pcb_image.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcb_image.Location = new System.Drawing.Point(19, 52);
            this.pcb_image.Name = "pcb_image";
            this.pcb_image.Size = new System.Drawing.Size(154, 134);
            this.pcb_image.TabIndex = 49;
            this.pcb_image.TabStop = false;
            // 
            // txt_contactDetails
            // 
            this.txt_contactDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_contactDetails.Location = new System.Drawing.Point(184, 130);
            this.txt_contactDetails.Name = "txt_contactDetails";
            this.txt_contactDetails.Size = new System.Drawing.Size(281, 24);
            this.txt_contactDetails.TabIndex = 23;
            this.txt_contactDetails.TextChanged += new System.EventHandler(this.txt_contactDetails_TextChanged);
            // 
            // lbl_contactDetails
            // 
            this.lbl_contactDetails.AutoSize = true;
            this.lbl_contactDetails.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_contactDetails.Location = new System.Drawing.Point(181, 109);
            this.lbl_contactDetails.Name = "lbl_contactDetails";
            this.lbl_contactDetails.Size = new System.Drawing.Size(119, 17);
            this.lbl_contactDetails.TabIndex = 22;
            this.lbl_contactDetails.Text = "Contact Details: *";
            // 
            // txt_email
            // 
            this.txt_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_email.Location = new System.Drawing.Point(471, 128);
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(290, 24);
            this.txt_email.TabIndex = 21;
            this.txt_email.TextChanged += new System.EventHandler(this.txt_email_TextChanged);
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_email.Location = new System.Drawing.Point(468, 107);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(58, 17);
            this.lbl_email.TabIndex = 20;
            this.lbl_email.Text = "Email: *";
            // 
            // txt_lastName
            // 
            this.txt_lastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_lastName.Location = new System.Drawing.Point(311, 73);
            this.txt_lastName.Name = "txt_lastName";
            this.txt_lastName.Size = new System.Drawing.Size(238, 24);
            this.txt_lastName.TabIndex = 19;
            this.txt_lastName.TextChanged += new System.EventHandler(this.txt_lastName_TextChanged);
            // 
            // lbl_lastName
            // 
            this.lbl_lastName.AutoSize = true;
            this.lbl_lastName.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lastName.Location = new System.Drawing.Point(308, 52);
            this.lbl_lastName.Name = "lbl_lastName";
            this.lbl_lastName.Size = new System.Drawing.Size(89, 17);
            this.lbl_lastName.TabIndex = 18;
            this.lbl_lastName.Text = "Last Name: *";
            // 
            // txt_middleName
            // 
            this.txt_middleName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_middleName.Location = new System.Drawing.Point(799, 73);
            this.txt_middleName.Name = "txt_middleName";
            this.txt_middleName.Size = new System.Drawing.Size(181, 24);
            this.txt_middleName.TabIndex = 17;
            this.txt_middleName.TextChanged += new System.EventHandler(this.txt_middleName_TextChanged);
            // 
            // lbl_middleName
            // 
            this.lbl_middleName.AutoSize = true;
            this.lbl_middleName.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_middleName.Location = new System.Drawing.Point(796, 52);
            this.lbl_middleName.Name = "lbl_middleName";
            this.lbl_middleName.Size = new System.Drawing.Size(97, 17);
            this.lbl_middleName.TabIndex = 16;
            this.lbl_middleName.Text = "Middle Name:";
            // 
            // txt_firstName
            // 
            this.txt_firstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_firstName.Location = new System.Drawing.Point(555, 73);
            this.txt_firstName.Name = "txt_firstName";
            this.txt_firstName.Size = new System.Drawing.Size(238, 24);
            this.txt_firstName.TabIndex = 15;
            this.txt_firstName.TextChanged += new System.EventHandler(this.txt_firstName_TextChanged);
            // 
            // lbl_firstName
            // 
            this.lbl_firstName.AutoSize = true;
            this.lbl_firstName.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_firstName.Location = new System.Drawing.Point(552, 52);
            this.lbl_firstName.Name = "lbl_firstName";
            this.lbl_firstName.Size = new System.Drawing.Size(91, 17);
            this.lbl_firstName.TabIndex = 14;
            this.lbl_firstName.Text = "First Name: *";
            // 
            // cmb_title
            // 
            this.cmb_title.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_title.FormattingEnabled = true;
            this.cmb_title.Items.AddRange(new object[] {
            "Mr.",
            "Mrs.",
            "Ms.",
            "Madam",
            "Sir",
            "Dr.",
            "Atty.",
            "Engr."});
            this.cmb_title.Location = new System.Drawing.Point(184, 73);
            this.cmb_title.Name = "cmb_title";
            this.cmb_title.Size = new System.Drawing.Size(121, 24);
            this.cmb_title.TabIndex = 13;
            this.cmb_title.SelectedIndexChanged += new System.EventHandler(this.cmb_title_SelectedIndexChanged);
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title.Location = new System.Drawing.Point(181, 52);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(49, 17);
            this.lbl_title.TabIndex = 12;
            this.lbl_title.Text = "Title: *";
            // 
            // accomo_panel
            // 
            this.accomo_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.accomo_panel.Controls.Add(this.lbl_guestInformation);
            this.accomo_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.accomo_panel.Location = new System.Drawing.Point(0, 0);
            this.accomo_panel.Name = "accomo_panel";
            this.accomo_panel.Size = new System.Drawing.Size(993, 31);
            this.accomo_panel.TabIndex = 11;
            // 
            // lbl_guestInformation
            // 
            this.lbl_guestInformation.AutoSize = true;
            this.lbl_guestInformation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_guestInformation.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_guestInformation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_guestInformation.Location = new System.Drawing.Point(11, 8);
            this.lbl_guestInformation.Name = "lbl_guestInformation";
            this.lbl_guestInformation.Size = new System.Drawing.Size(133, 14);
            this.lbl_guestInformation.TabIndex = 0;
            this.lbl_guestInformation.Text = "GUEST INFORMATION";
            // 
            // ReservationV2_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1300, 750);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.newWalkinReservation_Panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ReservationV2_Form";
            this.Text = "ReservationV2_Form";
            this.Load += new System.EventHandler(this.ReservationV2_Form_Load_1);
            this.newWalkinReservation_Panel.ResumeLayout(false);
            this.newWalkinReservation_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_newWalkinReservation)).EndInit();
            this.pnl_hotelInfo.ResumeLayout(false);
            this.pnl_hotelInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_maxxLogo)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnl_accomodationDetails.ResumeLayout(false);
            this.pnl_accomodationDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_roomQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listOfRooms)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panel35.PerformLayout();
            this.panel36.ResumeLayout(false);
            this.panel36.PerformLayout();
            this.settlementOption_panel.ResumeLayout(false);
            this.settlementOption_panel.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_image)).EndInit();
            this.accomo_panel.ResumeLayout(false);
            this.accomo_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel newWalkinReservation_Panel;
        private System.Windows.Forms.Label lbl_newWalkinReservation;
        private System.Windows.Forms.PictureBox pcb_newWalkinReservation;
        private System.Windows.Forms.Panel pnl_hotelInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_hotelAddress;
        private System.Windows.Forms.Label lbl_hotelStreet;
        private System.Windows.Forms.Label lbl_maxxHotelMakati;
        private System.Windows.Forms.PictureBox pcb_maxxLogo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel accomo_panel;
        private System.Windows.Forms.Label lbl_guestInformation;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.ComboBox cmb_title;
        private System.Windows.Forms.TextBox txt_firstName;
        private System.Windows.Forms.Label lbl_firstName;
        private System.Windows.Forms.TextBox txt_middleName;
        private System.Windows.Forms.Label lbl_middleName;
        private System.Windows.Forms.TextBox txt_lastName;
        private System.Windows.Forms.Label lbl_lastName;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.TextBox txt_contactDetails;
        private System.Windows.Forms.Label lbl_contactDetails;
        private System.Windows.Forms.Button btn_scanImage;
        private System.Windows.Forms.Button btn_attachImage;
        private System.Windows.Forms.PictureBox pcb_image;
        private System.Windows.Forms.Label lbl_dateOfBirth;
        private System.Windows.Forms.DateTimePicker dtb_dateOfBirth;
        private System.Windows.Forms.Label lbl_province;
        private System.Windows.Forms.TextBox txt_province;
        private System.Windows.Forms.Label lbl_city;
        private System.Windows.Forms.TextBox txt_city;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.TextBox txt_address;
        private System.Windows.Forms.ComboBox cmb_homeCountry;
        private System.Windows.Forms.Label lbl_homeCountry;
        private System.Windows.Forms.Label lbl_companyAgency;
        private System.Windows.Forms.TextBox txt_companyAgency;
        private System.Windows.Forms.Label lbl_visaNumber;
        private System.Windows.Forms.TextBox txt_visaNumber;
        private System.Windows.Forms.Label lbl_passportNumber;
        private System.Windows.Forms.TextBox txt_passportNumber;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btn_apply;
        private System.Windows.Forms.TextBox txt_discountDescription;
        private System.Windows.Forms.Label lbl_discountDescription;
        private System.Windows.Forms.Label lbl_discountAmount;
        private System.Windows.Forms.TextBox txt_discountAmount;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lbl_discount;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label lbl_credit;
        private System.Windows.Forms.Label lbl_type;
        private System.Windows.Forms.ComboBox cmb_type;
        private System.Windows.Forms.Label lbl_cardNumber;
        private System.Windows.Forms.TextBox txt_cardNumber;
        private System.Windows.Forms.Label lbl_expire;
        private System.Windows.Forms.TextBox txt_cvv;
        private System.Windows.Forms.TextBox txt_expire;
        private System.Windows.Forms.Label lbl_cvv;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.TextBox txt_change;
        private System.Windows.Forms.Label lbl_change;
        private System.Windows.Forms.TextBox txt_cashhanded;
        private System.Windows.Forms.Label lbl_cashHandedByGuest;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Label lbl_cash;
        private System.Windows.Forms.TextBox txt_cashPayment;
        private System.Windows.Forms.Label lbl_cashPayment;
        private System.Windows.Forms.Panel settlementOption_panel;
        private System.Windows.Forms.Label lbl_settlementOption;
        private System.Windows.Forms.Label lbl_requiredPrePayment;
        private System.Windows.Forms.TextBox txt_requiredPrePayment;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_reserveNow;
        private System.Windows.Forms.Panel pnl_accomodationDetails;
        private System.Windows.Forms.Button btn_companionName;
        private System.Windows.Forms.Label lbl_nights;
        private System.Windows.Forms.ComboBox cbox_children;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbox_adult;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbox_extrachildren;
        private System.Windows.Forms.ComboBox cbox_extraadults;
        private System.Windows.Forms.NumericUpDown numUD_roomQuantity;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_guestRemarks;
        private System.Windows.Forms.Label lbl_guestRemarks;
        private System.Windows.Forms.Button btn_viewRoomDetails;
        private System.Windows.Forms.Label lbl_amount;
        private System.Windows.Forms.TextBox txt_childTotalFee;
        private System.Windows.Forms.TextBox txt_adultsTotalFee;
        private System.Windows.Forms.Label lbl_feePerNight;
        private System.Windows.Forms.TextBox txt_childFee;
        private System.Windows.Forms.TextBox txt_adultsFee;
        private System.Windows.Forms.Label lbl_child;
        private System.Windows.Forms.Label lbl_adults;
        private System.Windows.Forms.Label lbl_additionalCompanion;
        private System.Windows.Forms.DataGridView dgv_listOfRooms;
        private System.Windows.Forms.Label lbl_notification;
        private System.Windows.Forms.Label lbl_numberOfNights;
        private System.Windows.Forms.DateTimePicker dtp_departureDate;
        private System.Windows.Forms.DateTimePicker dtp_arrivalDate;
        private System.Windows.Forms.Label lbl_checkOut;
        private System.Windows.Forms.Label lbl_chekIn;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txt_accomo_accomodationTotal;
        private System.Windows.Forms.Label lbl_accomodationTotal;
        private System.Windows.Forms.Label lbl_accomodationDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_RoomNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_RoomType;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_RoomRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Adult;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Child;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Available;
    }
}