﻿namespace maxxis
{
    partial class Main_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnl_Maxx_Hotel = new System.Windows.Forms.Panel();
            this.lbl_Maxxis_Hotel = new System.Windows.Forms.Label();
            this.lbl_FrontDeskSystem = new System.Windows.Forms.Label();
            this.pnl_FloorMenu_Summary = new System.Windows.Forms.Panel();
            this.pnl_FloorMenu_Selector = new System.Windows.Forms.Panel();
            this.lbl_PWD_Summary = new System.Windows.Forms.Label();
            this.lbl_UrbanView_Summary = new System.Windows.Forms.Label();
            this.lbl_UrbanCorner_Summary = new System.Windows.Forms.Label();
            this.lbl_UrbanRoom_Summary = new System.Windows.Forms.Label();
            this.lbl_All_Summary = new System.Windows.Forms.Label();
            this.lbl_VacantSummary = new System.Windows.Forms.Label();
            this.btn_2nd_Floor = new System.Windows.Forms.Button();
            this.btn_3rd_Floor = new System.Windows.Forms.Button();
            this.btn_5th_Floor = new System.Windows.Forms.Button();
            this.btn_6th_Floor = new System.Windows.Forms.Button();
            this.btn_7th_Floor = new System.Windows.Forms.Button();
            this.btn_8th_Floor = new System.Windows.Forms.Button();
            this.btn_9th_Floor = new System.Windows.Forms.Button();
            this.btn_10th_Floor = new System.Windows.Forms.Button();
            this.btn_All_Floors = new System.Windows.Forms.Button();
            this.lbl_Floor_Menu = new System.Windows.Forms.Label();
            this.pnl_RoomButtons = new System.Windows.Forms.Panel();
            this.btn_Dirty = new System.Windows.Forms.Button();
            this.btn_GuaranteedBuyer = new System.Windows.Forms.Button();
            this.btn_Reserved = new System.Windows.Forms.Button();
            this.btn_OutOfOrder = new System.Windows.Forms.Button();
            this.btn_DueOut = new System.Windows.Forms.Button();
            this.btn_Vacant = new System.Windows.Forms.Button();
            this.btn_Occupied = new System.Windows.Forms.Button();
            this.btn_AllRooms = new System.Windows.Forms.Button();
            this.pnl_OperatorMenu = new System.Windows.Forms.Panel();
            this.btn_reservationList = new System.Windows.Forms.Button();
            this.pnl_MenuSelector = new System.Windows.Forms.Panel();
            this.btn_SignOut = new System.Windows.Forms.Button();
            this.btn_RoomInformation = new System.Windows.Forms.Button();
            this.btn_GuestFolio = new System.Windows.Forms.Button();
            this.btn_FrontDesk = new System.Windows.Forms.Button();
            this.btn_GuestMessage = new System.Windows.Forms.Button();
            this.btn_WakeupCall = new System.Windows.Forms.Button();
            this.btn_NewReservation = new System.Windows.Forms.Button();
            this.btn_CheckOut = new System.Windows.Forms.Button();
            this.btn_Reports = new System.Windows.Forms.Button();
            this.btn_TransferRoom = new System.Windows.Forms.Button();
            this.btn_GuestRequest = new System.Windows.Forms.Button();
            this.btn_New_Walkin_Guest = new System.Windows.Forms.Button();
            this.btn_Loss_Breakage_Fines = new System.Windows.Forms.Button();
            this.btn_GuestArrivalList = new System.Windows.Forms.Button();
            this.pnl_MainPanel = new System.Windows.Forms.Panel();
            this.stat_strip = new System.Windows.Forms.StatusStrip();
            this.stat_lbl_Rooms = new System.Windows.Forms.ToolStripStatusLabel();
            this.stat_lbl_Pax = new System.Windows.Forms.ToolStripStatusLabel();
            this.divider_stat1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.pnl_2ndfloor = new System.Windows.Forms.Panel();
            this.btn_Room26 = new System.Windows.Forms.Button();
            this.btn_Room25 = new System.Windows.Forms.Button();
            this.btn_Room24 = new System.Windows.Forms.Button();
            this.btn_Room23 = new System.Windows.Forms.Button();
            this.btn_Room22 = new System.Windows.Forms.Button();
            this.btn_Room21 = new System.Windows.Forms.Button();
            this.pnl_3rdfloor = new System.Windows.Forms.Panel();
            this.btn_Room36 = new System.Windows.Forms.Button();
            this.btn_Room35 = new System.Windows.Forms.Button();
            this.btn_Room34 = new System.Windows.Forms.Button();
            this.btn_Room33 = new System.Windows.Forms.Button();
            this.btn_Room32 = new System.Windows.Forms.Button();
            this.btn_Room31 = new System.Windows.Forms.Button();
            this.pnl_5thfloor = new System.Windows.Forms.Panel();
            this.btn_Room56 = new System.Windows.Forms.Button();
            this.btn_Room55 = new System.Windows.Forms.Button();
            this.btn_Room54 = new System.Windows.Forms.Button();
            this.btn_Room53 = new System.Windows.Forms.Button();
            this.btn_Room52 = new System.Windows.Forms.Button();
            this.btn_Room51 = new System.Windows.Forms.Button();
            this.pnl_6thfloor = new System.Windows.Forms.Panel();
            this.btn_Room66 = new System.Windows.Forms.Button();
            this.btn_Room65 = new System.Windows.Forms.Button();
            this.btn_Room64 = new System.Windows.Forms.Button();
            this.btn_Room63 = new System.Windows.Forms.Button();
            this.btn_Room62 = new System.Windows.Forms.Button();
            this.btn_Room61 = new System.Windows.Forms.Button();
            this.pnl_7thfloor = new System.Windows.Forms.Panel();
            this.btn_Room76 = new System.Windows.Forms.Button();
            this.btn_Room75 = new System.Windows.Forms.Button();
            this.btn_Room74 = new System.Windows.Forms.Button();
            this.btn_Room73 = new System.Windows.Forms.Button();
            this.btn_Room72 = new System.Windows.Forms.Button();
            this.btn_Room71 = new System.Windows.Forms.Button();
            this.pnl_8thfloor = new System.Windows.Forms.Panel();
            this.btn_Room86 = new System.Windows.Forms.Button();
            this.btn_Room85 = new System.Windows.Forms.Button();
            this.btn_Room84 = new System.Windows.Forms.Button();
            this.btn_Room83 = new System.Windows.Forms.Button();
            this.btn_Room82 = new System.Windows.Forms.Button();
            this.btn_Room81 = new System.Windows.Forms.Button();
            this.pnl_9thfloor = new System.Windows.Forms.Panel();
            this.btn_Room96 = new System.Windows.Forms.Button();
            this.btn_Room95 = new System.Windows.Forms.Button();
            this.btn_Room94 = new System.Windows.Forms.Button();
            this.btn_Room93 = new System.Windows.Forms.Button();
            this.btn_Room92 = new System.Windows.Forms.Button();
            this.btn_Room91 = new System.Windows.Forms.Button();
            this.pnl_10thfloor = new System.Windows.Forms.Panel();
            this.btn_Room106 = new System.Windows.Forms.Button();
            this.btn_Room105 = new System.Windows.Forms.Button();
            this.btn_Room104 = new System.Windows.Forms.Button();
            this.btn_Room103 = new System.Windows.Forms.Button();
            this.btn_Room102 = new System.Windows.Forms.Button();
            this.btn_Room101 = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.lbl_timer = new System.Windows.Forms.Label();
            this.btn_refresh = new System.Windows.Forms.Button();
            this.pnl_Maxx_Hotel.SuspendLayout();
            this.pnl_FloorMenu_Summary.SuspendLayout();
            this.pnl_RoomButtons.SuspendLayout();
            this.pnl_OperatorMenu.SuspendLayout();
            this.pnl_MainPanel.SuspendLayout();
            this.stat_strip.SuspendLayout();
            this.pnl_2ndfloor.SuspendLayout();
            this.pnl_3rdfloor.SuspendLayout();
            this.pnl_5thfloor.SuspendLayout();
            this.pnl_6thfloor.SuspendLayout();
            this.pnl_7thfloor.SuspendLayout();
            this.pnl_8thfloor.SuspendLayout();
            this.pnl_9thfloor.SuspendLayout();
            this.pnl_10thfloor.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_Maxx_Hotel
            // 
            this.pnl_Maxx_Hotel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.pnl_Maxx_Hotel.Controls.Add(this.lbl_Maxxis_Hotel);
            this.pnl_Maxx_Hotel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl_Maxx_Hotel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnl_Maxx_Hotel.Location = new System.Drawing.Point(0, 0);
            this.pnl_Maxx_Hotel.Name = "pnl_Maxx_Hotel";
            this.pnl_Maxx_Hotel.Size = new System.Drawing.Size(200, 65);
            this.pnl_Maxx_Hotel.TabIndex = 6;
            // 
            // lbl_Maxxis_Hotel
            // 
            this.lbl_Maxxis_Hotel.AutoSize = true;
            this.lbl_Maxxis_Hotel.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 15F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Maxxis_Hotel.Location = new System.Drawing.Point(52, 15);
            this.lbl_Maxxis_Hotel.Name = "lbl_Maxxis_Hotel";
            this.lbl_Maxxis_Hotel.Size = new System.Drawing.Size(86, 24);
            this.lbl_Maxxis_Hotel.TabIndex = 0;
            this.lbl_Maxxis_Hotel.Text = "MAXXIS";
            this.lbl_Maxxis_Hotel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbl_FrontDeskSystem
            // 
            this.lbl_FrontDeskSystem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_FrontDeskSystem.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_FrontDeskSystem.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_FrontDeskSystem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_FrontDeskSystem.Location = new System.Drawing.Point(0, 0);
            this.lbl_FrontDeskSystem.Name = "lbl_FrontDeskSystem";
            this.lbl_FrontDeskSystem.Size = new System.Drawing.Size(1300, 45);
            this.lbl_FrontDeskSystem.TabIndex = 7;
            this.lbl_FrontDeskSystem.Text = "FRONT DESK SYSTEM";
            this.lbl_FrontDeskSystem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnl_FloorMenu_Summary
            // 
            this.pnl_FloorMenu_Summary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.pnl_FloorMenu_Summary.Controls.Add(this.btn_refresh);
            this.pnl_FloorMenu_Summary.Controls.Add(this.pnl_FloorMenu_Selector);
            this.pnl_FloorMenu_Summary.Controls.Add(this.lbl_PWD_Summary);
            this.pnl_FloorMenu_Summary.Controls.Add(this.lbl_UrbanView_Summary);
            this.pnl_FloorMenu_Summary.Controls.Add(this.lbl_UrbanCorner_Summary);
            this.pnl_FloorMenu_Summary.Controls.Add(this.lbl_UrbanRoom_Summary);
            this.pnl_FloorMenu_Summary.Controls.Add(this.lbl_All_Summary);
            this.pnl_FloorMenu_Summary.Controls.Add(this.lbl_VacantSummary);
            this.pnl_FloorMenu_Summary.Controls.Add(this.btn_2nd_Floor);
            this.pnl_FloorMenu_Summary.Controls.Add(this.btn_3rd_Floor);
            this.pnl_FloorMenu_Summary.Controls.Add(this.btn_5th_Floor);
            this.pnl_FloorMenu_Summary.Controls.Add(this.btn_6th_Floor);
            this.pnl_FloorMenu_Summary.Controls.Add(this.btn_7th_Floor);
            this.pnl_FloorMenu_Summary.Controls.Add(this.btn_8th_Floor);
            this.pnl_FloorMenu_Summary.Controls.Add(this.btn_9th_Floor);
            this.pnl_FloorMenu_Summary.Controls.Add(this.btn_10th_Floor);
            this.pnl_FloorMenu_Summary.Controls.Add(this.btn_All_Floors);
            this.pnl_FloorMenu_Summary.Controls.Add(this.lbl_Floor_Menu);
            this.pnl_FloorMenu_Summary.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnl_FloorMenu_Summary.Location = new System.Drawing.Point(0, 45);
            this.pnl_FloorMenu_Summary.Name = "pnl_FloorMenu_Summary";
            this.pnl_FloorMenu_Summary.Size = new System.Drawing.Size(200, 685);
            this.pnl_FloorMenu_Summary.TabIndex = 8;
            // 
            // pnl_FloorMenu_Selector
            // 
            this.pnl_FloorMenu_Selector.BackColor = System.Drawing.Color.Yellow;
            this.pnl_FloorMenu_Selector.Location = new System.Drawing.Point(-6, 80);
            this.pnl_FloorMenu_Selector.Name = "pnl_FloorMenu_Selector";
            this.pnl_FloorMenu_Selector.Size = new System.Drawing.Size(10, 38);
            this.pnl_FloorMenu_Selector.TabIndex = 0;
            // 
            // lbl_PWD_Summary
            // 
            this.lbl_PWD_Summary.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_PWD_Summary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_PWD_Summary.Location = new System.Drawing.Point(0, 578);
            this.lbl_PWD_Summary.Name = "lbl_PWD_Summary";
            this.lbl_PWD_Summary.Size = new System.Drawing.Size(200, 26);
            this.lbl_PWD_Summary.TabIndex = 16;
            this.lbl_PWD_Summary.Text = "PWD Urban Room (1)";
            this.lbl_PWD_Summary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_UrbanView_Summary
            // 
            this.lbl_UrbanView_Summary.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_UrbanView_Summary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_UrbanView_Summary.Location = new System.Drawing.Point(0, 552);
            this.lbl_UrbanView_Summary.Name = "lbl_UrbanView_Summary";
            this.lbl_UrbanView_Summary.Size = new System.Drawing.Size(200, 26);
            this.lbl_UrbanView_Summary.TabIndex = 15;
            this.lbl_UrbanView_Summary.Text = "Urban View (16)";
            this.lbl_UrbanView_Summary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_UrbanCorner_Summary
            // 
            this.lbl_UrbanCorner_Summary.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_UrbanCorner_Summary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_UrbanCorner_Summary.Location = new System.Drawing.Point(0, 526);
            this.lbl_UrbanCorner_Summary.Name = "lbl_UrbanCorner_Summary";
            this.lbl_UrbanCorner_Summary.Size = new System.Drawing.Size(200, 26);
            this.lbl_UrbanCorner_Summary.TabIndex = 14;
            this.lbl_UrbanCorner_Summary.Text = "Urban Corner (16)";
            this.lbl_UrbanCorner_Summary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_UrbanRoom_Summary
            // 
            this.lbl_UrbanRoom_Summary.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_UrbanRoom_Summary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_UrbanRoom_Summary.Location = new System.Drawing.Point(0, 500);
            this.lbl_UrbanRoom_Summary.Name = "lbl_UrbanRoom_Summary";
            this.lbl_UrbanRoom_Summary.Size = new System.Drawing.Size(200, 26);
            this.lbl_UrbanRoom_Summary.TabIndex = 13;
            this.lbl_UrbanRoom_Summary.Text = "Urban Room (15)";
            this.lbl_UrbanRoom_Summary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_All_Summary
            // 
            this.lbl_All_Summary.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_All_Summary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_All_Summary.Location = new System.Drawing.Point(0, 474);
            this.lbl_All_Summary.Name = "lbl_All_Summary";
            this.lbl_All_Summary.Size = new System.Drawing.Size(200, 26);
            this.lbl_All_Summary.TabIndex = 12;
            this.lbl_All_Summary.Text = "All (48)";
            this.lbl_All_Summary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_VacantSummary
            // 
            this.lbl_VacantSummary.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_VacantSummary.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.lbl_VacantSummary.Location = new System.Drawing.Point(0, 438);
            this.lbl_VacantSummary.Name = "lbl_VacantSummary";
            this.lbl_VacantSummary.Size = new System.Drawing.Size(200, 36);
            this.lbl_VacantSummary.TabIndex = 11;
            this.lbl_VacantSummary.Text = "Vacant Summary";
            this.lbl_VacantSummary.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_2nd_Floor
            // 
            this.btn_2nd_Floor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_2nd_Floor.FlatAppearance.BorderSize = 0;
            this.btn_2nd_Floor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_2nd_Floor.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_2nd_Floor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_2nd_Floor.Location = new System.Drawing.Point(0, 384);
            this.btn_2nd_Floor.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn_2nd_Floor.Name = "btn_2nd_Floor";
            this.btn_2nd_Floor.Size = new System.Drawing.Size(215, 38);
            this.btn_2nd_Floor.TabIndex = 10;
            this.btn_2nd_Floor.Text = "2ND FLOOR";
            this.btn_2nd_Floor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_2nd_Floor.UseVisualStyleBackColor = false;
            // 
            // btn_3rd_Floor
            // 
            this.btn_3rd_Floor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_3rd_Floor.FlatAppearance.BorderSize = 0;
            this.btn_3rd_Floor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_3rd_Floor.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_3rd_Floor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_3rd_Floor.Location = new System.Drawing.Point(0, 346);
            this.btn_3rd_Floor.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn_3rd_Floor.Name = "btn_3rd_Floor";
            this.btn_3rd_Floor.Size = new System.Drawing.Size(200, 38);
            this.btn_3rd_Floor.TabIndex = 9;
            this.btn_3rd_Floor.Text = "3RD FLOOR";
            this.btn_3rd_Floor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_3rd_Floor.UseVisualStyleBackColor = false;
            // 
            // btn_5th_Floor
            // 
            this.btn_5th_Floor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_5th_Floor.FlatAppearance.BorderSize = 0;
            this.btn_5th_Floor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_5th_Floor.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_5th_Floor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_5th_Floor.Location = new System.Drawing.Point(0, 308);
            this.btn_5th_Floor.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn_5th_Floor.Name = "btn_5th_Floor";
            this.btn_5th_Floor.Size = new System.Drawing.Size(200, 38);
            this.btn_5th_Floor.TabIndex = 8;
            this.btn_5th_Floor.Text = "5TH FLOOR";
            this.btn_5th_Floor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_5th_Floor.UseVisualStyleBackColor = false;
            // 
            // btn_6th_Floor
            // 
            this.btn_6th_Floor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_6th_Floor.FlatAppearance.BorderSize = 0;
            this.btn_6th_Floor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_6th_Floor.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_6th_Floor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_6th_Floor.Location = new System.Drawing.Point(0, 270);
            this.btn_6th_Floor.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn_6th_Floor.Name = "btn_6th_Floor";
            this.btn_6th_Floor.Size = new System.Drawing.Size(200, 38);
            this.btn_6th_Floor.TabIndex = 7;
            this.btn_6th_Floor.Text = "6TH FLOOR";
            this.btn_6th_Floor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_6th_Floor.UseVisualStyleBackColor = false;
            // 
            // btn_7th_Floor
            // 
            this.btn_7th_Floor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_7th_Floor.FlatAppearance.BorderSize = 0;
            this.btn_7th_Floor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_7th_Floor.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_7th_Floor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_7th_Floor.Location = new System.Drawing.Point(0, 232);
            this.btn_7th_Floor.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn_7th_Floor.Name = "btn_7th_Floor";
            this.btn_7th_Floor.Size = new System.Drawing.Size(200, 38);
            this.btn_7th_Floor.TabIndex = 6;
            this.btn_7th_Floor.Text = "7TH FLOOR";
            this.btn_7th_Floor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_7th_Floor.UseVisualStyleBackColor = false;
            // 
            // btn_8th_Floor
            // 
            this.btn_8th_Floor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_8th_Floor.FlatAppearance.BorderSize = 0;
            this.btn_8th_Floor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_8th_Floor.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_8th_Floor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_8th_Floor.Location = new System.Drawing.Point(0, 194);
            this.btn_8th_Floor.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn_8th_Floor.Name = "btn_8th_Floor";
            this.btn_8th_Floor.Size = new System.Drawing.Size(200, 38);
            this.btn_8th_Floor.TabIndex = 5;
            this.btn_8th_Floor.Text = "8TH FLOOR";
            this.btn_8th_Floor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_8th_Floor.UseVisualStyleBackColor = false;
            // 
            // btn_9th_Floor
            // 
            this.btn_9th_Floor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_9th_Floor.FlatAppearance.BorderSize = 0;
            this.btn_9th_Floor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_9th_Floor.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_9th_Floor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_9th_Floor.Location = new System.Drawing.Point(0, 156);
            this.btn_9th_Floor.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn_9th_Floor.Name = "btn_9th_Floor";
            this.btn_9th_Floor.Size = new System.Drawing.Size(200, 38);
            this.btn_9th_Floor.TabIndex = 4;
            this.btn_9th_Floor.Text = "9TH FLOOR";
            this.btn_9th_Floor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_9th_Floor.UseVisualStyleBackColor = false;
            // 
            // btn_10th_Floor
            // 
            this.btn_10th_Floor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_10th_Floor.FlatAppearance.BorderSize = 0;
            this.btn_10th_Floor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_10th_Floor.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_10th_Floor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_10th_Floor.Location = new System.Drawing.Point(0, 118);
            this.btn_10th_Floor.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn_10th_Floor.Name = "btn_10th_Floor";
            this.btn_10th_Floor.Size = new System.Drawing.Size(200, 38);
            this.btn_10th_Floor.TabIndex = 3;
            this.btn_10th_Floor.Text = "10TH FLOOR";
            this.btn_10th_Floor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_10th_Floor.UseVisualStyleBackColor = false;
            // 
            // btn_All_Floors
            // 
            this.btn_All_Floors.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_All_Floors.FlatAppearance.BorderSize = 0;
            this.btn_All_Floors.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_All_Floors.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_All_Floors.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_All_Floors.Location = new System.Drawing.Point(0, 80);
            this.btn_All_Floors.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn_All_Floors.Name = "btn_All_Floors";
            this.btn_All_Floors.Size = new System.Drawing.Size(200, 38);
            this.btn_All_Floors.TabIndex = 2;
            this.btn_All_Floors.Text = "All Floors";
            this.btn_All_Floors.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_All_Floors.UseVisualStyleBackColor = false;
            // 
            // lbl_Floor_Menu
            // 
            this.lbl_Floor_Menu.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Floor_Menu.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.lbl_Floor_Menu.Location = new System.Drawing.Point(0, 41);
            this.lbl_Floor_Menu.Name = "lbl_Floor_Menu";
            this.lbl_Floor_Menu.Size = new System.Drawing.Size(200, 36);
            this.lbl_Floor_Menu.TabIndex = 0;
            this.lbl_Floor_Menu.Text = "Floor Menu";
            this.lbl_Floor_Menu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnl_RoomButtons
            // 
            this.pnl_RoomButtons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.pnl_RoomButtons.Controls.Add(this.btn_Dirty);
            this.pnl_RoomButtons.Controls.Add(this.btn_GuaranteedBuyer);
            this.pnl_RoomButtons.Controls.Add(this.btn_Reserved);
            this.pnl_RoomButtons.Controls.Add(this.btn_OutOfOrder);
            this.pnl_RoomButtons.Controls.Add(this.btn_DueOut);
            this.pnl_RoomButtons.Controls.Add(this.btn_Vacant);
            this.pnl_RoomButtons.Controls.Add(this.btn_Occupied);
            this.pnl_RoomButtons.Controls.Add(this.btn_AllRooms);
            this.pnl_RoomButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_RoomButtons.Location = new System.Drawing.Point(200, 45);
            this.pnl_RoomButtons.Name = "pnl_RoomButtons";
            this.pnl_RoomButtons.Size = new System.Drawing.Size(1100, 47);
            this.pnl_RoomButtons.TabIndex = 9;
            // 
            // btn_Dirty
            // 
            this.btn_Dirty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.btn_Dirty.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.btn_Dirty.FlatAppearance.CheckedBackColor = System.Drawing.Color.Silver;
            this.btn_Dirty.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btn_Dirty.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.btn_Dirty.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Dirty.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Dirty.Location = new System.Drawing.Point(955, 3);
            this.btn_Dirty.Name = "btn_Dirty";
            this.btn_Dirty.Size = new System.Drawing.Size(120, 38);
            this.btn_Dirty.TabIndex = 7;
            this.btn_Dirty.Text = "     Dirty (0)";
            this.btn_Dirty.UseVisualStyleBackColor = false;
            // 
            // btn_GuaranteedBuyer
            // 
            this.btn_GuaranteedBuyer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.btn_GuaranteedBuyer.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.btn_GuaranteedBuyer.FlatAppearance.CheckedBackColor = System.Drawing.Color.Silver;
            this.btn_GuaranteedBuyer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btn_GuaranteedBuyer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.btn_GuaranteedBuyer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_GuaranteedBuyer.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_GuaranteedBuyer.Location = new System.Drawing.Point(792, 4);
            this.btn_GuaranteedBuyer.Name = "btn_GuaranteedBuyer";
            this.btn_GuaranteedBuyer.Size = new System.Drawing.Size(157, 38);
            this.btn_GuaranteedBuyer.TabIndex = 6;
            this.btn_GuaranteedBuyer.Text = "     Guaranteed B... (0)";
            this.btn_GuaranteedBuyer.UseVisualStyleBackColor = false;
            // 
            // btn_Reserved
            // 
            this.btn_Reserved.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.btn_Reserved.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.btn_Reserved.FlatAppearance.CheckedBackColor = System.Drawing.Color.Silver;
            this.btn_Reserved.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btn_Reserved.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.btn_Reserved.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Reserved.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Reserved.Location = new System.Drawing.Point(661, 4);
            this.btn_Reserved.Name = "btn_Reserved";
            this.btn_Reserved.Size = new System.Drawing.Size(120, 38);
            this.btn_Reserved.TabIndex = 5;
            this.btn_Reserved.Text = "     Reserved (0)";
            this.btn_Reserved.UseVisualStyleBackColor = false;
            // 
            // btn_OutOfOrder
            // 
            this.btn_OutOfOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.btn_OutOfOrder.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.btn_OutOfOrder.FlatAppearance.CheckedBackColor = System.Drawing.Color.Silver;
            this.btn_OutOfOrder.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btn_OutOfOrder.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.btn_OutOfOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_OutOfOrder.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_OutOfOrder.Location = new System.Drawing.Point(530, 4);
            this.btn_OutOfOrder.Name = "btn_OutOfOrder";
            this.btn_OutOfOrder.Size = new System.Drawing.Size(120, 38);
            this.btn_OutOfOrder.TabIndex = 4;
            this.btn_OutOfOrder.Text = "     Out Of Order(0)";
            this.btn_OutOfOrder.UseVisualStyleBackColor = false;
            // 
            // btn_DueOut
            // 
            this.btn_DueOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.btn_DueOut.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.btn_DueOut.FlatAppearance.CheckedBackColor = System.Drawing.Color.Silver;
            this.btn_DueOut.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btn_DueOut.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.btn_DueOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_DueOut.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DueOut.Location = new System.Drawing.Point(399, 4);
            this.btn_DueOut.Name = "btn_DueOut";
            this.btn_DueOut.Size = new System.Drawing.Size(120, 38);
            this.btn_DueOut.TabIndex = 3;
            this.btn_DueOut.Text = "     DueOut (0)";
            this.btn_DueOut.UseVisualStyleBackColor = false;
            // 
            // btn_Vacant
            // 
            this.btn_Vacant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.btn_Vacant.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.btn_Vacant.FlatAppearance.CheckedBackColor = System.Drawing.Color.Silver;
            this.btn_Vacant.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btn_Vacant.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.btn_Vacant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Vacant.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Vacant.Location = new System.Drawing.Point(268, 4);
            this.btn_Vacant.Name = "btn_Vacant";
            this.btn_Vacant.Size = new System.Drawing.Size(120, 38);
            this.btn_Vacant.TabIndex = 2;
            this.btn_Vacant.Text = "     Vacant(48)";
            this.btn_Vacant.UseVisualStyleBackColor = false;
            // 
            // btn_Occupied
            // 
            this.btn_Occupied.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.btn_Occupied.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.btn_Occupied.FlatAppearance.CheckedBackColor = System.Drawing.Color.Silver;
            this.btn_Occupied.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btn_Occupied.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.btn_Occupied.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Occupied.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Occupied.Location = new System.Drawing.Point(137, 4);
            this.btn_Occupied.Name = "btn_Occupied";
            this.btn_Occupied.Size = new System.Drawing.Size(120, 38);
            this.btn_Occupied.TabIndex = 1;
            this.btn_Occupied.Text = "     Occupied (0)";
            this.btn_Occupied.UseVisualStyleBackColor = false;
            // 
            // btn_AllRooms
            // 
            this.btn_AllRooms.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.btn_AllRooms.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.btn_AllRooms.FlatAppearance.CheckedBackColor = System.Drawing.Color.Silver;
            this.btn_AllRooms.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btn_AllRooms.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.btn_AllRooms.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_AllRooms.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AllRooms.Location = new System.Drawing.Point(6, 4);
            this.btn_AllRooms.Name = "btn_AllRooms";
            this.btn_AllRooms.Size = new System.Drawing.Size(120, 38);
            this.btn_AllRooms.TabIndex = 0;
            this.btn_AllRooms.Text = "     Rooms (48)";
            this.btn_AllRooms.UseVisualStyleBackColor = false;
            // 
            // pnl_OperatorMenu
            // 
            this.pnl_OperatorMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.pnl_OperatorMenu.Controls.Add(this.btn_reservationList);
            this.pnl_OperatorMenu.Controls.Add(this.pnl_MenuSelector);
            this.pnl_OperatorMenu.Controls.Add(this.btn_SignOut);
            this.pnl_OperatorMenu.Controls.Add(this.btn_RoomInformation);
            this.pnl_OperatorMenu.Controls.Add(this.btn_GuestFolio);
            this.pnl_OperatorMenu.Controls.Add(this.btn_FrontDesk);
            this.pnl_OperatorMenu.Controls.Add(this.btn_GuestMessage);
            this.pnl_OperatorMenu.Controls.Add(this.btn_WakeupCall);
            this.pnl_OperatorMenu.Controls.Add(this.btn_NewReservation);
            this.pnl_OperatorMenu.Controls.Add(this.btn_CheckOut);
            this.pnl_OperatorMenu.Controls.Add(this.btn_Reports);
            this.pnl_OperatorMenu.Controls.Add(this.btn_TransferRoom);
            this.pnl_OperatorMenu.Controls.Add(this.btn_GuestRequest);
            this.pnl_OperatorMenu.Controls.Add(this.btn_New_Walkin_Guest);
            this.pnl_OperatorMenu.Controls.Add(this.btn_Loss_Breakage_Fines);
            this.pnl_OperatorMenu.Controls.Add(this.btn_GuestArrivalList);
            this.pnl_OperatorMenu.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnl_OperatorMenu.Location = new System.Drawing.Point(1024, 92);
            this.pnl_OperatorMenu.Name = "pnl_OperatorMenu";
            this.pnl_OperatorMenu.Size = new System.Drawing.Size(276, 638);
            this.pnl_OperatorMenu.TabIndex = 10;
            // 
            // btn_reservationList
            // 
            this.btn_reservationList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_reservationList.FlatAppearance.BorderSize = 0;
            this.btn_reservationList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reservationList.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reservationList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_reservationList.Image = global::maxxis.Properties.Resources.Reservation_List;
            this.btn_reservationList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reservationList.Location = new System.Drawing.Point(37, 425);
            this.btn_reservationList.Name = "btn_reservationList";
            this.btn_reservationList.Size = new System.Drawing.Size(200, 35);
            this.btn_reservationList.TabIndex = 57;
            this.btn_reservationList.Text = "      Reservation List";
            this.btn_reservationList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reservationList.UseVisualStyleBackColor = false;
            this.btn_reservationList.Click += new System.EventHandler(this.btn_reservationList_Click);
            // 
            // pnl_MenuSelector
            // 
            this.pnl_MenuSelector.BackColor = System.Drawing.Color.MediumTurquoise;
            this.pnl_MenuSelector.Location = new System.Drawing.Point(271, 15);
            this.pnl_MenuSelector.Name = "pnl_MenuSelector";
            this.pnl_MenuSelector.Size = new System.Drawing.Size(10, 45);
            this.pnl_MenuSelector.TabIndex = 1;
            // 
            // btn_SignOut
            // 
            this.btn_SignOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_SignOut.FlatAppearance.BorderSize = 0;
            this.btn_SignOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SignOut.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SignOut.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_SignOut.Image = global::maxxis.Properties.Resources.Check_Out;
            this.btn_SignOut.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_SignOut.Location = new System.Drawing.Point(37, 584);
            this.btn_SignOut.Name = "btn_SignOut";
            this.btn_SignOut.Size = new System.Drawing.Size(200, 30);
            this.btn_SignOut.TabIndex = 0;
            this.btn_SignOut.Text = "      Sign-out";
            this.btn_SignOut.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_SignOut.UseVisualStyleBackColor = false;
            this.btn_SignOut.Click += new System.EventHandler(this.btn_SignOut_Click);
            // 
            // btn_RoomInformation
            // 
            this.btn_RoomInformation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_RoomInformation.FlatAppearance.BorderSize = 0;
            this.btn_RoomInformation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_RoomInformation.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_RoomInformation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_RoomInformation.Image = global::maxxis.Properties.Resources.Wake_up_Call;
            this.btn_RoomInformation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_RoomInformation.Location = new System.Drawing.Point(37, 15);
            this.btn_RoomInformation.Name = "btn_RoomInformation";
            this.btn_RoomInformation.Size = new System.Drawing.Size(200, 35);
            this.btn_RoomInformation.TabIndex = 4;
            this.btn_RoomInformation.Text = "      Room Information";
            this.btn_RoomInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_RoomInformation.UseVisualStyleBackColor = false;
            // 
            // btn_GuestFolio
            // 
            this.btn_GuestFolio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_GuestFolio.FlatAppearance.BorderSize = 0;
            this.btn_GuestFolio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_GuestFolio.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_GuestFolio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_GuestFolio.Image = global::maxxis.Properties.Resources.Guest_Information;
            this.btn_GuestFolio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_GuestFolio.Location = new System.Drawing.Point(37, 56);
            this.btn_GuestFolio.Name = "btn_GuestFolio";
            this.btn_GuestFolio.Size = new System.Drawing.Size(200, 35);
            this.btn_GuestFolio.TabIndex = 7;
            this.btn_GuestFolio.Text = "      Guest Folio";
            this.btn_GuestFolio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_GuestFolio.UseVisualStyleBackColor = false;
            this.btn_GuestFolio.Click += new System.EventHandler(this.btn_GuestFolio_Click);
            // 
            // btn_FrontDesk
            // 
            this.btn_FrontDesk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_FrontDesk.FlatAppearance.BorderSize = 0;
            this.btn_FrontDesk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_FrontDesk.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_FrontDesk.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_FrontDesk.Image = global::maxxis.Properties.Resources.Front_Desk;
            this.btn_FrontDesk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_FrontDesk.Location = new System.Drawing.Point(37, 548);
            this.btn_FrontDesk.Name = "btn_FrontDesk";
            this.btn_FrontDesk.Size = new System.Drawing.Size(200, 30);
            this.btn_FrontDesk.TabIndex = 13;
            this.btn_FrontDesk.Text = "      Front Desk";
            this.btn_FrontDesk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_FrontDesk.UseVisualStyleBackColor = false;
            this.btn_FrontDesk.Click += new System.EventHandler(this.btn_FrontDesk_Click);
            // 
            // btn_GuestMessage
            // 
            this.btn_GuestMessage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_GuestMessage.FlatAppearance.BorderSize = 0;
            this.btn_GuestMessage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_GuestMessage.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_GuestMessage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_GuestMessage.Image = global::maxxis.Properties.Resources.Guest_Message;
            this.btn_GuestMessage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_GuestMessage.Location = new System.Drawing.Point(37, 97);
            this.btn_GuestMessage.Name = "btn_GuestMessage";
            this.btn_GuestMessage.Size = new System.Drawing.Size(200, 35);
            this.btn_GuestMessage.TabIndex = 3;
            this.btn_GuestMessage.Text = "      Guest Message";
            this.btn_GuestMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_GuestMessage.UseVisualStyleBackColor = false;
            this.btn_GuestMessage.Click += new System.EventHandler(this.btn_GuestMessage_Click);
            // 
            // btn_WakeupCall
            // 
            this.btn_WakeupCall.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_WakeupCall.FlatAppearance.BorderSize = 0;
            this.btn_WakeupCall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_WakeupCall.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_WakeupCall.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_WakeupCall.Image = global::maxxis.Properties.Resources.Wake_up_Call;
            this.btn_WakeupCall.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_WakeupCall.Location = new System.Drawing.Point(37, 138);
            this.btn_WakeupCall.Name = "btn_WakeupCall";
            this.btn_WakeupCall.Size = new System.Drawing.Size(200, 35);
            this.btn_WakeupCall.TabIndex = 5;
            this.btn_WakeupCall.Text = "      Wake-up Call";
            this.btn_WakeupCall.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_WakeupCall.UseVisualStyleBackColor = false;
            // 
            // btn_NewReservation
            // 
            this.btn_NewReservation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_NewReservation.FlatAppearance.BorderSize = 0;
            this.btn_NewReservation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_NewReservation.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_NewReservation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_NewReservation.Image = global::maxxis.Properties.Resources.New_Reservation;
            this.btn_NewReservation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_NewReservation.Location = new System.Drawing.Point(37, 343);
            this.btn_NewReservation.Name = "btn_NewReservation";
            this.btn_NewReservation.Size = new System.Drawing.Size(200, 35);
            this.btn_NewReservation.TabIndex = 8;
            this.btn_NewReservation.Text = "      New Reservation";
            this.btn_NewReservation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_NewReservation.UseVisualStyleBackColor = false;
            this.btn_NewReservation.Click += new System.EventHandler(this.btn_NewReservation_Click);
            // 
            // btn_CheckOut
            // 
            this.btn_CheckOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_CheckOut.FlatAppearance.BorderSize = 0;
            this.btn_CheckOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_CheckOut.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_CheckOut.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_CheckOut.Image = global::maxxis.Properties.Resources.Check_Out;
            this.btn_CheckOut.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_CheckOut.Location = new System.Drawing.Point(37, 302);
            this.btn_CheckOut.Name = "btn_CheckOut";
            this.btn_CheckOut.Size = new System.Drawing.Size(200, 35);
            this.btn_CheckOut.TabIndex = 10;
            this.btn_CheckOut.Text = "      Check Out";
            this.btn_CheckOut.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_CheckOut.UseVisualStyleBackColor = false;
            // 
            // btn_Reports
            // 
            this.btn_Reports.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_Reports.FlatAppearance.BorderSize = 0;
            this.btn_Reports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Reports.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Reports.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_Reports.Image = global::maxxis.Properties.Resources.Reports;
            this.btn_Reports.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Reports.Location = new System.Drawing.Point(37, 507);
            this.btn_Reports.Name = "btn_Reports";
            this.btn_Reports.Size = new System.Drawing.Size(200, 35);
            this.btn_Reports.TabIndex = 1;
            this.btn_Reports.Text = "      Reports";
            this.btn_Reports.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Reports.UseVisualStyleBackColor = false;
            this.btn_Reports.Click += new System.EventHandler(this.btn_Reports_Click);
            // 
            // btn_TransferRoom
            // 
            this.btn_TransferRoom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_TransferRoom.FlatAppearance.BorderSize = 0;
            this.btn_TransferRoom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_TransferRoom.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TransferRoom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_TransferRoom.Image = global::maxxis.Properties.Resources.Transfer_Rooms;
            this.btn_TransferRoom.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TransferRoom.Location = new System.Drawing.Point(37, 261);
            this.btn_TransferRoom.Name = "btn_TransferRoom";
            this.btn_TransferRoom.Size = new System.Drawing.Size(200, 35);
            this.btn_TransferRoom.TabIndex = 6;
            this.btn_TransferRoom.Text = "      Transfer Room";
            this.btn_TransferRoom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TransferRoom.UseVisualStyleBackColor = false;
            // 
            // btn_GuestRequest
            // 
            this.btn_GuestRequest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_GuestRequest.FlatAppearance.BorderSize = 0;
            this.btn_GuestRequest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_GuestRequest.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_GuestRequest.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_GuestRequest.Image = global::maxxis.Properties.Resources.Guest_Request;
            this.btn_GuestRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_GuestRequest.Location = new System.Drawing.Point(37, 179);
            this.btn_GuestRequest.Name = "btn_GuestRequest";
            this.btn_GuestRequest.Size = new System.Drawing.Size(200, 35);
            this.btn_GuestRequest.TabIndex = 2;
            this.btn_GuestRequest.Text = "      Guest Request";
            this.btn_GuestRequest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_GuestRequest.UseVisualStyleBackColor = false;
            // 
            // btn_New_Walkin_Guest
            // 
            this.btn_New_Walkin_Guest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_New_Walkin_Guest.FlatAppearance.BorderSize = 0;
            this.btn_New_Walkin_Guest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_New_Walkin_Guest.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_New_Walkin_Guest.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_New_Walkin_Guest.Image = global::maxxis.Properties.Resources.New_Walkin;
            this.btn_New_Walkin_Guest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_New_Walkin_Guest.Location = new System.Drawing.Point(37, 384);
            this.btn_New_Walkin_Guest.Name = "btn_New_Walkin_Guest";
            this.btn_New_Walkin_Guest.Size = new System.Drawing.Size(200, 35);
            this.btn_New_Walkin_Guest.TabIndex = 11;
            this.btn_New_Walkin_Guest.Text = "      New Walk-in Guest";
            this.btn_New_Walkin_Guest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_New_Walkin_Guest.UseVisualStyleBackColor = false;
            this.btn_New_Walkin_Guest.Click += new System.EventHandler(this.btn_New_Walkin_Guest_Click);
            // 
            // btn_Loss_Breakage_Fines
            // 
            this.btn_Loss_Breakage_Fines.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_Loss_Breakage_Fines.FlatAppearance.BorderSize = 0;
            this.btn_Loss_Breakage_Fines.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Loss_Breakage_Fines.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Loss_Breakage_Fines.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_Loss_Breakage_Fines.Image = global::maxxis.Properties.Resources.Loss__Breakage__Fines;
            this.btn_Loss_Breakage_Fines.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Loss_Breakage_Fines.Location = new System.Drawing.Point(37, 220);
            this.btn_Loss_Breakage_Fines.Name = "btn_Loss_Breakage_Fines";
            this.btn_Loss_Breakage_Fines.Size = new System.Drawing.Size(200, 35);
            this.btn_Loss_Breakage_Fines.TabIndex = 9;
            this.btn_Loss_Breakage_Fines.Text = "      Loss/Breakage/Fines";
            this.btn_Loss_Breakage_Fines.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Loss_Breakage_Fines.UseVisualStyleBackColor = false;
            // 
            // btn_GuestArrivalList
            // 
            this.btn_GuestArrivalList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_GuestArrivalList.FlatAppearance.BorderSize = 0;
            this.btn_GuestArrivalList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_GuestArrivalList.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_GuestArrivalList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.btn_GuestArrivalList.Image = global::maxxis.Properties.Resources.Guest_Arrival_List;
            this.btn_GuestArrivalList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_GuestArrivalList.Location = new System.Drawing.Point(37, 466);
            this.btn_GuestArrivalList.Name = "btn_GuestArrivalList";
            this.btn_GuestArrivalList.Size = new System.Drawing.Size(200, 35);
            this.btn_GuestArrivalList.TabIndex = 12;
            this.btn_GuestArrivalList.Text = "      Guest Arrival List";
            this.btn_GuestArrivalList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_GuestArrivalList.UseVisualStyleBackColor = false;
            this.btn_GuestArrivalList.Click += new System.EventHandler(this.btn_GuestArrivalList_Click);
            // 
            // pnl_MainPanel
            // 
            this.pnl_MainPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.pnl_MainPanel.Controls.Add(this.stat_strip);
            this.pnl_MainPanel.Controls.Add(this.pnl_2ndfloor);
            this.pnl_MainPanel.Controls.Add(this.pnl_3rdfloor);
            this.pnl_MainPanel.Controls.Add(this.pnl_5thfloor);
            this.pnl_MainPanel.Controls.Add(this.pnl_6thfloor);
            this.pnl_MainPanel.Controls.Add(this.pnl_7thfloor);
            this.pnl_MainPanel.Controls.Add(this.pnl_8thfloor);
            this.pnl_MainPanel.Controls.Add(this.pnl_9thfloor);
            this.pnl_MainPanel.Controls.Add(this.pnl_10thfloor);
            this.pnl_MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_MainPanel.Location = new System.Drawing.Point(200, 92);
            this.pnl_MainPanel.Name = "pnl_MainPanel";
            this.pnl_MainPanel.Size = new System.Drawing.Size(824, 638);
            this.pnl_MainPanel.TabIndex = 11;
            this.pnl_MainPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl_MainPanel_Paint);
            // 
            // stat_strip
            // 
            this.stat_strip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.stat_strip.Font = new System.Drawing.Font("Book Antiqua", 9F);
            this.stat_strip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stat_lbl_Rooms,
            this.stat_lbl_Pax,
            this.divider_stat1});
            this.stat_strip.Location = new System.Drawing.Point(0, 616);
            this.stat_strip.Name = "stat_strip";
            this.stat_strip.Size = new System.Drawing.Size(824, 22);
            this.stat_strip.TabIndex = 56;
            this.stat_strip.Text = "LOL";
            // 
            // stat_lbl_Rooms
            // 
            this.stat_lbl_Rooms.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.stat_lbl_Rooms.ForeColor = System.Drawing.Color.Black;
            this.stat_lbl_Rooms.Name = "stat_lbl_Rooms";
            this.stat_lbl_Rooms.Size = new System.Drawing.Size(67, 17);
            this.stat_lbl_Rooms.Text = "Rooms: 48";
            // 
            // stat_lbl_Pax
            // 
            this.stat_lbl_Pax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.stat_lbl_Pax.Name = "stat_lbl_Pax";
            this.stat_lbl_Pax.Size = new System.Drawing.Size(70, 17);
            this.stat_lbl_Pax.Text = "(Pax 128/0)";
            // 
            // divider_stat1
            // 
            this.divider_stat1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.divider_stat1.Name = "divider_stat1";
            this.divider_stat1.Size = new System.Drawing.Size(11, 17);
            this.divider_stat1.Text = "|";
            // 
            // pnl_2ndfloor
            // 
            this.pnl_2ndfloor.Controls.Add(this.btn_Room26);
            this.pnl_2ndfloor.Controls.Add(this.btn_Room25);
            this.pnl_2ndfloor.Controls.Add(this.btn_Room24);
            this.pnl_2ndfloor.Controls.Add(this.btn_Room23);
            this.pnl_2ndfloor.Controls.Add(this.btn_Room22);
            this.pnl_2ndfloor.Controls.Add(this.btn_Room21);
            this.pnl_2ndfloor.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl_2ndfloor.Location = new System.Drawing.Point(6, 538);
            this.pnl_2ndfloor.Name = "pnl_2ndfloor";
            this.pnl_2ndfloor.Size = new System.Drawing.Size(575, 70);
            this.pnl_2ndfloor.TabIndex = 55;
            // 
            // btn_Room26
            // 
            this.btn_Room26.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room26.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room26.FlatAppearance.BorderSize = 2;
            this.btn_Room26.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room26.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room26.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room26.ForeColor = System.Drawing.Color.White;
            this.btn_Room26.Location = new System.Drawing.Point(485, 0);
            this.btn_Room26.Name = "btn_Room26";
            this.btn_Room26.Size = new System.Drawing.Size(90, 70);
            this.btn_Room26.TabIndex = 47;
            this.btn_Room26.Text = "Room 26";
            this.btn_Room26.UseVisualStyleBackColor = false;
            this.btn_Room26.Click += new System.EventHandler(this.btn_Room26_Click);
            this.btn_Room26.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room26_KeyUp);
            // 
            // btn_Room25
            // 
            this.btn_Room25.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room25.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room25.FlatAppearance.BorderSize = 2;
            this.btn_Room25.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room25.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room25.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room25.ForeColor = System.Drawing.Color.White;
            this.btn_Room25.Location = new System.Drawing.Point(389, 0);
            this.btn_Room25.Name = "btn_Room25";
            this.btn_Room25.Size = new System.Drawing.Size(90, 70);
            this.btn_Room25.TabIndex = 46;
            this.btn_Room25.Text = "Room 25";
            this.btn_Room25.UseVisualStyleBackColor = false;
            this.btn_Room25.Click += new System.EventHandler(this.btn_Room25_Click);
            this.btn_Room25.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room25_KeyUp);
            // 
            // btn_Room24
            // 
            this.btn_Room24.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room24.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room24.FlatAppearance.BorderSize = 2;
            this.btn_Room24.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room24.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room24.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room24.ForeColor = System.Drawing.Color.White;
            this.btn_Room24.Location = new System.Drawing.Point(293, 0);
            this.btn_Room24.Name = "btn_Room24";
            this.btn_Room24.Size = new System.Drawing.Size(90, 70);
            this.btn_Room24.TabIndex = 45;
            this.btn_Room24.Text = "Room 24";
            this.btn_Room24.UseVisualStyleBackColor = false;
            this.btn_Room24.Click += new System.EventHandler(this.btn_Room24_Click);
            this.btn_Room24.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room24_KeyUp);
            // 
            // btn_Room23
            // 
            this.btn_Room23.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room23.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room23.FlatAppearance.BorderSize = 2;
            this.btn_Room23.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room23.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room23.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room23.ForeColor = System.Drawing.Color.White;
            this.btn_Room23.Location = new System.Drawing.Point(197, 0);
            this.btn_Room23.Name = "btn_Room23";
            this.btn_Room23.Size = new System.Drawing.Size(90, 70);
            this.btn_Room23.TabIndex = 44;
            this.btn_Room23.Text = "Room 23";
            this.btn_Room23.UseVisualStyleBackColor = false;
            this.btn_Room23.Click += new System.EventHandler(this.btn_Room23_Click);
            this.btn_Room23.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room23_KeyUp);
            // 
            // btn_Room22
            // 
            this.btn_Room22.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room22.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room22.FlatAppearance.BorderSize = 2;
            this.btn_Room22.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room22.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room22.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room22.ForeColor = System.Drawing.Color.White;
            this.btn_Room22.Location = new System.Drawing.Point(101, 0);
            this.btn_Room22.Name = "btn_Room22";
            this.btn_Room22.Size = new System.Drawing.Size(90, 70);
            this.btn_Room22.TabIndex = 43;
            this.btn_Room22.Text = "Room 22";
            this.btn_Room22.UseVisualStyleBackColor = false;
            this.btn_Room22.Click += new System.EventHandler(this.btn_Room22_Click);
            this.btn_Room22.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room22_KeyUp);
            // 
            // btn_Room21
            // 
            this.btn_Room21.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room21.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room21.FlatAppearance.BorderSize = 2;
            this.btn_Room21.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room21.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room21.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room21.ForeColor = System.Drawing.Color.White;
            this.btn_Room21.Location = new System.Drawing.Point(5, 0);
            this.btn_Room21.Name = "btn_Room21";
            this.btn_Room21.Size = new System.Drawing.Size(90, 70);
            this.btn_Room21.TabIndex = 7;
            this.btn_Room21.Text = "Room 21";
            this.btn_Room21.UseVisualStyleBackColor = false;
            this.btn_Room21.Click += new System.EventHandler(this.btn_Room21_Click);
            this.btn_Room21.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room21_KeyUp);
            // 
            // pnl_3rdfloor
            // 
            this.pnl_3rdfloor.Controls.Add(this.btn_Room36);
            this.pnl_3rdfloor.Controls.Add(this.btn_Room35);
            this.pnl_3rdfloor.Controls.Add(this.btn_Room34);
            this.pnl_3rdfloor.Controls.Add(this.btn_Room33);
            this.pnl_3rdfloor.Controls.Add(this.btn_Room32);
            this.pnl_3rdfloor.Controls.Add(this.btn_Room31);
            this.pnl_3rdfloor.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl_3rdfloor.Location = new System.Drawing.Point(6, 462);
            this.pnl_3rdfloor.Name = "pnl_3rdfloor";
            this.pnl_3rdfloor.Size = new System.Drawing.Size(575, 70);
            this.pnl_3rdfloor.TabIndex = 54;
            // 
            // btn_Room36
            // 
            this.btn_Room36.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room36.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room36.FlatAppearance.BorderSize = 2;
            this.btn_Room36.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room36.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room36.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room36.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room36.ForeColor = System.Drawing.Color.White;
            this.btn_Room36.Location = new System.Drawing.Point(485, 0);
            this.btn_Room36.Name = "btn_Room36";
            this.btn_Room36.Size = new System.Drawing.Size(90, 75);
            this.btn_Room36.TabIndex = 42;
            this.btn_Room36.Text = "Room 36";
            this.btn_Room36.UseVisualStyleBackColor = false;
            this.btn_Room36.Click += new System.EventHandler(this.btn_Room36_Click);
            this.btn_Room36.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room36_KeyUp);
            // 
            // btn_Room35
            // 
            this.btn_Room35.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room35.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room35.FlatAppearance.BorderSize = 2;
            this.btn_Room35.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room35.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room35.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room35.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room35.ForeColor = System.Drawing.Color.White;
            this.btn_Room35.Location = new System.Drawing.Point(389, 0);
            this.btn_Room35.Name = "btn_Room35";
            this.btn_Room35.Size = new System.Drawing.Size(90, 75);
            this.btn_Room35.TabIndex = 41;
            this.btn_Room35.Text = "Room 35";
            this.btn_Room35.UseVisualStyleBackColor = false;
            this.btn_Room35.Click += new System.EventHandler(this.btn_Room35_Click);
            this.btn_Room35.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room35_KeyUp);
            // 
            // btn_Room34
            // 
            this.btn_Room34.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room34.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room34.FlatAppearance.BorderSize = 2;
            this.btn_Room34.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room34.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room34.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room34.ForeColor = System.Drawing.Color.White;
            this.btn_Room34.Location = new System.Drawing.Point(293, 0);
            this.btn_Room34.Name = "btn_Room34";
            this.btn_Room34.Size = new System.Drawing.Size(90, 75);
            this.btn_Room34.TabIndex = 40;
            this.btn_Room34.Text = "Room 34";
            this.btn_Room34.UseVisualStyleBackColor = false;
            this.btn_Room34.Click += new System.EventHandler(this.btn_Room34_Click);
            this.btn_Room34.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room34_KeyUp);
            // 
            // btn_Room33
            // 
            this.btn_Room33.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room33.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room33.FlatAppearance.BorderSize = 2;
            this.btn_Room33.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room33.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room33.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room33.ForeColor = System.Drawing.Color.White;
            this.btn_Room33.Location = new System.Drawing.Point(197, 0);
            this.btn_Room33.Name = "btn_Room33";
            this.btn_Room33.Size = new System.Drawing.Size(90, 75);
            this.btn_Room33.TabIndex = 39;
            this.btn_Room33.Text = "Room 33";
            this.btn_Room33.UseVisualStyleBackColor = false;
            this.btn_Room33.Click += new System.EventHandler(this.btn_Room33_Click);
            this.btn_Room33.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room33_KeyUp);
            // 
            // btn_Room32
            // 
            this.btn_Room32.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room32.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room32.FlatAppearance.BorderSize = 2;
            this.btn_Room32.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room32.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room32.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room32.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room32.ForeColor = System.Drawing.Color.White;
            this.btn_Room32.Location = new System.Drawing.Point(101, 0);
            this.btn_Room32.Name = "btn_Room32";
            this.btn_Room32.Size = new System.Drawing.Size(90, 75);
            this.btn_Room32.TabIndex = 38;
            this.btn_Room32.Text = "Room 32";
            this.btn_Room32.UseVisualStyleBackColor = false;
            this.btn_Room32.Click += new System.EventHandler(this.btn_Room32_Click);
            this.btn_Room32.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room32_KeyUp);
            // 
            // btn_Room31
            // 
            this.btn_Room31.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room31.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room31.FlatAppearance.BorderSize = 2;
            this.btn_Room31.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room31.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room31.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room31.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room31.ForeColor = System.Drawing.Color.White;
            this.btn_Room31.Location = new System.Drawing.Point(5, 0);
            this.btn_Room31.Name = "btn_Room31";
            this.btn_Room31.Size = new System.Drawing.Size(90, 70);
            this.btn_Room31.TabIndex = 6;
            this.btn_Room31.Text = "Room 31";
            this.btn_Room31.UseVisualStyleBackColor = false;
            this.btn_Room31.Click += new System.EventHandler(this.btn_Room31_Click);
            this.btn_Room31.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room31_KeyUp);
            // 
            // pnl_5thfloor
            // 
            this.pnl_5thfloor.Controls.Add(this.btn_Room56);
            this.pnl_5thfloor.Controls.Add(this.btn_Room55);
            this.pnl_5thfloor.Controls.Add(this.btn_Room54);
            this.pnl_5thfloor.Controls.Add(this.btn_Room53);
            this.pnl_5thfloor.Controls.Add(this.btn_Room52);
            this.pnl_5thfloor.Controls.Add(this.btn_Room51);
            this.pnl_5thfloor.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl_5thfloor.Location = new System.Drawing.Point(6, 386);
            this.pnl_5thfloor.Name = "pnl_5thfloor";
            this.pnl_5thfloor.Size = new System.Drawing.Size(575, 70);
            this.pnl_5thfloor.TabIndex = 53;
            // 
            // btn_Room56
            // 
            this.btn_Room56.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room56.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room56.FlatAppearance.BorderSize = 2;
            this.btn_Room56.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room56.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room56.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room56.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room56.ForeColor = System.Drawing.Color.White;
            this.btn_Room56.Location = new System.Drawing.Point(485, 0);
            this.btn_Room56.Name = "btn_Room56";
            this.btn_Room56.Size = new System.Drawing.Size(90, 74);
            this.btn_Room56.TabIndex = 37;
            this.btn_Room56.Text = "Room 56";
            this.btn_Room56.UseVisualStyleBackColor = false;
            this.btn_Room56.Click += new System.EventHandler(this.btn_Room56_Click);
            this.btn_Room56.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room56_KeyUp);
            // 
            // btn_Room55
            // 
            this.btn_Room55.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room55.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room55.FlatAppearance.BorderSize = 2;
            this.btn_Room55.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room55.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room55.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room55.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room55.ForeColor = System.Drawing.Color.White;
            this.btn_Room55.Location = new System.Drawing.Point(389, 0);
            this.btn_Room55.Name = "btn_Room55";
            this.btn_Room55.Size = new System.Drawing.Size(90, 74);
            this.btn_Room55.TabIndex = 36;
            this.btn_Room55.Text = "Room 55";
            this.btn_Room55.UseVisualStyleBackColor = false;
            this.btn_Room55.Click += new System.EventHandler(this.btn_Room55_Click);
            this.btn_Room55.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room55_KeyUp);
            // 
            // btn_Room54
            // 
            this.btn_Room54.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room54.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room54.FlatAppearance.BorderSize = 2;
            this.btn_Room54.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room54.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room54.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room54.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room54.ForeColor = System.Drawing.Color.White;
            this.btn_Room54.Location = new System.Drawing.Point(292, 0);
            this.btn_Room54.Name = "btn_Room54";
            this.btn_Room54.Size = new System.Drawing.Size(90, 74);
            this.btn_Room54.TabIndex = 35;
            this.btn_Room54.Text = "Room 54";
            this.btn_Room54.UseVisualStyleBackColor = false;
            this.btn_Room54.Click += new System.EventHandler(this.btn_Room54_Click);
            this.btn_Room54.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room54_KeyUp);
            // 
            // btn_Room53
            // 
            this.btn_Room53.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room53.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room53.FlatAppearance.BorderSize = 2;
            this.btn_Room53.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room53.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room53.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room53.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room53.ForeColor = System.Drawing.Color.White;
            this.btn_Room53.Location = new System.Drawing.Point(197, 0);
            this.btn_Room53.Name = "btn_Room53";
            this.btn_Room53.Size = new System.Drawing.Size(90, 74);
            this.btn_Room53.TabIndex = 34;
            this.btn_Room53.Text = "Room 53";
            this.btn_Room53.UseVisualStyleBackColor = false;
            this.btn_Room53.Click += new System.EventHandler(this.btn_Room53_Click);
            this.btn_Room53.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room53_KeyUp);
            // 
            // btn_Room52
            // 
            this.btn_Room52.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room52.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room52.FlatAppearance.BorderSize = 2;
            this.btn_Room52.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room52.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room52.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room52.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room52.ForeColor = System.Drawing.Color.White;
            this.btn_Room52.Location = new System.Drawing.Point(101, 0);
            this.btn_Room52.Name = "btn_Room52";
            this.btn_Room52.Size = new System.Drawing.Size(90, 74);
            this.btn_Room52.TabIndex = 33;
            this.btn_Room52.Text = "Room 52";
            this.btn_Room52.UseVisualStyleBackColor = false;
            this.btn_Room52.Click += new System.EventHandler(this.btn_Room52_Click);
            this.btn_Room52.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room52_KeyUp);
            // 
            // btn_Room51
            // 
            this.btn_Room51.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room51.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room51.FlatAppearance.BorderSize = 2;
            this.btn_Room51.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room51.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room51.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room51.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room51.ForeColor = System.Drawing.Color.White;
            this.btn_Room51.Location = new System.Drawing.Point(5, 0);
            this.btn_Room51.Name = "btn_Room51";
            this.btn_Room51.Size = new System.Drawing.Size(90, 70);
            this.btn_Room51.TabIndex = 5;
            this.btn_Room51.Text = "Room 51";
            this.btn_Room51.UseVisualStyleBackColor = false;
            this.btn_Room51.Click += new System.EventHandler(this.btn_Room51_Click);
            this.btn_Room51.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room51_KeyUp);
            // 
            // pnl_6thfloor
            // 
            this.pnl_6thfloor.Controls.Add(this.btn_Room66);
            this.pnl_6thfloor.Controls.Add(this.btn_Room65);
            this.pnl_6thfloor.Controls.Add(this.btn_Room64);
            this.pnl_6thfloor.Controls.Add(this.btn_Room63);
            this.pnl_6thfloor.Controls.Add(this.btn_Room62);
            this.pnl_6thfloor.Controls.Add(this.btn_Room61);
            this.pnl_6thfloor.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl_6thfloor.Location = new System.Drawing.Point(6, 310);
            this.pnl_6thfloor.Name = "pnl_6thfloor";
            this.pnl_6thfloor.Size = new System.Drawing.Size(575, 70);
            this.pnl_6thfloor.TabIndex = 52;
            // 
            // btn_Room66
            // 
            this.btn_Room66.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room66.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room66.FlatAppearance.BorderSize = 2;
            this.btn_Room66.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room66.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room66.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room66.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room66.ForeColor = System.Drawing.Color.White;
            this.btn_Room66.Location = new System.Drawing.Point(485, 0);
            this.btn_Room66.Name = "btn_Room66";
            this.btn_Room66.Size = new System.Drawing.Size(90, 70);
            this.btn_Room66.TabIndex = 32;
            this.btn_Room66.Text = "Room 66";
            this.btn_Room66.UseVisualStyleBackColor = false;
            this.btn_Room66.Click += new System.EventHandler(this.btn_Room66_Click);
            this.btn_Room66.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room66_KeyUp);
            // 
            // btn_Room65
            // 
            this.btn_Room65.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room65.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room65.FlatAppearance.BorderSize = 2;
            this.btn_Room65.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room65.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room65.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room65.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room65.ForeColor = System.Drawing.Color.White;
            this.btn_Room65.Location = new System.Drawing.Point(389, 0);
            this.btn_Room65.Name = "btn_Room65";
            this.btn_Room65.Size = new System.Drawing.Size(90, 70);
            this.btn_Room65.TabIndex = 31;
            this.btn_Room65.Text = "Room 65";
            this.btn_Room65.UseVisualStyleBackColor = false;
            this.btn_Room65.Click += new System.EventHandler(this.btn_Room65_Click);
            this.btn_Room65.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room65_KeyUp);
            // 
            // btn_Room64
            // 
            this.btn_Room64.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room64.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room64.FlatAppearance.BorderSize = 2;
            this.btn_Room64.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room64.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room64.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room64.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room64.ForeColor = System.Drawing.Color.White;
            this.btn_Room64.Location = new System.Drawing.Point(293, 0);
            this.btn_Room64.Name = "btn_Room64";
            this.btn_Room64.Size = new System.Drawing.Size(90, 70);
            this.btn_Room64.TabIndex = 30;
            this.btn_Room64.Text = "Room 64";
            this.btn_Room64.UseVisualStyleBackColor = false;
            this.btn_Room64.Click += new System.EventHandler(this.btn_Room64_Click);
            this.btn_Room64.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room64_KeyUp);
            // 
            // btn_Room63
            // 
            this.btn_Room63.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room63.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room63.FlatAppearance.BorderSize = 2;
            this.btn_Room63.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room63.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room63.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room63.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room63.ForeColor = System.Drawing.Color.White;
            this.btn_Room63.Location = new System.Drawing.Point(197, 0);
            this.btn_Room63.Name = "btn_Room63";
            this.btn_Room63.Size = new System.Drawing.Size(90, 70);
            this.btn_Room63.TabIndex = 29;
            this.btn_Room63.Text = "Room 63";
            this.btn_Room63.UseVisualStyleBackColor = false;
            this.btn_Room63.Click += new System.EventHandler(this.btn_Room63_Click);
            this.btn_Room63.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room63_KeyUp);
            // 
            // btn_Room62
            // 
            this.btn_Room62.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room62.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room62.FlatAppearance.BorderSize = 2;
            this.btn_Room62.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room62.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room62.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room62.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room62.ForeColor = System.Drawing.Color.White;
            this.btn_Room62.Location = new System.Drawing.Point(101, 0);
            this.btn_Room62.Name = "btn_Room62";
            this.btn_Room62.Size = new System.Drawing.Size(90, 70);
            this.btn_Room62.TabIndex = 28;
            this.btn_Room62.Text = "Room 62";
            this.btn_Room62.UseVisualStyleBackColor = false;
            this.btn_Room62.Click += new System.EventHandler(this.btn_Room62_Click);
            this.btn_Room62.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room62_KeyUp);
            // 
            // btn_Room61
            // 
            this.btn_Room61.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room61.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room61.FlatAppearance.BorderSize = 2;
            this.btn_Room61.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room61.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room61.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room61.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room61.ForeColor = System.Drawing.Color.White;
            this.btn_Room61.Location = new System.Drawing.Point(5, 0);
            this.btn_Room61.Name = "btn_Room61";
            this.btn_Room61.Size = new System.Drawing.Size(90, 70);
            this.btn_Room61.TabIndex = 4;
            this.btn_Room61.Text = "Room 61";
            this.btn_Room61.UseVisualStyleBackColor = false;
            this.btn_Room61.Click += new System.EventHandler(this.btn_Room61_Click);
            this.btn_Room61.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room61_KeyUp);
            // 
            // pnl_7thfloor
            // 
            this.pnl_7thfloor.Controls.Add(this.btn_Room76);
            this.pnl_7thfloor.Controls.Add(this.btn_Room75);
            this.pnl_7thfloor.Controls.Add(this.btn_Room74);
            this.pnl_7thfloor.Controls.Add(this.btn_Room73);
            this.pnl_7thfloor.Controls.Add(this.btn_Room72);
            this.pnl_7thfloor.Controls.Add(this.btn_Room71);
            this.pnl_7thfloor.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl_7thfloor.Location = new System.Drawing.Point(6, 234);
            this.pnl_7thfloor.Name = "pnl_7thfloor";
            this.pnl_7thfloor.Size = new System.Drawing.Size(575, 70);
            this.pnl_7thfloor.TabIndex = 51;
            // 
            // btn_Room76
            // 
            this.btn_Room76.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room76.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room76.FlatAppearance.BorderSize = 2;
            this.btn_Room76.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room76.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room76.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room76.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room76.ForeColor = System.Drawing.Color.White;
            this.btn_Room76.Location = new System.Drawing.Point(485, 0);
            this.btn_Room76.Name = "btn_Room76";
            this.btn_Room76.Size = new System.Drawing.Size(90, 70);
            this.btn_Room76.TabIndex = 27;
            this.btn_Room76.Text = "Room 76";
            this.btn_Room76.UseVisualStyleBackColor = false;
            this.btn_Room76.Click += new System.EventHandler(this.btn_Room76_Click);
            this.btn_Room76.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room76_KeyUp);
            // 
            // btn_Room75
            // 
            this.btn_Room75.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room75.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room75.FlatAppearance.BorderSize = 2;
            this.btn_Room75.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room75.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room75.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room75.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room75.ForeColor = System.Drawing.Color.White;
            this.btn_Room75.Location = new System.Drawing.Point(389, 0);
            this.btn_Room75.Name = "btn_Room75";
            this.btn_Room75.Size = new System.Drawing.Size(90, 70);
            this.btn_Room75.TabIndex = 26;
            this.btn_Room75.Text = "Room 75";
            this.btn_Room75.UseVisualStyleBackColor = false;
            this.btn_Room75.Click += new System.EventHandler(this.btn_Room75_Click);
            this.btn_Room75.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room75_KeyUp);
            // 
            // btn_Room74
            // 
            this.btn_Room74.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room74.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room74.FlatAppearance.BorderSize = 2;
            this.btn_Room74.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room74.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room74.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room74.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room74.ForeColor = System.Drawing.Color.White;
            this.btn_Room74.Location = new System.Drawing.Point(293, 0);
            this.btn_Room74.Name = "btn_Room74";
            this.btn_Room74.Size = new System.Drawing.Size(90, 70);
            this.btn_Room74.TabIndex = 25;
            this.btn_Room74.Text = "Room 74";
            this.btn_Room74.UseVisualStyleBackColor = false;
            this.btn_Room74.Click += new System.EventHandler(this.btn_Room74_Click);
            this.btn_Room74.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room74_KeyUp);
            // 
            // btn_Room73
            // 
            this.btn_Room73.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room73.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room73.FlatAppearance.BorderSize = 2;
            this.btn_Room73.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room73.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room73.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room73.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room73.ForeColor = System.Drawing.Color.White;
            this.btn_Room73.Location = new System.Drawing.Point(197, 0);
            this.btn_Room73.Name = "btn_Room73";
            this.btn_Room73.Size = new System.Drawing.Size(90, 70);
            this.btn_Room73.TabIndex = 24;
            this.btn_Room73.Text = "Room 73";
            this.btn_Room73.UseVisualStyleBackColor = false;
            this.btn_Room73.Click += new System.EventHandler(this.btn_Room73_Click);
            this.btn_Room73.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room73_KeyUp);
            // 
            // btn_Room72
            // 
            this.btn_Room72.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room72.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room72.FlatAppearance.BorderSize = 2;
            this.btn_Room72.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room72.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room72.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room72.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room72.ForeColor = System.Drawing.Color.White;
            this.btn_Room72.Location = new System.Drawing.Point(101, 0);
            this.btn_Room72.Name = "btn_Room72";
            this.btn_Room72.Size = new System.Drawing.Size(90, 70);
            this.btn_Room72.TabIndex = 23;
            this.btn_Room72.Text = "Room 72";
            this.btn_Room72.UseVisualStyleBackColor = false;
            this.btn_Room72.Click += new System.EventHandler(this.btn_Room72_Click);
            this.btn_Room72.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room72_KeyUp);
            // 
            // btn_Room71
            // 
            this.btn_Room71.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room71.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room71.FlatAppearance.BorderSize = 2;
            this.btn_Room71.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room71.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room71.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room71.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room71.ForeColor = System.Drawing.Color.White;
            this.btn_Room71.Location = new System.Drawing.Point(5, 0);
            this.btn_Room71.Name = "btn_Room71";
            this.btn_Room71.Size = new System.Drawing.Size(90, 70);
            this.btn_Room71.TabIndex = 3;
            this.btn_Room71.Text = "Room 71";
            this.btn_Room71.UseVisualStyleBackColor = false;
            this.btn_Room71.Click += new System.EventHandler(this.btn_Room71_Click);
            this.btn_Room71.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room71_KeyUp);
            // 
            // pnl_8thfloor
            // 
            this.pnl_8thfloor.Controls.Add(this.btn_Room86);
            this.pnl_8thfloor.Controls.Add(this.btn_Room85);
            this.pnl_8thfloor.Controls.Add(this.btn_Room84);
            this.pnl_8thfloor.Controls.Add(this.btn_Room83);
            this.pnl_8thfloor.Controls.Add(this.btn_Room82);
            this.pnl_8thfloor.Controls.Add(this.btn_Room81);
            this.pnl_8thfloor.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl_8thfloor.Location = new System.Drawing.Point(6, 158);
            this.pnl_8thfloor.Name = "pnl_8thfloor";
            this.pnl_8thfloor.Size = new System.Drawing.Size(575, 70);
            this.pnl_8thfloor.TabIndex = 50;
            // 
            // btn_Room86
            // 
            this.btn_Room86.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room86.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room86.FlatAppearance.BorderSize = 2;
            this.btn_Room86.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room86.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room86.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room86.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room86.ForeColor = System.Drawing.Color.White;
            this.btn_Room86.Location = new System.Drawing.Point(484, 0);
            this.btn_Room86.Name = "btn_Room86";
            this.btn_Room86.Size = new System.Drawing.Size(90, 70);
            this.btn_Room86.TabIndex = 22;
            this.btn_Room86.Text = "Room 86";
            this.btn_Room86.UseVisualStyleBackColor = false;
            this.btn_Room86.Click += new System.EventHandler(this.btn_Room86_Click);
            this.btn_Room86.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room86_KeyUp);
            // 
            // btn_Room85
            // 
            this.btn_Room85.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room85.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room85.FlatAppearance.BorderSize = 2;
            this.btn_Room85.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room85.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room85.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room85.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room85.ForeColor = System.Drawing.Color.White;
            this.btn_Room85.Location = new System.Drawing.Point(389, 0);
            this.btn_Room85.Name = "btn_Room85";
            this.btn_Room85.Size = new System.Drawing.Size(90, 70);
            this.btn_Room85.TabIndex = 21;
            this.btn_Room85.Text = "Room 85";
            this.btn_Room85.UseVisualStyleBackColor = false;
            this.btn_Room85.Click += new System.EventHandler(this.btn_Room85_Click);
            this.btn_Room85.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room85_KeyUp);
            // 
            // btn_Room84
            // 
            this.btn_Room84.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room84.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room84.FlatAppearance.BorderSize = 2;
            this.btn_Room84.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room84.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room84.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room84.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room84.ForeColor = System.Drawing.Color.White;
            this.btn_Room84.Location = new System.Drawing.Point(293, 0);
            this.btn_Room84.Name = "btn_Room84";
            this.btn_Room84.Size = new System.Drawing.Size(90, 70);
            this.btn_Room84.TabIndex = 20;
            this.btn_Room84.Text = "Room 84";
            this.btn_Room84.UseVisualStyleBackColor = false;
            this.btn_Room84.Click += new System.EventHandler(this.btn_Room84_Click);
            this.btn_Room84.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room84_KeyUp);
            // 
            // btn_Room83
            // 
            this.btn_Room83.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room83.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room83.FlatAppearance.BorderSize = 2;
            this.btn_Room83.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room83.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room83.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room83.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room83.ForeColor = System.Drawing.Color.White;
            this.btn_Room83.Location = new System.Drawing.Point(197, 0);
            this.btn_Room83.Name = "btn_Room83";
            this.btn_Room83.Size = new System.Drawing.Size(90, 70);
            this.btn_Room83.TabIndex = 19;
            this.btn_Room83.Text = "Room 83";
            this.btn_Room83.UseVisualStyleBackColor = false;
            this.btn_Room83.Click += new System.EventHandler(this.btn_Room83_Click);
            this.btn_Room83.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room83_KeyUp);
            // 
            // btn_Room82
            // 
            this.btn_Room82.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room82.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room82.FlatAppearance.BorderSize = 2;
            this.btn_Room82.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room82.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room82.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room82.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room82.ForeColor = System.Drawing.Color.White;
            this.btn_Room82.Location = new System.Drawing.Point(100, 0);
            this.btn_Room82.Name = "btn_Room82";
            this.btn_Room82.Size = new System.Drawing.Size(90, 70);
            this.btn_Room82.TabIndex = 18;
            this.btn_Room82.Text = "Room 82";
            this.btn_Room82.UseVisualStyleBackColor = false;
            this.btn_Room82.Click += new System.EventHandler(this.btn_Room82_Click);
            this.btn_Room82.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room82_KeyUp);
            // 
            // btn_Room81
            // 
            this.btn_Room81.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room81.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room81.FlatAppearance.BorderSize = 2;
            this.btn_Room81.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room81.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room81.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room81.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room81.ForeColor = System.Drawing.Color.White;
            this.btn_Room81.Location = new System.Drawing.Point(4, 0);
            this.btn_Room81.Name = "btn_Room81";
            this.btn_Room81.Size = new System.Drawing.Size(90, 70);
            this.btn_Room81.TabIndex = 2;
            this.btn_Room81.Text = "Room 81";
            this.btn_Room81.UseVisualStyleBackColor = false;
            this.btn_Room81.Click += new System.EventHandler(this.btn_Room81_Click);
            this.btn_Room81.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room81_KeyUp);
            // 
            // pnl_9thfloor
            // 
            this.pnl_9thfloor.Controls.Add(this.btn_Room96);
            this.pnl_9thfloor.Controls.Add(this.btn_Room95);
            this.pnl_9thfloor.Controls.Add(this.btn_Room94);
            this.pnl_9thfloor.Controls.Add(this.btn_Room93);
            this.pnl_9thfloor.Controls.Add(this.btn_Room92);
            this.pnl_9thfloor.Controls.Add(this.btn_Room91);
            this.pnl_9thfloor.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl_9thfloor.Location = new System.Drawing.Point(6, 82);
            this.pnl_9thfloor.Name = "pnl_9thfloor";
            this.pnl_9thfloor.Size = new System.Drawing.Size(575, 70);
            this.pnl_9thfloor.TabIndex = 49;
            // 
            // btn_Room96
            // 
            this.btn_Room96.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room96.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room96.FlatAppearance.BorderSize = 2;
            this.btn_Room96.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room96.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room96.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room96.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room96.ForeColor = System.Drawing.Color.White;
            this.btn_Room96.Location = new System.Drawing.Point(485, 0);
            this.btn_Room96.Name = "btn_Room96";
            this.btn_Room96.Size = new System.Drawing.Size(90, 70);
            this.btn_Room96.TabIndex = 17;
            this.btn_Room96.Text = "Room 96";
            this.btn_Room96.UseVisualStyleBackColor = false;
            this.btn_Room96.Click += new System.EventHandler(this.btn_Room96_Click);
            this.btn_Room96.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room96_KeyUp);
            // 
            // btn_Room95
            // 
            this.btn_Room95.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room95.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room95.FlatAppearance.BorderSize = 2;
            this.btn_Room95.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room95.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room95.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room95.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room95.ForeColor = System.Drawing.Color.White;
            this.btn_Room95.Location = new System.Drawing.Point(389, 0);
            this.btn_Room95.Name = "btn_Room95";
            this.btn_Room95.Size = new System.Drawing.Size(90, 70);
            this.btn_Room95.TabIndex = 16;
            this.btn_Room95.Text = "Room 95";
            this.btn_Room95.UseVisualStyleBackColor = false;
            this.btn_Room95.Click += new System.EventHandler(this.btn_Room95_Click);
            this.btn_Room95.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room95_KeyUp);
            // 
            // btn_Room94
            // 
            this.btn_Room94.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room94.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room94.FlatAppearance.BorderSize = 2;
            this.btn_Room94.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room94.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room94.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room94.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room94.ForeColor = System.Drawing.Color.White;
            this.btn_Room94.Location = new System.Drawing.Point(293, 0);
            this.btn_Room94.Name = "btn_Room94";
            this.btn_Room94.Size = new System.Drawing.Size(90, 70);
            this.btn_Room94.TabIndex = 15;
            this.btn_Room94.Text = "Room 94";
            this.btn_Room94.UseVisualStyleBackColor = false;
            this.btn_Room94.Click += new System.EventHandler(this.btn_Room94_Click);
            this.btn_Room94.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room94_KeyUp);
            // 
            // btn_Room93
            // 
            this.btn_Room93.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room93.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room93.FlatAppearance.BorderSize = 2;
            this.btn_Room93.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room93.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room93.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room93.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room93.ForeColor = System.Drawing.Color.White;
            this.btn_Room93.Location = new System.Drawing.Point(197, 0);
            this.btn_Room93.Name = "btn_Room93";
            this.btn_Room93.Size = new System.Drawing.Size(90, 70);
            this.btn_Room93.TabIndex = 14;
            this.btn_Room93.Text = "Room 93";
            this.btn_Room93.UseVisualStyleBackColor = false;
            this.btn_Room93.Click += new System.EventHandler(this.btn_Room93_Click);
            this.btn_Room93.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room93_KeyUp);
            // 
            // btn_Room92
            // 
            this.btn_Room92.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room92.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room92.FlatAppearance.BorderSize = 2;
            this.btn_Room92.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room92.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room92.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room92.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room92.ForeColor = System.Drawing.Color.White;
            this.btn_Room92.Location = new System.Drawing.Point(101, 0);
            this.btn_Room92.Name = "btn_Room92";
            this.btn_Room92.Size = new System.Drawing.Size(90, 70);
            this.btn_Room92.TabIndex = 13;
            this.btn_Room92.Text = "Room 92";
            this.btn_Room92.UseVisualStyleBackColor = false;
            this.btn_Room92.Click += new System.EventHandler(this.btn_Room92_Click);
            this.btn_Room92.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room92_KeyUp);
            // 
            // btn_Room91
            // 
            this.btn_Room91.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room91.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room91.FlatAppearance.BorderSize = 2;
            this.btn_Room91.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room91.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room91.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room91.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room91.ForeColor = System.Drawing.Color.White;
            this.btn_Room91.Location = new System.Drawing.Point(5, 0);
            this.btn_Room91.Name = "btn_Room91";
            this.btn_Room91.Size = new System.Drawing.Size(90, 70);
            this.btn_Room91.TabIndex = 1;
            this.btn_Room91.Text = "Room 91";
            this.btn_Room91.UseVisualStyleBackColor = false;
            this.btn_Room91.Click += new System.EventHandler(this.btn_Room91_Click);
            this.btn_Room91.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room91_KeyUp);
            // 
            // pnl_10thfloor
            // 
            this.pnl_10thfloor.Controls.Add(this.btn_Room106);
            this.pnl_10thfloor.Controls.Add(this.btn_Room105);
            this.pnl_10thfloor.Controls.Add(this.btn_Room104);
            this.pnl_10thfloor.Controls.Add(this.btn_Room103);
            this.pnl_10thfloor.Controls.Add(this.btn_Room102);
            this.pnl_10thfloor.Controls.Add(this.btn_Room101);
            this.pnl_10thfloor.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl_10thfloor.Location = new System.Drawing.Point(6, 6);
            this.pnl_10thfloor.Name = "pnl_10thfloor";
            this.pnl_10thfloor.Size = new System.Drawing.Size(575, 70);
            this.pnl_10thfloor.TabIndex = 48;
            // 
            // btn_Room106
            // 
            this.btn_Room106.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room106.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room106.FlatAppearance.BorderSize = 2;
            this.btn_Room106.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room106.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room106.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room106.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room106.ForeColor = System.Drawing.Color.White;
            this.btn_Room106.Location = new System.Drawing.Point(484, 0);
            this.btn_Room106.Name = "btn_Room106";
            this.btn_Room106.Size = new System.Drawing.Size(90, 70);
            this.btn_Room106.TabIndex = 12;
            this.btn_Room106.Text = "Room 106";
            this.btn_Room106.UseVisualStyleBackColor = false;
            this.btn_Room106.Click += new System.EventHandler(this.btn_Room106_Click);
            this.btn_Room106.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room106_KeyUp);
            // 
            // btn_Room105
            // 
            this.btn_Room105.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room105.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room105.FlatAppearance.BorderSize = 2;
            this.btn_Room105.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room105.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room105.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room105.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room105.ForeColor = System.Drawing.Color.White;
            this.btn_Room105.Location = new System.Drawing.Point(388, 0);
            this.btn_Room105.Name = "btn_Room105";
            this.btn_Room105.Size = new System.Drawing.Size(90, 70);
            this.btn_Room105.TabIndex = 11;
            this.btn_Room105.Text = "Room 105";
            this.btn_Room105.UseVisualStyleBackColor = false;
            this.btn_Room105.Click += new System.EventHandler(this.btn_Room105_Click);
            this.btn_Room105.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room105_KeyUp);
            // 
            // btn_Room104
            // 
            this.btn_Room104.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room104.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room104.FlatAppearance.BorderSize = 2;
            this.btn_Room104.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room104.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room104.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room104.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room104.ForeColor = System.Drawing.Color.White;
            this.btn_Room104.Location = new System.Drawing.Point(292, 0);
            this.btn_Room104.Name = "btn_Room104";
            this.btn_Room104.Size = new System.Drawing.Size(90, 70);
            this.btn_Room104.TabIndex = 10;
            this.btn_Room104.Text = "Room 104";
            this.btn_Room104.UseVisualStyleBackColor = false;
            this.btn_Room104.Click += new System.EventHandler(this.btn_Room104_Click);
            this.btn_Room104.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room104_KeyUp);
            // 
            // btn_Room103
            // 
            this.btn_Room103.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room103.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room103.FlatAppearance.BorderSize = 2;
            this.btn_Room103.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room103.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room103.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room103.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room103.ForeColor = System.Drawing.Color.White;
            this.btn_Room103.Location = new System.Drawing.Point(196, 0);
            this.btn_Room103.Name = "btn_Room103";
            this.btn_Room103.Size = new System.Drawing.Size(90, 70);
            this.btn_Room103.TabIndex = 9;
            this.btn_Room103.Text = "Room 103";
            this.btn_Room103.UseVisualStyleBackColor = false;
            this.btn_Room103.Click += new System.EventHandler(this.btn_Room103_Click);
            this.btn_Room103.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room103_KeyUp);
            // 
            // btn_Room102
            // 
            this.btn_Room102.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room102.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room102.FlatAppearance.BorderSize = 2;
            this.btn_Room102.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room102.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room102.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room102.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room102.ForeColor = System.Drawing.Color.White;
            this.btn_Room102.Location = new System.Drawing.Point(100, 0);
            this.btn_Room102.Name = "btn_Room102";
            this.btn_Room102.Size = new System.Drawing.Size(90, 70);
            this.btn_Room102.TabIndex = 8;
            this.btn_Room102.Text = "Room 102";
            this.btn_Room102.UseVisualStyleBackColor = false;
            this.btn_Room102.Click += new System.EventHandler(this.btn_Room102_Click);
            this.btn_Room102.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room102_KeyUp);
            // 
            // btn_Room101
            // 
            this.btn_Room101.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Room101.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_Room101.FlatAppearance.BorderSize = 2;
            this.btn_Room101.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_Room101.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_Room101.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Room101.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Room101.ForeColor = System.Drawing.Color.White;
            this.btn_Room101.Location = new System.Drawing.Point(4, 2);
            this.btn_Room101.Name = "btn_Room101";
            this.btn_Room101.Size = new System.Drawing.Size(90, 68);
            this.btn_Room101.TabIndex = 0;
            this.btn_Room101.Text = "Room 101";
            this.btn_Room101.UseVisualStyleBackColor = false;
            this.btn_Room101.Click += new System.EventHandler(this.btn_Room101_Click);
            this.btn_Room101.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btn_Room101_KeyUp);
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // lbl_timer
            // 
            this.lbl_timer.AutoSize = true;
            this.lbl_timer.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_timer.ForeColor = System.Drawing.SystemColors.Control;
            this.lbl_timer.Location = new System.Drawing.Point(206, 7);
            this.lbl_timer.Name = "lbl_timer";
            this.lbl_timer.Size = new System.Drawing.Size(75, 31);
            this.lbl_timer.TabIndex = 17;
            this.lbl_timer.Text = "Timer";
            // 
            // btn_refresh
            // 
            this.btn_refresh.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.btn_refresh.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_refresh.FlatAppearance.BorderSize = 2;
            this.btn_refresh.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btn_refresh.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btn_refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_refresh.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_refresh.ForeColor = System.Drawing.Color.White;
            this.btn_refresh.Location = new System.Drawing.Point(32, 632);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(135, 30);
            this.btn_refresh.TabIndex = 48;
            this.btn_refresh.Text = "REFRESH";
            this.btn_refresh.UseVisualStyleBackColor = false;
            this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
            // 
            // Main_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.ClientSize = new System.Drawing.Size(1300, 730);
            this.Controls.Add(this.lbl_timer);
            this.Controls.Add(this.pnl_MainPanel);
            this.Controls.Add(this.pnl_OperatorMenu);
            this.Controls.Add(this.pnl_RoomButtons);
            this.Controls.Add(this.pnl_FloorMenu_Summary);
            this.Controls.Add(this.pnl_Maxx_Hotel);
            this.Controls.Add(this.lbl_FrontDeskSystem);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Main_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Main_Form_Load);
            this.pnl_Maxx_Hotel.ResumeLayout(false);
            this.pnl_Maxx_Hotel.PerformLayout();
            this.pnl_FloorMenu_Summary.ResumeLayout(false);
            this.pnl_RoomButtons.ResumeLayout(false);
            this.pnl_OperatorMenu.ResumeLayout(false);
            this.pnl_MainPanel.ResumeLayout(false);
            this.pnl_MainPanel.PerformLayout();
            this.stat_strip.ResumeLayout(false);
            this.stat_strip.PerformLayout();
            this.pnl_2ndfloor.ResumeLayout(false);
            this.pnl_3rdfloor.ResumeLayout(false);
            this.pnl_5thfloor.ResumeLayout(false);
            this.pnl_6thfloor.ResumeLayout(false);
            this.pnl_7thfloor.ResumeLayout(false);
            this.pnl_8thfloor.ResumeLayout(false);
            this.pnl_9thfloor.ResumeLayout(false);
            this.pnl_10thfloor.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnl_Maxx_Hotel;
        private System.Windows.Forms.Label lbl_Maxxis_Hotel;
        private System.Windows.Forms.Label lbl_FrontDeskSystem;
        private System.Windows.Forms.Panel pnl_FloorMenu_Summary;
        private System.Windows.Forms.Panel pnl_FloorMenu_Selector;
        private System.Windows.Forms.Label lbl_PWD_Summary;
        private System.Windows.Forms.Label lbl_UrbanView_Summary;
        private System.Windows.Forms.Label lbl_UrbanCorner_Summary;
        private System.Windows.Forms.Label lbl_UrbanRoom_Summary;
        private System.Windows.Forms.Label lbl_All_Summary;
        private System.Windows.Forms.Label lbl_VacantSummary;
        private System.Windows.Forms.Button btn_2nd_Floor;
        private System.Windows.Forms.Button btn_3rd_Floor;
        private System.Windows.Forms.Button btn_5th_Floor;
        private System.Windows.Forms.Button btn_6th_Floor;
        private System.Windows.Forms.Button btn_7th_Floor;
        private System.Windows.Forms.Button btn_8th_Floor;
        private System.Windows.Forms.Button btn_9th_Floor;
        private System.Windows.Forms.Button btn_10th_Floor;
        private System.Windows.Forms.Button btn_All_Floors;
        private System.Windows.Forms.Label lbl_Floor_Menu;
        private System.Windows.Forms.Panel pnl_RoomButtons;
        private System.Windows.Forms.Button btn_Dirty;
        private System.Windows.Forms.Button btn_GuaranteedBuyer;
        private System.Windows.Forms.Button btn_Reserved;
        private System.Windows.Forms.Button btn_OutOfOrder;
        private System.Windows.Forms.Button btn_DueOut;
        private System.Windows.Forms.Button btn_Vacant;
        private System.Windows.Forms.Button btn_Occupied;
        private System.Windows.Forms.Button btn_AllRooms;
        private System.Windows.Forms.Panel pnl_OperatorMenu;
        private System.Windows.Forms.Panel pnl_MenuSelector;
        private System.Windows.Forms.Button btn_SignOut;
        private System.Windows.Forms.Button btn_FrontDesk;
        private System.Windows.Forms.Button btn_RoomInformation;
        private System.Windows.Forms.Button btn_GuestFolio;
        private System.Windows.Forms.Button btn_Reports;
        private System.Windows.Forms.Button btn_GuestMessage;
        private System.Windows.Forms.Button btn_WakeupCall;
        private System.Windows.Forms.Button btn_NewReservation;
        private System.Windows.Forms.Button btn_CheckOut;
        private System.Windows.Forms.Button btn_TransferRoom;
        private System.Windows.Forms.Button btn_GuestRequest;
        private System.Windows.Forms.Button btn_GuestArrivalList;
        private System.Windows.Forms.Button btn_New_Walkin_Guest;
        private System.Windows.Forms.Button btn_Loss_Breakage_Fines;
        private System.Windows.Forms.Panel pnl_MainPanel;
        private System.Windows.Forms.StatusStrip stat_strip;
        private System.Windows.Forms.ToolStripStatusLabel stat_lbl_Rooms;
        private System.Windows.Forms.ToolStripStatusLabel stat_lbl_Pax;
        private System.Windows.Forms.ToolStripStatusLabel divider_stat1;
        private System.Windows.Forms.Panel pnl_2ndfloor;
        private System.Windows.Forms.Button btn_Room26;
        private System.Windows.Forms.Button btn_Room25;
        private System.Windows.Forms.Button btn_Room24;
        private System.Windows.Forms.Button btn_Room23;
        private System.Windows.Forms.Button btn_Room22;
        private System.Windows.Forms.Button btn_Room21;
        private System.Windows.Forms.Panel pnl_3rdfloor;
        private System.Windows.Forms.Button btn_Room36;
        private System.Windows.Forms.Button btn_Room35;
        private System.Windows.Forms.Button btn_Room34;
        private System.Windows.Forms.Button btn_Room33;
        private System.Windows.Forms.Button btn_Room32;
        private System.Windows.Forms.Button btn_Room31;
        private System.Windows.Forms.Panel pnl_5thfloor;
        private System.Windows.Forms.Button btn_Room56;
        private System.Windows.Forms.Button btn_Room55;
        private System.Windows.Forms.Button btn_Room54;
        private System.Windows.Forms.Button btn_Room53;
        private System.Windows.Forms.Button btn_Room52;
        private System.Windows.Forms.Button btn_Room51;
        private System.Windows.Forms.Panel pnl_6thfloor;
        private System.Windows.Forms.Button btn_Room66;
        private System.Windows.Forms.Button btn_Room65;
        private System.Windows.Forms.Button btn_Room64;
        private System.Windows.Forms.Button btn_Room63;
        private System.Windows.Forms.Button btn_Room62;
        private System.Windows.Forms.Button btn_Room61;
        private System.Windows.Forms.Panel pnl_7thfloor;
        private System.Windows.Forms.Button btn_Room76;
        private System.Windows.Forms.Button btn_Room75;
        private System.Windows.Forms.Button btn_Room74;
        private System.Windows.Forms.Button btn_Room73;
        private System.Windows.Forms.Button btn_Room72;
        private System.Windows.Forms.Button btn_Room71;
        private System.Windows.Forms.Panel pnl_8thfloor;
        private System.Windows.Forms.Button btn_Room86;
        private System.Windows.Forms.Button btn_Room85;
        private System.Windows.Forms.Button btn_Room84;
        private System.Windows.Forms.Button btn_Room83;
        private System.Windows.Forms.Button btn_Room82;
        private System.Windows.Forms.Button btn_Room81;
        private System.Windows.Forms.Panel pnl_9thfloor;
        private System.Windows.Forms.Button btn_Room96;
        private System.Windows.Forms.Button btn_Room95;
        private System.Windows.Forms.Button btn_Room94;
        private System.Windows.Forms.Button btn_Room93;
        private System.Windows.Forms.Button btn_Room92;
        private System.Windows.Forms.Button btn_Room91;
        private System.Windows.Forms.Panel pnl_10thfloor;
        private System.Windows.Forms.Button btn_Room106;
        private System.Windows.Forms.Button btn_Room105;
        private System.Windows.Forms.Button btn_Room104;
        private System.Windows.Forms.Button btn_Room103;
        private System.Windows.Forms.Button btn_Room102;
        private System.Windows.Forms.Button btn_Room101;
        private System.Windows.Forms.Button btn_reservationList;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label lbl_timer;
        private System.Windows.Forms.Button btn_refresh;
    }
}

