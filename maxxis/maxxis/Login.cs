﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace maxxis
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        /*******************************************************************************
        * Variables and Instantiations                                                 *
        *******************************************************************************/

        //Instantiation
        login_class LC;

        //values

        string username;
        string password;

        /*******************************************************************************
        * Private Properties                                                           *
        *******************************************************************************/
        private static string userId;

        /*******************************************************************************
        * Public Properties                                                           *
        *******************************************************************************/
        public string UserId
        {
            get { return userId; }
            set { userId = value; }
        }

        /*******************************************************************************
        * Methods                                                                      *
        *******************************************************************************/
        private void showMainForm()
        {

        }

        private void values_default()
        {
            txt_user.Clear();
            txt_password.Clear();
        }

        private void processCredentials()
        {
            if (txt_password.Text != "" && txt_user.Text != "")
            {
                username = txt_user.Text;
                password = txt_password.Text;

                LC = new login_class();

                //Calls the loginConfirmation method in login_class
                LC.loginConfirmation(username, password);

                //Checks if the credentials inserted returned a true value
                if (LC.ConfirmLogin)
                {
                    Main_Form frm = new Main_Form();

                    this.Hide();
                    frm.ShowDialog();

                    //Restores the variables used to default value;
                    values_default();
                    this.Show();
                }
                else
                {
                    MessageBox.Show("Invalid Login Credentials");
                }
            }
            else
            {
                MessageBox.Show("Please fill up the username and password");
            }
        }


        /*******************************************************************************
        * Control Properties                                                           *
        *******************************************************************************/

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            processCredentials();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txt_user_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void txt_password_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void txt_user_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                processCredentials();
            }
        }

        private void txt_password_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                processCredentials();
            }
        }
    }
}
