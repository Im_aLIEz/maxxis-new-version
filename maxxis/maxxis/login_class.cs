﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace maxxis
{
    class login_class
    {
        /*******************************************************************************
        * Variables and Instantiations                                                 *
        *******************************************************************************/


        //Connection String
        string con;

        //Database connection and reader
        SqlConnection conDatabase;
        SqlCommand cmdDataBase;
        SqlDataReader dataReader;

        //Values
        string userId;
        string fullName;
        string shiftNumber;
        string position;
        string username;
        string password;


        //Instantiations
        Login login;
        userInfo_class UIC;
        Main_Form MF;

        /*******************************************************************************
        * Protected Properties                                                         *
        *******************************************************************************/



        /*******************************************************************************
        * Private Properties                                                           *
        *******************************************************************************/
        private static bool confirmLogin;


        /*******************************************************************************
        * Public Properties                                                            *
        *******************************************************************************/
        public bool ConfirmLogin
        {
            get { return confirmLogin; }
            set { confirmLogin = value; }
        }


        /*******************************************************************************
        * Protected Methods                                                            *
        *******************************************************************************/
        protected void defaultValues()
        {
            confirmLogin = false;
            username = "";
            password = "";
        }

        /*******************************************************************************
        * Private Methods                                                              *
        *******************************************************************************/


        /*******************************************************************************
        * Public Methods                                                               *
        *******************************************************************************/

        public void loginConfirmation(string UserName_param, string Password_param)
        {
            try
            {
                defaultValues();

                //Instantiations
                login = new Login();
                UIC = new userInfo_class();
                MF = new Main_Form();

                //Store the username and password from login form
                username = UserName_param;
                password = Password_param;

                //connection string
                con = System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString;

                conDatabase = new SqlConnection(con);

                conDatabase.Open();

                //Query
                cmdDataBase = new SqlCommand("SELECT dbo.tbl_login.user_id, dbo.tbl_employee.fullName, dbo.tbl_login.username, dbo.tbl_login.password, dbo.tbl_login.shiftNumber, dbo.tbl_login.position " +
                                             " FROM dbo.tbl_employee INNER JOIN " +
                                             " dbo.tbl_login ON dbo.tbl_employee.user_id = dbo.tbl_login.user_id WHERE username = @username AND password = @password", conDatabase);
                cmdDataBase.Parameters.AddWithValue("@username", UserName_param);
                cmdDataBase.Parameters.AddWithValue("@password", Password_param);

                //Execute the query
                dataReader = cmdDataBase.ExecuteReader();

                //Checks if the login credentials matches the Database data
                while (dataReader.Read())
                {
                    if (dataReader["username"].ToString().Equals(username) && dataReader["password"].ToString().Equals(password))
                    {
                        confirmLogin = true;
                        userId = dataReader["user_id"].ToString();
                    }
                    else
                    {
                        confirmLogin = false;
                    }
                }

                if (confirmLogin)
                {
                    //record the userinfo for audit trail
                    UIC.getUserInfo_login(userId);
                }

                conDatabase.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
