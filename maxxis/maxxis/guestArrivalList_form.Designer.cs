﻿namespace maxxis
{
    partial class guestArrivalList_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnl_GuestArrival_Title = new System.Windows.Forms.Panel();
            this.lbl_GuestArrivalList = new System.Windows.Forms.Label();
            this.pcb_GuestArrivalList_Icon = new System.Windows.Forms.PictureBox();
            this.btn_GoBack = new System.Windows.Forms.Button();
            this.dgv_Informations = new System.Windows.Forms.DataGridView();
            this.pnl_Data_Info = new System.Windows.Forms.Panel();
            this.pnl_CheckInDate = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dtp_CheckInDate = new System.Windows.Forms.DateTimePicker();
            this.lbl_CheckInDate = new System.Windows.Forms.Label();
            this.colFolioNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGuestFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRooms = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCheckinDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAdult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colChild = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAirportPickup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFlightNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnl_GuestArrival_Title.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_GuestArrivalList_Icon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Informations)).BeginInit();
            this.pnl_Data_Info.SuspendLayout();
            this.pnl_CheckInDate.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_GuestArrival_Title
            // 
            this.pnl_GuestArrival_Title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.pnl_GuestArrival_Title.Controls.Add(this.lbl_GuestArrivalList);
            this.pnl_GuestArrival_Title.Controls.Add(this.pcb_GuestArrivalList_Icon);
            this.pnl_GuestArrival_Title.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_GuestArrival_Title.Location = new System.Drawing.Point(0, 0);
            this.pnl_GuestArrival_Title.Name = "pnl_GuestArrival_Title";
            this.pnl_GuestArrival_Title.Size = new System.Drawing.Size(1180, 66);
            this.pnl_GuestArrival_Title.TabIndex = 1;
            // 
            // lbl_GuestArrivalList
            // 
            this.lbl_GuestArrivalList.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_GuestArrivalList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_GuestArrivalList.Location = new System.Drawing.Point(80, 1);
            this.lbl_GuestArrivalList.Name = "lbl_GuestArrivalList";
            this.lbl_GuestArrivalList.Size = new System.Drawing.Size(276, 68);
            this.lbl_GuestArrivalList.TabIndex = 0;
            this.lbl_GuestArrivalList.Text = "Guest Arrival List";
            this.lbl_GuestArrivalList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pcb_GuestArrivalList_Icon
            // 
            this.pcb_GuestArrivalList_Icon.BackgroundImage = global::maxxis.Properties.Resources.Guest_Arrival_List_2;
            this.pcb_GuestArrivalList_Icon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_GuestArrivalList_Icon.Dock = System.Windows.Forms.DockStyle.Left;
            this.pcb_GuestArrivalList_Icon.Location = new System.Drawing.Point(0, 0);
            this.pcb_GuestArrivalList_Icon.Name = "pcb_GuestArrivalList_Icon";
            this.pcb_GuestArrivalList_Icon.Size = new System.Drawing.Size(91, 66);
            this.pcb_GuestArrivalList_Icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pcb_GuestArrivalList_Icon.TabIndex = 1;
            this.pcb_GuestArrivalList_Icon.TabStop = false;
            // 
            // btn_GoBack
            // 
            this.btn_GoBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_GoBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_GoBack.FlatAppearance.BorderSize = 0;
            this.btn_GoBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_GoBack.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_GoBack.ForeColor = System.Drawing.Color.White;
            this.btn_GoBack.Image = global::maxxis.Properties.Resources.Go_Back;
            this.btn_GoBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_GoBack.Location = new System.Drawing.Point(12, 446);
            this.btn_GoBack.Name = "btn_GoBack";
            this.btn_GoBack.Size = new System.Drawing.Size(128, 44);
            this.btn_GoBack.TabIndex = 0;
            this.btn_GoBack.Text = "GO BACK";
            this.btn_GoBack.UseVisualStyleBackColor = false;
            this.btn_GoBack.Click += new System.EventHandler(this.btn_GoBack_Click);
            // 
            // dgv_Informations
            // 
            this.dgv_Informations.AllowUserToAddRows = false;
            this.dgv_Informations.AllowUserToDeleteRows = false;
            this.dgv_Informations.BackgroundColor = System.Drawing.Color.Silver;
            this.dgv_Informations.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 7, 0, 7);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Informations.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_Informations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Informations.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colFolioNumber,
            this.colGuestFullName,
            this.colRooms,
            this.ColCheckinDate,
            this.colAdult,
            this.colChild,
            this.ColAirportPickup,
            this.colFlightNumber,
            this.colTime,
            this.Column1});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_Informations.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_Informations.EnableHeadersVisualStyles = false;
            this.dgv_Informations.Location = new System.Drawing.Point(12, 10);
            this.dgv_Informations.Name = "dgv_Informations";
            this.dgv_Informations.ReadOnly = true;
            this.dgv_Informations.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Informations.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_Informations.RowHeadersVisible = false;
            this.dgv_Informations.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_Informations.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_Informations.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgv_Informations.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_Informations.Size = new System.Drawing.Size(1156, 426);
            this.dgv_Informations.TabIndex = 1;
            this.dgv_Informations.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Informations_CellDoubleClick);
            this.dgv_Informations.MouseEnter += new System.EventHandler(this.dgv_Informations_MouseEnter);
            // 
            // pnl_Data_Info
            // 
            this.pnl_Data_Info.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.pnl_Data_Info.Controls.Add(this.dgv_Informations);
            this.pnl_Data_Info.Controls.Add(this.btn_GoBack);
            this.pnl_Data_Info.Location = new System.Drawing.Point(0, 98);
            this.pnl_Data_Info.Name = "pnl_Data_Info";
            this.pnl_Data_Info.Size = new System.Drawing.Size(1180, 502);
            this.pnl_Data_Info.TabIndex = 3;
            this.pnl_Data_Info.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl_Data_Info_Paint);
            this.pnl_Data_Info.Enter += new System.EventHandler(this.pnl_Data_Info_Enter);
            // 
            // pnl_CheckInDate
            // 
            this.pnl_CheckInDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.pnl_CheckInDate.Controls.Add(this.panel1);
            this.pnl_CheckInDate.Controls.Add(this.dtp_CheckInDate);
            this.pnl_CheckInDate.Controls.Add(this.lbl_CheckInDate);
            this.pnl_CheckInDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_CheckInDate.Location = new System.Drawing.Point(0, 66);
            this.pnl_CheckInDate.Name = "pnl_CheckInDate";
            this.pnl_CheckInDate.Size = new System.Drawing.Size(1180, 32);
            this.pnl_CheckInDate.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1180, 32);
            this.panel1.TabIndex = 3;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CalendarFont = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.CalendarTitleBackColor = System.Drawing.Color.White;
            this.dateTimePicker1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Location = new System.Drawing.Point(132, 6);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Check In Date:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtp_CheckInDate
            // 
            this.dtp_CheckInDate.CalendarTitleBackColor = System.Drawing.Color.White;
            this.dtp_CheckInDate.Location = new System.Drawing.Point(110, 6);
            this.dtp_CheckInDate.Name = "dtp_CheckInDate";
            this.dtp_CheckInDate.Size = new System.Drawing.Size(200, 20);
            this.dtp_CheckInDate.TabIndex = 1;
            // 
            // lbl_CheckInDate
            // 
            this.lbl_CheckInDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CheckInDate.Location = new System.Drawing.Point(9, 0);
            this.lbl_CheckInDate.Name = "lbl_CheckInDate";
            this.lbl_CheckInDate.Size = new System.Drawing.Size(98, 29);
            this.lbl_CheckInDate.TabIndex = 0;
            this.lbl_CheckInDate.Text = "Check In Date:";
            this.lbl_CheckInDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colFolioNumber
            // 
            this.colFolioNumber.HeaderText = "Folio#";
            this.colFolioNumber.Name = "colFolioNumber";
            this.colFolioNumber.ReadOnly = true;
            this.colFolioNumber.Width = 110;
            // 
            // colGuestFullName
            // 
            this.colGuestFullName.HeaderText = "Guest Full Name";
            this.colGuestFullName.Name = "colGuestFullName";
            this.colGuestFullName.ReadOnly = true;
            this.colGuestFullName.Width = 230;
            // 
            // colRooms
            // 
            this.colRooms.HeaderText = "Rooms";
            this.colRooms.Name = "colRooms";
            this.colRooms.ReadOnly = true;
            // 
            // ColCheckinDate
            // 
            this.ColCheckinDate.HeaderText = "C/I Date";
            this.ColCheckinDate.Name = "ColCheckinDate";
            this.ColCheckinDate.ReadOnly = true;
            this.ColCheckinDate.Width = 150;
            // 
            // colAdult
            // 
            this.colAdult.HeaderText = "Adult";
            this.colAdult.Name = "colAdult";
            this.colAdult.ReadOnly = true;
            this.colAdult.Width = 80;
            // 
            // colChild
            // 
            this.colChild.HeaderText = "Child";
            this.colChild.Name = "colChild";
            this.colChild.ReadOnly = true;
            this.colChild.Width = 80;
            // 
            // ColAirportPickup
            // 
            this.ColAirportPickup.HeaderText = "Airpot Pickup";
            this.ColAirportPickup.Name = "ColAirportPickup";
            this.ColAirportPickup.ReadOnly = true;
            this.ColAirportPickup.Width = 230;
            // 
            // colFlightNumber
            // 
            this.colFlightNumber.HeaderText = "Flight No.";
            this.colFlightNumber.Name = "colFlightNumber";
            this.colFlightNumber.ReadOnly = true;
            this.colFlightNumber.Width = 80;
            // 
            // colTime
            // 
            this.colTime.HeaderText = "Time";
            this.colTime.Name = "colTime";
            this.colTime.ReadOnly = true;
            this.colTime.Width = 80;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "id";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // guestArrivalList_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1180, 600);
            this.Controls.Add(this.pnl_CheckInDate);
            this.Controls.Add(this.pnl_Data_Info);
            this.Controls.Add(this.pnl_GuestArrival_Title);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "guestArrivalList_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "guestArrivalList_form";
            this.Load += new System.EventHandler(this.guestArrivalList_form_Load_1);
            this.pnl_GuestArrival_Title.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcb_GuestArrivalList_Icon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Informations)).EndInit();
            this.pnl_Data_Info.ResumeLayout(false);
            this.pnl_CheckInDate.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_GuestArrival_Title;
        private System.Windows.Forms.Label lbl_GuestArrivalList;
        private System.Windows.Forms.PictureBox pcb_GuestArrivalList_Icon;
        private System.Windows.Forms.Button btn_GoBack;
        private System.Windows.Forms.DataGridView dgv_Informations;
        private System.Windows.Forms.Panel pnl_Data_Info;
        private System.Windows.Forms.Panel pnl_CheckInDate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtp_CheckInDate;
        private System.Windows.Forms.Label lbl_CheckInDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFolioNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGuestFullName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRooms;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCheckinDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAdult;
        private System.Windows.Forms.DataGridViewTextBoxColumn colChild;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAirportPickup;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFlightNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
    }
}