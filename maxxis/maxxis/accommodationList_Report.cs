﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace maxxis
{
    class accommodationList_Report
    {
        public string FolioNumber { get; set; }

        public string FullName { get; set; }

        public string ReservationDate { get; set; }

        public string CheckInDate { get; set; }

        public string DueOut { get; set; }

        public int NumberOfNights { get; set; }

        public float PreAuthorizedAmount { get; set; }
    }
}
